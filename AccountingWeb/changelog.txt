*************************************
****** AccountingWeb Changelog ******
*************************************


v0.1.1:
	- login.jsp:
		Se a�ade el n�mero de versi�n al texto "RICOH ACCOUNTING PACKAGE".
	- extract.jsp:
		Se mueve el bot�n "Generar ZIP" m�s abajo para que sea visible en navegadores modernos.
	- mfps.jsp, cecos.jsp, useractions.jsp, usermfps.jsp, users.jsp:
		Se mueve el bot�n "Ver informe" m�s abajo para que sea visible en navegadores modernos.