<%--
    Document   : menu
    Created on : 27-jul-2010, 12:31:09
    Author     : Alexis.Hidalgo
--%>
<%@page import="ricoh.accounting.objects.Resume"%>
<%@ page import="java.util.*" %>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Ricoh Accounting Package</title>
        <link rel="stylesheet" type="text/css" href="./css/style.css">
        <script type="text/javascript">
            if(typeof String.prototype.trim !== 'function') {
                String.prototype.trim = function() {
                  return this.replace(/^\s+|\s+$/g, ''); 
                }
              }
              
            function habilitarCampos()
            {
                var isValid = true;
                if(document.getElementById("serialnumber").value === "")
                {
                    isValid = false;
                    alert ("El campo número de serie es obligatorio");
                }
                else
                    document.getElementById("serialnumber").disabled=false;
                
                return isValid;
            }
            function showFormAdd()
            {
                  document.getElementById("loginForm").style.display='block';
                  document.getElementById("addDiv").style.display='none';
                  
                  var form = document.getElementById("loginForm");
                  form.action = "insertMfp.do?action=insert";
                  
                  document.getElementById("serialnumber").disabled=false;
            }
            
            function showFormEdit(posicion)
            {
                document.getElementById("loginForm").style.display='block';
                document.getElementById("addDiv").style.display='none';
                  
                var form = document.getElementById("loginForm");
                form.action = "insertMfp.do?action=edit";
               
                document.getElementById("serialnumber").disabled=true;
                document.getElementById("serialnumber").value = document.getElementById('tablaMFP').tBodies[0].rows[posicion].cells[0].innerText.trim();
                document.getElementById("hostname").value = document.getElementById('tablaMFP').tBodies[0].rows[posicion].cells[1].innerText.trim();
                document.getElementById("ipaddress").value = document.getElementById('tablaMFP').tBodies[0].rows[posicion].cells[2].innerText.trim();
                document.getElementById("modelname").value = document.getElementById('tablaMFP').tBodies[0].rows[posicion].cells[3].innerText.trim();
                document.getElementById("statusCombo").value = document.getElementById('tablaMFP').tBodies[0].rows[posicion].cells[4].children[0][0].value;
                document.getElementById("location").value = document.getElementById('tablaMFP').tBodies[0].rows[posicion].cells[5].innerText.trim();
                document.getElementById("zone").value = document.getElementById('tablaMFP').tBodies[0].rows[posicion].cells[6].innerText.trim();
                document.getElementById("CompanyCombo").value = document.getElementById('tablaMFP').tBodies[0].rows[posicion].cells[7].innerText.trim();

            }
        </script>
    </head>
    <body class="main">

    <%@ include file="cabecera.jsp" %>
    <%@ include file="principal.jsp" %>

        <div>
            <div style="margin-left: 225px;">
                <h3>Administración de MFPs</h3>
            </div>
                      
            <%
                String code = (String) session.getAttribute("errorCode");

                if (code != null)
                {
                    if (code.equals("empty_date"))
                        out.print("<script language=\"JavaScript\" type=\"text/javascript\">alert(\"Introduzca las fechas de inicio y final.\");</script>");
                    else if (code.equals("parse_fail"))
                        out.print("<script language=\"JavaScript\" type=\"text/javascript\">alert(\"Se ha producido un error al leer la fecha. Por favor, introdúzcala de nuevo.\");</script>");
                    else if (code.equals("start_after_end"))
                        out.print("<script language=\"JavaScript\" type=\"text/javascript\">alert(\"La fecha de inicio ha de ser anterior a la de fin.\");</script>");
                    else if (code.equals("db_error"))
                        out.print("<script language=\"JavaScript\" type=\"text/javascript\">alert(\"Se ha producido un error contactando la Base de Datos.\");</script>");
                    else if (code.equals("process_error"))
                        out.print("<script language=\"JavaScript\" type=\"text/javascript\">alert(\"Se ha producido un error generando los datos.\");</script>");
                    else if (code.equals("process_error_mfpsmenu")){
                        out.print("<script language=\"JavaScript\" type=\"text/javascript\">alert(\"Se ha producido un error generando los datos de equipos\");</script>");
                         //out.print("<script language=\"JavaScript\" type=\"text/javascript\">javascript:mpfs_Error();</script>");
                   }                       
                    else if (code.equals("end_after_today"))
                        out.print("<script language=\"JavaScript\" type=\"text/javascript\">alert(\"La fecha de fin no puede ser posterior a la fecha del día en curso.\");</script>");

                    session.removeAttribute("errorCode");
               }
               %>
            <div style="float:right; padding:15px; text-align:center; width:50px;">
                <img src="./images/csv256.png" height="30" width="30" style="margin-bottom:5px;"/>
                <br>
                <a href="./downloadFile.do?file=export_mfps">Exportar</a>
            </div>
            <!--<div style="float:right; padding:15px; text-align:center; width:50px;">
                <img src="./images/csv256.png" height="30" width="30" style="margin-bottom:5px;"/>
                <br>
                <a href="./downloadFile.do?file=import_mfps">Importar</a>
            </div>-->
        </div>
        <div class="settingFlatContsDivision">
            <img src="./images/settingFlatContsDivision.gif">
        </div>

        <div id="addDiv" style="margin-left:242px; padding-bottom:5px; padding-top:5px;">
            <img src="./images/cmdAddUser-r.gif">
            <a href="javascript:showFormAdd()">
                Añadir MFP
            </a>
        </div>

            <form id="loginForm" method="post" action="insertMfp.do?action=insert" style="display: none; padding-bottom:30px;" onsubmit="return habilitarCampos()">
           <div style="margin-left: 225px; clear: left;">
           <table>
                <tr>
                   <td>Número de serie: </td>
                   <td><input name="serialnumber" maxlength="32" size="35" type="text" id="serialnumber"></td>
                </tr>
                <tr>
                   <td>Nombre de máquina: </td>
                   <td><input name="hostname" maxlength="32" size="35" type="text" id="hostname"></td>
                </tr>
                <tr>
                   <td>Dirección IP: </td>
                   <td><input name="ipaddress" maxlength="32" size="35" type="text" id="ipaddress"></td>
                </tr>
                <tr>
                <td>Modelo: </td>
                   <td><input name="modelname" maxlength="32" size="35" type="text" id="modelname"></td>
                </tr>
                <tr>
                   <td>Estado: </td>
                   <td><SELECT NAME="statusCombo" SIZE=1 id="statusCombo"> 
                            <OPTION VALUE="online">online</OPTION>
                            <OPTION VALUE="offline">offline</OPTION>
                        </SELECT>  
                    </td>
                </tr>
                <tr>
                   <td>Región: </td>
                   <td><input name="location" maxlength="32" size="35" type="text" id="location"></td>
                </tr>
                <tr>
                   <td>Oficina: </td>
                   <td><input name="zone" maxlength="32" size="35" type="text" id="zone"></td>
                </tr>
                <tr>
                   <td>Empresa: </td>
                   <td><SELECT NAME="CompanyCombo" SIZE=1 id="CompanyCombo"> 
            <%
             if (session.getAttribute("companiesList") != null){
                List companyList = (List)session.getAttribute("companiesList");

                ListIterator companiesIterator = companyList.listIterator();
                while (companiesIterator.hasNext())
                {
                    String company = (String)companiesIterator.next();
            %>

                    <OPTION VALUE="<%=company%>"><%=company%></OPTION>
              
            <%
                }
            %>
                    </SELECT>  
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td><input name="insert" value="Aceptar" type="submit" id="btnInsert"></td>
                </tr>
            </table>
           </div>
        </form>

        <%
        Resume resume;
        int numRows, startIndex = 1, numRecordsPerPage = 20, remain, increment, numPages;

        if(request.getParameter("startIndex") != null)
            startIndex = Integer.parseInt(request.getParameter("startIndex"));

        if(request.getParameter("numRecords") != null)
            numRecordsPerPage = Integer.parseInt(request.getParameter("numRecords"));

        List mfps= (List)session.getAttribute("mfpsList");
            numRows = mfps.size();
            numRows = numRows / 8;
            
            numPages = numRows /numRecordsPerPage;

            if((remain = numRows % numRecordsPerPage) != 0)
                numPages++;

            if(((startIndex + numRecordsPerPage) <= numRows) || remain == 0)
                increment = startIndex + numRecordsPerPage - 1;

            else
                increment = startIndex + remain - 1;
 %>       
            <div style="display: block; clear: left; margin-left:250px; margin-top:10px; width: 100%">
                <span style="padding-right:20px;">Núm. total de Registros: <%=numRows%></span>
                <span style="padding-right:20px;">Núm. de Registros por página:
                    <select name="numRecords" onchange="location = 'mfpsmenu.jsp?numRecords=' + this.options[this.selectedIndex].value;">
                        <%
                        if (numRecordsPerPage == 20)
                            out.print("<option selected value=\"20\">20</option>");
                        else
                            out.print("<option value=\"20\">20</option>");

                        if (numRecordsPerPage == 50)
                            out.print("<option selected value=\"50\">50</option>");
                        else
                            out.print("<option value=\"50\">50</option>");

                        if (numRecordsPerPage == 100)
                            out.print("<option selected value=\"100\">100</option>");
                        else
                            out.print("<option value=\"100\">100</option>");

                        if (numRecordsPerPage == 200)
                            out.print("<option selected value=\"200\">200</option>");
                        else
                            out.print("<option value=\"200\">200</option>");

                        if (numRecordsPerPage == 500)
                            out.print("<option selected value=\"500\">500</option>");
                        else
                            out.print("<option value=\"500\">500</option>");

                        if (numRecordsPerPage == numRows)
                            out.print("<option selected value=" + numRows + ">TODOS</option>");
                        else
                            out.print("<option value=" + numRows + ">TODOS</option>");
%>
                    </select>
                </span>
                <span style="padding-right:20px;">Núm. de páginas: <%=numPages%></span>
            <div style="display:inline;">
<%
            if(increment < numRows)
            {
%>
                <span style="padding-right:10px;">Registros <%=startIndex%> a <%=increment%></span>
<%
            }
            else if (numRows == 0)
            {
%>
                <span style="padding-right:10px;">Registros <%=numRows%> a <%=numRows%></span>
<%
            }

            else
            {
%>
                <span style="padding-right:10px;">Registros <%=startIndex%> a <%=numRows%></span>
<%
            }
    //out.println("</table>");
    //out.println("<table><tr>Resultados");

        if(startIndex == 1)
        {
%>
           <img src="./images/btnArrowLeft2-d.gif"/>
           <img src="./images/btnArrowLeft1-d.gif"/>
<%
        }
        else
        {
%>
            <a href="mfpsmenu.jsp?startIndex=1&numRecords=<%=numRecordsPerPage%>">
                <img src="./images/btnArrowLeft2-r.gif"/>
            </a>
            <a href="mfpsmenu.jsp?startIndex=<%=startIndex - numRecordsPerPage%>&numRecords=<%=numRecordsPerPage%>">
                <img src="./images/btnArrowLeft1-r.gif"/>
            </a>
<%
        }

        if((startIndex > numRows -numRecordsPerPage))
        {
%>
            <img src="./images/btnArrowRight1-d.gif"/>
            <img src="./images/btnArrowRight2-d.gif"/>
<%
        }
        else
        {
%>
            <a href="mfpsmenu.jsp?startIndex=<%=startIndex + numRecordsPerPage%>&numRecords=<%=numRecordsPerPage%>">
                <img src="./images/btnArrowRight1-r.gif"/>
            </a>
            <a href="mfpsmenu.jsp?startIndex=<%=numRecordsPerPage * (numPages - 1) + 1%>&numRecords=<%=numRecordsPerPage%>">
                <img src="./images/btnArrowRight2-r.gif"/>
            </a>
<%
        }
%>                
            </div>
        </div>
                    
        <div style="margin-left: 200px; clear: left;">
        <table class="list" id="tablaMFP">
            <tr class="listTitle">
                <td>Número de serie</td>
                <td>Nombre de máquina</td>
                <td>Dirección IP</td>
                <td>Modelo</td>
                <td>Estado</td>
                <td>Región</td>
                <td>Oficina</td>
                <td>Empresa</td>
                <td>Eliminar</td>
                <td>Editar</td>
            </tr>

<%
    int offset = 0;
    offset = (startIndex-1) * 8;
    if (numRows > 0)
    {
        for(int i = startIndex; i <= increment; i++)
        {
            
            String ipaddress = (String)mfps.get(offset);
            String modeltype = (String)mfps.get(offset + 1);
            String status = (String)mfps.get(offset + 2);
            String hostname = (String)mfps.get(offset + 3);
            String location = (String)mfps.get(offset + 4);
            String zone = (String)mfps.get(offset + 5);
            String mfpserial = (String)mfps.get(offset + 6);
            String company = (String)mfps.get(offset + 7);
            
            offset = offset + 8;
%>            

              <tr class="list">
                  <td>
                      <%=mfpserial%>
                  </td>
                  <td>
                      <%=hostname%>
                  </td>
                  <td>
                      <%=ipaddress%>
                  </td>
                  <td>
                      <%=modeltype%>
                  </td>
                  <td>
                    <%
                        if (status.equalsIgnoreCase("online")){
                    %>
                    <FORM action="insertMfp.do?action=update&mfpserial=<%=mfpserial%>" method="post">
                        <SELECT NAME="statusComboList" SIZE=1 onchange="this.form.submit()"> 
                            <OPTION VALUE="online" selected="selected">online</OPTION>
                            <OPTION VALUE="offline">offline</OPTION>
                        </SELECT>
                    </FORM>
                    <%
                        }else{
                    %>
                    <FORM action="insertMfp.do?action=update&mfpserial=<%=mfpserial%>" method="post">
                        <SELECT NAME="statusComboList" SIZE=1 onchange="this.form.submit()"> 
                            <OPTION VALUE="online" >online</OPTION>
                            <OPTION VALUE="offline" selected="selected">offline</OPTION>
                        </SELECT> 
                    </FORM>
                    <%
                        }
                    %>
                  </td>
                  <td>
                      <%=location%>
                  </td>
                  <td>
                      <%=zone%>
                  </td>
                  <td>
                      <%=company%>
                  </td>
                  <td>
                      <a href="deleteMfp.do?action=delete&mfpserial=<%=mfpserial%>">
                          <img src="./images/cmdDelete-r.gif" style="padding-bottom:1px; padding-top:5px;">
                      </a>
                  </td>
                  <td>
                      <a href="javascript:showFormEdit(<%=i%>)">
                          <img src="./images/file_edit_.png" style="padding-bottom:1px; padding-top:5px;">
                      </a>
                  </td>
              </tr>
<%
            }
       }
   }
%>
        </table>
        </div>
    </body>
</html>
