<%-- 
    Document   : usersaggregated
    Created on : 25-ene-2016, 12:06:08
    Author     : Francisco.Madera
--%>

<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@ page import="java.util.Date" %>
<%@ page import="ricoh.accounting.objects.Accumulated" %>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Ricoh Accounting Package</title>
        <link rel="stylesheet" type="text/css" href="./css/style.css">
    </head>

    <%@ include file="cabecera.jsp" %>
    <%@ include file="principal.jsp" %>

    <body class="main">
        <div>
            <div style="margin-left: 225px;">
                <h3>Informe de contadores de <%=(String)session.getAttribute("tipo")%> por <%=(String)session.getAttribute("intervaloTiempo")%> entre las fechas <%=(String)session.getAttribute("iniDate")%> y <%=(String)session.getAttribute("endDate")%></h3>
            </div>
            <div style="float:right; padding:15px; text-align:center; width:50px;">
                <img src="./images/csv256.png" height="30" width="30" style="margin-bottom:5px;"/>
                <br>
                <a href="./downloadFile.do?file=users">Descargar Fichero</a>
            </div>
        </div>
        <div class="settingFlatContsDivision">
            <img src="./images/settingFlatContsDivision.gif">
        </div>
<%
        Accumulated accumulated;
        int numRows, startIndex = 1, numRecordsPerPage = 20, remain, increment, numPages;

        if(request.getParameter("startIndex") != null)
            startIndex = Integer.parseInt(request.getParameter("startIndex"));

        if(request.getParameter("numRecords") != null)
            numRecordsPerPage = Integer.parseInt(request.getParameter("numRecords"));

        List accumulatedList = (List)session.getAttribute("accumulatedList");
            numRows = accumulatedList.size();

            numPages = numRows /numRecordsPerPage;

            if((remain = numRows % numRecordsPerPage) != 0)
                numPages++;

            if(((startIndex + numRecordsPerPage) <= numRows) || remain == 0)
                increment = startIndex + numRecordsPerPage - 1;

            else
                increment = startIndex + remain - 1;
 %>
            <div style="display: block; clear: left; margin-left:50px; margin-top:10px; width: 100%">
                <span style="padding-right:20px;">Núm. total de Registros: <%=numRows%></span>
                <span style="padding-right:20px;">Núm. de Registros por página:
                    <select name="numRecords" onchange="location = 'usersaccumulated.jsp?numRecords=' + this.options[this.selectedIndex].value;">
                        <%
                        if (numRecordsPerPage == 20)
                            out.print("<option selected value=\"20\">20</option>");
                        else
                            out.print("<option value=\"20\">20</option>");

                        if (numRecordsPerPage == 50)
                            out.print("<option selected value=\"50\">50</option>");
                        else
                            out.print("<option value=\"50\">50</option>");

                        if (numRecordsPerPage == 100)
                            out.print("<option selected value=\"100\">100</option>");
                        else
                            out.print("<option value=\"100\">100</option>");

                        if (numRecordsPerPage == 200)
                            out.print("<option selected value=\"200\">200</option>");
                        else
                            out.print("<option value=\"200\">200</option>");

                        if (numRecordsPerPage == 500)
                            out.print("<option selected value=\"500\">500</option>");
                        else
                            out.print("<option value=\"500\">500</option>");

                        if (numRecordsPerPage == 1000)
                            out.print("<option selected value=\"1000\">1000</option>");
                        else
                            out.print("<option value=\"1000\">1000</option>");

                        if (numRecordsPerPage == 2000)
                            out.print("<option selected value=\"2000\">2000</option>");
                        else
                            out.print("<option value=\"2000\">2000</option>");

                        if (numRecordsPerPage == 5000)
                            out.print("<option selected value=\"5000\">5000</option>");
                        else
                            out.print("<option value=\"5000\">5000</option>");

                        if (numRecordsPerPage == numRows)
                            out.print("<option selected value=" + numRows + ">TODOS</option>");
                        else
                            out.print("<option value=" + numRows + ">TODOS</option>");
%>
                    </select>
                </span>
                <span style="padding-right:20px;">Núm. de páginas: <%=numPages%></span>
                <div style="display:inline;">
<%
            if(increment < numRows)
            {
%>
                <span style="padding-right:10px;">Registros <%=startIndex%> a <%=increment%></span>
<%
            }
            else if (numRows == 0)
            {
%>
                <span style="padding-right:10px;">Registros <%=numRows%> a <%=numRows%></span>
<%
            }

            else
            {
%>
                <span style="padding-right:10px;">Registros <%=startIndex%> a <%=numRows%></span>
<%
            }
    //out.println("</table>");
    //out.println("<table><tr>Resultados");

        if(startIndex == 1)
        {
%>
           <img src="./images/btnArrowLeft2-d.gif"/>
           <img src="./images/btnArrowLeft1-d.gif"/>
<%
        }
        else
        {
%>
            <a href="usersaccumulated.jsp?startIndex=1&numRecords=<%=numRecordsPerPage%>">
                <img src="./images/btnArrowLeft2-r.gif"/>
            </a>
            <a href="usersaccumulated.jsp?startIndex=<%=startIndex - numRecordsPerPage%>&numRecords=<%=numRecordsPerPage%>">
                <img src="./images/btnArrowLeft1-r.gif"/>
            </a>
<%
        }

        if((startIndex > numRows -numRecordsPerPage))
        {
%>
            <img src="./images/btnArrowRight1-d.gif"/>
            <img src="./images/btnArrowRight2-d.gif"/>
<%
        }
        else
        {
%>
            <a href="usersaccumulated.jsp?startIndex=<%=startIndex + numRecordsPerPage%>&numRecords=<%=numRecordsPerPage%>">
                <img src="./images/btnArrowRight1-r.gif"/>
            </a>
            <a href="usersaccumulated.jsp?startIndex=<%=numRecordsPerPage * (numPages - 1) + 1%>&numRecords=<%=numRecordsPerPage%>">
                <img src="./images/btnArrowRight2-r.gif"/>
            </a>
<%
        }
%>
            </div>
        </div>
        <div style="margin-left:30px; margin-right: 10px; margin-bottom: 20px; clear: left;">
        <table class="list">
            <tr class="listTitle">
                <%
                    String filtrado = (String)session.getAttribute("filtrado");
                     if (filtrado.equalsIgnoreCase("Usuario"))
                            out.print("<td>Nombre de Usuario</td>");
                     else if (filtrado.equalsIgnoreCase("Oficina"))
                            out.print("<td>Nombre Oficina</td>");
                     else if (filtrado.equalsIgnoreCase("Region"))
                            out.print("<td>Nombre Region</td>");
                     else
                         out.print("<td>Nombre</td>");
                %>
                
                <td>Fecha</td>
                <td>Documentos Impresos</td>
                <td>Total de páginas</td>
            </tr>
<%
    if (numRows > 0)
    {
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        for(int i = startIndex; i <= increment; i++)
        {
            accumulated = (Accumulated) accumulatedList.get(i-1);
            
%>
            <tr class="list">
                <td class="listTitle">
                    <%=accumulated.getUser() %>
                </td>
                <td>
                    <%=dateFormat.format(accumulated.getDate())%>
                </td>
                <td>
                    <%=accumulated.getDocuments()%>
                </td>
                <td>
                    <%=accumulated.getPages()%>
                </td>
            </tr>
<%
        }
    }
%>
        </table>
        </div>
    </body>
</html>