<%--
    Document   : menu
    Created on : 15-feb-2016, 12:31:09
    Author     : David.Yunta
--%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="ricoh.accounting.objects.Accumulated" %>
<%@page import="ricoh.accounting.objects.Resume"%>
<%@ page import="java.util.*" %>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Ricoh Accounting Package</title>
        <link rel="stylesheet" type="text/css" href="./css/style.css">
        <style type="text/css">@import url(./css/calendar-blue2.css);</style>
        <script type="text/javascript" src="calendar.js"></script>
        <script type="text/javascript" src="lang/calendar-es.js"></script>
        <script type="text/javascript" src="calendar-setup.js"></script>
        
        <script type="text/javascript">
            function showForm()
            {
                  document.getElementById("loginForm").style.display='block';
                  document.getElementById("addDiv").style.display='none';
            }
        </script>
        
        <script type="text/javascript">
            // This function gets called when the end-user clicks on some date.
            function selected(cal, date)
            {
              cal.sel.value = date; // just update the date in the input field.
            }

            function calIniSelected(cal, date)
            {
                  var el = document.getElementById("iniDate");
                  el.value = date;
            }

            function calEndSelected(cal, date)
            {
                  var el = document.getElementById("endDate");
                  el.value = date;
            }


            function showCalendars()
            {
              var parentIniCal = document.getElementById("ini_calendar-container");
              var parentEndCal = document.getElementById("end_calendar-container");

              // construct a calendar giving only the "selected" handler.
              var IniCal = new Calendar(1, null, calIniSelected);
              var EndCal = new Calendar(1, null, calEndSelected);

              IniCal.setDateFormat("%d/%m/%Y");
              EndCal.setDateFormat("%d/%m/%Y");

              // this call must be the last as it might use data initialized above; if
              // we specify a parent, as opposite to the "showCalendar" function above,
              // then we create a flat calendar -- not popup.  Hidden, though, but...
              IniCal.create(parentIniCal);
              EndCal.create(parentEndCal);

              // ... we can show it here.
              IniCal.show();
              EndCal.show();
            }
        </script>
    </head>
    <body class="main" onload="showCalendars()">

    <%@ include file="cabecera.jsp" %>
    <%@ include file="principal.jsp" %>

        <div>
            <div style="margin-left: 225px;">
                <h3>Informes de Control</h3>
            </div>
                      
            <%
                String code = (String) session.getAttribute("errorCode");

                if (code != null)
                {
                    if (code.equals("empty_date"))
                        out.print("<script language=\"JavaScript\" type=\"text/javascript\">alert(\"Introduzca las fechas de inicio y final.\");</script>");
                    else if (code.equals("parse_fail"))
                        out.print("<script language=\"JavaScript\" type=\"text/javascript\">alert(\"Se ha producido un error al leer la fecha. Por favor, introdúzcala de nuevo.\");</script>");
                    else if (code.equals("start_after_end"))
                        out.print("<script language=\"JavaScript\" type=\"text/javascript\">alert(\"La fecha de inicio ha de ser anterior a la de fin.\");</script>");
                    else if (code.equals("db_error"))
                        out.print("<script language=\"JavaScript\" type=\"text/javascript\">alert(\"Se ha producido un error contactando la Base de Datos.\");</script>");
                    else if (code.equals("process_error"))
                        out.print("<script language=\"JavaScript\" type=\"text/javascript\">alert(\"Se ha producido un error generando los datos.\");</script>");
                    else if (code.equals("end_after_today"))
                        out.print("<script language=\"JavaScript\" type=\"text/javascript\">alert(\"La fecha de fin no puede ser posterior a la fecha del día en curso.\");</script>");
                    else if (code.equals("empty_tipoVista"))
                        out.print("<script language=\"JavaScript\" type=\"text/javascript\">alert(\"Se ha producido un error al leer el tipo vista. Por favor, selecciona un valor.\");</script>");
                    else if (code.equals("empty_tipoInforme"))
                        out.print("<script language=\"JavaScript\" type=\"text/javascript\">alert(\"Se ha producido un error al leer el tipo informe. Por favor, selecciona un valor.\");</script>");
                    
                    session.removeAttribute("errorCode");
               }
               %>
        </div>
        <div class="settingFlatContsDivision">
            <img src="./images/settingFlatContsDivision.gif">
        </div>

        <form action="./listUsersAccumulated.do" method="post">
            <div style="margin-left: 225px;">
                <h4>Seleccione el intervalo de fechas:</h4>
            </div>
                
            <div style="margin-left: 225px;">
                <table style="border-collapse: separate;">
                    <tr>
                        <td>
                            <div>
                                Fecha de Inicio:
                                <input type="text" id="iniDate" name="iniDate" readonly="readonly"/>
                            </div>
                        </td>

                        <td>
                            <div style="margin-left: 200px;">
                                Fecha de Fin:
                                <input type="text" id="endDate" name="endDate" readonly="readonly"/>
                            </div>
                        </td>
                        <td>
                            <div style="margin-left: 50px;">
                                Tipo Informe:
                                <select id="tipoInforme" name="tipoInforme" style="width: max-content">
                                    <option value="1">Número de Documentos impresos por hora</option>
                                    <option value="2">Número de Documentos impresos por día</option>
                                    <option value="3">Número de Documentos impresos por semana</option>
                                    <option value="4">Número de Documentos impresos por mes</option>
                                    <option value="5">Número de Copias por hora</option>
                                    <option value="6">Número de Copias por día</option>
                                    <option value="7">Número de Copias por Semana</option>
                                    <option value="8">Número de Copias por mes</option>
                                    <option value="9">Número de Hojas utilizadas por hora</option>
                                    <option value="10">Número de Hojas utilizadas por día</option>
                                    <option value="11">Número de Hojas utilizadas por Semana</option>
                                    <option value="12">Número de Hojas utilizadas por mes</option>
                                    <option value="13">Número de hojas impresas por tóner</option>
                                    <option value="14">Tiempo total de disponibilidad del servicio</option>
                                </select>
                            </div>
                            <br>
                            <div style="margin-left: 50px;">
                                Tipo Vista Información:
                                <select id="tipoVista" name="tipoVista" style="width: 150px">
                                    <option value="Usuario">Por usuario</option>
                                    <option value="Region">Por región</option>
                                    <option value="Oficina">Por oficina</option>
                                    <option value="Horario">Por horario</option>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr></tr>
                    <tr>
                        <td>
                            <div style="position: fixed; margin-top: 15px;" id="ini_calendar-container"></div>
                        </td>

                        <td>
                            <div style="margin-top: 15px; margin-left: 200px; position: fixed;" id="end_calendar-container"></div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>

                        <td>
                        </td>
                    </tr>

               </table>
            </div>

            <div>
                <input type="submit" name="displayCounters" value="Ver Informe" style="margin-top: 200px; margin-left: 525px;">
            </div>
        </form>
        

        <%
        Resume resume;
        int numRows, startIndex = 1, numRecordsPerPage = 20, remain, increment, numPages;

        if(request.getParameter("startIndex") != null)
            startIndex = Integer.parseInt(request.getParameter("startIndex"));

        if(request.getParameter("numRecords") != null)
            numRecordsPerPage = Integer.parseInt(request.getParameter("numRecords"));

        List companiesList= (List)session.getAttribute("companiesList");
            numRows = companiesList.size();
            
            numPages = numRows /numRecordsPerPage;

            if((remain = numRows % numRecordsPerPage) != 0)
                numPages++;

            if(((startIndex + numRecordsPerPage) <= numRows) || remain == 0)
                increment = startIndex + numRecordsPerPage - 1;

            else
                increment = startIndex + remain - 1;
 %>       
            <div style="display: block; clear: left; margin-left:250px; margin-top:10px; width: 100%">
                <span style="padding-right:20px;">Núm. total de Registros: <%=numRows%></span>
                <span style="padding-right:20px;">Núm. de Registros por página:
                    <select name="numRecords" onchange="location = 'companiesmenu.jsp?numRecords=' + this.options[this.selectedIndex].value;">
                        <%
                        if (numRecordsPerPage == 20)
                            out.print("<option selected value=\"20\">20</option>");
                        else
                            out.print("<option value=\"20\">20</option>");

                        if (numRecordsPerPage == 50)
                            out.print("<option selected value=\"50\">50</option>");
                        else
                            out.print("<option value=\"50\">50</option>");

                        if (numRecordsPerPage == 100)
                            out.print("<option selected value=\"100\">100</option>");
                        else
                            out.print("<option value=\"100\">100</option>");

                        if (numRecordsPerPage == 200)
                            out.print("<option selected value=\"200\">200</option>");
                        else
                            out.print("<option value=\"200\">200</option>");

                        if (numRecordsPerPage == 500)
                            out.print("<option selected value=\"500\">500</option>");
                        else
                            out.print("<option value=\"500\">500</option>");

                        if (numRecordsPerPage == numRows)
                            out.print("<option selected value=" + numRows + ">TODOS</option>");
                        else
                            out.print("<option value=" + numRows + ">TODOS</option>");
%>
                    </select>
                </span>
                <span style="padding-right:20px;">Núm. de páginas: <%=numPages%></span>
            <div style="display:inline;">
<%
            if(increment < numRows)
            {
%>
                <span style="padding-right:10px;">Registros <%=startIndex%> a <%=increment%></span>
<%
            }
            else if (numRows == 0)
            {
%>
                <span style="padding-right:10px;">Registros <%=numRows%> a <%=numRows%></span>
<%
            }

            else
            {
%>
                <span style="padding-right:10px;">Registros <%=startIndex%> a <%=numRows%></span>
<%
            }
    //out.println("</table>");
    //out.println("<table><tr>Resultados");

        if(startIndex == 1)
        {
%>
           <img src="./images/btnArrowLeft2-d.gif"/>
           <img src="./images/btnArrowLeft1-d.gif"/>
<%
        }
        else
        {
%>
            <a href="companiesmenu.jsp?startIndex=1&numRecords=<%=numRecordsPerPage%>">
                <img src="./images/btnArrowLeft2-r.gif"/>
            </a>
            <a href="companiesmenu.jsp?startIndex=<%=startIndex - numRecordsPerPage%>&numRecords=<%=numRecordsPerPage%>">
                <img src="./images/btnArrowLeft1-r.gif"/>
            </a>
<%
        }

        if((startIndex > numRows -numRecordsPerPage))
        {
%>
            <img src="./images/btnArrowRight1-d.gif"/>
            <img src="./images/btnArrowRight2-d.gif"/>
<%
        }
        else
        {
%>
            <a href="companiesmenu.jsp?startIndex=<%=startIndex + numRecordsPerPage%>&numRecords=<%=numRecordsPerPage%>">
                <img src="./images/btnArrowRight1-r.gif"/>
            </a>
            <a href="companiesmenu.jsp?startIndex=<%=numRecordsPerPage * (numPages - 1) + 1%>&numRecords=<%=numRecordsPerPage%>">
                <img src="./images/btnArrowRight2-r.gif"/>
            </a>
<%
        }
%>                
            </div>
        </div>
                    
        <div style="margin-left: 250px; clear: left;">
        <table class="list">
            <tr class="listTitle">
                <td width="400">Nombre de empresa</td>
                <td>Eliminar</td>
            </tr>

<%
    if (numRows > 0)
    {
        for(int i = startIndex; i <= increment; i++)
        {
            
            String companyname = (String)companiesList.get(i-1);

%>            

              <tr class="list">
                  <td>
                      <%=companyname%>
                  </td>
                  <td>
                      <a href="deleteCompany.do?action=delete&companyname=<%=companyname%>">
                          <img src="./images/cmdDelete-r.gif" style="padding-bottom:1px; padding-top:5px;">
                      </a>
                  </td>
              </tr>
<%
            }
       }
  
%>
        </table>
        </div>
    </body>
</html>
