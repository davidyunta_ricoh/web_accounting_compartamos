<%--
    Document   : login
    Created on : 15-jul-2010, 12:31:09
    Author     : Alexis.Hidalgo
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Ricoh Accounting Package</title>
    <link rel="stylesheet" type="text/css" href="./css/style.css">
</head>
<body class="login">    

    <%
    String code = (String) session.getAttribute("errorCode");

    if (code != null)
    {
        if (code.equals("empty_date"))
            out.print("<script language=\"JavaScript\" type=\"text/javascript\">alert(\"Introduzca las fechas de inicio y final.\");</script>");
        else if (code.equals("parse_fail"))
            out.print("<script language=\"JavaScript\" type=\"text/javascript\">alert(\"Se ha producido un error al leer la fecha. Por favor, introdúzcala de nuevo.\");</script>");
        else if (code.equals("start_after_end"))
            out.print("<script language=\"JavaScript\" type=\"text/javascript\">alert(\"La fecha de inicio ha de ser anterior a la de fin.\");</script>");
        else if (code.equals("db_error"))
            out.print("<script language=\"JavaScript\" type=\"text/javascript\">alert(\"Se ha producido un error contactando la Base de Datos.\");</script>");
        else if (code.equals("process_error"))
            out.print("<script language=\"JavaScript\" type=\"text/javascript\">alert(\"Se ha producido un error en el proceso de autenticación\");</script>");
        else if (code.equals("end_after_today"))
            out.print("<script language=\"JavaScript\" type=\"text/javascript\">alert(\"La fecha de fin no puede ser posterior a la fecha del día en curso.\");</script>");

        session.removeAttribute("errorCode");
    }
    %>
    <form id="loginForm" method="post" action="login.do">
        <table cellpadding="0" width="100%"><tbody>
            <tr height="60">
                <td align="right" width="97%"></td>
                <td><img src="./images/spacer.gif" width="0"></td>
            </tr>
            <tr height="5">
                <td colspan="3" background="./images/lSdwTop.gif"></td>
            </tr>
            <tr height="204">
                <td colspan="3" align="center" bgcolor="#000000"><img src="./images/loginWebNavi.jpg"></td>
            </tr>
            <tr height="15">
                <td colspan="3" background="./images/lSdwBtmDG.gif"></td>
            </tr>
            <tr height="40" bgcolor="#787878">
                <td colspan="3" align="center">RICOH ACCOUNTING PACKAGE v0.1.4</td>
            </tr>
            <tr height="8" bgcolor="#787878">
                <td colspan="3"></td>
            </tr>
            <tr height="8">
                <td colspan="3" background="./images/lSdwBtmLG.gif"></td>
            </tr>
            <tr height="36">
                <td colspan="3" bgcolor="#c7c7c7"></td>
            </tr>
            <tr height="10">
                <td colspan="3" background="./images/lSdwBtmWh.gif"></td>
            </tr>
            <tr>
                <td colspan="2" valign="top">
                    <table cellpadding="0"><tbody>
                        <tr>
                            <td colspan="3" height="8"></td>
                        </tr>
                        <tr>
                            <td width="20"></td>
                            <td><b>User name: </b></td>
                            <td><input name="user" maxlength="32" size="50" type="text"></td>
                        </tr>
                        <tr height="3">
                            <td colspan="3"></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><b>Password: </b></td>
                            <td><input name="pass" maxlength="128" size="50" type="password"></td>
                        </tr>
                        <tr height="3">
                            <td colspan="3"></td>
                        </tr>
                        <tr>
                            <td colspan="2"></td>
                            <td><input name="login" value="Login" type="submit"></td>
                        </tr>
                    </tbody></table>
                </td>
            </tr>
        </tbody></table>
    </form>
</body>
</html>
