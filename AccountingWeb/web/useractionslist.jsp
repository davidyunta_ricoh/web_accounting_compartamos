<%-- 
    Document   : useractionslist
    Created on : 22-mar-2011, 0:26:11
    Author     : Alexis.Hidalgo
--%>

<%@ page import="java.util.*" %>
<%@ page import="ricoh.accounting.objects.Action"%>
<%@ page import="java.text.SimpleDateFormat" %>


<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Ricoh Accounting Package</title>
        <link rel="stylesheet" type="text/css" href="./css/style.css">
    </head>

    <%@ include file="cabecera.jsp" %>
    <%@ include file="principal.jsp" %>

    <body class="main">
        <div>
            <div style="margin-left: 225px;">
                <%
                  if (session.getAttribute("user") == null){
                %>
                    <h3>Informe de actividad de Usuarios entre las fechas <%=(String)session.getAttribute("iniDate")%> y <%=(String)session.getAttribute("endDate")%></h3>
                <%                
                  }else{
                %>                               
                    <h3>Informe de actividad de Usuario <%=(String)session.getAttribute("user")%> entre las fechas <%=(String)session.getAttribute("iniDate")%> y <%=(String)session.getAttribute("endDate")%></h3>
                <%
                  }
                %>
            </div>
            <div style="float:right; padding:15px; text-align:center; width:50px;">
                <img src="./images/csv256.png" height="30" width="30" style="margin-bottom:5px;"/>
                <br>
                <a href="./downloadFile.do?file=useractions">Descargar Fichero</a>
            </div>


        </div>
        <div class="settingFlatContsDivision">
            <img src="./images/settingFlatContsDivision.gif">
        </div>
<%
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");

        boolean bCC_Activation = ((String)session.getAttribute("CC_ACTIVATION")).equalsIgnoreCase("true");
        
        Action action;
        int numRows, startIndex = 1, numRecordsPerPage = 20, remain, increment, numPages;
        boolean showDocName = false;

        if(request.getParameter("startIndex") != null)
            startIndex = Integer.parseInt(request.getParameter("startIndex"));

        if(request.getParameter("numRecords") != null)
            numRecordsPerPage = Integer.parseInt(request.getParameter("numRecords"));

        if(request.getParameter("showDoc") != null)
            showDocName = Boolean.parseBoolean(request.getParameter("showDoc"));


        List actions = (List)session.getAttribute("actionList");
            numRows = actions.size();

            numPages = numRows /numRecordsPerPage;

            if((remain = numRows % numRecordsPerPage) != 0)
                numPages++;

            if(((startIndex + numRecordsPerPage) <= numRows) || remain == 0)
                increment = startIndex + numRecordsPerPage - 1;

            else
                increment = startIndex + remain - 1;
 %>
            <div style="display: block; clear: left; margin-left:50px; margin-top:10px; width: 100%">
                <span style="padding-right:20px;">Núm. total de Registros: <%=numRows%></span>
                <span style="padding-right:20px;">Núm. de Registros por página:
                    <select name="numRecords" onchange="location = 'useractionslist.jsp?showDoc=<%=showDocName%>&numRecords=' + this.options[this.selectedIndex].value;">
                        <%
                        if (numRecordsPerPage == 20)
                            out.print("<option selected value=\"20\">20</option>");
                        else
                            out.print("<option value=\"20\">20</option>");

                        if (numRecordsPerPage == 50)
                            out.print("<option selected value=\"50\">50</option>");
                        else
                            out.print("<option value=\"50\">50</option>");

                        if (numRecordsPerPage == 100)
                            out.print("<option selected value=\"100\">100</option>");
                        else
                            out.print("<option value=\"100\">100</option>");

                        if (numRecordsPerPage == 200)
                            out.print("<option selected value=\"200\">200</option>");
                        else
                            out.print("<option value=\"200\">200</option>");

                        if (numRecordsPerPage == 500)
                            out.print("<option selected value=\"500\">500</option>");
                        else
                            out.print("<option value=\"500\">500</option>");

                        if (numRecordsPerPage == 1000)
                            out.print("<option selected value=\"1000\">1000</option>");
                        else
                            out.print("<option value=\"1000\">1000</option>");

                        if (numRecordsPerPage == 2000)
                            out.print("<option selected value=\"2000\">2000</option>");
                        else
                            out.print("<option value=\"2000\">2000</option>");

                        if (numRecordsPerPage == 5000)
                            out.print("<option selected value=\"5000\">5000</option>");
                        else
                            out.print("<option value=\"5000\">5000</option>");

                        if (numRecordsPerPage == numRows)
                            out.print("<option selected value=" + numRows + ">TODOS</option>");
                        else
                            out.print("<option value=" + numRows + ">TODOS</option>");
%>
                    </select>
                </span>
                <span style="padding-right:20px;">Núm. de páginas: <%=numPages%></span>
                <div style="display:inline;">
<%
            if(increment < numRows)
            {
%>
                <span style="padding-right:10px;">Registros <%=startIndex%> a <%=increment%></span>
<%
            }
            else if (numRows == 0)
            {
%>
                <span style="padding-right:10px;">Registros <%=numRows%> a <%=numRows%></span>
<%
            }
            else
            {
%>
                <span style="padding-right:10px;">Registros <%=startIndex%> a <%=numRows%></span>
<%
            }
    //out.println("</table>");
    //out.println("<table><tr>Resultados");

        if(startIndex == 1)
        {
%>
           <img src="./images/btnArrowLeft2-d.gif"/>
           <img src="./images/btnArrowLeft1-d.gif"/>
<%
        }
        else
        {
%>
            <a href="useractionslist.jsp?startIndex=1&numRecords=<%=numRecordsPerPage%>&showDoc=<%=showDocName%>">
                <img src="./images/btnArrowLeft2-r.gif"/>
            </a>
            <a href="useractionslist.jsp?startIndex=<%=startIndex - numRecordsPerPage%>&numRecords=<%=numRecordsPerPage%>&showDoc=<%=showDocName%>">
                <img src="./images/btnArrowLeft1-r.gif"/>
            </a>
<%
        }

        if((startIndex > numRows -numRecordsPerPage))
        {
%>
            <img src="./images/btnArrowRight1-d.gif"/>
            <img src="./images/btnArrowRight2-d.gif"/>
<%
        }
        else
        {
%>
            <a href="useractionslist.jsp?startIndex=<%=startIndex + numRecordsPerPage%>&numRecords=<%=numRecordsPerPage%>&showDoc=<%=showDocName%>">
                <img src="./images/btnArrowRight1-r.gif"/>
            </a>
            <a href="useractionslist.jsp?startIndex=<%=numRecordsPerPage * (numPages - 1) + 1%>&numRecords=<%=numRecordsPerPage%>&showDoc=<%=showDocName%>">
                <img src="./images/btnArrowRight2-r.gif"/>
            </a>
<%
        }
%>
            </div>
            <span style="padding-left:20px;">Nombre de Documento:
                <select name="showDocName" onchange="location = 'useractionslist.jsp?startIndex=<%=startIndex%>&numRecords=<%=numRecordsPerPage%>&showDoc=' + this.options[this.selectedIndex].value;">
                    <%
                        if (showDocName)
                        {
                            out.print("<option selected value=true>Mostrar</option>");
                            out.print("<option value=false>Ocultar</option>");
                        }
                        else
                        {
                            out.print("<option selected value=false>Ocultar</option>");
                            out.print("<option value=true>Mostrar</option>");
                        }
                    %>
                </select>
            </span>
        </div>
        <div style="margin-left:30px; margin-right: 10px; margin-bottom: 20px; clear: left;">
        <table class="list">
            <tr class="listTitle">
                <td>Nombre de Usuario</td>
                
                <%
                    if (bCC_Activation){
                %>
                
                <td>Nombre completo de Usuario</td>
                <td>Nombre Centro de Coste</td>
                <td>Centro de Coste</td>
                
                <%
                   }
                %>
                
                <td>Nombre de Impresora</td>
                <td>Número de Serie</td>
                <td>Ubicación</td>
                <td>Zona</td>
                <td>Fecha</td>
                <td>Hora</td>
                <td>Tipo</td>
                <td>Número de páginas</td>
                <td>B/N o Color</td>
                <td>Tamaño de página</td>
                <td>Detalle</td>
            </tr>
<%
    if (numRows > 0)
    {
        for(int i = startIndex; i <= increment; i++)
        {
            action = (Action) actions.get(i-1);
%>
            <tr class="list">
                <td class="listTitle">
                    <%
                        if (action.getUserName().isEmpty())
                        {
                            %><%=action.getPrinter().getHostname()%><%
                        }
                        else
                        {   
                            String[] split = action.getUserName().split("_");
                            if (split.length < 3){
                                 if (action.getUserName().contains("noname")){

                                 }else{
                                %>
                                    <%=action.getUserName()%>
                                <%
                                }
                                %>
                          <%
                            }else{
                            %>
                                <%=split[2]%>
                            <%
                            }
                        }
                    %>
                </td>
                
                <%
                    if (bCC_Activation){
                %>
                
                <td>
                    <%=action.getUser().getFullname()%>
                </td>
                <td>
                    <%=action.getUser().getDepartment()%>
                </td>
                <td>
                    <%=action.getUser().getCostCenter()%>
                </td>
                
                <%
                   }
                %>
                <td>
                    <%=action.getPrinter().getHostname()%>
                </td>
                <td class="listTitle">
                    <%=action.getMFPserial()%>
                </td>
                <td>
                    <%=action.getPrinter().getLocation()%>
                </td>
                <td>
                    <%=action.getPrinter().getZone()%>
                </td>
                <td>
                    <%=dateFormat.format(action.getDate())%>
                </td>
                <td>
                    <%=timeFormat.format(action.getDateTime())%>
                </td>
                <td>
                <%
                    if(action.getType().equals("printer.print") || action.getType().equals("printer.secret-print") || action.getType().equals("printer.trial-storage-print")
                        || action.getType().equals("printer.trial-print") || action.getType().equals("printer.pausedocument-print")
                        || action.getType().equals("printer.keepdocument-storage-print") || action.getType().equals("printer.keepdocument-reprint")
                        || action.getType().equals("report") || action.getType().equals("report.state") || action.getType().equals("fax.pctranser")
                        || action.getType().equals("documentbox.storedocument-print"))
                        {
                %>
                            Impresión
                <%
                        }
                    else if(action.getType().equals("copy") || action.getType().equals("copy.storage"))
                        {
                %>
                            Copia
                <%
                        }
                    else if(action.getType().equals("copy.storage") || action.getType().equals("scanner.storage") || action.getType().equals("documentbox.storage")
                        || action.getType().equals("scanner.deliver-storage") || action.getType().equals("documentbox.storage-network")
                        || action.getType().equals("scanner.linkdeliver-storage"))
                        {
                %>
                            Escaneado almacenado
                <%
                        }
                    else if(action.getType().equals("scanner.deliver") || action.getType().equals("scanner.deliver-storage") || action.getType().equals("scanner.storedocument-forward")
                        || action.getType().equals("scanner.linkdeliver-storage") || action.getType().equals("scanner.storedocument-deliver")
                        || action.getType().equals("scanner.storedocument-linkdeliver") || action.getType().equals("documentbox.storedocument-forward"))
                        {
                %>
                            Escaneado enviado
                <%
                        }
                    else if(action.getType().equals("fax.transfer") || action.getType().equals("fax.pctranser"))
                        {
                %>
                            Fax enviado
                <%
                        }
                    else if(action.getType().equals("fax.print") || action.getType().equals("fax.receive")
                        || action.getType().equals("fax.receive-deliver") || action.getType().equals("fax.receive-storage"))
                        {
                %>
                            Fax recibido
                <%
                        }
                    else
                        {}

                %>
                </td>
                <td>
                    <%=action.getNumPages()%>
                </td>
                <td>
                <%
                    if(action.getColorMode().equals("blackandwhite"))
                    {
                %>
                        b/n
                <%
                    }
                    else if(action.getColorMode().equals("color"))
                    {
                %>
                        color
                <%
                    }
                    else
                        {}
                %>
                </td>
                <td>
                    <%=action.getPageSize()%>
                </td>
                <td>
                <%
                    if(action.getType().equals("fax.transfer") || action.getType().equals("fax.pctransfer") || action.getType().equals("scanner.deliver")
                        || action.getType().equals("scanner.deliver-storage") || action.getType().equals("scanner.storedocument-forward")
                        || action.getType().equals("scanner.linkdeliver-storage") || action.getType().equals("scanner.storedocument-deliver")
                        || action.getType().equals("scanner.storedocument-linkdeliver") || action.getType().equals("documentbox.storedocument-forward"))
                    {
                        if (action.getType().equals("scanner.deliver-storage") || action.getType().equals("scanner.linkdeliver-storage"))
                            {%>(escaneado almacenado)<%}
                        if (action.getType().equals("documentbox.storedocument-forward"))
                            {%>(documentbox enviado)<%}
                        if (action.getDestination().isEmpty())
                            {%>(desconocido)<%}
                        else 
                        {%>
                            <%=action.getDestination()%>
                        <%
                        }
                    }
                    else if(action.getType().equals("fax.print") || action.getType().equals("fax.receive")
                        || action.getType().equals("fax.receive-deliver") || action.getType().equals("fax.receive-storage"))
                    {
                        if (action.getSender().isEmpty())
                            {%>(desconocido)<%}
                        else
                            {%><%=action.getSender()%><%}
                    }
                    else if(action.getType().equals("printer.print") || action.getType().equals("printer.secret-print") || action.getType().equals("printer.trial-storage-print")
                        || action.getType().equals("printer.trial-print") || action.getType().equals("printer.pausedocument-print")
                        || action.getType().equals("printer.keepdocument-storage-print") || action.getType().equals("printer.keepdocument-reprint"))
                            {
                                if (showDocName)
                                {
                                    %><%=action.getDocuName()%><%
                                }
                                else
                                {
                                    %>(impresión)<%
                                }
                            }
                    else if (action.getType().equals("fax.print"))
                            {
                                if (showDocName)
                                {
                                    %><%=action.getDocuName()%><%
                                }
                                else
                                {
                                    %>(impresión fax)<%
                                }
                            }
                    else if (action.getType().equals("report") || action.getType().equals("report.state"))
                            {
                                if (showDocName)
                                {
                                    %><%=action.getDocuName()%><%
                                }
                                else
                                {
                                    %>(impresión informe)<%
                                }
                            }
                    else if (action.getType().equals("documentbox.storedocument-print"))
                            {
                                if (showDocName)
                                {
                                    %><%=action.getDocuName()%><%
                                }
                                else
                                {
                                    %>(documentbox impresión)<%
                                }
                            }
                    else if(action.getType().equals("copy"))
                        {%>(copia)<%}
                    else if(action.getType().equals("copy.storage"))
                        {%>(copia almacenada)<%}
                    else if (action.getType().equals("scanner.storage"))
                        {%>(escaneado almacenado)<%}
                    else if (action.getType().equals("documentbox.storage") || action.getType().equals("documentbox.storage-network"))
                        {%>(documentbox almacenado)<%}
                    else
                        {%>(desconocido)<%}
            %>
                </td>
            </tr>
<%
        }
    }
%>
        </table>
        </div>
    </body>
</html>
