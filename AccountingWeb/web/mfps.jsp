<%-- 
    Document   : mfps
    Created on : 23-jul-2010, 10:06:54
    Author     : alexis.hidalgo
--%>
<%@ page import="java.util.*" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Ricoh Accounting Package</title>
        <style type="text/css">@import url(./css/calendar-blue2.css);</style>
        <script type="text/javascript" src="calendar.js"></script>
        <script type="text/javascript" src="lang/calendar-es.js"></script>
        <script type="text/javascript" src="calendar-setup.js"></script>
        <link rel="stylesheet" type="text/css" href="./css/style.css">

        <script type="text/javascript">
            // This function gets called when the end-user clicks on some date.
            function selected(cal, date)
            {
              cal.sel.value = date; // just update the date in the input field.
            }

            function calIniSelected(cal, date)
            {
                  var el = document.getElementById("iniDate");
                  el.value = date;
            }

            function calEndSelected(cal, date)
            {
                  var el = document.getElementById("endDate");
                  el.value = date;
            }


            function showCalendars()
            {
              var parentIniCal = document.getElementById("ini_calendar-container");
              var parentEndCal = document.getElementById("end_calendar-container");

              // construct a calendar giving only the "selected" handler.
              var IniCal = new Calendar(1, null, calIniSelected);
              var EndCal = new Calendar(1, null, calEndSelected);

              IniCal.setDateFormat("%d/%m/%Y");
              EndCal.setDateFormat("%d/%m/%Y");

              // this call must be the last as it might use data initialized above; if
              // we specify a parent, as opposite to the "showCalendar" function above,
              // then we create a flat calendar -- not popup.  Hidden, though, but...
              IniCal.create(parentIniCal);
              EndCal.create(parentEndCal);

              // ... we can show it here.
              IniCal.show();
              EndCal.show();
            }
        </script>
    </head>

    <%@ include file="cabecera.jsp" %>
    <%@ include file="principal.jsp" %>

    <body class="main" onload="showCalendars()">
        
            <div>
                <table>
                    <tr>
                        <td>
                            <div style="margin-left: 150px;margin-top: 10px">
                                <h3>Seleccione el intervalo de fechas para visualizar el consumo de las MFPs</h3>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
          <form action="./listMFPS.do" method="post">
                <%
                String code = (String) session.getAttribute("errorCode");

                if (code != null)
                {
                    if (code.equals("empty_date"))
                        out.print("<script language=\"JavaScript\" type=\"text/javascript\">alert(\"Introduzca las fechas de inicio y final.\");</script>");
                    else if (code.equals("parse_fail"))
                        out.print("<script language=\"JavaScript\" type=\"text/javascript\">alert(\"Se ha producido un error al leer la fecha. Por favor, introdúzcala de nuevo.\");</script>");
                    else if (code.equals("start_after_end"))
                        out.print("<script language=\"JavaScript\" type=\"text/javascript\">alert(\"La fecha de inicio ha de ser anterior a la de fin.\");</script>");
                    else if (code.equals("db_error"))
                        out.print("<script language=\"JavaScript\" type=\"text/javascript\">alert(\"Se ha producido un error contactando la Base de Datos.\");</script>");
                    else if (code.equals("process_error"))
                        out.print("<script language=\"JavaScript\" type=\"text/javascript\">alert(\"Se ha producido un error generando los datos.\");</script>");
                    else if (code.equals("end_after_today"))
                        out.print("<script language=\"JavaScript\" type=\"text/javascript\">alert(\"La fecha de fin no puede ser posterior a la fecha del día en curso.\");</script>");

                    session.removeAttribute("errorCode");
               }
               %>
            <div style="margin-left: 225px;">
                <table style="border-collapse: separate;">
                    <tr>
                        <td>
                            <div>
                                Fecha de Inicio:
                                <input type="text" id="iniDate" name="iniDate" readonly="true"/>
                            </div>
                        </td>
                        
                        <td>
                            <div style="margin-left: 200px;">
                                Fecha de Fin:
                                <input type="text" id="endDate" name="endDate" readonly="true"/>
                            </div>
                        </td>
                    </tr>
                    <tr></tr>
                    <tr>
                        <td>
                            <div style="position: fixed; margin-top: 15px;" id="ini_calendar-container"></div>
                        </td>
                        
                        <td>
                            <div style="margin-top: 15px; margin-left: 200px; position: fixed;" id="end_calendar-container"></div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>

                        <td>
                        </td>
                    </tr>

               </table>
            </div>
            <div>
                <input type="submit" name="displayCounters" value="Ver Informe" style="margin-top: 200px; margin-left: 525px;">
            </div>
        </form>
    </body>
</html>


