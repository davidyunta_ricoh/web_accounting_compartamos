<%-- 
    Document   : usermfplist
    Created on : 21-mar-2011, 18:18:03
    Author     : Alexis.Hidalgo
--%>

<%@ page import="java.util.*" %>
<%@ page import="ricoh.accounting.objects.Resume"%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Ricoh Accounting Package</title>
        <link rel="stylesheet" type="text/css" href="./css/style.css">
    </head>

    <%@ include file="cabecera.jsp" %>
    <%@ include file="principal.jsp" %>

    <body class="main">
        <div>
            <div style="margin-left: 225px;">
                <h3>Informe de contadores totales por Usuario en MFP entre las fechas <%=(String)session.getAttribute("iniDate")%> y <%=(String)session.getAttribute("endDate")%></h3>
            </div>
            <div style="float:right; padding:15px; text-align:center; width:50px;">
                <img src="./images/csv256.png" height="30" width="30" style="margin-bottom:5px;"/>
                <br>
                <a href="./downloadFile.do?file=usermfps">Descargar Fichero</a>
            </div>


        </div>
        <div class="settingFlatContsDivision">
            <img src="./images/settingFlatContsDivision.gif">
        </div>
<%
        Resume resume;
        int numRows, startIndex = 1, numRecordsPerPage = 20, remain, increment, numPages;

        boolean bCC_Activation = ((String)session.getAttribute("CC_ACTIVATION")).equalsIgnoreCase("true");
        
        if(request.getParameter("startIndex") != null)
            startIndex = Integer.parseInt(request.getParameter("startIndex"));

        if(request.getParameter("numRecords") != null)
            numRecordsPerPage = Integer.parseInt(request.getParameter("numRecords"));

        List mfpResumes = (List)session.getAttribute("resumeList");
            numRows = mfpResumes.size();

            numPages = numRows /numRecordsPerPage;

            if((remain = numRows % numRecordsPerPage) != 0)
                numPages++;

            if(((startIndex + numRecordsPerPage) <= numRows) || remain == 0)
                increment = startIndex + numRecordsPerPage - 1;

            else
                increment = startIndex + remain - 1;
 %>
            <div style="display: block; clear: left; margin-left:50px; margin-top:10px; width: 100%">
                <span style="padding-right:20px;">Núm. total de Registros: <%=numRows%></span>
                <span style="padding-right:20px;">Núm. de Registros por página:
                    <select name="numRecords" onchange="location = 'usermfpslist.jsp?numRecords=' + this.options[this.selectedIndex].value;">
                        <%
                        if (numRecordsPerPage == 20)
                            out.print("<option selected value=\"20\">20</option>");
                        else
                            out.print("<option value=\"20\">20</option>");

                        if (numRecordsPerPage == 50)
                            out.print("<option selected value=\"50\">50</option>");
                        else
                            out.print("<option value=\"50\">50</option>");

                        if (numRecordsPerPage == 100)
                            out.print("<option selected value=\"100\">100</option>");
                        else
                            out.print("<option value=\"100\">100</option>");

                        if (numRecordsPerPage == 200)
                            out.print("<option selected value=\"200\">200</option>");
                        else
                            out.print("<option value=\"200\">200</option>");

                        if (numRecordsPerPage == 500)
                            out.print("<option selected value=\"500\">500</option>");
                        else
                            out.print("<option value=\"500\">500</option>");

                        if (numRecordsPerPage == 1000)
                            out.print("<option selected value=\"1000\">1000</option>");
                        else
                            out.print("<option value=\"1000\">1000</option>");

                        if (numRecordsPerPage == 2000)
                            out.print("<option selected value=\"2000\">2000</option>");
                        else
                            out.print("<option value=\"2000\">2000</option>");

                        if (numRecordsPerPage == 5000)
                            out.print("<option selected value=\"5000\">5000</option>");
                        else
                            out.print("<option value=\"5000\">5000</option>");

                        if (numRecordsPerPage == numRows)
                            out.print("<option selected value=" + numRows + ">TODOS</option>");
                        else
                            out.print("<option value=" + numRows + ">TODOS</option>");
%>
                    </select>
                </span>
                <span style="padding-right:20px;">Núm. de páginas: <%=numPages%></span>
                <div style="display:inline;">
<%
            if(increment < numRows)
            {
%>
                <span style="padding-right:10px;">Registros <%=startIndex%> a <%=increment%></span>
<%
            }
            else if (numRows == 0)
            {
%>
                <span style="padding-right:10px;">Registros <%=numRows%> a <%=numRows%></span>
<%
            }

            else
            {
%>
                <span style="padding-right:10px;">Registros <%=startIndex%> a <%=numRows%></span>
<%
            }
    //out.println("</table>");
    //out.println("<table><tr>Resultados");

        if(startIndex == 1)
        {
%>
           <img src="./images/btnArrowLeft2-d.gif"/>
           <img src="./images/btnArrowLeft1-d.gif"/>
<%
        }
        else
        {
%>
            <a href="usermfpslist.jsp?startIndex=1&numRecords=<%=numRecordsPerPage%>">
                <img src="./images/btnArrowLeft2-r.gif"/>
            </a>
            <a href="usermfpslist.jsp?startIndex=<%=startIndex - numRecordsPerPage%>&numRecords=<%=numRecordsPerPage%>">
                <img src="./images/btnArrowLeft1-r.gif"/>
            </a>
<%
        }

        if((startIndex > numRows -numRecordsPerPage))
        {
%>
            <img src="./images/btnArrowRight1-d.gif"/>
            <img src="./images/btnArrowRight2-d.gif"/>
<%
        }
        else
        {
%>
            <a href="usermfpslist.jsp?startIndex=<%=startIndex + numRecordsPerPage%>&numRecords=<%=numRecordsPerPage%>">
                <img src="./images/btnArrowRight1-r.gif"/>
            </a>
            <a href="usermfpslist.jsp?startIndex=<%=numRecordsPerPage * (numPages - 1) + 1%>&numRecords=<%=numRecordsPerPage%>">
                <img src="./images/btnArrowRight2-r.gif"/>
            </a>
<%
        }
%>
            </div>
        </div>
        <div style="margin-left:30px; margin-right: 10px; margin-bottom: 20px; clear: left;">
        <table class="list">
            <tr class="listTitle">
                <td>Nombre de Usuario</td>
                
                <%
                    if (bCC_Activation){
                %>
                
                <td>Nombre completo de Usuario</td>
                <td>Nombre Centro de Coste</td>
                <td>Centro de Coste</td>
                
                <%
                   }
                %>
                
                <td>Nombre de Impresora</td>
                <td>Número de Serie</td>
                <td>Región</td>
                <td>Oficina</td>
                <td>Total de Páginas</td>
                <td>Impresión Color</td>
                <td>Impresión B/N</td>
                <td>Copia Color</td>
                <td>Copia B/N</td>
<!--            <td>Escaneado Almacenado</td>
                <td>Escaneado Enviado</td>
                <td>Fax Enviado</td>
                <td>Fax Recibido</td>-->
            </tr>
<%
    if (numRows > 0)
    {
        for(int i = startIndex; i <= increment; i++)
        {
            resume = (Resume) mfpResumes.get(i-1);
%>
            <tr class="list">
                <td class="listTitle">
                        <%
                        if (resume.getUser().getUsername().isEmpty())
                        {
                            %><%=resume.getPrinter().getHostname()%><%
                        }
                        else
                        {
                        %>
                          <a href="./listUserActions.do?user=<%=resume.getUser().getUsername()%>&iniDate=<%=(String)session.getAttribute("iniDate")%>&endDate=<%=(String)session.getAttribute("endDate")%>">  
                            <%
                                String[] split = resume.getUser().getUsername().split("_");
                                if (split.length < 3){  
                                     if (resume.getUser().getUsername().contains("noname")){

                                     }else{
                                    %>
                                        <%=resume.getUser().getUsername()%>
                                    <%
                                     }
                                }else{
                            %>
                                <%=split[2]%>
                             <%
                                }
                             %>
                          </a>
                        <%
                        }
                        %>
                </td>
                
                <%
                    if (bCC_Activation){
                %>
                
                <td>
                    <%=resume.getUser().getFullname()%>
                </td>
                <td>
                    <%=resume.getUser().getDepartment()%>
                </td>
                <td>
                    <%=resume.getUser().getCostCenter()%>
                </td>
                
                <%
                   }
                %>
                
                <td>
                    <%=resume.getPrinter().getHostname()%>
                </td>

                <td class="listTitle">
                    <a href="./listMFPS.do?mfp=<%=resume.getPrinter().getMfpSerial()%>&iniDate=<%=(String)session.getAttribute("iniDate")%>&endDate=<%=(String)session.getAttribute("endDate")%>">
                        <%=resume.getPrinter().getMfpSerial()%>
                    </a>
                </td>
                <td>
                    <%=resume.getPrinter().getLocation()%>
                </td>
                <td>
                    <%=resume.getPrinter().getZone()%>
                </td>
                <td>
                    <%=resume.getTotal()%>
                </td>
                <td>
                    <%=resume.getPrintColor()%>
                </td>
                <td>
                    <%=resume.getPrintBW()%>
                </td>
                <td>
                    <%=resume.getCopyColor()%>
                </td>
                <td>
                    <%=resume.getCopyBW()%>
                </td>
<!--            <td>
                    <%--<%=resume.getScanStored()%>--%>
                </td>
                <td>
                    <%--<%=resume.getScanSend()%>--%>
                </td>
                <td>
                    <%--<%=resume.getFaxSend()%>--%>
                </td>
                <td>
                    <%--<%=resume.getFaxReceived()%>--%>
                </td>-->
            </tr>
<%
        }
    }
%>
        </table>
        </div>
    </body>
</html>
