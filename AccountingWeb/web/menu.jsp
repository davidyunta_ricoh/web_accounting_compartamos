<%-- 
    Document   : menu
    Created on : 15-jul-2010, 12:31:09
    Author     : Alexis.Hidalgo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Ricoh Accounting Package</title>
    <link rel="stylesheet" type="text/css" href="./css/style.css">
</head>
<body class="main">
<%@ include file="cabecera.jsp" %>
<%@ include file="principal.jsp" %>

    <div>
        <table>
            <tr>
                <td>
                    <div style="margin-left: 60px;"><h3>Menú Principal</h3></div>
                </td>
                <td align="right">
                    <div style="text-align:right; font-size:12px;margin-left:920px;margin-bottom:40px">
                        <%  
                        if (session.getAttribute("isAdmin") != null)
                        {
                            boolean isAdmin = (Boolean)session.getAttribute("isAdmin");
                                //java.util.Vector mfps = (java.util.Vector)session.getAttribute("mfps");

                            if (!isAdmin)
                            {
                                //MENU DE ADMINISTRADOR:
                        %>     
                        <table>
                            <tr>
                                <td><h3>Empresa:  </h3></td>
                                <td><h3 style="color:#161991">  <%=session.getAttribute("company")%></h3></td>
                            </tr>
                        </table>
                        <%
                            }
                        }  
                        %>
                    </div>
                </td>
            </tr>
        </table>
    </div>

    <div class="settingFlatContsDivision">
        <img src="./images/settingFlatContsDivision.gif">
    </div>
    <%
    String code = (String) session.getAttribute("errorCode");

    if (code != null)
    {
        if (code.equals("empty_date"))
            out.print("<script language=\"JavaScript\" type=\"text/javascript\">alert(\"Introduzca las fechas de inicio y final.\");</script>");
        else if (code.equals("parse_fail"))
            out.print("<script language=\"JavaScript\" type=\"text/javascript\">alert(\"Se ha producido un error al leer la fecha. Por favor, introdúzcala de nuevo.\");</script>");
        else if (code.equals("start_after_end"))
            out.print("<script language=\"JavaScript\" type=\"text/javascript\">alert(\"La fecha de inicio ha de ser anterior a la de fin.\");</script>");
        else if (code.equals("db_error"))
            out.print("<script language=\"JavaScript\" type=\"text/javascript\">alert(\"Se ha producido un error contactando la Base de Datos.\");</script>");
        else if (code.equals("process_error"))
            out.print("<script language=\"JavaScript\" type=\"text/javascript\">alert(\"Se ha producido un error generando los datos.\");</script>");
        else if (code.equals("end_after_today"))
            out.print("<script language=\"JavaScript\" type=\"text/javascript\">alert(\"La fecha de fin no puede ser posterior a la fecha del día en curso.\");</script>");

        session.removeAttribute("errorCode");
    }
    %>

    <%  
    if (session.getAttribute("isAdmin") != null){
        boolean isAdmin = (Boolean)session.getAttribute("isAdmin");
            //java.util.Vector mfps = (java.util.Vector)session.getAttribute("mfps");

        if (isAdmin)
        {
            //MENU DE ADMINISTRADOR:
    %>        
    <div style="margin-left: 225px; margin-top:50px;">
        <table>
            <tr>
                <td valign="top" class="adminlogo"><img src="./images/iconInstrument48B.gif"></td>
                <td>
                    <table width="500">
                        <tr bgcolor="#BDD7EF">
                            <td align="left">
                                <div class="standard" style="font-weight:bold;;font-size:18px">Administración</div>
                            </td>
                            <td>        
                                <tr height="25"></tr>
                                <tr style="display:block; margin-bottom:30px;">
                                    <td>
                                        <a href="./appAdmin.do" style="font-family:Arial; font-size:16px; font-weight:bold;">
                                            Administración de la Aplicación
                                        </a>
                                    </td>
                                </tr>
                                <tr style="display:block; margin-bottom:30px;">
                                    <td>
                                        <a href="./userAdmin.do" style="font-family:Arial; font-size:16px; font-weight:bold;">
                                            Administración de Usuarios
                                        </a>
                                    </td>
                                </tr>
                                <tr style="display:block; margin-bottom:30px;">
                                    <td>
                                        <a href="./mfpsAdmin.do" style="font-family:Arial; font-size:16px; font-weight:bold;">
                                            Administración de MFPs
                                        </a>
                                    </td>
                                </tr>
<!--                            <tr style="display:block; margin-bottom:30px;">
                                    <td>
                                        <a href="./companiesAdmin.do" style="font-family:Arial; font-size:16px; font-weight:bold;">
                                            Administración de Empresas
                                        </a>
                                    </td>
                                </tr>-->
                                <tr height="25"></tr>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td valign="top"><img height="35" width="35" src="./images/csv256.png"></td>
                <td>
                    <table width="500">
                        <tr bgcolor="#BDD7EF">
                            <td align="left">
                                <div class="standard" style="font-weight:bold;font-size:18px">Informes</div>
                            </td>
                            <td>        
                                <tr height="25"></tr>
                                <tr style="display:block; margin-bottom:30px;">
                                    <td>
                                        <a href="./menuUsers.do?type=imp" style="font-family:Arial; font-size:16px; font-weight:bold;">
                                            Ver informe de contadores de Impresión
                                        </a>
                                    </td>
                                </tr>
                                <tr style="display:block; margin-bottom:30px;">
                                    <td>
                                        <a href="./menuUsers.do?type=copy" style="font-family:Arial; font-size:16px; font-weight:bold;">
                                            Ver informe de contadores de Copias
                                        </a>
                                    </td>
                                </tr>
                                <tr style="display:block; margin-bottom:30px;">
                                    <td>
                                        <a href="./menuUsers.do?type=hojas" style="font-family:Arial; font-size:16px; font-weight:bold;">
                                            Ver informe de contadores de Hojas
                                        </a>
                                    </td>
                                </tr>
                                <tr style="display:block; margin-bottom:30px;">
                                    <td>
                                        <a href="./menuTotalPagesMFPs.do" style="font-family:Arial; font-size:16px; font-weight:bold;">
                                            Ver informe de contadores totales de páginas impresas por MFP
                                        </a>
                                    </td>
                                </tr>
                                <tr style="display:block; margin-bottom:30px;">
                                    <td>
                                        <a href="./menuUserMFPs.do" style="font-family:Arial; font-size:16px; font-weight:bold;">
                                            Ver informe de contadores totales por Usuario en MFP
                                        </a>
                                    </td>
                                </tr>
<!--                                <tr style="display:block; margin-bottom:30px;">
                                    <td>
                                        <a href="./extract.jsp" style="font-family:Arial; font-size:16px; font-weight:bold;">
                                            Generar ficheros ZIP de Accounting
                                        </a>
                                    </td>
                                </tr>-->
                                <tr height="25"></tr>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <input type="hidden" value="" name="process">
    <%
        }
        else
        {
            //MENU DE USUARIO:
    %>
    <div style="margin-left: 225px; margin-top:50px;">
    <table>
        <tr>
            <td valign="top"><img height="35" width="35" src="./images/csv256.png"></td>
            <td>
                <table width="500">
                        <tr bgcolor="#BDD7EF">
                            <td align="left">
                                <div class="standard" style="font-weight:bold;font-size:18px">Informes</div>
                            </td>
                            <td>        
                                <tr height="25"></tr>
                                <tr style="display:block; margin-bottom:30px;">
                                    <td>
                                        <a href="./menuUsers.do?type=imp" style="font-family:Arial; font-size:16px; font-weight:bold;">
                                            Ver informe de contadores de Impresión
                                        </a>
                                    </td>
                                </tr>
                                <tr style="display:block; margin-bottom:30px;">
                                    <td>
                                        <a href="./menuUsers.do?type=copy" style="font-family:Arial; font-size:16px; font-weight:bold;">
                                            Ver informe de contadores de Copias
                                        </a>
                                    </td>
                                </tr>
                                <tr style="display:block; margin-bottom:30px;">
                                    <td>
                                        <a href="./menuUsers.do?type=hojas" style="font-family:Arial; font-size:16px; font-weight:bold;">
                                            Ver informe de contadores de Hojas
                                        </a>
                                    </td>
                                </tr>
                                <tr style="display:block; margin-bottom:30px;">
                                    <td>
                                        <a href="./menuTotalPagesMFPs.do" style="font-family:Arial; font-size:16px; font-weight:bold;">
                                            Ver informe de contadores totales de páginas impresas por MFP
                                        </a>
                                    </td>
                                </tr>
                                <tr style="display:block; margin-bottom:30px;">
                                    <td>
                                        <a href="./menuUserMFPs.do" style="font-family:Arial; font-size:16px; font-weight:bold;">
                                            Ver informe de contadores totales por Usuario en MFP
                                        </a>
                                    </td>
                                </tr>
                                <tr height="25"></tr>
                            </td>
                        </tr>
                    </table>
            </td>
        </tr>
        </table>
    </div>
    <%
        }
    }
    %>
</body>
</html>
