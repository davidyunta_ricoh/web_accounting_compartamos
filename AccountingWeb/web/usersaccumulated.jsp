<%-- 
    Document   : usersaggregated
    Created on : 25-ene-2016, 12:06:08
    Author     : Francisco.Madera
--%>

<%@page import="org.apache.commons.collections4.Predicate"%>
<%@page import="ricoh.accounting.utils.CalculosFechas"%>
<%@page import="java.util.concurrent.TimeUnit"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="org.apache.commons.collections4.CollectionUtils"%>
<%@page import="org.apache.commons.collections4.IterableUtils"%>
<%@ page import="java.util.Date" %>
<%@ page import="ricoh.accounting.objects.Accumulated" %>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Ricoh Accounting Package</title>
        <link rel="stylesheet" type="text/css" href="./css/style.css">
    </head>

    <%@ include file="cabecera.jsp" %>
    <%@ include file="principal.jsp" %>

    <body class="main">
        <div>
            <div style="margin-left: 225px;">
                <h3>Informe de contadores de <%=(String)session.getAttribute("tipo")%> por <%=(String)session.getAttribute("intervaloTiempo")%> entre las fechas <%=(String)session.getAttribute("iniDate")%> y <%=(String)session.getAttribute("endDate")%></h3>
            </div>
            <div style="float:right; padding:15px; text-align:center; width:50px;">
                <img src="./images/csv256.png" height="30" width="30" style="margin-bottom:5px;"/>
                <br>
                <a href="./downloadFile.do?file=useraccumulated">Descargar Fichero</a>
            </div>
        </div>
        <div class="settingFlatContsDivision">
            <img src="./images/settingFlatContsDivision.gif">
        </div>
<%
        Accumulated accumulated;
        int numRows, startIndex = 1, numRecordsPerPage = 20, remain, increment, numPages;
        String strFechaInicio = (String)session.getAttribute("iniDate"), strFechaFin = (String)session.getAttribute("endDate");
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date fechaInicio = dateFormat.parse(strFechaInicio), fechaFin = dateFormat.parse(strFechaFin);
        final long MILLSECS_PER_DAY = 24 * 60 * 60 * 1000;
        
        if(request.getParameter("startIndex") != null)
            startIndex = Integer.parseInt(request.getParameter("startIndex"));

        if(request.getParameter("numRecords") != null)
            numRecordsPerPage = Integer.parseInt(request.getParameter("numRecords"));

        List accumulatedList = (List)session.getAttribute("accumulatedList");
            numRows = accumulatedList.size();

            if (numRecordsPerPage == 0)
            {
                numPages = 0;
                remain = 0;
            }
            else
            {
                numPages = numRows /numRecordsPerPage;

                if((remain = numRows % numRecordsPerPage) != 0)
                    numPages++;
            }

            if(((startIndex + numRecordsPerPage) <= numRows) || remain == 0)
                increment = startIndex + numRecordsPerPage - 1;

            else
                increment = startIndex + remain - 1;
 %>
            <div style="display: block; clear: left; margin-left:50px; margin-top:10px; width: 100%">
                <span style="padding-right:20px;">Núm. total de Registros: <%=numRows%></span>
                <span style="padding-right:20px;">Núm. de Registros por página:
                    <select name="numRecords" onchange="location = 'usersaccumulated.jsp?numRecords=' + this.options[this.selectedIndex].value;">
                        <%
                        if (numRecordsPerPage == 20)
                            out.print("<option selected value=\"20\">20</option>");
                        else
                            out.print("<option value=\"20\">20</option>");

                        if (numRecordsPerPage == 50)
                            out.print("<option selected value=\"50\">50</option>");
                        else
                            out.print("<option value=\"50\">50</option>");

                        if (numRecordsPerPage == 100)
                            out.print("<option selected value=\"100\">100</option>");
                        else
                            out.print("<option value=\"100\">100</option>");

                        if (numRecordsPerPage == 200)
                            out.print("<option selected value=\"200\">200</option>");
                        else
                            out.print("<option value=\"200\">200</option>");

                        if (numRecordsPerPage == 500)
                            out.print("<option selected value=\"500\">500</option>");
                        else
                            out.print("<option value=\"500\">500</option>");

                        if (numRecordsPerPage == 1000)
                            out.print("<option selected value=\"1000\">1000</option>");
                        else
                            out.print("<option value=\"1000\">1000</option>");

                        if (numRecordsPerPage == 2000)
                            out.print("<option selected value=\"2000\">2000</option>");
                        else
                            out.print("<option value=\"2000\">2000</option>");

                        if (numRecordsPerPage == 5000)
                            out.print("<option selected value=\"5000\">5000</option>");
                        else
                            out.print("<option value=\"5000\">5000</option>");

                        if (numRecordsPerPage == numRows)
                            out.print("<option selected value=" + numRows + ">TODOS</option>");
                        else
                            out.print("<option value=" + numRows + ">TODOS</option>");
%>
                    </select>
                </span>
                <span style="padding-right:20px;">Núm. de páginas: <%=numPages%></span>
                <div style="display:inline;">
<%
            if(increment < numRows)
            {
%>
                <span style="padding-right:10px;">Registros <%=startIndex%> a <%=increment%></span>
<%
            }
            else if (numRows == 0)
            {
%>
                <span style="padding-right:10px;">Registros <%=numRows%> a <%=numRows%></span>
<%
            }

            else
            {
%>
                <span style="padding-right:10px;">Registros <%=startIndex%> a <%=numRows%></span>
<%
            }
    //out.println("</table>");
    //out.println("<table><tr>Resultados");

        if(startIndex == 1)
        {
%>
           <img src="./images/btnArrowLeft2-d.gif"/>
           <img src="./images/btnArrowLeft1-d.gif"/>
<%
        }
        else
        {
%>
            <a href="usersaccumulated.jsp?startIndex=1&numRecords=<%=numRecordsPerPage%>">
                <img src="./images/btnArrowLeft2-r.gif"/>
            </a>
            <a href="usersaccumulated.jsp?startIndex=<%=startIndex - numRecordsPerPage%>&numRecords=<%=numRecordsPerPage%>">
                <img src="./images/btnArrowLeft1-r.gif"/>
            </a>
<%
        }

        if((startIndex > numRows -numRecordsPerPage))
        {
%>
            <img src="./images/btnArrowRight1-d.gif"/>
            <img src="./images/btnArrowRight2-d.gif"/>
<%
        }
        else
        {
%>
            <a href="usersaccumulated.jsp?startIndex=<%=startIndex + numRecordsPerPage%>&numRecords=<%=numRecordsPerPage%>">
                <img src="./images/btnArrowRight1-r.gif"/>
            </a>
            <a href="usersaccumulated.jsp?startIndex=<%=numRecordsPerPage * (numPages - 1) + 1%>&numRecords=<%=numRecordsPerPage%>">
                <img src="./images/btnArrowRight2-r.gif"/>
            </a>
<%
        }
%>
            </div>
        </div>
        <div style="margin-left:30px; margin-right: 10px; margin-bottom: 20px; clear: left;">
            <table class="list" border="1" cellspacing="0px" cellpadding="10px">
            <thead>
                <tr class="listTitle">
                    <%
                    String filtrado = (String)session.getAttribute("filtrado");
                     if (filtrado.equalsIgnoreCase("Usuario"))
                            out.print("<th rowspan='2'>Nombre de Usuario</th>");
                     else if (filtrado.equalsIgnoreCase("Oficina"))
                            out.print("<th rowspan='2'>Nombre Oficina</th>");
                     else if (filtrado.equalsIgnoreCase("Region"))
                            out.print("<th rowspan='2'>Nombre Region</th>");
                     else
                         out.print("<td>Nombre</td>");
                    %>
                
                    <% 
                        List<String> semanas = CalculosFechas.CalculoNumeroSemanas(fechaInicio, fechaFin);
                        List<String> meses = CalculosFechas.CalculoNumeroMeses(fechaInicio, fechaFin);
                        
                        String agrupacion = (String)session.getAttribute("intervaloTiempo");
                        if (agrupacion.equalsIgnoreCase("hora"))
                            out.print("<th colspan='17'>IMPRESIONES POR HORA (Num.Total Páginas) - DÍA " + (String)session.getAttribute("iniDate")+ "</th>");
                        else if (agrupacion.equalsIgnoreCase("dia"))
                            out.print("<th colspan='" + ((fechaFin.getTime() - fechaInicio.getTime()) /  MILLSECS_PER_DAY + 1) + "'>IMPRESIONES POR DÍA (Num.Total Páginas) - DÍA(MES - AÑO)</th>");
                        else if (agrupacion.equalsIgnoreCase("semana"))
                            out.print("<th colspan='" + semanas.size() + "'>IMPRESIONES POR SEMANA (Num.Total Páginas) - SEMANA(MES - AÑO)</th>");
                        else if (agrupacion.equalsIgnoreCase("mes"))
                            out.print("<th colspan='" + meses.size() + "'>IMPRESIONES POR MES (Num.Total Páginas)- MES (AÑO)</th>");
                    %>
                    
<!--                    <th rowspan="2">Total</th>-->
                </tr>
                <tr class="listTitle">
                    <%
                        if (agrupacion.equalsIgnoreCase("hora"))
                        {
                            for(int i=6; i<=22;i++)
                            {
                                out.print("<th>" + i + " H</th>");
                            } 
                        }
                        else if (agrupacion.equalsIgnoreCase("dia"))
                         {
                            Calendar calendarFechaInicio = Calendar.getInstance();
                            calendarFechaInicio.setTime(fechaInicio);
                            for(int i=0; i<=((fechaFin.getTime() - fechaInicio.getTime()) /  MILLSECS_PER_DAY);i++)
                            {
                                out.print("<th>" + calendarFechaInicio.get(Calendar.DATE) + " ( " + (calendarFechaInicio.get(Calendar.MONTH) + 1) + "/" + calendarFechaInicio.get(Calendar.YEAR) + " ) </th>");
                                calendarFechaInicio.add(Calendar.DATE, 1);
                            } 
                        }
                        else if (agrupacion.equalsIgnoreCase("semana"))
                            {
                                for(String semana : semanas)
                                {
                                    out.print("<th>" + semana + " </th>");
                                } 
                            }
                        else if (agrupacion.equalsIgnoreCase("mes"))
                            {
                                for(String mes : meses)
                                {
                                    out.print("<th>" + mes + " </th>");
                                } 
                            }
                    %>
                </tr>
            </thead>
            <tfoot>
                <tr class="listTitle">
                    <th>TOTAL</th>
                    <%
                        if (agrupacion.equalsIgnoreCase("hora"))
                            //out.print("<th colspan='17'></th>");
                        {
                            
                            dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
                            for(int hora =6;hora<=22;hora++)
                            {
                                int cont = 0;
                                for(Object registro : accumulatedList)
                                {
                                    cont = cont + ((Accumulated)registro).getTotalPagesByBate(dateFormat.parse((String)session.getAttribute("iniDate") + " " + hora +":00"));                      
                                }
                                
                                out.print("<th> " + cont + " </th>");    
                            } 
                            
                        }
                        else if (agrupacion.equalsIgnoreCase("dia"))
                            //out.print("<th colspan='" + (fechaFin.getTime() - fechaInicio.getTime()) /  MILLSECS_PER_DAY + "'></th>");
                        {
                            Calendar calendar = Calendar.getInstance();
                            calendar.setTime(fechaInicio);
                            for(int dia=0;dia<=((fechaFin.getTime() - fechaInicio.getTime()) /  MILLSECS_PER_DAY);dia++)
                            {
                                int cont = 0;
                                for(Object registro : accumulatedList)
                                {
                                    cont = cont + ((Accumulated)registro).getTotalPagesByBate(calendar.getTime());                      
                                }
                                
                                out.print("<th> " + cont + " </th>"); 
                                
                                calendar.add(Calendar.DATE, 1);
                            } 
                            
                        }
                        else if (agrupacion.equalsIgnoreCase("semana"))
                            //out.print("<th colspan='" + semanas.size() + "'></th>");
                        {
                            final Calendar calendar = Calendar.getInstance();
                            calendar.setTime(fechaInicio);
                            for(int i=0;i<semanas.size();i++)
                            {
                                int cont = 0;
                                for(Object registro : accumulatedList)
                                {
                                    Accumulated.ContadorDocumentos obj = IterableUtils.find(((Accumulated)registro).getListaRegistros(), new Predicate<Accumulated.ContadorDocumentos>() {
                                        @Override
                                        public boolean evaluate(Accumulated.ContadorDocumentos o) {
                                            Calendar calendarDate = Calendar.getInstance();
                                            calendarDate.setTime(o.getDate());
                                            return o.getDate() != null && calendarDate.get(Calendar.YEAR) == calendar.get(Calendar.YEAR) && calendarDate.get(Calendar.MONTH) == calendar.get(Calendar.MONTH) && CalculosFechas.GetNumeroSemana(calendarDate.getTime()) == CalculosFechas.GetNumeroSemana(calendar.getTime());
                                        }
                                    });
                                    if(obj != null)
                                        cont = cont + obj.getPages();                      
                                }
                                
                                out.print("<th> " + cont + " </th>"); 
                                
                                calendar.add(Calendar.DATE, 7);
                            } 
                        }
                        else if (agrupacion.equalsIgnoreCase("mes"))
                            //out.print("<th colspan='" + meses.size() + "'></th>");
                        {
                            final Calendar calendar = Calendar.getInstance();
                            calendar.setTime(fechaInicio);
                            for(int i=0;i<meses.size();i++)
                            {
                                int cont = 0;
                                for(Object registro : accumulatedList)
                                {
                                    Accumulated.ContadorDocumentos obj = IterableUtils.find(((Accumulated)registro).getListaRegistros(), new Predicate<Accumulated.ContadorDocumentos>() {
                                        @Override
                                        public boolean evaluate(Accumulated.ContadorDocumentos o) {
                                            Calendar calendarDate = Calendar.getInstance();
                                            calendarDate.setTime(o.getDate());
                                            return o.getDate() != null && calendarDate.get(Calendar.YEAR) == calendar.get(Calendar.YEAR) && calendarDate.get(Calendar.MONTH) == calendar.get(Calendar.MONTH);
                                        }
                                    });
                                    if(obj != null)
                                        cont = cont + obj.getPages();                      
                                }
                                
                                out.print("<th> " + cont + " </th>"); 
                                
                                calendar.add(Calendar.MONTH, 1);
                            } 
                        }
                    %>
<!--                    <th>
                        <%
//                            int cont = 0;
//                            for(Object registro : accumulatedList)
//                            {
//                                cont = cont + ((Accumulated)registro).getTotalPages();                      
//                            }
//                            out.print(cont);
                        %>
                    </th>-->
                </tr>
            </tfoot>
            
<%
    if (numRows > 0)
    {
        dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        for(int i = startIndex; i <= increment; i++)
        {
            accumulated = (Accumulated) accumulatedList.get(i-1);
            
%>
            <tr class="list">
                <td class="listTitle">
                    <%=accumulated.getFilterRow()%>
                </td>
                
                <%  
                    if (agrupacion.equalsIgnoreCase("hora"))
                    {
                        Accumulated.ContadorDocumentos tmp = new Accumulated.ContadorDocumentos();
                        dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
                        for(int hora =6;hora<=22;hora++)
                        {
                            tmp.setDate(dateFormat.parse((String)session.getAttribute("iniDate") + " " + hora +":00"));
                            int index = accumulated.getListaRegistros().indexOf(tmp);
                            if(index == -1)
                                out.print("<th>0</th>");
                            else
                                out.print("<th>" + accumulated.getListaRegistros().get(index).getPages() + "</th>");
                        } 
                    }
                    else if (agrupacion.equalsIgnoreCase("dia"))
                    {
                        Accumulated.ContadorDocumentos tmp = new Accumulated.ContadorDocumentos();

                        Calendar calendar = Calendar.getInstance();
                        calendar.setTime(fechaInicio);
                        for(int dia=0;dia<=((fechaFin.getTime() - fechaInicio.getTime()) /  MILLSECS_PER_DAY);dia++)
                        {
                            tmp.setDate(calendar.getTime());
                            int index = accumulated.getListaRegistros().indexOf(tmp);
                            if(index == -1)
                                out.print("<th>0</th>");
                            else
                                out.print("<th>" + accumulated.getListaRegistros().get(index).getPages() + "</th>");
                            calendar.add(Calendar.DATE, 1);
                        } 
                    }
                    else if (agrupacion.equalsIgnoreCase("semana"))
                    {
                        final Calendar calendar = Calendar.getInstance();
                        calendar.setTime(fechaInicio);
                        for(int cont=0;cont<semanas.size();cont++)
                        {
                            Accumulated.ContadorDocumentos obj = IterableUtils.find(accumulated.getListaRegistros(), new Predicate<Accumulated.ContadorDocumentos>() {
                                @Override
                                public boolean evaluate(Accumulated.ContadorDocumentos o) {
                                    Calendar calendarDate = Calendar.getInstance();
                                    calendarDate.setTime(o.getDate());
                                    return o.getDate() != null && calendarDate.get(Calendar.YEAR) == calendar.get(Calendar.YEAR) && calendarDate.get(Calendar.MONTH) == calendar.get(Calendar.MONTH) && CalculosFechas.GetNumeroSemana(calendarDate.getTime()) == CalculosFechas.GetNumeroSemana(calendar.getTime());
                                }
                            });
                            if(obj == null)
                                out.print("<th>0</th>");
                            else
                                out.print("<th>" + obj.getPages() + "</th>");
                            
                            calendar.add(Calendar.DATE, 7);
                        } 
                    }
                    else if (agrupacion.equalsIgnoreCase("mes"))
                        {
                        final Calendar calendar = Calendar.getInstance();
                        calendar.setTime(fechaInicio);
                        for(int cont=0;cont<meses.size();cont++)
                        {
                            Accumulated.ContadorDocumentos obj = IterableUtils.find(accumulated.getListaRegistros(), new Predicate<Accumulated.ContadorDocumentos>() {
                                @Override
                                public boolean evaluate(Accumulated.ContadorDocumentos o) {
                                    Calendar calendarDate = Calendar.getInstance();
                                    calendarDate.setTime(o.getDate());
                                    return o.getDate() != null && calendarDate.get(Calendar.YEAR) == calendar.get(Calendar.YEAR) && calendarDate.get(Calendar.MONTH) == calendar.get(Calendar.MONTH);
                                }
                            });
                            if(obj == null)
                                out.print("<th>0</th>");
                            else
                                out.print("<th>" + obj.getPages() + "</th>");
                            
                            calendar.add(Calendar.MONTH, 1);
                        } 
                    }
                %>
                
                
<!--                <td>
                    <%--<%=accumulated.getTotalPages()%>--%>
                </td>-->
            </tr>
<%
        }
    }
%>
        </table>
        </div>
    </body>
</html>