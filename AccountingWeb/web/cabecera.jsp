<%--
    Document   : cabecera
    Created on : 15-jul-2010, 12:31:09
    Author     : Alexis.Hidalgo
--%>
<table cellpadding="0" class="logoArea">
    <tbody>
        <tr>
            <td nowrap="" class="logo">
                <table height="42" cellpadding="0" >
                    <tbody>
                        <tr>
                            <td class="headtitle"><img src="./images/ricohaccountingpackage.GIF"></td>
                        </tr>
                    </tbody>
                </table>
            </td>
            <td valign="top"><img height="40" width="130" src="./images/headerSlope.gif"></td>
            <td align="right" width="100%" class="headerButtons">
            <table cellpadding="0">
                <tbody>
                    <tr align="right">
                        <td>
                            <form target="_top" onsubmit="return false;" action="/cap-es/header.do" method="post" id="headerForm">
                                <a href="./logout.do">
                                    <img height="20" align="absmiddle" width="27" src="./images/headerBtnLogOff-r.gif" name="logoutBtn" id="logoutBtn">Desconectar
                                </a>
                            </form>
                        </td>
                    </tr>
                    <tr>
                        <td height="10px"></td>
                    </tr>
                    <tr align="right">
                        <td class="user">
                            Bienvenido <%=session.getAttribute("userName")%> 
                        </td>
                    </tr>
                </tbody>
            </table>
            </td>
        </tr>
    </tbody>
</table>