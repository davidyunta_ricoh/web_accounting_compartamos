<%-- 
    Document   : zipmenu
    Created on : 28-jul-2010, 20:17:57
    Author     : Alexis.Hidalgo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Ricoh Accounting Package</title>
        <link rel="stylesheet" type="text/css" href="./css/style.css">
    </head>
    <body class="main">
    <%@ include file="cabecera.jsp" %>
    <%@ include file="principal.jsp" %>

    <div>
        <div style="margin-left: 225px;">
            <h3>Menú de Descarga</h3>
        </div>
    </div>
    <div class="settingFlatContsDivision">
        <img src="./images/settingFlatContsDivision.gif">
    </div>

    <div style="margin-left: 225px; margin-top:50px;">
    <table>
        <tr style="display:block; margin-bottom:50px;">
            <td>
                <img src="./images/csv256.png" height="30" width="30" style="margin-bottom:5px;"/>
            </td>
            <td>
                <a href="./downloadFile.do?file=zipusers" style="margin-left:10px;">Descargar Fichero ZIP de Usuarios</a>
            </td>
        </tr>
        <tr style="display:block; margin-bottom:50px;">
            <td>
                <img src="./images/csv256.png" height="30" width="30" style="margin-bottom:5px;"/>
            </td>
            <td>
                <a href="./downloadFile.do?file=zipmfps" style="margin-left:10px;">Descargar Fichero ZIP de Impresoras</a>
            </td>
        </tr>
        <tr style="display:block; margin-bottom:50px;">
            <td>
                <img src="./images/csv256.png" height="30" width="30" style="margin-bottom:5px;"/>
            </td>
            <td>
                <a href="./downloadFile.do?file=zipcecos" style="margin-left:10px;">Descargar Fichero ZIP de Centros de Coste</a>
            </td>
        </tr>
    </table>
    </div>

    </body>
</html>
