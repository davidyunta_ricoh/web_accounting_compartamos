<%--
    Document   : menu
    Created on : 27-jul-2010, 12:31:09
    Author     : Alexis.Hidalgo
--%>
<%@ page import="java.util.*" %>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Ricoh Accounting Package</title>
        <link rel="stylesheet" type="text/css" href="./css/style.css">
        <script type="text/javascript">
            function showForm()
            {
                  document.getElementById("loginForm").style.display='block';
                  document.getElementById("addDiv").style.display='none';
            }
        </script>
    </head>
    <body class="main">

    <%@ include file="cabecera.jsp" %>
    <%@ include file="principal.jsp" %>

        <div>
            <div style="margin-left: 225px;">
                <h3>Administración de Usuarios</h3>
            </div>
        </div>
        <div class="settingFlatContsDivision">
            <img src="./images/settingFlatContsDivision.gif">
        </div>

        <div id="addDiv" style="margin-left:242px; padding-bottom:5px; padding-top:5px;">
            <img src="./images/cmdAddUser-r.gif">
            <a href="javascript:showForm()">
                Añadir Usuario
            </a>
        </div>

        <form id="loginForm" method="post" action="insertUser.do?action=insert" style="display: none; padding-bottom:30px;">
           <div style="margin-left: 225px; clear: left;">
           <table>
                <tr>
                   <td>Nombre de Usuario: </td>
                   <td><input name="user" maxlength="32" size="35" type="text"></td>
                </tr>
                <tr>
                   <td>Contraseña: </td>
                   <td><input name="pass" maxlength="32" size="35" type="password"></td>
                </tr>
                <tr>
                   <td>Nombre de Usuario Completo: </td>
                   <td><input name="fullusername" maxlength="32" size="35" type="text"></td>
                </tr>
                <tr>
                   <td>Usuario Administrador: </td>
                   <td><input name="isAdmin" type="checkbox"></td>
                </tr>
                <tr>
                   <td>Empresa: </td>
                   <td><SELECT NAME="CompanyCombo" SIZE=1> 
            <%
             if (session.getAttribute("companiesList") != null){
                List companyList = (List)session.getAttribute("companiesList");

                ListIterator companiesIterator = companyList.listIterator();
                while (companiesIterator.hasNext())
                {
                    String company = (String)companiesIterator.next();
            %>

                    <OPTION VALUE="<%=company%>"><%=company%></OPTION>
              
            <%
                }
            %>
                    </SELECT>  
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td><input name="insert" value="Añadir" type="submit"></td>
                </tr>
            </table>
           </div>
        </form>

        <div style="margin-left: 225px; clear: left;">
        <table class="list">
            <tr class="listTitle">
                <td>Nombre de Usuario</td>
                <td>Nombre de Usuario Completo</td>
                <td>Empresa</td>
                <td>Eliminar</td>
            </tr>

<%
            List userList = (List)session.getAttribute("usersList");

            ListIterator usersIterator = userList.listIterator();
            while (usersIterator.hasNext())
            {
                String userName = (String)usersIterator.next();
                String fulluserName = (String)usersIterator.next();
                String companyName = (String)usersIterator.next();
%>
              <tr class="list">
                  <td>
                      <%=userName%>
                  </td>
                  <td>
                      <%=fulluserName%>
                  </td>
                  <td>
                      <%=companyName%>
                  </td>
                  <td>
                      <a href="deleteUser.do?action=delete&user=<%=userName%>">
                          <img src="./images/cmdDelete-r.gif" style="padding-bottom:1px; padding-top:5px;">
                      </a>
                  </td>
              </tr>
<%
            }
       }
%>
        </table>
        </div>
    </body>
</html>
