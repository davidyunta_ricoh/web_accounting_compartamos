<%--
    Document   : configuration
    Created on : 28-jul-2010, 9:31:09
    Author     : Alexis.Hidalgo
--%>
<%@ page import="java.util.*" %>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Ricoh Accounting Package</title>
        <link rel="stylesheet" type="text/css" href="./css/style.css">
        <script type="text/javascript">
            function showEditable()
            {
                  document.forms["configurationForm"].editConf.disabled = true;
                  document.forms["configurationForm"].saveConf.disabled = false;

                  document.forms["configurationForm"].databasehost.readOnly = false;
                  document.forms["configurationForm"].databaseport.readOnly = false;
                  document.forms["configurationForm"].databaseuser.readOnly = false;
                  document.forms["configurationForm"].databasepass.readOnly = false;
                  document.forms["configurationForm"].databasename.readOnly = false;

                  document.forms["configurationForm"].extdatabasehost.readOnly = false;
                  document.forms["configurationForm"].extdatabaseinstance.readOnly = false;
                  document.forms["configurationForm"].extdatabaseport.readOnly = false;
                  document.forms["configurationForm"].extdatabaseuser.readOnly = false;
                  document.forms["configurationForm"].extdatabasepass.readOnly = false;
                  document.forms["configurationForm"].extdatabasename.readOnly = false;

                  document.forms["configurationForm"].txtpath.readOnly = false;
                  document.forms["configurationForm"].zippath.readOnly = false;
                  
                  document.forms["configurationForm"].smtpserver.readOnly = false;
                  document.forms["configurationForm"].smtpport.readOnly = false;
                  document.forms["configurationForm"].fromaddress.readOnly = false;
                  document.forms["configurationForm"].toaddress.readOnly = false;
                  document.forms["configurationForm"].emailsubject.readOnly = false;
                  document.forms["configurationForm"].parseremailbody.readOnly = false;
                  document.forms["configurationForm"].totalsemailbody.readOnly = false;
            }
        </script>

    </head>
    <body class="main">

    <%@ include file="cabecera.jsp" %>
    <%@ include file="principal.jsp" %>

        <div>
            <div style="margin-left: 225px;">
                <h3>Administración de la Aplicación</h3>
            </div>
        </div>
        <div class="settingFlatContsDivision">
            <img src="./images/settingFlatContsDivision.gif">
        </div>
         <div style="margin-left: 225px; clear: left;">
            <% 
                String configfile = (String)session.getAttribute("configfile");
                if (configfile != null && configfile.equalsIgnoreCase("parser")){
            %>
            <FORM action="appAdmin.do?action=change" method="post">
                <SELECT NAME="configComboList" SIZE=1 onchange="this.form.submit()"> 
                    <OPTION VALUE="web" >Web</OPTION>
                    <OPTION VALUE="parser" selected="selected">Parser</OPTION>
                    <!--<OPTION VALUE="extractor">Extractor</OPTION>-->
                </SELECT>
            </FORM>
            <% 
               }else if (configfile != null && configfile.equalsIgnoreCase("extractor")){              
            %>
            <FORM action="appAdmin.do?action=change" method="post">
                <SELECT NAME="configComboList" SIZE=1 onchange="this.form.submit()"> 
                    <OPTION VALUE="web" >Web</OPTION>
                    <OPTION VALUE="parser">Parser</OPTION>
                    <!--<OPTION VALUE="extractor" selected="selected">Extractor</OPTION>-->
                </SELECT>
            </FORM>
            <% 
               }else {              
            %>
            <FORM action="appAdmin.do?action=change" method="post">
                <SELECT NAME="configComboList" SIZE=1 onchange="this.form.submit()"> 
                    <OPTION VALUE="web" selected="selected">Web</OPTION>
                    <OPTION VALUE="parser">Parser</OPTION>
                    <!--<OPTION VALUE="extractor">Extractor</OPTION>-->
                </SELECT>
            </FORM>
            <% 
               }            
            %>
        </div>
        <br>
        <br>
        <div style="margin-left: 225px; clear: left;">
        <form id="configurationForm" method="post" action="appAdmin.do?action=modify">            
            <table>
                <tr style="display:block; margin-bottom:20px;">
                    <td style="width:375px;">
                         <b>Dirección IP del Servidor de Base de Datos: </b>
                    </td>
                    <td>
                        <input name="databasehost" readonly maxlength="128" size="35" type="text" value="<%=(String)session.getAttribute("dbHost")%>" style="margin-left:25px;">
                    </td>
                </tr>
                <tr style="display:block; margin-bottom:20px;">
                    <td style="width:375px;">
                        <b>Puerto del Servidor de Base de Datos: </b>
                    </td>
                    <td>
                        <input name="databaseport" readonly maxlength="128" size="35" type="text" value="<%=(String)session.getAttribute("dbPort")%>" style="margin-left:25px;">
                    </td>
                </tr>
                <tr style="display:block; margin-bottom:20px;">
                    <td style="width:375px;">
                         <b>Usuario del Servidor de Base de Datos: </b>
                    </td>
                    <td>
                         <input name="databaseuser" readonly maxlength="128" size="35" type="text" value="<%=(String)session.getAttribute("dbUser")%>" style="margin-left:25px;">
                    </td>
                </tr>
                <tr style="display:block; margin-bottom:20px;">
                    <td style="width:375px;">
                        <b>Contraseña del Servidor de Base de Datos: </b>
                    </td>
                    <td>
                        <input name="databasepass" readonly maxlength="128" size="37" type="password" value="<%=(String)session.getAttribute("dbPass")%>" style="margin-left:25px;">
                    </td>
                </tr>
                <tr style="display:block; margin-bottom:20px;">
                    <td style="width:375px;">
                         <b>Nombre de la Base de Datos: </b>
                    </td>
                    <td>
                         <input name="databasename" readonly maxlength="128" size="35" type="text" value="<%=(String)session.getAttribute("dbName")%>" style="margin-left:25px;">
                    </td>
                </tr>
            </table>
            <br>
            <br>
<!--            <table>
                <tr style="display:block; margin-bottom:20px;">
                    <td style="width:375px;">
                         <b>Directorio de Extracción de Ficheros TXT: </b>
                    </td>
                    <td>
                         <input name="txtpath" readonly maxlength="128" size="35" type="text" value="<%=(String)session.getAttribute("txtOutput")%>" style="margin-left:25px;">
                    </td>
                </tr>
                <tr style="display:block; margin-bottom:20px;">
                    <td style="width:375px;">
                         <b>Directorio de Extracción de Ficheros ZIP: </b>
                    </td>
                    <td>
                         <input name="zippath" readonly maxlength="128" size="35" type="text" value="<%=(String)session.getAttribute("zipOutput")%>" style="margin-left:25px;">
                    </td>
                </tr>
            </table>  -->
             <%
                 configfile = (String)session.getAttribute("configfile");
                if (configfile != null && configfile.equalsIgnoreCase("parser")){
             
             %> 
             <table>
                <tr style="display:block; margin-bottom:20px;">
                    <td style="width:375px;">
                         <b>Dirección IP del Servidor Streamline de Base de Datos: </b>
                    </td>
                    <td>
                        <input name="extdatabasehost" readonly maxlength="128" size="35" type="text" value="<%=(String)session.getAttribute("extDbHost")%>" style="margin-left:25px;">
                    </td>
                </tr>
                <tr style="display:block; margin-bottom:20px;">
                    <td style="width:375px;">
                         <b>Instancia del Servidor Streamline de Base de Datos: </b>
                    </td>
                    <td>
                        <input name="extdatabaseinstance" readonly maxlength="128" size="35" type="text" value="<%=(String)session.getAttribute("extDbInstance")%>" style="margin-left:25px;">
                    </td>
                </tr>

                <tr style="display:block; margin-bottom:20px;">
                    <td style="width:375px;">
                        <b>Puerto del Servidor Streamline de Base de Datos: </b>
                    </td>
                    <td>
                        <input name="extdatabaseport" readonly maxlength="128" size="35" type="text" value="<%=(String)session.getAttribute("extDbPort")%>" style="margin-left:25px;">
                    </td>
                </tr>
                <tr style="display:block; margin-bottom:20px;">
                    <td style="width:375px;">
                         <b>Usuario del Servidor Streamline de Base de Datos: </b>
                    </td>
                    <td>
                         <input name="extdatabaseuser" readonly maxlength="128" size="35" type="text" value="<%=(String)session.getAttribute("extDbUser")%>" style="margin-left:25px;">
                    </td>
                </tr>
                <tr style="display:block; margin-bottom:20px;">
                    <td style="width:375px;">
                        <b>Contraseña del Servidor Streamline de Base de Datos: </b>
                    </td>
                    <td>
                        <input name="extdatabasepass" readonly maxlength="128" size="37" type="password" value="<%=(String)session.getAttribute("extDbPass")%>" style="margin-left:25px;">
                    </td>
                </tr>
                <tr style="display:block; margin-bottom:20px;">
                    <td style="width:375px;">
                         <b>Nombre de la Base de Datos Streamline: </b>
                    </td>
                    <td>
                         <input name="extdatabasename" readonly maxlength="128" size="35" type="text" value="<%=(String)session.getAttribute("extDbName")%>" style="margin-left:25px;">
                    </td>
                </tr>
           </table>
            <br>
            <br>
            <table>
                <tr style="display:block; margin-bottom:20px;">
                    <td style="width:375px;">
                         <b>Servidor SMTP: </b>
                    </td>
                    <td>
                         <input name="smtpserver" readonly maxlength="128" size="35" type="text" value="<%=(String)session.getAttribute("smtpserver")%>" style="margin-left:25px;">
                    </td>
                </tr>
                <tr style="display:block; margin-bottom:20px;">
                    <td style="width:375px;">
                         <b>Puerto del Servidor SMTP: </b>
                    </td>
                    <td>
                         <input name="smtpport" readonly maxlength="128" size="35" type="text" value="<%=(String)session.getAttribute("smtpport")%>" style="margin-left:25px;">
                    </td>
                </tr>
                <tr style="display:block; margin-bottom:20px;">
                    <td style="width:375px;">
                         <b>Dirección del Remitente: </b>
                    </td>
                    <td>
                         <input name="fromaddress" readonly maxlength="128" size="35" type="text" value="<%=(String)session.getAttribute("fromaddress")%>" style="margin-left:25px;">
                    </td>
                </tr>
                <tr style="display:block; margin-bottom:20px;">
                    <td style="width:375px;">
                         <b>Dirección del Destinatario: </b>
                    </td>
                    <td>
                         <input name="toaddress" readonly maxlength="128" size="35" type="text" value="<%=(String)session.getAttribute("toaddress")%>" style="margin-left:25px;">
                    </td>
                </tr>
                <tr style="display:block; margin-bottom:20px;">
                    <td style="width:375px;">
                         <b>Asunto del Correo Electrónico: </b>
                    </td>
                    <td>
                         <input name="emailsubject" readonly maxlength="128" size="35" type="text" value="<%=(String)session.getAttribute("emailsubject")%>" style="margin-left:25px;">
                    </td>
                </tr>
                <tr style="display:block; margin-bottom:20px;">
                    <td style="width:375px;">
                         <b>Cuerpo del Correo Electrónico Parser: </b>
                    </td>
                    <td>
                         <input name="parseremailbody" readonly maxlength="128" size="35" type="text" value="<%=(String)session.getAttribute("parseremailbody")%>" style="margin-left:25px;">
                    </td>
                </tr>
                <tr style="display:block; margin-bottom:20px;">
                    <td style="width:375px;">
                         <b>Cuerpo del Correo Electrónico Totales: </b>
                    </td>
                    <td>
                         <input name="totalsemailbody" readonly maxlength="128" size="35" type="text" value="<%=(String)session.getAttribute("totalsemailbody")%>" style="margin-left:25px;">
                    </td>
                </tr>
            </table>
                    
             <%
                }       
             %>  
            <br>
            <table>
                <tr style="display:block; margin-bottom:20px;">
                   <td>
                       <input name="editConf" value="Editar Configuración" type="button" onclick="showEditable();" style="margin-left:70px; margin-top:15px">
                   </td>
                  <td>
                      <input name="saveConf" value="Guardar Configuración" type="submit" disabled style="margin-left:100px; margin-top:15px">
                   </td>

                </tr>
            </table>
	</form>
        </div>
    </body>
</html>

