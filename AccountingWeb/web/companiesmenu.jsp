<%--
    Document   : menu
    Created on : 27-jul-2010, 12:31:09
    Author     : Alexis.Hidalgo
--%>
<%@page import="ricoh.accounting.objects.Resume"%>
<%@ page import="java.util.*" %>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Ricoh Accounting Package</title>
        <link rel="stylesheet" type="text/css" href="./css/style.css">
        <script type="text/javascript">
            function showForm()
            {
                  document.getElementById("loginForm").style.display='block';
                  document.getElementById("addDiv").style.display='none';
            }
        </script>
    </head>
    <body class="main">

    <%@ include file="cabecera.jsp" %>
    <%@ include file="principal.jsp" %>

        <div>
            <div style="margin-left: 225px;">
                <h3>Administración de Empresas</h3>
            </div>
                      
            <%
                String code = (String) session.getAttribute("errorCode");

                if (code != null)
                {
                    if (code.equals("empty_date"))
                        out.print("<script language=\"JavaScript\" type=\"text/javascript\">alert(\"Introduzca las fechas de inicio y final.\");</script>");
                    else if (code.equals("parse_fail"))
                        out.print("<script language=\"JavaScript\" type=\"text/javascript\">alert(\"Se ha producido un error al leer la fecha. Por favor, introdúzcala de nuevo.\");</script>");
                    else if (code.equals("start_after_end"))
                        out.print("<script language=\"JavaScript\" type=\"text/javascript\">alert(\"La fecha de inicio ha de ser anterior a la de fin.\");</script>");
                    else if (code.equals("db_error"))
                        out.print("<script language=\"JavaScript\" type=\"text/javascript\">alert(\"Se ha producido un error contactando la Base de Datos.\");</script>");
                    else if (code.equals("process_error"))
                        out.print("<script language=\"JavaScript\" type=\"text/javascript\">alert(\"Se ha producido un error generando los datos.\");</script>");
                    else if (code.equals("process_error_mfpsmenu")){
                        out.print("<script language=\"JavaScript\" type=\"text/javascript\">alert(\"Se ha producido un error generando los datos de equipos\");</script>");
                         //out.print("<script language=\"JavaScript\" type=\"text/javascript\">javascript:mpfs_Error();</script>");
                   }                       
                    else if (code.equals("end_after_today"))
                        out.print("<script language=\"JavaScript\" type=\"text/javascript\">alert(\"La fecha de fin no puede ser posterior a la fecha del día en curso.\");</script>");

                    session.removeAttribute("errorCode");
               }
               %>
        </div>
        <div class="settingFlatContsDivision">
            <img src="./images/settingFlatContsDivision.gif">
        </div>

        <div id="addDiv" style="margin-left:242px; padding-bottom:5px; padding-top:5px;">
            <img src="./images/cmdAddUser-r.gif">
            <a href="javascript:showForm()">
                Añadir Empresa
            </a>
        </div>

        <form id="loginForm" method="post" action="insertCompany.do?action=insert" style="display: none; padding-bottom:30px;">
           <div style="margin-left: 225px; clear: left;">
           <table>
                <tr>
                   <td>Nombre de empresa: </td>
                   <td><input name="companyname" maxlength="32" size="35" type="text"></td>
                </tr>
                <tr>
                    <td></td>
                    <td><input name="insert" value="Añadir" type="submit"></td>
                </tr>
            </table>
           </div>
        </form>

        <%
        Resume resume;
        int numRows, startIndex = 1, numRecordsPerPage = 20, remain, increment, numPages;

        if(request.getParameter("startIndex") != null)
            startIndex = Integer.parseInt(request.getParameter("startIndex"));

        if(request.getParameter("numRecords") != null)
            numRecordsPerPage = Integer.parseInt(request.getParameter("numRecords"));

        List companiesList= (List)session.getAttribute("companiesList");
            numRows = companiesList.size();
            
            numPages = numRows /numRecordsPerPage;

            if((remain = numRows % numRecordsPerPage) != 0)
                numPages++;

            if(((startIndex + numRecordsPerPage) <= numRows) || remain == 0)
                increment = startIndex + numRecordsPerPage - 1;

            else
                increment = startIndex + remain - 1;
 %>       
            <div style="display: block; clear: left; margin-left:250px; margin-top:10px; width: 100%">
                <span style="padding-right:20px;">Núm. total de Registros: <%=numRows%></span>
                <span style="padding-right:20px;">Núm. de Registros por página:
                    <select name="numRecords" onchange="location = 'companiesmenu.jsp?numRecords=' + this.options[this.selectedIndex].value;">
                        <%
                        if (numRecordsPerPage == 20)
                            out.print("<option selected value=\"20\">20</option>");
                        else
                            out.print("<option value=\"20\">20</option>");

                        if (numRecordsPerPage == 50)
                            out.print("<option selected value=\"50\">50</option>");
                        else
                            out.print("<option value=\"50\">50</option>");

                        if (numRecordsPerPage == 100)
                            out.print("<option selected value=\"100\">100</option>");
                        else
                            out.print("<option value=\"100\">100</option>");

                        if (numRecordsPerPage == 200)
                            out.print("<option selected value=\"200\">200</option>");
                        else
                            out.print("<option value=\"200\">200</option>");

                        if (numRecordsPerPage == 500)
                            out.print("<option selected value=\"500\">500</option>");
                        else
                            out.print("<option value=\"500\">500</option>");

                        if (numRecordsPerPage == numRows)
                            out.print("<option selected value=" + numRows + ">TODOS</option>");
                        else
                            out.print("<option value=" + numRows + ">TODOS</option>");
%>
                    </select>
                </span>
                <span style="padding-right:20px;">Núm. de páginas: <%=numPages%></span>
            <div style="display:inline;">
<%
            if(increment < numRows)
            {
%>
                <span style="padding-right:10px;">Registros <%=startIndex%> a <%=increment%></span>
<%
            }
            else if (numRows == 0)
            {
%>
                <span style="padding-right:10px;">Registros <%=numRows%> a <%=numRows%></span>
<%
            }

            else
            {
%>
                <span style="padding-right:10px;">Registros <%=startIndex%> a <%=numRows%></span>
<%
            }
    //out.println("</table>");
    //out.println("<table><tr>Resultados");

        if(startIndex == 1)
        {
%>
           <img src="./images/btnArrowLeft2-d.gif"/>
           <img src="./images/btnArrowLeft1-d.gif"/>
<%
        }
        else
        {
%>
            <a href="companiesmenu.jsp?startIndex=1&numRecords=<%=numRecordsPerPage%>">
                <img src="./images/btnArrowLeft2-r.gif"/>
            </a>
            <a href="companiesmenu.jsp?startIndex=<%=startIndex - numRecordsPerPage%>&numRecords=<%=numRecordsPerPage%>">
                <img src="./images/btnArrowLeft1-r.gif"/>
            </a>
<%
        }

        if((startIndex > numRows -numRecordsPerPage))
        {
%>
            <img src="./images/btnArrowRight1-d.gif"/>
            <img src="./images/btnArrowRight2-d.gif"/>
<%
        }
        else
        {
%>
            <a href="companiesmenu.jsp?startIndex=<%=startIndex + numRecordsPerPage%>&numRecords=<%=numRecordsPerPage%>">
                <img src="./images/btnArrowRight1-r.gif"/>
            </a>
            <a href="companiesmenu.jsp?startIndex=<%=numRecordsPerPage * (numPages - 1) + 1%>&numRecords=<%=numRecordsPerPage%>">
                <img src="./images/btnArrowRight2-r.gif"/>
            </a>
<%
        }
%>                
            </div>
        </div>
                    
        <div style="margin-left: 250px; clear: left;">
        <table class="list">
            <tr class="listTitle">
                <td width="400">Nombre de empresa</td>
                <td>Eliminar</td>
            </tr>

<%
    if (numRows > 0)
    {
        for(int i = startIndex; i <= increment; i++)
        {
            
            String companyname = (String)companiesList.get(i-1);

%>            

              <tr class="list">
                  <td>
                      <%=companyname%>
                  </td>
                  <td>
                      <a href="deleteCompany.do?action=delete&companyname=<%=companyname%>">
                          <img src="./images/cmdDelete-r.gif" style="padding-bottom:1px; padding-top:5px;">
                      </a>
                  </td>
              </tr>
<%
            }
       }
  
%>
        </table>
        </div>
    </body>
</html>
