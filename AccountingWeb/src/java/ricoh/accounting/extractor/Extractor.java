/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ricoh.accounting.extractor;

import java.io.File;
import java.io.FileNotFoundException;
import ricoh.accounting.interfaces.IExtractorManager;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Properties;
import java.util.logging.Level;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.collections4.Predicate;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import ricoh.accounting.database.DataBaseManager;
import ricoh.accounting.ifactory.InterfaceManager;
import ricoh.accounting.interfaces.IFileManager;
import ricoh.accounting.objects.Accumulated;
import ricoh.accounting.objects.Action;
import ricoh.accounting.objects.ObjectManager;
import ricoh.accounting.objects.Resume;
import ricoh.accounting.objects.Resume.resumeType;
import ricoh.accounting.objects.User;
import ricoh.accounting.remotecc.RemoteccPlugin;
import ricoh.accounting.utils.CalculosFechas;

/**
 *
 * @author alexis.hidalgo
 */
public class Extractor implements IExtractorManager
{
    //private IDataBaseManager dbManager;
    private IFileManager fileManager;

    private List dbMFPsList;

    private Date START_DATE;
    private Date END_DATE;

    private String REPORTS_OUTPUT_PATH = "";
    private String ZIP_OUTPUT_PATH = "";
    private String usersZipName = "";
    private String printersZipName = "";
    private String cecosZipName = "";

    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private SimpleDateFormat zipDateFormat = new SimpleDateFormat("yyyyMM");

    private final int DETAILS_FILE_PART1 = 1;
    private final int DETAILS_FILE_PART2 = 2;
    private final int DETAILS_FILE_PART3 = 3;
    private final int DETAILS_FILE_PART4 = 4;

    public static Logger logger = Logger.getLogger(Extractor.class.getName());

    private String CONFIG_FILE = "conf/config.properties";
    private String SQL_FILE = "conf/sql.properties";
    private String CSV_CCUSER_FOLDER = "";
    private String CC_OBTAINING_ACTIVATION_MODE = "";
    RemoteccPlugin remoteccplugin;
    
    private String AD_AUTHSIMPLE = "";
    private String AD_HOST = "";
    private String AD_PORT = "";
    private String AD_USER = "";
    private String AD_PASS = "";
    private String AD_BASE = "";
    private String AD_FILTER = "";
    private String AD_USER_USERNAME = "";
    private String AD_USER_FULLNAME = "";
    private String AD_USER_CC = "";
    private String AD_USER_CCNAME = "";

    private ObjectManager objectManager;

    public Extractor(String propFile)
    {
        try
        {
            this.CONFIG_FILE = propFile;

            //Leer fichero properties
            Properties prop = new Properties();
            prop.load(new FileInputStream(this.getCONFIG_FILE()));

            START_DATE = dateFormat.parse(prop.getProperty("Date.Start"));
            END_DATE = dateFormat.parse(prop.getProperty("Date.End"));

            REPORTS_OUTPUT_PATH = prop.getProperty("Output.Path");
            ZIP_OUTPUT_PATH = prop.getProperty("ZIP.Path");
            
            AD_AUTHSIMPLE = prop.getProperty("AD.AuthSimple");
            AD_HOST = prop.getProperty("AD.Host");
            AD_PORT = prop.getProperty("AD.Port");
            AD_USER = prop.getProperty("AD.User");
            AD_PASS = prop.getProperty("AD.Pass");
            AD_BASE = prop.getProperty("AD.Base");
            AD_FILTER = prop.getProperty("AD.Filter");
            AD_USER_USERNAME = prop.getProperty("AD.UserUsername");
            AD_USER_FULLNAME = prop.getProperty("AD.UserFullName");
            AD_USER_CC = prop.getProperty("AD.UserCC");
            AD_USER_CCNAME = prop.getProperty("AD.UserCCName");

            usersZipName = IExtractorManager.USERS_ZIP_FILENAME + "_" + zipDateFormat.format(END_DATE);
            printersZipName = IExtractorManager.PRINTERS_ZIP_FILENAME + "_" + zipDateFormat.format(END_DATE);
            cecosZipName = IExtractorManager.CECOS_ZIP_FILENAME + "_" + zipDateFormat.format(END_DATE);

            fileManager = InterfaceManager.getFileManagerInstance();
            objectManager = new ObjectManager();
            //dbManager = InterfaceManager.getDataBaseManagerInstance("conf/config.properties");
            dbMFPsList = new ArrayList();
        }
        catch (ParseException ex)
        {
            logger.error(ex.getMessage());
            logger.error(ex.getStackTrace());
        }
        catch (IOException ex)
        {
            logger.error(ex.getMessage());
            logger.error(ex.getStackTrace());
        }
    }

    public boolean generateAllReports()
    {
        boolean allOK = true;
        final boolean[] THREAD_STATUS = new boolean[8];
        
//        int t = 0;
//        while(t < 8){
//            THREAD_STATUS[t] = true; 
//            t++;
//        }

        try
        {
//            if (!dbManager.isConnected())
//            {
//                //Open connection with the database
//                if (!dbManager.connect())
//                {
//                    logger.error("generateAllReports() : Cannot open DataBase connection");
//                    System.out.println("ERROR: Cannot open DataBase connection");
//                    return false;
//                }
//            }

            //Launch a thread to generate the resume users file
            Thread ResumeUsers_Thread = new Thread(new Runnable()
            {
                public void run() {   
                    if (!(THREAD_STATUS[0] = generateResumeUsers("")))
                    {
                        logger.error("generateAllReports() : Error creating Users Resume file. The File is not created");
                        System.out.println("ERROR: Creating Users Resume file. The File is not created");
                    }
                }
            });
            ResumeUsers_Thread.start();

            //Launch a thread to generate the resume users - mfp file
            Thread ResumeUsersPrinters_Thread = new Thread(new Runnable()
            {
                public void run() {
                    if (!(THREAD_STATUS[1] = generateResumeUserPrinters("")))
                    {
                        logger.error("generateAllReports() : Error creating Users MFPs Resume file. The File is not created");
                        System.out.println("ERROR: Creating Users MFPs Resume file. The File is not created");
                    }
                }
            });
            ResumeUsersPrinters_Thread.start();

            //Launch a thread to generate the resume printers file
            Thread ResumePrinters_Thread = new Thread(new Runnable()
            {
                public void run() {
                    if (!(THREAD_STATUS[2] = generateResumePrinters("",""))) //ull!!!!
                    {
                        logger.error("generateAllReports() : Error creating MFPs Resume file. The File is not created");
                        System.out.println("ERROR: Creating MFPs Resume file. The File is not created");
                    }
                }
            });
            ResumePrinters_Thread.start();

            //Launch a thread to generate the details file part 1
            Thread DetailsUsers1_Thread = new Thread(new Runnable()
            {
                public void run() {
                    if (!(THREAD_STATUS[3] = generateDetailsByUser(DETAILS_FILE_PART1, "", "")))//ull!!!!
                    {
                        logger.error("generateAllReports() : Error creating Users Details file. The File is not created");
                        System.out.println("ERROR: Creating Users Details file. The File is not created");
                    }
                }
            });
            
            
            //Launch a thread to generate the details file part 2
            Thread DetailsUsers2_Thread = new Thread(new Runnable()
            {
                public void run() {
                    if (!(THREAD_STATUS[4] = generateDetailsByUser(DETAILS_FILE_PART2, "", "")))//ull!!!!
                    {
                        logger.error("generateAllReports() : Error creating Users Details file. The File is not created");
                        System.out.println("ERROR: Creating Users Details file. The File is not created");
                    }
                }
            });

            DetailsUsers1_Thread.start();
            DetailsUsers2_Thread.start();

            DetailsUsers2_Thread.join();
            DetailsUsers1_Thread.join();
            
            //Launch a thread to generate the details file part 3
            Thread DetailsUsers3_Thread = new Thread(new Runnable()
            {
                public void run() {
                    if (!(THREAD_STATUS[5] = generateDetailsByUser(DETAILS_FILE_PART3, "", "")))//ull!!!!
                    {
                        logger.error("generateAllReports() : Error creating Users Details file. The File is not created");
                        System.out.println("ERROR: Creating Users Details file. The File is not created");
                    }
                }
            });
            

            //Launch a thread to generate the details file part 4
            Thread DetailsUsers4_Thread = new Thread(new Runnable()
            {
                public void run() {
                    if (!(THREAD_STATUS[6] = generateDetailsByUser(DETAILS_FILE_PART4, "", "")))//ull!!!!
                    {
                        logger.error("generateAllReports() : Error creating Users Details file. The File is not created");
                        System.out.println("ERROR: Creating Users Details file. The File is not created");
                    }
                }
            });

            //Launch a thread to generate the resume cc file
            Thread ResumeCostCenters_Thread = new Thread(new Runnable()
            {
                public void run() {
                    if (!(THREAD_STATUS[7] = generateResumeCostCenters("")))
                    {
                        logger.error("generateAllReports() : Error creating CCs Resume file. The File is not created");
                        System.out.println("ERROR: Creating Users CCs Resume file. The File is not created");
                    }
                }
            });
            ResumeCostCenters_Thread.start();


            DetailsUsers3_Thread.start();
            DetailsUsers4_Thread.start();

            DetailsUsers4_Thread.join();
            DetailsUsers3_Thread.join();


            //Wait all threads to finish
            ResumePrinters_Thread.join();
            ResumeUsers_Thread.join();
            ResumeUsersPrinters_Thread.join();
            ResumeCostCenters_Thread.join();
            

            for (int i = 0; i < THREAD_STATUS.length; i++)
                allOK = allOK & THREAD_STATUS[i];

//            if (allOK)
//            {//Launch a thread to generate the details file
//                if (!(allOK = generateDetailsUsers()))
//                {
//                    logger.error("generateAllReports() : Error creating Users Details file. The File is not created");
//                    System.out.println("ERROR: Creating Users Details file. The File is not created");
//                }
//            }

            if (allOK)
            {
                List detailFiles = new ArrayList();
                detailFiles.add(IExtractorManager.DETAIL_USERS_FILENAME + "_" + DETAILS_FILE_PART1);
                detailFiles.add(IExtractorManager.DETAIL_USERS_FILENAME + "_" + DETAILS_FILE_PART2);
                detailFiles.add(IExtractorManager.DETAIL_USERS_FILENAME + "_" + DETAILS_FILE_PART3);
                detailFiles.add(IExtractorManager.DETAIL_USERS_FILENAME + "_" + DETAILS_FILE_PART4);

                if (!(allOK = fileManager.concatFiles(detailFiles, REPORTS_OUTPUT_PATH, IExtractorManager.DETAIL_USERS_FILENAME)))
                {
                    logger.error("generateAllReports() : Error merging parts to Users Details file. The File is not created");
                    System.out.println("ERROR: Merging parts to Users Details file. The File is not created");
                }

            }

            if (allOK)
            {
                List userFiles = new ArrayList();
                userFiles.add(IExtractorManager.RESUME_USERS_FILENAME);
                userFiles.add(IExtractorManager.DETAIL_USERS_FILENAME);
                userFiles.add(IExtractorManager.RESUME_USER_PRINTERS_FILENAME);
                //files.add("Desconocido" + IExtractorManager.RESUME_USER_PRINTERS_FILENAME);

                List mfpFiles = new ArrayList();
                mfpFiles.add(IExtractorManager.RESUME_PRINTERS_FILENAME);

                List ccFiles = new ArrayList();
                ccFiles.add(IExtractorManager.RESUME_CECO_FILENAME);

                if (!(allOK = fileManager.createZIPFile(REPORTS_OUTPUT_PATH, ZIP_OUTPUT_PATH, usersZipName, userFiles)))
                {
                    logger.error("generateAllReports() : Error creating Users ZIP file. The File is not created");
                    System.out.println("ERROR: Creating Users ZIP file. The File is not created");
                }


                if (!(allOK = fileManager.createZIPFile(REPORTS_OUTPUT_PATH, ZIP_OUTPUT_PATH, printersZipName, mfpFiles)))
                {
                    logger.error("generateAllReports() : Error creating MFPs ZIP file. The File is not created");
                    System.out.println("ERROR: Creating MFPs ZIP file. The File is not created");
                }


                if (!(allOK = fileManager.createZIPFile(REPORTS_OUTPUT_PATH, ZIP_OUTPUT_PATH, cecosZipName, ccFiles)))
                {
                    logger.error("generateAllReports() : Error creating CCs ZIP file. The File is not created");
                    System.out.println("ERROR: Creating CCs ZIP file. The File is not created");
                }

            }
        }
        catch (Exception ex)
        {
            logger.error(ex.getMessage());
            logger.error(ex.getStackTrace());
            allOK = false;
        }
        finally
        {
//            //Close connection with the database
//            if (!dbManager.disconnect())
//                logger.error("generateAllReports() : Cannot close DataBase connection");

            return allOK;
        }

    }

    public boolean generateResumeUserPrinters(String company)
    {
        String printerName, userName;
        Resume resume;
        User user = null;
        List userPrinters, users;
        ListIterator usersIterator, printersIterator;

        DataBaseManager dbManager = InterfaceManager.getDataBaseManagerInstance(this.getCONFIG_FILE(), this.getSQL_FILE());
        fileManager = InterfaceManager.getFileManagerInstance();

        // Obtain cost center ids from csv'S /WEB-INF/CC
        getCCs();        
        
        if (!dbManager.isConnected())
        {
            //Open connection with the database
            if (!dbManager.connect())
            {
                logger.error("generateResumeUsersPrinters() : Cannot open DataBase connection");
                System.out.println("ERROR: Cannot open DataBase connection");
                return false;
            }
        }
        
//        bbvaPlug = getPluginInstance();

       
        if (company.equalsIgnoreCase(""))
            users = dbManager.getReportingUsers();
        else
            users = dbManager.getReportingUsers_PerCompany(company);

        if (fileManager.createFile(REPORTS_OUTPUT_PATH, IExtractorManager.RESUME_USER_PRINTERS_FILENAME))
        {
            logger.info("Creating User - MFP Resume file. Start Time : " + new Date().toString());
            System.out.println("-> Creating User - MFP Resume file. Start Time : " + new Date().toString());

            String text = "Nombre de Usuario;Nombre de Impresora;Número de Serie;Región;Oficina;Total de Páginas;" +
                    "Impresión Color;Impresión B/N;Copia Color;Copia B/N;" +
                    "Mes;Año\n";
            fileManager.appendText2File(IExtractorManager.RESUME_USER_PRINTERS_FILENAME, text);

            usersIterator = users.listIterator();
            while (usersIterator.hasNext())
            {
                userName = (String)usersIterator.next();

                // ***************************** ADD FOR BBVA ************
//                user = getUserObject(bbvaPlug, userName);
                // ***************************** ADD FOR BBVA ************               
                
                if (company.equalsIgnoreCase(""))
                    userPrinters = dbManager.getUserMFPs(userName);
                else
                    userPrinters = dbManager.getUserMFPs(userName,company);
                
                // ***************************** ADD FOR MASSIMO *******
                if (CC_OBTAINING_ACTIVATION_MODE.equalsIgnoreCase("CSV")){
                    user = getUserByID(userName);
                }else if (CC_OBTAINING_ACTIVATION_MODE.equalsIgnoreCase("extDB")){
                    user = getUserObject(remoteccplugin, userName);
                    if (user == null)
                        user = new User(userName, "", "", "");
                }else if (CC_OBTAINING_ACTIVATION_MODE.equalsIgnoreCase("AD")){
                    if (!userName.isEmpty()) {
//                        user = getADUserByID(userName);
                        user = getUserByID(userName);
                    } else {
                        user = new User(userName, "", "", "");
                    }
                }else{
//                    if (user == null)
                    user = new User(userName, "", "", "");
                }
//                    user = null;
//                    user = getUserByID(userName);
//                    if (user == null)
//                        user = new User(userName, "", "");
                // ***************************** ADD FOR MASSIMO *******
                    
//                userPrinters = dbManager.getUserMFPs(userName);

//                System.out.println("Número de MFPs utilizadas por el usuario " + userName +
//                        " en el periodo " + START_DATE + " - " + END_DATE + " --> " + userPrinters.size());

                printersIterator = userPrinters.listIterator();
                while (printersIterator.hasNext())
                {
                    printerName = (String)printersIterator.next();
                    resume = dbManager.getTotalsBetweenDates(userName, printerName, START_DATE, END_DATE);

                    // ***************************** ADD FOR BBVA *******
                    resume.setUser(user);
                    // ***************************** ADD FOR BBVA *******

//                    System.out.println("Escribiendo entrada de usuario: " + userName + " ; impresora: " + printerName);
                    fileManager.appendResume2File(IExtractorManager.RESUME_USER_PRINTERS_FILENAME, resume);
                }
            }
            logger.info("Creating User - MFP Resume file. End Time : " + new Date().toString());
            System.out.println("-> Creating User - MFP Resume file. End Time : " + new Date().toString());


            //Close connection with the database
            if (!dbManager.disconnect())
                logger.error("generateResumeUserPrinters() : Cannot close DataBase connection");

            return fileManager.closeFile(IExtractorManager.RESUME_USER_PRINTERS_FILENAME);

        }
        else
        {
            //Close connection with the database
            if (!dbManager.disconnect())
                logger.error("generateResumeUserPrinters() : Cannot close DataBase connection");

            return false;
        }
    }
    
    
    public boolean generateResumeUserAccumulated(String company, String tipoVistaInformacion, String type, String tipoInforme)
    {
        final long MILLSECS_PER_DAY = 24 * 60 * 60 * 1000;
        String printerName, filter, sourceType;
        Resume resume;
        User user = null;
        List userPrinters, filters;
        ListIterator filtersIterator, iteratorAccumulated;

        DataBaseManager dbManager = InterfaceManager.getDataBaseManagerInstance(this.getCONFIG_FILE(), this.getSQL_FILE());
        fileManager = InterfaceManager.getFileManagerInstance();

        // Obtain cost center ids from csv'S /WEB-INF/CC
        getCCs();        
        
        if (!dbManager.isConnected())
        {
            //Open connection with the database
            if (!dbManager.connect())
            {
                logger.error("generateResumeUsersPrinters() : Cannot open DataBase connection");
                System.out.println("ERROR: Cannot open DataBase connection");
                return false;
            }
        }
        
        //        bbvaPlug = getPluginInstance();
        
        if(tipoVistaInformacion.equalsIgnoreCase("Usuario"))
        {
            if (company.equalsIgnoreCase(""))
                filters = dbManager.getReportingUsers();
            else
                filters = dbManager.getReportingUsers_PerCompany(company);
        }else if(tipoVistaInformacion.equalsIgnoreCase("Region"))
        {
            if (company.equalsIgnoreCase(""))
                filters = dbManager.getReportingZones();
            else
                filters = dbManager.getReportingZones_PerCompany(company);
        }
        else if(tipoVistaInformacion.equalsIgnoreCase("Oficina"))
        {
            if (company.equalsIgnoreCase(""))
                filters = dbManager.getReportingLocations();
            else
                filters = dbManager.getReportingLocations_PerCompany(company);
        }
        else
            filters = new ArrayList();
       
        if (type.equalsIgnoreCase("imp"))
            sourceType = "printer.print";
        else if (type.equalsIgnoreCase("copy"))
            sourceType = "copy";    
        else
            sourceType = null;

        if (fileManager.createFile(REPORTS_OUTPUT_PATH, IExtractorManager.RESUME_USER_ACCUMULATED_FILENAME))
        {
            logger.info("Creating User - Accumulated Resume file. Start Time : " + new Date().toString());
            System.out.println("-> Creating User - Accumulated Resume file. Start Time : " + new Date().toString());
            
            List<String> semanas = CalculosFechas.CalculoNumeroSemanas(START_DATE, END_DATE);
            List<String> meses = CalculosFechas.CalculoNumeroMeses(START_DATE, END_DATE);
            
            String text = "Nombre " + tipoVistaInformacion + ";";
            
            if (tipoInforme.equalsIgnoreCase("hour"))
            {
                for(int i=6; i<=22;i++)
                {
                    text = text + i + " H;";
                } 
            }
            else if (tipoInforme.equalsIgnoreCase("day"))
            {
                Calendar calendarFechaInicio = Calendar.getInstance();
                calendarFechaInicio.setTime(START_DATE);
                for(int i=0; i<=((END_DATE.getTime() - START_DATE.getTime()) /  MILLSECS_PER_DAY);i++)
                {
                    text = text + "Día " + calendarFechaInicio.get(Calendar.DATE) + " ( " + calendarFechaInicio.get(Calendar.MONTH) + 1 + "/" + calendarFechaInicio.get(Calendar.YEAR) + " ;";
                    calendarFechaInicio.add(Calendar.DATE, 1);
                } 
            }
            else if (tipoInforme.equalsIgnoreCase("week"))
            {
                for(String semana : semanas)
                {
                    text = text + "Semana " + semana + " ;";
                } 
            }
            else if (tipoInforme.equalsIgnoreCase("month"))
            {
                for(String mes : meses)
                {
                    text = text + "Mes " + mes + " ;";
                } 
            }
            
            text = text + "\n";
            
            //text = "Nombre de Usuario;HORA - 6H ;HORA - 7H ;HORA - 8H ;HORA - 9H ;HORA - 10H ;HORA - 11H ;HORA - 12H ;HORA - 13H ;HORA - 14H ;HORA - 15H ;HORA - 16H ;HORA - 17H ;HORA - 18H ;HORA - 19H ;HORA - 20H ;HORA - 21H ;HORA - 22H ;Total\n";
            
            fileManager.appendText2File(IExtractorManager.RESUME_USER_ACCUMULATED_FILENAME, text);
            
            //text = ";6H;7H;8H;9H;10H;11H;12H;13H;14H;15H;16H;17H;18H;19H;20H;21H;22H;\n";
            //fileManager.appendText2File(IExtractorManager.RESUME_USER_ACCUMULATED_FILENAME, text);

            filtersIterator = filters.listIterator();
            List accumulatedList = new ArrayList();
            
            while (filtersIterator.hasNext())
            {
                filter = (String)filtersIterator.next();
                List usersAcuumulatedList = dbManager.getUsersAccumulated(filter, START_DATE, END_DATE, type, sourceType, tipoInforme, tipoVistaInformacion);
                accumulatedList.addAll(usersAcuumulatedList);
                
                iteratorAccumulated = usersAcuumulatedList.listIterator();
                while (iteratorAccumulated.hasNext())
                {
                    Accumulated accumulated = (Accumulated)iteratorAccumulated.next();
                    text = "";
                    text = text + filter + ";";
                    
                    if(tipoInforme.equalsIgnoreCase("hour"))
                    {
                        Accumulated.ContadorDocumentos tmp = new Accumulated.ContadorDocumentos();
                        for(int hora =6;hora<=22;hora++)
                        {
                            Calendar calendar = Calendar.getInstance();
                            calendar.setTime(START_DATE);
                            calendar.set(Calendar.HOUR, hora);
                            tmp.setDate(calendar.getTime());
                            
                            int index = accumulated.getListaRegistros().indexOf(tmp);
                            if(index == -1)
                                text = text + "0" + ";";
                            else
                                text = text + accumulated.getListaRegistros().get(index).getPages() + ";";
                        } 
                    }
                    else if(tipoInforme.equalsIgnoreCase("week"))
                    {
                        final Calendar calendar = Calendar.getInstance();
                        calendar.setTime(START_DATE);
                        
                        for(int cont=0;cont<semanas.size();cont++)
                        {
                            Accumulated.ContadorDocumentos obj = IterableUtils.find(accumulated.getListaRegistros(), new Predicate<Accumulated.ContadorDocumentos>() {
                                @Override
                                public boolean evaluate(Accumulated.ContadorDocumentos o) {
                                    Calendar calendarDate = Calendar.getInstance();
                                    calendarDate.setTime(o.getDate());
                                    return o.getDate() != null && calendarDate.get(Calendar.YEAR) == calendar.get(Calendar.YEAR) && calendarDate.get(Calendar.MONTH) == calendar.get(Calendar.MONTH) && CalculosFechas.GetNumeroSemana(calendarDate.getTime()) == CalculosFechas.GetNumeroSemana(calendar.getTime());
                                }
                            });
                            if(obj == null)
                                text = text + "0" + ";";
                            else
                                text = text + obj.getPages() + ";";
                            
                            calendar.add(Calendar.DATE, 7);
                        } 
                    }
                    else if(tipoInforme.equalsIgnoreCase("day"))
                    {
                        Accumulated.ContadorDocumentos tmp = new Accumulated.ContadorDocumentos();

                        Calendar calendar = Calendar.getInstance();
                        calendar.setTime(START_DATE);
                        for(int dia=0;dia<=((END_DATE.getTime() - START_DATE.getTime()) /  MILLSECS_PER_DAY);dia++)
                        {
                            tmp.setDate(calendar.getTime());
                            int index = accumulated.getListaRegistros().indexOf(tmp);
                            if(index == -1)
                                text = text + "0" + ";";
                            else
                                text = text + accumulated.getListaRegistros().get(index).getPages()+ ";";

                            calendar.add(Calendar.DATE, 1);
                        } 
                    }
                    else if(tipoInforme.equalsIgnoreCase("month"))
                    {
                        final Calendar calendar = Calendar.getInstance();
                        calendar.setTime(START_DATE);
                        
                        for(int cont=0;cont<meses.size();cont++)
                        {
                            Accumulated.ContadorDocumentos obj = IterableUtils.find(accumulated.getListaRegistros(), new Predicate<Accumulated.ContadorDocumentos>() {
                                @Override
                                public boolean evaluate(Accumulated.ContadorDocumentos o) {
                                    Calendar calendarDate = Calendar.getInstance();
                                    calendarDate.setTime(o.getDate());
                                    return o.getDate() != null && calendarDate.get(Calendar.YEAR) == calendar.get(Calendar.YEAR) && calendarDate.get(Calendar.MONTH) == calendar.get(Calendar.MONTH);
                                }
                            });
                            if(obj == null)
                                text = text + "0" + ";";
                            else
                                text = text + obj.getPages() + ";";
                            
                            calendar.add(Calendar.MONTH, 1);
                        } 
                    }
                    text = text + "\n";
                    
                    fileManager.appendText2File(IExtractorManager.RESUME_USER_ACCUMULATED_FILENAME, text);
                }

            }
            
            text = "";
            text = text + "TOTAL;";
            
            final Calendar calendar = Calendar.getInstance();
            calendar.setTime(START_DATE);
            
            if (tipoInforme.equalsIgnoreCase("hour"))
            {
                
                for(int hora =6;hora<=22;hora++)
                {
                    calendar.set(Calendar.HOUR_OF_DAY, hora);
                    int cont = 0;
                    for(Object registro : accumulatedList)
                    {
                        cont = cont + ((Accumulated)registro).getTotalPagesByBate(calendar.getTime());                      
                    }

                    text = text + cont + ";";   
                } 
            }
            else if (tipoInforme.equalsIgnoreCase("day"))
            {

                for(int dia=0;dia<=((END_DATE.getTime() - START_DATE.getTime()) /  MILLSECS_PER_DAY);dia++)
                {
                    int cont = 0;
                    for(Object registro : accumulatedList)
                    {
                        cont = cont + ((Accumulated)registro).getTotalPagesByBate(calendar.getTime());                      
                    }

                    text = text + cont + ";";  

                    calendar.add(Calendar.DATE, 1);
                } 

            }
            else if (tipoInforme.equalsIgnoreCase("week"))
            {

                for(int i=0;i<semanas.size();i++)
                {
                    int cont = 0;
                    for(Object registro : accumulatedList)
                    {
                        Accumulated.ContadorDocumentos obj = IterableUtils.find(((Accumulated)registro).getListaRegistros(), new Predicate<Accumulated.ContadorDocumentos>() {
                            @Override
                            public boolean evaluate(Accumulated.ContadorDocumentos o) {
                                Calendar calendarDate = Calendar.getInstance();
                                calendarDate.setTime(o.getDate());
                                return o.getDate() != null && calendarDate.get(Calendar.YEAR) == calendar.get(Calendar.YEAR) && calendarDate.get(Calendar.MONTH) == calendar.get(Calendar.MONTH) && CalculosFechas.GetNumeroSemana(calendarDate.getTime()) == CalculosFechas.GetNumeroSemana(calendar.getTime());
                            }
                        });
                        if(obj != null)
                            cont = cont + obj.getPages();                        
                    }

                    text = text + cont + ";";  

                    calendar.add(Calendar.DATE, 7);
                } 

            }
            else if (tipoInforme.equalsIgnoreCase("month"))
            {

                for(int i=0;i<meses.size();i++)
                {
                    int cont = 0;
                    for(Object registro : accumulatedList)
                    {
                        Accumulated.ContadorDocumentos obj = IterableUtils.find(((Accumulated)registro).getListaRegistros(), new Predicate<Accumulated.ContadorDocumentos>() {
                            @Override
                            public boolean evaluate(Accumulated.ContadorDocumentos o) {
                                Calendar calendarDate = Calendar.getInstance();
                                calendarDate.setTime(o.getDate());
                                return o.getDate() != null && calendarDate.get(Calendar.YEAR) == calendar.get(Calendar.YEAR) && calendarDate.get(Calendar.MONTH) == calendar.get(Calendar.MONTH);
                            }
                        });
                        if(obj != null)
                            cont = cont + obj.getPages();                        
                    }

                    text = text + cont + ";";  

                    calendar.add(Calendar.MONTH, 1);
                } 

            }

            text = text + "\n";

            fileManager.appendText2File(IExtractorManager.RESUME_USER_ACCUMULATED_FILENAME, text);
            
            logger.info("Creating User - MFP Resume file. End Time : " + new Date().toString());
            System.out.println("-> Creating User - MFP Resume file. End Time : " + new Date().toString());


            //Close connection with the database
            if (!dbManager.disconnect())
                logger.error("generateResumeUserPrinters() : Cannot close DataBase connection");

            return fileManager.closeFile(IExtractorManager.RESUME_USER_ACCUMULATED_FILENAME);

        }
        else
        {
            //Close connection with the database
            if (!dbManager.disconnect())
                logger.error("generateResumeUserPrinters() : Cannot close DataBase connection");

            return false;
        }
    }

    public boolean generateResumeCostCenters(String company)
    {
        String ceco;
        Resume resume;
        List cecoList;

        DataBaseManager dbManager = InterfaceManager.getDataBaseManagerInstance(this.getCONFIG_FILE(), this.getSQL_FILE());

        if (!dbManager.isConnected())
        {
            //Open connection with the database
            if (!dbManager.connect())
            {
                logger.error("generateResumeCostCenters() : Cannot open DataBase connection");
                System.out.println("ERROR: Cannot open DataBase connection");
                return false;
            }
        }

        if (company.equalsIgnoreCase(""))
            cecoList = dbManager.getCCs();
        else
            cecoList = dbManager.getCCs(company);
        //System.out.println("Número de usuarios --> " + users.size());

        if (fileManager.createFile(REPORTS_OUTPUT_PATH, IExtractorManager.RESUME_CECO_FILENAME))
        {
            logger.info("Creating CCs Resume file. Start Time : " + new Date().toString());
            System.out.println("-> Creating CCs Resume file. Start Time : " + new Date().toString());

            String text = "Código Centro de Coste;Nombre Centro de Coste;Total de Páginas;" +
                    "Impresión Color;Impresión B/N;Copia Color;Copia B/N;Escaneado Enviado;Escaneado Almacenado;" +
                    "Fax Enviado;Fax Recibido;Mes;Año\n";
            fileManager.appendText2File(IExtractorManager.RESUME_CECO_FILENAME, text);

            ListIterator cecoListIterator = cecoList.listIterator();
            while (cecoListIterator.hasNext())
            {
                ceco = (String)cecoListIterator.next();
                //System.out.println("Escribiendo entrada de usuario: " + userName);

                resume = dbManager.getTotalsBetweenDates(ceco, START_DATE, END_DATE, resumeType.CC_RESUME);
                
                if (resume.getUser().getUsername().contains("noname")){
                    if (resume.getUser().getUsername().split("_").length>2){
                        resume.getUser().setUsername(resume.getUser().getUsername().split("_")[2]);
                    }else
                        resume.getUser().setUsername("");
                }
                if (resume.getId().contains("noname")){
                    if (resume.getId().split("_").length>2){
                        resume.setId(resume.getId().split("_")[2]);
                    }else
                        resume.setId("");
                }
                
                if (resume != null)
                    fileManager.appendResume2File(IExtractorManager.RESUME_CECO_FILENAME, resume);

            }
            logger.info("Creating CCs Resume file. End Time : " + new Date().toString());
            System.out.println("-> Creating CCs Resume file. End Time : " + new Date().toString());

            //Close connection with the database
            if (!dbManager.disconnect())
                logger.error("generateResumeCostCenters() : Cannot close DataBase connection");

            return fileManager.closeFile(IExtractorManager.RESUME_CECO_FILENAME);
        }
        else
        {
            //Close connection with the database
            if (!dbManager.disconnect())
                logger.error("generateResumeCostCenters() : Cannot close DataBase connection");

            return false;
        }
    }
    
 

    public boolean generateResumePrinters(String company, String mfpserial)
    {
        String printerName;
        Resume resume;
        List printers;

        DataBaseManager dbManager = InterfaceManager.getDataBaseManagerInstance(this.getCONFIG_FILE(), this.getSQL_FILE());

        if (!dbManager.isConnected())
        {
            //Open connection with the database
            if (!dbManager.connect())
            {
                logger.error("generateResumePrinters() : Cannot open DataBase connection");
                System.out.println("ERROR: Cannot open DataBase connection");
                return false;
            }
        }

        if (company.equalsIgnoreCase("")){
            if (mfpserial == null || mfpserial.isEmpty())
                printers = dbManager.getMFPs();
            else
                printers = dbManager.getMFPs(mfpserial);
        }else{
            if (mfpserial == null || mfpserial.isEmpty())
                printers = dbManager.getMFPs_PerCompany(company);
            else
                printers = dbManager.getMFPs_PerCompany(company, mfpserial);           
        }
         
        //System.out.println("Número de MFPs --> " + printers.size());
        
        if (fileManager.createFile(REPORTS_OUTPUT_PATH, IExtractorManager.RESUME_PRINTERS_FILENAME))
        {
            logger.info("Creating MFPs Resume file. Start Time : " + new Date().toString());
            System.out.println("-> Creating MFPs Resume file. Start Time : " + new Date().toString());

            String text = "Número de Serie;Nombre de Impresora;Región;Oficina;Total de Páginas;" +
                    "Impresión Color;Impresión B/N;Copia Color;Copia B/N;" +
                    "Mes;Año\n";
            fileManager.appendText2File(IExtractorManager.RESUME_PRINTERS_FILENAME, text);


            ListIterator printersIterator = printers.listIterator();
            while (printersIterator.hasNext())
            {
                printerName = (String)printersIterator.next();
                resume = dbManager.getTotalsBetweenDates(printerName, START_DATE, END_DATE, resumeType.MFP_RESUME);

                //System.out.println("Escribiendo entrada de impresora: " + printerName);
                fileManager.appendResume2File(IExtractorManager.RESUME_PRINTERS_FILENAME, resume);
            }
            //Close connection with the database
            if (!dbManager.disconnect())
                logger.error("generateResumeUsers() : Cannot close DataBase connection");

//            if (mfpserial == null)
//                addOfflinePrintersResume2File(IExtractorManager.RESUME_PRINTERS_FILENAME, resumeType.MFP_RESUME, printers, company);

            logger.info("Creating MFPs Resume file. End Time : " + new Date().toString());
            System.out.println("-> Creating MFPs Resume file. End Time : " + new Date().toString());

            return fileManager.closeFile(IExtractorManager.RESUME_PRINTERS_FILENAME);
        }
        else
        {
            //Close connection with the database
            if (!dbManager.disconnect())
                logger.error("generateResumePrinters() : Cannot close DataBase connection");

            return false;
        }
    }

    private void generateResumeDesconocido(String company)
    {
        String printerName;
        Resume resume;
        User user;
        List desconocidoPrinters = null;
        DataBaseManager dbManager = InterfaceManager.getDataBaseManagerInstance(this.getCONFIG_FILE(), this.getSQL_FILE());

        if (!dbManager.isConnected())
        {
            //Open connection with the database
            if (!dbManager.connect())
            {
                logger.error("generateResumeDesconocido() : Cannot open DataBase connection");
                System.out.println("ERROR: Cannot open DataBase connection");
                return;
            }
        }

        if (company.equalsIgnoreCase(""))
            desconocidoPrinters = dbManager.getUnknownUserMFPs();
        else
            desconocidoPrinters = dbManager.getUnknownUserMFPs(company);

        //System.out.println("Número de MFPs del usuario \"desconocido\"--> " + desconocidoPrinters.size());

        ListIterator printersIterator = desconocidoPrinters.listIterator();
        while (printersIterator.hasNext())
        {
            printerName = (String)printersIterator.next();
            resume = dbManager.getTotalsBetweenDates(printerName, START_DATE, END_DATE, resumeType.DESCONOCIDO_MFP_RESUME);

            // ***************************** ADD FOR BBVA *******
            System.out.println("resume.getId()).getHostname(): " + resume.getId());
            resume.setId(dbManager.getMFPbyID(resume.getId()).getHostname());
            user = new User(printerName, "", "","");
            resume.setUser(user);
            // ***************************** ADD FOR BBVA *******

            //System.out.println("Escribiendo entrada de impresora de \"desconocido\": " + printerName);
            fileManager.appendResume2File(IExtractorManager.RESUME_USERS_FILENAME, resume);
        }
        //Close connection with the database
        if (!dbManager.disconnect())
            logger.error("generateResumeDesconocido() : Cannot close DataBase connection");

//        addOfflinePrintersResume2File(IExtractorManager.RESUME_USERS_FILENAME, resumeType.USER_RESUME, null, "");

    }

    public boolean generateResumeUsers(String company)
    {
        String userName;
        Resume resume;
        List users;
        User user = null;

        DataBaseManager dbManager = InterfaceManager.getDataBaseManagerInstance(this.getCONFIG_FILE(), this.getSQL_FILE());
        fileManager = InterfaceManager.getFileManagerInstance();

        // Obtain cost center ids from csv'S /WEB-INF/CC
        getCCs();
        
        if (!dbManager.isConnected())
        {
            //Open connection with the database
            if (!dbManager.connect())
            {
                logger.error("generateResumeUsers() : Cannot open DataBase connection");
                System.out.println("ERROR: Cannot open DataBase connection");
                return false;
            }
        }

//        bbvaPlug = getPluginInstance();

        if (company.equalsIgnoreCase(""))
            users = dbManager.getReportingUsers();
        else
            users = dbManager.getReportingUsers_PerCompany(company);

        //System.out.println("Número de usuarios --> " + users.size());

        if (fileManager.createFile(REPORTS_OUTPUT_PATH, IExtractorManager.RESUME_USERS_FILENAME))
        {
            logger.info("Creating Users Resume file. Start Time : " + new Date().toString());
            System.out.println("-> Creating Users Resume file. Start Time : " + new Date().toString());

            String text = "Nombre de Usuario;Nombre completo de Usuario;Nombre Centro de Coste;Centro de Coste;Total de Páginas;" +
                    "Impresión Color;Impresión B/N;Copia Color;Copia B/N;Escaneado Enviado;Escaneado Almacenado;" +
                    "Fax Enviado;Fax Recibido;Mes;Año\n";
            fileManager.appendText2File(IExtractorManager.RESUME_USERS_FILENAME, text);

            ListIterator usersIterator = users.listIterator();
            while (usersIterator.hasNext())
            {
                userName = (String)usersIterator.next();
                //System.out.println("Escribiendo entrada de usuario: " + userName);

                if (userName.isEmpty())
                    generateResumeDesconocido(company);
                else
                {
                    // ***************************** ADD FOR BBVA ************
//                    user = getUserObject(bbvaPlug, userName);
                    // ***************************** ADD FOR BBVA ************

                    resume = dbManager.getTotalsBetweenDates(userName, START_DATE, END_DATE, resumeType.USER_RESUME);

                    // ***************************** ADD FOR MASSIMO *******
                    if (CC_OBTAINING_ACTIVATION_MODE.equalsIgnoreCase("CSV")){
                        user = getUserByID(userName);
                    }else if (CC_OBTAINING_ACTIVATION_MODE.equalsIgnoreCase("extDB")){
                        user = getUserObject(remoteccplugin, userName);
                        if (user == null)
                            user = new User(userName, "", "", "");
                    }else if (CC_OBTAINING_ACTIVATION_MODE.equalsIgnoreCase("AD")){
                        if (!userName.isEmpty()) {
//                            user = getADUserByID(userName);
                            user = getUserByID(userName);
                        } else {
                            user = new User(userName, "", "", "");
                        }
                    }else{
//                        if (user == null)
                            user = new User(userName, "", "", "");
                    }
//                    user = null;
//                    user = getUserByID(userName);
                    if (resume.getId().contains("noname")){
                        if (resume.getId().split("_").length>2){
                            resume.setId(userName.split("_")[2]);
                        }else
                            resume.setId("");
                    }
                        
//                    if (user == null)
//                        user = new User(userName, "", "");
                    resume.setUser(user);
                    // ***************************** ADD FOR MASSIMO *******
                    
                    // ***************************** ADD FOR BBVA *******
//                    resume.setUser(user);
                    // ***************************** ADD FOR BBVA *******

                    fileManager.appendResume2File(IExtractorManager.RESUME_USERS_FILENAME, resume);
                }
            }
            logger.info("Creating Users Resume file. End Time : " + new Date().toString());
            System.out.println("-> Creating Users Resume file. End Time : " + new Date().toString());

            //Close connection with the database
            if (!dbManager.disconnect())
                logger.error("generateResumeUsers() : Cannot close DataBase connection");

            return fileManager.closeFile(IExtractorManager.RESUME_USERS_FILENAME);
        }
        else
        {
            //Close connection with the database
            if (!dbManager.disconnect())
                logger.error("generateResumeUsers() : Cannot close DataBase connection");

            return false;
        }
    }

    private boolean generateDetailsByUser(int fileIndex, String company, String username2)
    {
        List actionsList, users;
        String username;
        Action action;
        ListIterator actionIt;
        int minIndex = 0, maxIndex = 0;
        User user = null;

        DataBaseManager dbManager = InterfaceManager.getDataBaseManagerInstance(this.getCONFIG_FILE(), this.getSQL_FILE());
        fileManager = InterfaceManager.getFileManagerInstance();

        // Obtain cost center ids from csv'S /WEB-INF/CC
        getCCs();
        
        if (!dbManager.isConnected())
        {
            //Open connection with the database
            if (!dbManager.connect())
            {
                logger.error("generateDetailsByUser() : Cannot open DataBase connection");
                System.out.println("ERROR: Cannot open DataBase connection");
                return false;
            }
        }

//        bbvaPlug = getPluginInstance();

        if (company.equalsIgnoreCase("")){
            if ((username2 == null)||(username2.equalsIgnoreCase(""))){
                users = dbManager.getReportingUsers();
            }else{
                users = dbManager.getReportingUsers(username2);
            }
        }else{
            if ((username2 == null)||(username2.equalsIgnoreCase(""))){
                users = dbManager.getReportingUsers_PerCompany(company);
            }else{
                users = dbManager.getReportingUsers_PerCompany(company, username2);
            }
        }

        int partOfUsers = users.size()/4;

        //System.out.println("Número de usuarios --> " + users.size());
        switch (fileIndex)
        {
            case DETAILS_FILE_PART1:
                minIndex = 0;
                maxIndex = partOfUsers - 1;
                break;
            case DETAILS_FILE_PART2:
                minIndex = partOfUsers;
                maxIndex = 2 * partOfUsers - 1;
                break;
            case DETAILS_FILE_PART3:
                minIndex = 2 * partOfUsers;
                maxIndex = 3 * partOfUsers - 1;
                break;
            case DETAILS_FILE_PART4:
                minIndex = 3* partOfUsers;
                maxIndex = users.size() - 1;
                break;
        }

        Runtime r = Runtime.getRuntime();

        if (fileManager.createFile(REPORTS_OUTPUT_PATH, IExtractorManager.DETAIL_USERS_FILENAME + "_" + fileIndex))
        {
            logger.info("Creating Users Detail file " + fileIndex + ". Start Time : " + new Date().toString());
            System.out.println("-> Creating Users Detail file " + fileIndex + ". Start Time : " + new Date().toString());
        }
        else
            return false;

        if (fileIndex == DETAILS_FILE_PART1)
        {
            String text = "Nombre de Usuario;Nombre completo de Usuario;Nombre Centro de Coste;Centro de Coste;Nombre de Impresora;Número de Serie;Ubicación;Zona;Fecha;Hora;" +
                    "Tipo;Número de páginas;B/N o Color;Tamaño de página;Detalle;Mes;Año\n";
            fileManager.appendText2File(IExtractorManager.DETAIL_USERS_FILENAME + "_" + fileIndex, text);
        }

        for (int i = minIndex; i <= maxIndex; i++)
        {
            username = (String) users.get(i);

            // ***************************** ADD FOR BBVA ************
//            user = getUserObject(bbvaPlug, username);
            // ***************************** ADD FOR BBVA ************
            
            // ***************************** ADD FOR MASSIMO *******
            if (CC_OBTAINING_ACTIVATION_MODE.equalsIgnoreCase("CSV")){
                user = getUserByID(username);
            }else if (CC_OBTAINING_ACTIVATION_MODE.equalsIgnoreCase("extDB")){
                user = getUserObject(remoteccplugin, username);
                if (user == null)
                    user = new User(username, "", "", "");
            }else if (CC_OBTAINING_ACTIVATION_MODE.equalsIgnoreCase("AD")){
                if (!username.isEmpty()) {
//                    user = getADUserByID(username);
                    user = getUserByID(username);
                } else {
                    user = new User(username, "", "", "");
                }
            }else{
                if (user == null)
                    user = new User(username, "", "", "");
            }
////            user = null;
////            user = getUserByID(username);
//            if (user == null)
//                user = new User(username, "", "");
            // ***************************** ADD FOR MASSIMO *******

            actionsList = dbManager.getIdActions(company, username, START_DATE, END_DATE, Action.actionType.USER_ACTION);
            actionIt = actionsList.listIterator();

//                if (dbManager.fillActionsRSbyID(username, START_DATE, END_DATE, Action.actionType.USER_ACTION))
//                {
//                    while (dbManager.hasMoreActions(Action.actionType.USER_ACTION))

            while (actionIt.hasNext())
            {
                //action = dbManager.getActionFromRS(Action.actionType.USER_ACTION);
                action = (Action) actionIt.next();

                // ***************************** ADD FOR BBVA *******
                action.setUser(user);
                if(action.getUserName().contains("noname")){
                    if (action.getUserName().split("_").length>2){
                        action.setUserName(action.getUserName().split("_")[2]);
                    }else
                    action.setUserName("");
                }
                // ***************************** ADD FOR BBVA *******

//                addMFPtoList(action.getMFPserial());
                //System.out.println("Escribiendo detalle de usuario: " + username);
                fileManager.appendAction2File(IExtractorManager.DETAIL_USERS_FILENAME + "_" + fileIndex, action, Action.actionType.USER_ACTION);
            }

//                }
//                else
//                    return false;
        }
        //Close connection with the database
        if (!dbManager.disconnect())
            logger.error("generateDetailsByUser() : Cannot close DataBase connection");


//        if (fileIndex == DETAILS_FILE_PART4)
//            addOfflinePrintersDetails2File(IExtractorManager.DETAIL_USERS_FILENAME + "_" + fileIndex, Action.actionType.USER_ACTION, dbMFPsList);

        logger.info("Creating Users Detail file " + fileIndex + ". End Time : " + new Date().toString());
        System.out.println("-> Creating Users Detail file " + fileIndex + ". End Time : " + new Date().toString());

        r.gc();
        
        return fileManager.closeFile(IExtractorManager.DETAIL_USERS_FILENAME + "_" + fileIndex);
    }

    private synchronized void addMFPtoList(String mfpSerial)
    {
        if (!dbMFPsList.contains(mfpSerial))
            dbMFPsList.add(mfpSerial);
    }

//    public boolean generateDetailsUsers()
//    {
//        Action action;
//        User user;
//
//        Runtime r = Runtime.getRuntime();
//
//        IDataBaseManager dbManager = InterfaceManager.getDataBaseManagerInstance(this.getCONFIG_FILE(), this.getSQL_FILE());
//
//        if (!dbManager.isConnected())
//        {
//            //Open connection with the database
//            if (!dbManager.connect())
//            {
//                logger.error("generateDetailsUsers() : Cannot open DataBase connection");
//                System.out.println("ERROR: Cannot open DataBase connection");
//                return false;
//            }
//        }
//
//
//        if (fileManager.createFile(REPORTS_OUTPUT_PATH, IExtractorManager.DETAIL_USERS_FILENAME))
//        {
//            logger.info("Creating Users Detail file. Start Time : " + new Date().toString());
//            System.out.println("-> Creating Users Detail file. Start Time : " + new Date().toString());
//
//            String text = "Nombre de Usuario;Nombre Centro de Coste;Centro de Coste;Nombre de Impresora;Número de Serie;Ubicación;Zona;Fecha;Hora;" +
//                    "Tipo;Número de páginas;B/N o Color;Tamaño de página;Detalle;Mes;Año\n";
//            fileManager.appendText2File(IExtractorManager.DETAIL_USERS_FILENAME, text);
//
//            if (dbManager.fillActionsRS(START_DATE, END_DATE, Action.actionType.USER_ACTION))
//            {
//                while (dbManager.hasMoreActions(Action.actionType.USER_ACTION))
//                {
//                    action = dbManager.getActionFromRS(Action.actionType.USER_ACTION);
//                    r.gc();
//                    addMFPtoList(action.getMFPserial());
//
//                    // ***************************** ADD FOR BBVA ************
//                    if (action.getUserName().isEmpty())
//                        user = new User(action.getMFPserial(), "", "");
//                    else
//                    {
//                        // ***************************** ADD FOR BBVA ************
//                        user = getUserObject(bbvaPlug, action.getUserName());
//                        // ***************************** ADD FOR BBVA ************
//                    }
//
//                    action.setUser(user);
//                    // ***************************** ADD FOR BBVA ************
//
//                    fileManager.appendAction2File(IExtractorManager.DETAIL_USERS_FILENAME, action, Action.actionType.USER_ACTION);
//                }
//                //addOfflinePrintersDetails2File(IExtractorManager.DETAIL_USERS_FILENAME, Action.actionType.USER_ACTION, dbMFPsList);
//
//                logger.info("Creating Users Detail file. End Time : " + new Date().toString());
//                System.out.println("-> Creating Users Detail file. End Time : " + new Date().toString());
//
//                //Close connection with the database
//                if (!dbManager.disconnect())
//                    logger.error("generateDetailsByUser() : Cannot close DataBase connection");
//
//                return fileManager.closeFile(IExtractorManager.DETAIL_USERS_FILENAME);
//            }
//            else
//            {
//                //Close connection with the database
//                if (!dbManager.disconnect())
//                    logger.error("generateDetailsByUser() : Cannot close DataBase connection");
//
//                return false;
//            }
//        }
//        else
//        {
//            //Close connection with the database
//            if (!dbManager.disconnect())
//                logger.error("generateDetailsByUser() : Cannot close DataBase connection");
//
//            return false;
//        }
//    }

//    public boolean generateDetailsPrinters()
//    {
//        Action action;
//        Runtime r = Runtime.getRuntime();
//        BBVAPlugin bbvaPlug;
//        User user;
//
//
//        IDataBaseManager dbManager = InterfaceManager.getDataBaseManagerInstance(this.getCONFIG_FILE(), this.getSQL_FILE());
//
//        bbvaPlug = getPluginInstance();
//
//        if (fileManager.createFile(REPORTS_OUTPUT_PATH, IExtractorManager.DETAIL_PRINTERS_FILENAME))
//        {
//            logger.info("Creating MFPs Detail file. Start Time : " + new Date().toString());
//            System.out.println("-> Creating MFPs Detail file. Start Time : " + new Date().toString());
//
//            String text = "Número de Serie;Nombre de Impresora;Ubicación;Zona;Nombre de Usuario;Nombre Centro de Coste;Centro de Coste;Fecha;Hora;Tipo;Número de páginas;B/N o Color;Tamaño de página;Detalle;Mes;Año\n";
//            fileManager.appendText2File(IExtractorManager.DETAIL_PRINTERS_FILENAME, text);
//
//            if (dbManager.fillActionsRS(START_DATE, END_DATE, Action.actionType.MFP_ACTION))
//            {
//                while (dbManager.hasMoreActions(Action.actionType.MFP_ACTION))
//                {
//                    action = dbManager.getActionFromRS(Action.actionType.MFP_ACTION);
//                    r.gc();
//                    addMFPtoList(action.getMFPserial());
//
//                    // ***************************** ADD FOR BBVA ************
//                    if (action.getUserName().isEmpty())
//                        user = new User(action.getMFPserial(), "", "");
//                    else
//                    {
//                        // ***************************** ADD FOR BBVA ************
//                        user = getUserObject(bbvaPlug, action.getUserName());
//                        // ***************************** ADD FOR BBVA ************
//                    }
//
//                    action.setUser(user);
//                    // ***************************** ADD FOR BBVA ************
//
//                    fileManager.appendAction2File(IExtractorManager.DETAIL_PRINTERS_FILENAME, action, Action.actionType.MFP_ACTION);
//                }
//                //addOfflinePrintersDetails2File(IExtractorManager.DETAIL_PRINTERS_FILENAME, Action.actionType.MFP_ACTION, dbMFPsList);
//
//                logger.info("Creating MFPs Detail file. End Time : " + new Date().toString());
//                System.out.println("-> Creating MFPs Detail file. End Time : " + new Date().toString());
//
//                //Close connection with the database
//                if (!dbManager.disconnect())
//                    logger.error("generateDetailsPrinters() : Cannot close DataBase connection");
//
//                return fileManager.closeFile(IExtractorManager.DETAIL_PRINTERS_FILENAME);
//            }
//            else
//            {
//                //Close connection with the database
//                if (!dbManager.disconnect())
//                    logger.error("generateDetailsPrinters() : Cannot close DataBase connection");
//
//                return false;
//            }
//        }
//        else
//        {
//            //Close connection with the database
//            if (!dbManager.disconnect())
//                logger.error("generateDetailsPrinters() : Cannot close DataBase connection");
//
//            return false;
//        }
//    }

    public boolean generateAllReports(Date dateStart, Date dateEnd)
    {
        this.START_DATE = dateStart;
        this.END_DATE = dateEnd;
        this.usersZipName = IExtractorManager.USERS_ZIP_FILENAME + "_" + zipDateFormat.format(END_DATE);
        this.printersZipName = IExtractorManager.PRINTERS_ZIP_FILENAME + "_" + zipDateFormat.format(END_DATE);
        this.cecosZipName = IExtractorManager.CECOS_ZIP_FILENAME + "_" + zipDateFormat.format(END_DATE);


//        if (dbManager == null)
//            return false;

        return generateAllReports();
    }

//    public boolean generateResumeUserPrinters(String userName, Date dateStart, Date dateEnd)
//    {
//        this.START_DATE = dateStart;
//        this.END_DATE = dateEnd;
//
//        if (!dbManager.isConnected())
//        {
//             //Open connection with the database
//            if (!dbManager.connect())
//                return false;
//        }
//
//        boolean state = generateResumeUserPrinters(userName);
//
//        //Close connection with the database
//        dbManager.disconnect();
//
//        return state;
//    }

    public boolean generateResumePrinters(Date dateStart, Date dateEnd)
    {
        String company = "";
        String mfpserial = "";
        this.START_DATE = dateStart;
        this.END_DATE = dateEnd;

//        if (!dbManager.isConnected())
//        {
//            //Open connection with the database
//            if (!dbManager.connect())
//            {
//                logger.error("generateResumePrinters() : Cannot open DataBase connection");
//                System.out.println("ERROR: Cannot open DataBase connection");
//                return false;
//            }
//        }

        boolean state = generateResumePrinters(company, mfpserial);

//        //Close connection with the database
//        if (!dbManager.disconnect())
//            logger.error("generateResumePrinters() : Cannot close DataBase connection");

        return state;
    }
    
    public boolean generateResumePrinters(String company, String mfpserial, Date dateStart, Date dateEnd)
    {
        this.START_DATE = dateStart;
        this.END_DATE = dateEnd;

//        if (!dbManager.isConnected())
//        {
//            //Open connection with the database
//            if (!dbManager.connect())
//            {
//                logger.error("generateResumePrinters() : Cannot open DataBase connection");
//                System.out.println("ERROR: Cannot open DataBase connection");
//                return false;
//            }
//        }

        boolean state = generateResumePrinters(company, mfpserial);

//        //Close connection with the database
//        if (!dbManager.disconnect())
//            logger.error("generateResumePrinters() : Cannot close DataBase connection");

        return state;
    }

    public boolean generateResumeUsers(Date dateStart, Date dateEnd)
    {
        String company = "";
        this.START_DATE = dateStart;
        this.END_DATE = dateEnd;

//        if (!dbManager.isConnected())
//        {
//            //Open connection with the database
//            if (!dbManager.connect())
//            {
//                logger.error("generateResumeUsers() : Cannot open DataBase connection");
//                System.out.println("ERROR: Cannot open DataBase connection");
//                return false;
//            }
//        }
        
        boolean state = generateResumeUsers(company);

//        //Close connection with the database
//        if (!dbManager.disconnect())
//            logger.error("generateResumeUsers() : Cannot close DataBase connection");

        return state;
    }
    
    public boolean generateResumeUsers(String company, Date dateStart, Date dateEnd)
    {
        this.START_DATE = dateStart;
        this.END_DATE = dateEnd;

//        if (!dbManager.isConnected())
//        {
//            //Open connection with the database
//            if (!dbManager.connect())
//            {
//                logger.error("generateResumeUsers() : Cannot open DataBase connection");
//                System.out.println("ERROR: Cannot open DataBase connection");
//                return false;
//            }
//        }
        
        boolean state = generateResumeUsers(company);

//        //Close connection with the database
//        if (!dbManager.disconnect())
//            logger.error("generateResumeUsers() : Cannot close DataBase connection");

        return state;
    }

    public boolean generateResumeUserPrinters(Date dateStart, Date dateEnd)
    {
        String company = "";
        this.START_DATE = dateStart;
        this.END_DATE = dateEnd;

//        if (!dbManager.isConnected())
//        {
//            //Open connection with the database
//            if (!dbManager.connect())
//            {
//                logger.error("generateResumeUsers() : Cannot open DataBase connection");
//                System.out.println("ERROR: Cannot open DataBase connection");
//                return false;
//            }
//        }

        boolean state = generateResumeUserPrinters(company);

//        //Close connection with the database
//        if (!dbManager.disconnect())
//            logger.error("generateResumeUsers() : Cannot close DataBase connection");

        return state;
    }


    
    public boolean generateResumeUserPrinters(String company, Date dateStart, Date dateEnd)
    {
        this.START_DATE = dateStart;
        this.END_DATE = dateEnd;

//        if (!dbManager.isConnected())
//        {
//            //Open connection with the database
//            if (!dbManager.connect())
//            {
//                logger.error("generateResumeUsers() : Cannot open DataBase connection");
//                System.out.println("ERROR: Cannot open DataBase connection");
//                return false;
//            }
//        }

        boolean state = generateResumeUserPrinters(company);

//        //Close connection with the database
//        if (!dbManager.disconnect())
//            logger.error("generateResumeUsers() : Cannot close DataBase connection");

        return state;
    }

    public boolean generateResumeUserAccumulated(Date dateStart, Date dateEnd, String tipoVistaInformacion, String type, String tipoInforme)
    {
        String company = "";
        this.START_DATE = dateStart;
        this.END_DATE = dateEnd;

//        if (!dbManager.isConnected())
//        {
//            //Open connection with the database
//            if (!dbManager.connect())
//            {
//                logger.error("generateResumeUsers() : Cannot open DataBase connection");
//                System.out.println("ERROR: Cannot open DataBase connection");
//                return false;
//            }
//        }

        boolean state = generateResumeUserAccumulated(company, tipoVistaInformacion, type, tipoInforme);

//        //Close connection with the database
//        if (!dbManager.disconnect())
//            logger.error("generateResumeUsers() : Cannot close DataBase connection");

        return state;
    }
    
    
    public boolean generateResumeUserAccumulated(String company, Date dateStart, Date dateEnd, String tipoVistaInformacion, String type, String tipoInforme)
    {
        this.START_DATE = dateStart;
        this.END_DATE = dateEnd;

//        if (!dbManager.isConnected())
//        {
//            //Open connection with the database
//            if (!dbManager.connect())
//            {
//                logger.error("generateResumeUsers() : Cannot open DataBase connection");
//                System.out.println("ERROR: Cannot open DataBase connection");
//                return false;
//            }
//        }

        boolean state = generateResumeUserAccumulated(company, tipoVistaInformacion, type, tipoInforme);

//        //Close connection with the database
//        if (!dbManager.disconnect())
//            logger.error("generateResumeUsers() : Cannot close DataBase connection");

        return state;
    }
    
    public boolean generateResumeCostCenters(Date dateStart, Date dateEnd)
    {
        String company = "";
        this.START_DATE = dateStart;
        this.END_DATE = dateEnd;

//        if (!dbManager.isConnected())
//        {
//            //Open connection with the database
//            if (!dbManager.connect())
//            {
//                logger.error("generateResumeUsers() : Cannot open DataBase connection");
//                System.out.println("ERROR: Cannot open DataBase connection");
//                return false;
//            }
//        }

        boolean state = generateResumeCostCenters(company);

//        //Close connection with the database
//        if (!dbManager.disconnect())
//            logger.error("generateResumeUsers() : Cannot close DataBase connection");

        return state;
    }

    public boolean generateResumeCostCenters(String company, Date dateStart, Date dateEnd)
    {
        this.START_DATE = dateStart;
        this.END_DATE = dateEnd;

//        if (!dbManager.isConnected())
//        {
//            //Open connection with the database
//            if (!dbManager.connect())
//            {
//                logger.error("generateResumeUsers() : Cannot open DataBase connection");
//                System.out.println("ERROR: Cannot open DataBase connection");
//                return false;
//            }
//        }

        boolean state = generateResumeCostCenters(company);

//        //Close connection with the database
//        if (!dbManager.disconnect())
//            logger.error("generateResumeUsers() : Cannot close DataBase connection");

        return state;
    }
    
    public boolean generateDetailsUsers(Date dateStart, Date dateEnd)
    {
        this.START_DATE = dateStart;
        this.END_DATE = dateEnd;

//        if (!dbManager.isConnected())
//        {
//            //Open connection with the database
//            if (!dbManager.connect())
//            {
//                logger.error("generateDetailsUsers() : Cannot open DataBase connection");
//                System.out.println("ERROR: Cannot open DataBase connection");
//                return false;
//            }
//        }

        boolean state = true; //generateDetailsUsers();

//        //Close connection with the database
//        if (!dbManager.disconnect())
//            logger.error("generateDetailsUsers() : Cannot close DataBase connection");

        return state;
    }

    public boolean generateDetailsByUser(final String company, final String mfpserial, Date dateStart, Date dateEnd)
    {
        this.START_DATE = dateStart;
        this.END_DATE = dateEnd;

        boolean allOK = true;
        final boolean[] THREAD_STATUS = new boolean[4];


//        if (!dbManager.isConnected())
//        {
//            //Open connection with the database
//            if (!dbManager.connect())
//            {
//                logger.error("generateDetailsByUser() : Cannot open DataBase connection");
//                System.out.println("ERROR: Cannot open DataBase connection");
//                return false;
//            }
//        }

        try
        {
            //Launch a thread to generate the details file part 1
            Thread DetailsUsers1_Thread = new Thread(new Runnable()
            {
                public void run() {
                    if (!(THREAD_STATUS[0] = generateDetailsByUser(DETAILS_FILE_PART1, company, mfpserial)))
                    {
                        logger.error("generateDetailsByUser() : Error creating Users Details file. The File is not created");
                        System.out.println("ERROR: Creating Users Details file. The File is not created");
                    }
                }
            });
            DetailsUsers1_Thread.start();

            //Launch a thread to generate the details file part 2
            Thread DetailsUsers2_Thread = new Thread(new Runnable()
            {
                public void run() {
                    if (!(THREAD_STATUS[1] = generateDetailsByUser(DETAILS_FILE_PART2, company, mfpserial)))
                    {
                        logger.error("generateDetailsByUser() : Error creating Users Details file. The File is not created");
                        System.out.println("ERROR: Creating Users Details file. The File is not created");
                    }
                }
            });
            DetailsUsers2_Thread.start();

            //Launch a thread to generate the details file part 3
            Thread DetailsUsers3_Thread = new Thread(new Runnable()
            {
                public void run() {
                    if (!(THREAD_STATUS[2] = generateDetailsByUser(DETAILS_FILE_PART3, company, mfpserial)))
                    {
                        logger.error("generateDetailsByUser() : Error creating Users Details file. The File is not created");
                        System.out.println("ERROR: Creating Users Details file. The File is not created");
                    }
                }
            });
            DetailsUsers3_Thread.start();

            DetailsUsers1_Thread.join();

            //Launch a thread to generate the details file part 4
            Thread DetailsUsers4_Thread = new Thread(new Runnable()
            {
                public void run() {
                    if (!(THREAD_STATUS[3] = generateDetailsByUser(DETAILS_FILE_PART4, company, mfpserial)))
                    {
                        logger.error("generateDetailsByUser() : Error creating Users Details file. The File is not created");
                        System.out.println("ERROR: Creating Users Details file. The File is not created");
                    }
                }
            });
            DetailsUsers4_Thread.start();

            //Wait all threads to finish
            DetailsUsers2_Thread.join();
            DetailsUsers3_Thread.join();
            DetailsUsers4_Thread.join();


            for (int i = 0; i < THREAD_STATUS.length; i++)
                allOK = allOK & THREAD_STATUS[i];

            if (allOK)
            {
                List detailFiles = new ArrayList();
                detailFiles.add(IExtractorManager.DETAIL_USERS_FILENAME + "_" + DETAILS_FILE_PART1);
                detailFiles.add(IExtractorManager.DETAIL_USERS_FILENAME + "_" + DETAILS_FILE_PART2);
                detailFiles.add(IExtractorManager.DETAIL_USERS_FILENAME + "_" + DETAILS_FILE_PART3);
                detailFiles.add(IExtractorManager.DETAIL_USERS_FILENAME + "_" + DETAILS_FILE_PART4);

                if (!(allOK = fileManager.concatFiles(detailFiles, REPORTS_OUTPUT_PATH, IExtractorManager.DETAIL_USERS_FILENAME)))
                {
                    logger.error("generateDetailsByUser() : Error merging parts to Users Details file. The File is not created");
                    System.out.println("ERROR: Merging parts to Users Details file. The File is not created");
                }
            }
        }
        catch (Exception ex)
        {
            logger.error(ex.getMessage());
            logger.error(ex.getStackTrace());
            allOK = false;
        }
        finally
        {
//            //Close connection with the database
//            if (!dbManager.disconnect())
//                logger.error("generateAllReports() : Cannot close DataBase connection");

            return allOK;
        }
    }

    public boolean generateDetailsPrinters(Date dateStart, Date dateEnd)
    {
        this.START_DATE = dateStart;
        this.END_DATE = dateEnd;

//        if (!dbManager.isConnected())
//        {
//            //Open connection with the database
//            if (!dbManager.connect())
//            {
//                logger.error("generateDetailsPrinters() : Cannot open DataBase connection");
//                System.out.println("ERROR: Cannot open DataBase connection");
//                return false;
//            }
//        }

        boolean state = generateDetailsPrinters();

//        //Close connection with the database
//        if (!dbManager.disconnect())
//            logger.error("generateDetailsPrinters() : Cannot close DataBase connection");

        return state;
    }

    public String getUsersZipPath()
    {
        String path = ZIP_OUTPUT_PATH;

        if (!path.isEmpty())
        {
            if (path.lastIndexOf("/") != (path.length()-1))
                path = path + "/";
        }

        path = path + usersZipName + ".zip";
        return path;
    }

    public String getPrintersZipPath()
    {
        String path = ZIP_OUTPUT_PATH;

        if (!path.isEmpty())
        {
            if (path.lastIndexOf("/") != (path.length()-1))
                path = path + "/";
        }

        path = path + printersZipName + ".zip";
        return path;
    }

    public String getCECOsZipPath()
    {
        String path = ZIP_OUTPUT_PATH;

        if (!path.isEmpty())
        {
            if (path.lastIndexOf("/") != (path.length()-1))
                path = path + "/";
        }

        path = path + cecosZipName + ".zip";
        return path;
    }

    public String getResumeUserPrintersFilePath(String userName)
    {
        String path = REPORTS_OUTPUT_PATH;

        if (!path.isEmpty())
        {
            if (path.lastIndexOf("/") != (path.length()-1))
                path = path + "/";
        }

        if (userName.equals(""))
            path = path + "Desconocido" + IExtractorManager.RESUME_USER_PRINTERS_FILENAME + ".csv";
        else
            path = path + userName + IExtractorManager.RESUME_USER_PRINTERS_FILENAME + ".csv";

        return path;
    }

    public String getResumePrintersFilePath()
    {
        String path = REPORTS_OUTPUT_PATH;

        if (!path.isEmpty())
        {
            if (path.lastIndexOf("/") != (path.length()-1))
                path = path + "/";
        }

        path = path + IExtractorManager.RESUME_PRINTERS_FILENAME + ".csv";
        return path;
    }

    public String getResumeUsersFilePath()
    {
        String path = REPORTS_OUTPUT_PATH;

        if (!path.isEmpty())
        {
            if (path.lastIndexOf("/") != (path.length()-1))
                path = path + "/";
        }

        path = path + IExtractorManager.RESUME_USERS_FILENAME + ".csv";
        return path;
    }
    
    public String getPrintersFilePath()
    {
        String path = REPORTS_OUTPUT_PATH;

        if (!path.isEmpty())
        {
            if (path.lastIndexOf("/") != (path.length()-1))
                path = path + "/";
        }

        path = path + IExtractorManager.PRINTERS_FILENAME + ".csv";
        return path;
    }

    public String getResumeUserPrintersFilePath()
    {
        String path = REPORTS_OUTPUT_PATH;

        if (!path.isEmpty())
        {
            if (path.lastIndexOf("/") != (path.length()-1))
                path = path + "/";
        }

        path = path + IExtractorManager.RESUME_USER_PRINTERS_FILENAME + ".csv";
        return path;
    }

    public String getResumeCostCentersFilePath()
    {
        String path = REPORTS_OUTPUT_PATH;

        if (!path.isEmpty())
        {
            if (path.lastIndexOf("/") != (path.length()-1))
                path = path + "/";
        }

        path = path + IExtractorManager.RESUME_CECO_FILENAME + ".csv";
        return path;
    }

    public String getDetailsUsersFilePath()
    {
        String path = REPORTS_OUTPUT_PATH;

        if (!path.isEmpty())
        {
            if (path.lastIndexOf("/") != (path.length()-1))
                path = path + "/";
        }

        path = path + IExtractorManager.DETAIL_USERS_FILENAME + ".csv";
        return path;
    }

    public String getDetailsPrintersFilePath()
    {
        String path = REPORTS_OUTPUT_PATH;

        if (!path.isEmpty())
        {
            if (path.lastIndexOf("/") != (path.length()-1))
                path = path + "/";
        }

        path = path + IExtractorManager.DETAIL_PRINTERS_FILENAME + ".csv";
        return path;
    }


    public static void main(String[] args)
    {
//        try
//        {
            PropertyConfigurator.configure("conf/log4j.properties");

            Extractor extractor = new Extractor("conf/config.properties");
            extractor.generateAllReports();
            
//            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
//            Date start = dateFormat.parse("2010-06-15");
//            Date end = dateFormat.parse("2010-07-12");
//
//            extractor.generateAllReports(start, end);
//
//            extractor.generateDetailsByUser();
//
//            extractor.generateResumePrinters(start, end);
//            extractor.generateResumeUsers(start, end);
//            extractor.generateDetailsUsers(start, end);
//            extractor.generateResumeUserPrinters("", start, end);
//
//            end = dateFormat.parse("2010-05-5");
//            extractor.generateDetailsByUser();
//
//
//            extractor.generateDetailsPrinters(start, end);

            //logger.info("getDetailsPrintersFilePath: " + extractor.getDetailsPrintersFilePath());
            logger.info("getDetailsUsersFilePath: " + extractor.getDetailsUsersFilePath());
            logger.info("getResumePrintersFilePath: " + extractor.getResumePrintersFilePath());
            logger.info("getResumeUsersFilePath: " + extractor.getResumeUsersFilePath());
            logger.info("getResumeUserPrintersFilePath: " + extractor.getResumeUserPrintersFilePath(""));
            logger.info("getResumeCostCentersFilePath: " + extractor.getResumeCostCentersFilePath());
            logger.info("getPrintersZipPath: " + extractor.getPrintersZipPath());
            logger.info("getUsersZipPath: " + extractor.getUsersZipPath());
//        }
//        catch (ParseException ex)
//        {
//            logger.error(ex.getMessage());
//            logger.error(ex.getStackTrace());
//        }

    }

//    private void addOfflinePrintersDetails2File(final String fileName, final actionType actionType, final List dbMfps)
//    {
//        Printer p;
//        Action offlineAction;
//        IDataBaseManager dbManager = InterfaceManager.getDataBaseManagerInstance(this.CONFIG_FILE);
//
//        if (!dbManager.isConnected())
//        {
//            //Open connection with the database
//            if (!dbManager.connect())
//            {
//                logger.error("addOfflinePrintersDetails2File() : Cannot open DataBase connection");
//                System.out.println("ERROR: Cannot open DataBase connection");
//                return;
//            }
//        }
//
//
//        List offlineMfps = dbManager.getMFPsOnState(Printer.OFFLINE);
//
//        ListIterator it = offlineMfps.listIterator();
//        while(it.hasNext())
//        {
//            p = (Printer)it.next();
//            if (!dbMfps.contains(p.getMfpSerial()))
//            {
//                if (p.getLastBWCounter() != 0)
//                {
//                    offlineAction = new Action();
//                    offlineAction.setColorMode("blackandwhite");
//                    offlineAction.setMFPserial(p.getMfpSerial());
//                    offlineAction.setNumPages(p.getLastBWCounter());
//                    offlineAction.setType("printer.print");
//                    offlineAction.setDate(END_DATE);
//                    offlineAction.setPrinter(p);
//                    fileManager.appendAction2File(fileName, offlineAction, actionType);
//                }
//
//                if (p.getLastColorCounter() != 0)
//                {
//                    offlineAction = new Action();
//                    offlineAction.setColorMode("color");
//                    offlineAction.setMFPserial(p.getMfpSerial());
//                    offlineAction.setNumPages(p.getLastColorCounter());
//                    offlineAction.setType("printer.print");
//                    offlineAction.setDate(END_DATE);
//                    offlineAction.setPrinter(p);
//                    fileManager.appendAction2File(fileName, offlineAction, actionType);
//                }
//            }
//        }
//        //Close connection with the database
//        if (!dbManager.disconnect())
//            logger.error("addOfflinePrintersDetails2File() : Cannot close DataBase connection");
//    }

//    private void addOfflinePrintersResume2File(final String fileName, final resumeType resumeType, final List mfplist, String company)
//    {
//        Printer p;
//        User u;
//        Resume resume;
//        List dbMfps;
//        IDataBaseManager dbManager = InterfaceManager.getDataBaseManagerInstance(this.getCONFIG_FILE(), this.getSQL_FILE());
//
//        if (!dbManager.isConnected())
//        {
//            //Open connection with the database
//            if (!dbManager.connect())
//            {
//                logger.error("addOfflinePrintersResume2File() : Cannot open DataBase connection");
//                System.out.println("ERROR: Cannot open DataBase connection");
//                return;
//            }
//        }
//        List offlineMfps = null;
//        if (company.equalsIgnoreCase(""))
//          offlineMfps = dbManager.getMFPsOnState(Printer.OFFLINE);
//        else
//          offlineMfps = dbManager.getMFPsOnState(Printer.OFFLINE, company);
//        List onlineMfps;
//
//        if (mfplist == null)
//            dbMfps = dbManager.getMFPs();
//        else
//            dbMfps = mfplist;
//
//        ListIterator it = offlineMfps.listIterator();
//        while(it.hasNext())
//        {
//            p = (Printer)it.next();
//            if (!dbMfps.contains(p.getMfpSerial()))
//            {
//                resume = new Resume(p.getMfpSerial(), END_DATE);
//                resume.setPrinter(p);
//
//                // ***************************** ADD FOR BBVA *******
//                if (resumeType.equals(Resume.resumeType.USER_RESUME))
//                    resume.setId(p.getHostname());
//                // ***************************** ADD FOR BBVA *******
//
//                u = new User("", "", "");
//                resume.setUser(u);
//
//                fileManager.appendResume2File(fileName, resume);
//            }
//        }
//
//        if(resumeType.equals(Resume.resumeType.MFP_RESUME))
//        {
//            if (company.equalsIgnoreCase(""))
//                onlineMfps = dbManager.getMFPsOnState(Printer.ONLINE);
//            else
//                onlineMfps = dbManager.getMFPsOnState(Printer.ONLINE, company);
//            
//            it = onlineMfps.listIterator();
//            while(it.hasNext())
//            {
//                p = (Printer)it.next();
//                if (!dbMfps.contains(p.getMfpSerial()))
//                {
//                    resume = new Resume(p.getMfpSerial(), END_DATE);
//                    resume.setPrinter(p);
//
//                    u =new User("", "", "");
//                    resume.setUser(u);
//
//                    fileManager.appendResume2File(fileName, resume);
//                }
//            }
//        }
//        //Close connection with the database
//        if (!dbManager.disconnect())
//            logger.error("addOfflinePrintersResume2File() : Cannot close DataBase connection");
//
//    }

    /**
     * @return the CONFIG_FILE
     */
    public String getCONFIG_FILE() {
        return CONFIG_FILE;
    }

    /**
     * @param CONFIG_FILE the CONFIG_FILE to set
     */
    public void setCONFIG_FILE(String CONFIG_FILE) {
        this.CONFIG_FILE = CONFIG_FILE;
    }

    /**
     * @return the SQL_FILE
     */
    public String getSQL_FILE() {
        return SQL_FILE;
    }

    /**
     * @param SQL_FILE the SQL_FILE to set
     */
    public void setSQL_FILE(String SQL_FILE) {
        this.SQL_FILE = SQL_FILE;
    }

    private RemoteccPlugin getPluginInstance()
    {
        // ***************************** ADD FOR BBVA *******************************************************
        RemoteccPlugin plugin = null;
        
        try
        {
            plugin = new RemoteccPlugin(this.getCONFIG_FILE(), this.getSQL_FILE());
        }
        catch (FileNotFoundException ex)
        {
            logger.error("getPluginInstance() : Cannot open External DataBase connection");
            logger.error(ex.getMessage());
            logger.error(ex.getStackTrace());
            System.out.println("ERROR: Cannot open External DataBase connection");
        }
        catch (IOException ex)
        {
            logger.error("getPluginInstance() : Cannot open External DataBase connection");
            logger.error(ex.getMessage());
            logger.error(ex.getStackTrace());
            System.out.println("ERROR: Cannot open External DataBase connection");
        }
        return plugin;
        // ***************************** ADD FOR BBVA *******************************************************
    }

    private User getUserObject(RemoteccPlugin bbvaPlug, String userName)
    {
        User user;

        if (this.objectManager.getUser_CCHash().containsKey(userName))
            return this.objectManager.getUser_CCHash().get(userName);
        else
        {
            if (bbvaPlug != null && !userName.isEmpty())
            {
                user = bbvaPlug.getUserByID(userName);
                if (user == null)
                    user = new User(userName, "", "","");
                else
                    this.objectManager.fillUser_CCHash(user);
            }
            else
                user = new User(userName, "", "","");

            return user;
        }

    }
    
      private User getUserByID(String username){
        User u = new User(username,"","","");
        
        if (objectManager.getCCUserRegisterHash().containsKey(username)){
            u = (User)objectManager.getCCUserRegisterHash().get(username);
        }
        if (u.getUsername().contains("noname")){
            if (u.getUsername().split("_").length>2){
                u.setUsername(u.getUsername().split("_")[2]);
            }else
                u.setUsername("");
        }

        return u;
    }
      
    private static SearchControls getSimpleSearchControls() {
        SearchControls searchControls = new SearchControls();
        searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
        searchControls.setTimeLimit(30000);
        return searchControls;
    }
    
    private void getCCs(){
                //Leer fichero properties
        Properties prop = new Properties();
        try {
            prop.load(new FileInputStream(this.getCONFIG_FILE()));
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(Extractor.class.getName()).log(Level.SEVERE, null, ex);
        }

        CSV_CCUSER_FOLDER = prop.getProperty("CSV.CCUserPath");
        CC_OBTAINING_ACTIVATION_MODE = prop.getProperty("Mode.CCObtaining");
       
        System.out.println("CC_OBTAINING_ACTIVATION_MODE: " + CC_OBTAINING_ACTIVATION_MODE);
        if (CC_OBTAINING_ACTIVATION_MODE.equalsIgnoreCase("CSV")){
            if (!checkCCFiles())
            {
                System.out.println("ERROR: Procesing CC " + CSV_CCUSER_FOLDER + " User csv file");
            }
        }else if (CC_OBTAINING_ACTIVATION_MODE.equalsIgnoreCase("extDB")){
            remoteccplugin = getPluginInstance();
            System.out.println("INFO: [Extracto: getCCs] We will get CC by means of External DB");
        }else if (CC_OBTAINING_ACTIVATION_MODE.equalsIgnoreCase("AD")) {
            if (!checkCCActiveDirectory()) {
                System.out.println("ERROR: Obtaining CC info from Active Directory");
            }
        }
    }
    
     private boolean checkCCFiles()
    {
//        String value;
        List files = new LinkedList();
        System.out.println("-> Checking " + CSV_CCUSER_FOLDER + " cc files");

        files = getFileNames(CSV_CCUSER_FOLDER);
        Iterator it = files.iterator();
        while(it.hasNext()){
        
            String strFile = it.next().toString();
            if (!fileManager.openFile("", strFile, false))
            {
                System.out.println("ERROR: Cannot open " + strFile + " file");
                return false;
            }

            while (fileManager.hasMoreLines(strFile))
            {
                User u = new User("", "", "","");
                u.setUsername(fileManager.readAttribute(strFile, "USERNAME").toLowerCase());
                u.setCostCenter(fileManager.readAttribute(strFile, "CC"));
                u.setDepartment(fileManager.readAttribute(strFile, "CCNAME"));
                u.setFullname(fileManager.readAttribute(strFile, "FULLNAME"));

                if (!objectManager.getCCUserRegisterHash().containsKey(u.getUsername()))
                {
                    System.out.println("-> " + strFile + " master file has new MFP " + u.getUsername() + " ( CC: " +  u.getCostCenter() + ")");
                    objectManager.add2CCUserHash(u);
                }
//                else if (!p.equals((Printer)objectManager.getMfpRegisterHash().get(p.getMfpSerial())))
//                {
//                    logger.info(MFP_INVENTORY_FILE + " master file has updated info for MFP " + p.getIpAddress() + " (" + p.getMfpSerial() + ")");
//                    System.out.println("-> " + MFP_INVENTORY_FILE + " master file has updated info for MFP " + p.getIpAddress() + " (" + p.getMfpSerial() + ")");
//                    objectManager.update2MfpRegisterHash(p);
//                }
            }
            //Close the input file
            if (!fileManager.closeFile(strFile)){
                System.err.println("checkCCFiles() : Cannot close " + strFile + " file");
                return false;
            }
        }

        return true;
    }
     
     private boolean checkCCActiveDirectory()
     {
         String provider = "ldap://" + AD_HOST + ":" + AD_PORT;
         Hashtable env = new Hashtable(11);

         env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
         env.put(Context.PROVIDER_URL, provider);
         env.put("com.sun.jndi.ldap.connect.timeout", "10000");

         if (AD_AUTHSIMPLE.equalsIgnoreCase("true")) {
             System.out.println("Authentication: SIMPLE");
             env.put(Context.SECURITY_AUTHENTICATION, "simple");
             env.put(Context.SECURITY_PRINCIPAL, AD_USER);
             env.put(Context.SECURITY_CREDENTIALS, AD_PASS);
         } 
         else {
             System.out.println("Authentication: NONE");
             env.put(Context.SECURITY_AUTHENTICATION, "none");
         }
         
         try {
             System.out.println("Creating initial context...");
             DirContext ctx = new InitialDirContext(env);
             System.out.println("Initial context created");

             // TODO: ver que filtro aplicamos
             NamingEnumeration namingEnum = ctx.search(AD_BASE, AD_FILTER, getSimpleSearchControls());

             while (namingEnum.hasMore()) {
                 SearchResult sRes = (SearchResult) namingEnum.next();
                 Attributes attrs = sRes.getAttributes();
                 
                 User u = new User("", "", "", "");
                 // TODO: debemos saber a que campos del AD se corresponden el nombre y CC del user
                 u.setUsername(attrs.get(AD_USER_USERNAME).toString());
                 u.setCostCenter(attrs.get(AD_USER_CC).toString());
                 u.setDepartment(attrs.get(AD_USER_CCNAME).toString());
                 u.setFullname(attrs.get(AD_USER_FULLNAME).toString());
                 
                 if (!objectManager.getCCUserRegisterHash().containsKey(u.getUsername()))
                 {
                     System.out.println("-> " + AD_HOST + " ActiveDirectory has new user " + u.getUsername() + " ( CC: " + u.getCostCenter() + ")");
                     objectManager.add2CCUserHash(u);
                 }
             }
             
             return true;
             
         } catch (Exception e) {
             System.out.println("checkCCActiveDirectory() : An error has ocurred: " + e);
             return false;
         }
     }
     
    public List getFileNames(String spoolPath)
    {
        File[] spoolFiles = null;
        List filesList = new ArrayList();
        File f = new File(spoolPath);
        spoolFiles = f.listFiles();

        for (int i = 0; i < spoolFiles.length; i++)
        { 
            System.out.println("Extract process CSV Files: " + spoolFiles[i].getName());
            filesList.add(spoolPath+"/"+spoolFiles[i].getName());          
        }

        return filesList;
    }
    
    public boolean generatePrinters()
    {
        List printers;

        DataBaseManager dbManager = InterfaceManager.getDataBaseManagerInstance(this.getCONFIG_FILE(), this.getSQL_FILE());

        if (!dbManager.isConnected())
        {
            //Open connection with the database
            if (!dbManager.connect())
            {
                logger.error("generatePrinters() : Cannot open DataBase connection");
                System.out.println("ERROR: Cannot open DataBase connection");
                return false;
            }
        }


        printers = dbManager.getRegisterMFPs();
  
         
                
        //System.out.println("Número de MFPs --> " + printers.size());
        
        if (fileManager.createFile(REPORTS_OUTPUT_PATH, IExtractorManager.PRINTERS_FILENAME))
        {
            logger.info("Creating MFPs file. Start Time : " + new Date().toString());
            System.out.println("-> Creating MFPs file. Start Time : " + new Date().toString());

            String text = "IP_ADDRESS;MODEL_NAME;STATUS;HOSTNAME;LOCATION;" +
                    "ZONE;SERIAL_NUMBER;COMPANY\n";
            fileManager.appendText2File(IExtractorManager.PRINTERS_FILENAME, text);


            ListIterator printersIterator = printers.listIterator();
            while (printersIterator.hasNext())
            {
                text = getTextToInsert(printersIterator);
                //System.out.println("Escribiendo entrada de impresora: " + printerName);
                fileManager.appendText2File(IExtractorManager.PRINTERS_FILENAME, text);
            }
            //Close connection with the database
            if (!dbManager.disconnect())
                logger.error("generatePrinters() : Cannot close DataBase connection");


            logger.info("Creating MFPs file. End Time : " + new Date().toString());
            System.out.println("-> Creating MFPs file. End Time : " + new Date().toString());

            return fileManager.closeFile(IExtractorManager.PRINTERS_FILENAME);
        }
        else
        {
            //Close connection with the database
            if (!dbManager.disconnect())
                logger.error("generatePrinters() : Cannot close DataBase connection");

            return false;
        }
    }
    
    public String getTextToInsert(ListIterator list){
        String strReturn = "";
        
        strReturn = strReturn + (String)list.next() + ";";
        strReturn = strReturn + (String)list.next() + ";";
        strReturn = strReturn + (String)list.next() + ";";
        strReturn = strReturn + (String)list.next() + ";";
        strReturn = strReturn + (String)list.next() + ";";
        strReturn = strReturn + (String)list.next() + ";";
        strReturn = strReturn + (String)list.next() + ";";
        strReturn = strReturn + (String)list.next() + "\n";
        
        return strReturn;
    }

    public boolean generateResumeUsers() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public boolean generateResumePrinters() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public boolean generateResumeUserPrinters() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public boolean generateDetailsByUser(Date dateStart, Date dateEnd) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public boolean generateResumeCostCenters() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public boolean generateDetailsPrinters() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public boolean generateDetailsUsers() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String getResumeUserAccumulatedFilePath() {
        String path = REPORTS_OUTPUT_PATH;

        if (!path.isEmpty())
        {
            if (path.lastIndexOf("/") != (path.length()-1))
                path = path + "/";
        }

        path = path + IExtractorManager.RESUME_USER_ACCUMULATED_FILENAME + ".csv";
        return path;
    }

    public String getResumeUserAccumulatedFilePath(String userName)
    {
        String path = REPORTS_OUTPUT_PATH;

        if (!path.isEmpty())
        {
            if (path.lastIndexOf("/") != (path.length()-1))
                path = path + "/";
        }

        if (userName.equals(""))
            path = path + "Desconocido" + IExtractorManager.RESUME_USER_ACCUMULATED_FILENAME + ".csv";
        else
            path = path + userName + IExtractorManager.RESUME_USER_ACCUMULATED_FILENAME + ".csv";

        return path;
    }
}
