/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ricoh.accounting.utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 *
 * @author david.yunta
 */
public class CalculosFechas {
    
//    /**
//     * Devuelve el número de semanas reales del año que hay entre dos fechas.
//     * @param f_Inicio fecha inicio
//     * @param f_Fin fecha fin
//     * @return número de semanas reales del año
//     */
//    public static int CalculoTotalNumeroSemanas (Date f_Inicio, Date f_Fin)
//    {   
//        List listaNumerosSemanas = CalculoNumeroSemanas(f_Inicio, f_Fin);
//        if (listaNumerosSemanas != null)
//            return listaNumerosSemanas.size();
//        else return 0;
//    }
    
    /**
     * Devuelve el número de la semana del año de todas las semanas que hay entre dos fechas.
     * @param f_Inicio fecha inicio
     * @param f_Fin fecha fin
     * @return lista de números correspondientes a los números de las semana del año
     */
    public static List<String> CalculoNumeroSemanas (Date f_Inicio, Date f_Fin)
    {
        List<String> listaNumerosSemanas = new ArrayList();
 
        Calendar calendarInicio = Calendar.getInstance();
        calendarInicio.setFirstDayOfWeek( Calendar.MONDAY);
        calendarInicio.setMinimalDaysInFirstWeek( 4 );
        calendarInicio.setTime(f_Inicio);
        
        Calendar calendarFin = Calendar.getInstance();
        calendarFin.setFirstDayOfWeek( Calendar.MONDAY);
        calendarFin.setMinimalDaysInFirstWeek( 4 );
        calendarFin.setTime(f_Fin);
        
        int numberWeekOfYearFechaInicio = calendarInicio.get(Calendar.WEEK_OF_YEAR);
        listaNumerosSemanas.add(String.valueOf(numberWeekOfYearFechaInicio) + " (" + (calendarInicio.get(Calendar.MONTH) + 1) + "-" + calendarInicio.get(Calendar.YEAR) + ") ");
        
        
        calendarInicio.add(Calendar.DATE, 7);
        while((calendarFin.getTimeInMillis() - calendarInicio.getTimeInMillis()) > 0){
           numberWeekOfYearFechaInicio = calendarInicio.get(Calendar.WEEK_OF_YEAR);
           listaNumerosSemanas.add(String.valueOf(numberWeekOfYearFechaInicio) + " (" + (calendarInicio.get(Calendar.MONTH) + 1) + "-" + calendarInicio.get(Calendar.YEAR) + ") ");
           calendarInicio.add(Calendar.DATE, 7);
        }
        
        int numberWeekOfYearFechaFin = calendarFin.get(Calendar.WEEK_OF_YEAR);
        if(numberWeekOfYearFechaFin != numberWeekOfYearFechaInicio)
            listaNumerosSemanas.add(String.valueOf(numberWeekOfYearFechaFin) + " (" + (calendarFin.get(Calendar.MONTH) + 1) + "-" + calendarFin.get(Calendar.YEAR) + ") ");
        
        return listaNumerosSemanas;
    }
    
    
//    /**
//     * Devuelve el número de meses reales del año que hay entre dos fechas.
//     * @param f_Inicio fecha inicio
//     * @param f_Fin fecha fin
//     * @return número de semanas reales del año
//     */
//    public static int CalculoTotalNumeroMeses (Date f_Inicio, Date f_Fin)
//    {   
//        List listaNumerosMeses = CalculoNumeroMeses(f_Inicio, f_Fin);
//        if (listaNumerosMeses != null)
//            return listaNumerosMeses.size();
//        else return 0;
//    }
    
    /**
     * Devuelve el número del mes del año de todos los meses que hay entre dos fechas.
     * @param f_Inicio fecha inicio
     * @param f_Fin fecha fin
     * @return lista de números correspondientes a los números de los meses del año
     */
    public static List<String> CalculoNumeroMeses (Date f_Inicio, Date f_Fin)
    {
        List<String> listaNumerosSemanas = new ArrayList();
 
        Calendar calendarInicio = Calendar.getInstance();
        calendarInicio.setFirstDayOfWeek( Calendar.MONDAY);
        calendarInicio.setMinimalDaysInFirstWeek( 4 );
        calendarInicio.setTime(f_Inicio);
        
        Calendar calendarFin = Calendar.getInstance();
        calendarFin.setFirstDayOfWeek( Calendar.MONDAY);
        calendarFin.setMinimalDaysInFirstWeek( 4 );
        calendarFin.setTime(f_Fin);
        
        int numberWeekOfYearFechaInicio = calendarInicio.get(Calendar.MONTH) + 1;
        listaNumerosSemanas.add(String.valueOf(numberWeekOfYearFechaInicio) + " (" + calendarInicio.get(Calendar.YEAR) + ") ");
        
        
        calendarInicio.add(Calendar.MONTH, 1);
        while((calendarFin.getTimeInMillis() - calendarInicio.getTimeInMillis()) > 0){
           numberWeekOfYearFechaInicio = calendarInicio.get(Calendar.MONTH) + 1;
           listaNumerosSemanas.add(String.valueOf(numberWeekOfYearFechaInicio) + " (" + calendarInicio.get(Calendar.YEAR) + ") ");
           calendarInicio.add(Calendar.MONTH, 1);
        }
        
        int numberWeekOfYearFechaFin = calendarFin.get(Calendar.MONTH) + 1;
        if(numberWeekOfYearFechaFin != numberWeekOfYearFechaInicio)
            listaNumerosSemanas.add(String.valueOf(numberWeekOfYearFechaFin) + " (" + calendarFin.get(Calendar.YEAR) + ") ");
        
        return listaNumerosSemanas;
    }
    
    
    /**
     * Devuelve el número de la semana del año de la fecha.
     * @param date fecha
     * @return Número de la semana del año
     */
    public static int GetNumeroSemana(Date date)
    {
 
        Calendar calendar = Calendar.getInstance();
        calendar.setFirstDayOfWeek( Calendar.MONDAY);
        calendar.setMinimalDaysInFirstWeek( 4 );
        calendar.setTime(date);

        int numberWeekOfYear = calendar.get(Calendar.WEEK_OF_YEAR);

        return numberWeekOfYear;
    }
}
