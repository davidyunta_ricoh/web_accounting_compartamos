package ricoh.accounting.actions;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import javax.servlet.http.*;
import ricoh.accounting.database.DataBaseManager;
import ricoh.accounting.ifactory.InterfaceManager;
import ricoh.accounting.objects.Printer;
import ricoh.accounting.objects.Resume;
import ricoh.accounting.objects.Resume.resumeType;
import ricoh.accounting.objects.User;

/**
 *
 * @author Alexis.Hidalgo
 */
public class ShowMFPs extends org.apache.struts.action.Action {

    private static final String SUCCESS = "SUCCESS";
    private static final String FAIL = "FAIL";

    private DataBaseManager dbManager;


    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
    {
        String printerName;
        Resume resume;
        List printersWithResume, resumeList = new ArrayList();
        Map registeredMFPs;
        Date START_DATE, END_DATE, EXEC_DATE;
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Printer printer;
        List printers;

        Runtime.getRuntime().gc();

        HttpSession session = request.getSession(true);
        
        String company = (String)session.getAttribute("company");
        Boolean isAdmin = (Boolean)session.getAttribute("isAdmin");
        
        String strDateIni = request.getParameter("iniDate");
        String strDateEnd = request.getParameter("endDate");
        String mfpserial = request.getParameter("mfp");

        if (strDateIni.isEmpty() || strDateEnd.isEmpty())
        {
            session.setAttribute("errorCode", "empty_date");
            return mapping.findForward(FAIL);
        }

        try
        {
            START_DATE = dateFormat.parse(strDateIni);
            END_DATE = dateFormat.parse(strDateEnd);

            EXEC_DATE = getExecutingDate();
        }
        catch (ParseException ex)
        {
            System.out.println(ex.getMessage());
            System.out.println(ex.getStackTrace());

            session.setAttribute("errorCode", "parse_fail");
            return mapping.findForward(FAIL);
        }

        if (START_DATE.after(END_DATE))
        {
            session.setAttribute("errorCode", "start_after_end");
            return mapping.findForward(FAIL);
        }
        else if (END_DATE.after(EXEC_DATE))
        {
            session.setAttribute("errorCode", "end_after_today");
            return mapping.findForward(FAIL);
        }

        try
        {
            dbManager = InterfaceManager.getDataBaseManagerInstance(
                    servlet.getServletContext().getRealPath("/WEB-INF/config.properties"),
                    servlet.getServletContext().getRealPath("/WEB-INF/sql.properties"));

            if (!dbManager.isConnected())
            {
                //Open connection with the database
                if (!dbManager.connect())
                {
                    //logger.error("generateAllReports() : Cannot open DataBase connection");
                    System.out.println("ERROR: Cannot open DataBase connection");
                    session.setAttribute("errorCode", "db_error");
                    return mapping.findForward(FAIL);
                }
            }
           if (isAdmin){
                if (mfpserial == null){
                   printers = dbManager.getMFPs();//registeredMFPs = dbManager.getRegisteredMFPs();
//                   printers = addOfflinePrintersResume2File(resumeType.MFP_RESUME, printers, "");
                   session.removeAttribute("mfp");
                }else{
                   printers = dbManager.getMFPs(mfpserial);
                   session.setAttribute("mfp", mfpserial);
                }
           }else{
               if (mfpserial == null){
                    printers = dbManager.getMFPs_PerCompany(company);//registeredMFPs = dbManager.getRegisteredMFPs(company);
//                    printers = addOfflinePrintersResume2File(resumeType.MFP_RESUME, printers, company);
                    session.removeAttribute("mfp");
               }else{
                   printers = dbManager.getMFPs_PerCompany(company, mfpserial);
                   session.setAttribute("mfp", mfpserial);
               }
           }

            printersWithResume = dbManager.getMFPs();

//            Iterator mfpsIterator = registeredMFPs.keySet().iterator();
            ListIterator mfpsIterator = printers.listIterator();
            while (mfpsIterator.hasNext())
            {
                printerName = (String)mfpsIterator.next();
                
                if (printersWithResume.contains(printerName))
                    resume = dbManager.getTotalsBetweenDates(printerName, START_DATE, END_DATE, resumeType.MFP_RESUME);
                else
                {
                    resume = new Resume(printerName, END_DATE);
                    printer = dbManager.getMFPbyID(printerName);
                    resume.setPrinter(printer);
                }

                resumeList.add(resume);
            }
            //Close connection with the database
            if (!dbManager.disconnect()){}
                //logger.error("generateAllReports() : Cannot close DataBase connection");

        }
        catch (Exception ex)
        {
            System.out.println(ex.getMessage());
            System.out.println(ex.getStackTrace());

            //Close connection with the database
            if (!dbManager.disconnect()){}
                //logger.error("generateAllReports() : Cannot close DataBase connection");

            session.setAttribute("errorCode", "process_error");
            return mapping.findForward(FAIL);
        }

        session.setAttribute("resumeList", resumeList);
        session.setAttribute("iniDate", strDateIni);
        session.setAttribute("endDate", strDateEnd);
        session.removeAttribute("errorCode");

        return mapping.findForward(SUCCESS);
    }

    private Date getExecutingDate() throws ParseException
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = dateFormat.format(new Date());
        return dateFormat.parse(dateString);
    }

    private List addOfflinePrintersResume2File(final resumeType resumeType, final List mfplist, String company)
    {
        Printer p;
        User u;
        Resume resume;
        List dbMfps, returnMfpsList = null;
        

        if (!dbManager.isConnected())
        {
            //Open connection with the database
            if (!dbManager.connect())
            {
//                logger.error("addOfflinePrintersResume2File() : Cannot open DataBase connection");
                System.out.println("ERROR: Cannot open DataBase connection");
                return returnMfpsList;
            }
        }
        List offlineMfps = null;
        if (company.equalsIgnoreCase(""))
          offlineMfps = dbManager.getMFPsOnState(Printer.OFFLINE);
        else
          offlineMfps = dbManager.getMFPsOnState(Printer.OFFLINE, company);
        List onlineMfps;

//        if (mfplist == null)
//            dbMfps = dbManager.getMFPs();
//        else
            dbMfps = mfplist;

        returnMfpsList = dbMfps;
        
        ListIterator it = offlineMfps.listIterator();
        while(it.hasNext())
        {
            p = (Printer)it.next();
            if (!dbMfps.contains(p.getMfpSerial()))
            {
//                resume = new Resume(p.getMfpSerial(), END_DATE);
//                resume.setPrinter(p);
//
//                // ***************************** ADD FOR BBVA *******
//                if (resumeType.equals(Resume.resumeType.USER_RESUME))
//                    resume.setId(p.getHostname());
//                // ***************************** ADD FOR BBVA *******
//
//                u = new User("", "", "");
//                resume.setUser(u);

                returnMfpsList.add(p.getMfpSerial());
            }
        }

        if(resumeType.equals(Resume.resumeType.MFP_RESUME))
        {
            if (company.equalsIgnoreCase(""))
                onlineMfps = dbManager.getMFPsOnState(Printer.ONLINE);
            else
                onlineMfps = dbManager.getMFPsOnState(Printer.ONLINE, company);
            
            it = onlineMfps.listIterator();
            while(it.hasNext())
            {
                p = (Printer)it.next();
                if (!dbMfps.contains(p.getMfpSerial()))
                {
//                    resume = new Resume(p.getMfpSerial(), END_DATE);
//                    resume.setPrinter(p);
//
//                    u =new User("", "", "");
//                    resume.setUser(u);

                   returnMfpsList.add(p.getMfpSerial());
                }
            }
        }
        //Close connection with the database
//        if (!dbManager.disconnect())
//            System.out.println("addOfflinePrintersResume2File() : Cannot close DataBase connection");
//            logger.error("addOfflinePrintersResume2File() : Cannot close DataBase connection");
        
        return returnMfpsList;
    }
    
}
