/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ricoh.accounting.actions;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import ricoh.accounting.database.DataBaseManager;
import ricoh.accounting.ifactory.InterfaceManager;
import ricoh.accounting.interfaces.IFileManager;
import ricoh.accounting.objects.ObjectManager;
import ricoh.accounting.objects.Resume;
import ricoh.accounting.objects.Resume.resumeType;
import ricoh.accounting.objects.User;
import ricoh.accounting.remotecc.RemoteccPlugin;

/**
 *
 * @author david.yunta
 */
public class TotalPagesMFPAction extends org.apache.struts.action.Action {

    /* forward name="success" path="" */
    private static final String SUCCESS = "SUCCESS";
    private static final String FAIL = "FAIL";
    
    private DataBaseManager dbManager;

    RemoteccPlugin remoteccplugin;;
    
    
    /**
     * This is the action called from the Struts framework.
     *
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        
        String printerName, userName;
        Resume resume;
        List users, userPrinters, resumeList = new ArrayList();
        Date START_DATE, END_DATE, EXEC_DATE;
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        User user;
        Iterator usersIterator, printersIterator;

        Runtime.getRuntime().gc();

        HttpSession session = request.getSession(true);

        String company = (String) session.getAttribute("company");
        Boolean isAdmin = (Boolean) session.getAttribute("isAdmin");

        String strDateIni = request.getParameter("iniDate");
        String strDateEnd = request.getParameter("endDate");

        if (strDateIni.isEmpty() || strDateEnd.isEmpty()) {
            session.setAttribute("errorCode", "empty_date");
            return mapping.findForward(FAIL);
        }

        try {
            START_DATE = dateFormat.parse(strDateIni);
            END_DATE = dateFormat.parse(strDateEnd);

            EXEC_DATE = getExecutingDate();
        } catch (ParseException ex) {
            System.out.println(ex.getMessage());
            System.out.println(ex.getStackTrace());

            session.setAttribute("errorCode", "parse_fail");
            return mapping.findForward(FAIL);
        }

        if (START_DATE.after(END_DATE)) {
            session.setAttribute("errorCode", "start_after_end");
            return mapping.findForward(FAIL);
        } else if (END_DATE.after(EXEC_DATE)) {
            session.setAttribute("errorCode", "end_after_today");
            return mapping.findForward(FAIL);
        }

        try {
            dbManager = InterfaceManager.getDataBaseManagerInstance(
                    servlet.getServletContext().getRealPath("/WEB-INF/config.properties"),
                    servlet.getServletContext().getRealPath("/WEB-INF/sql.properties"));

            if (!dbManager.isConnected()) {
                //Open connection with the database
                if (!dbManager.connect()) {
                    //logger.error("generateAllReports() : Cannot open DataBase connection");
                    System.out.println("ERROR: Cannot open DataBase connection");
                    session.setAttribute("errorCode", "db_error");
                    return mapping.findForward(FAIL);
                }
            }


            if (isAdmin) {
                userPrinters = dbManager.getMFPs();
            } else {
                userPrinters = dbManager.getMFPs_PerCompany(company);
            }

            printersIterator = userPrinters.listIterator();
            while (printersIterator.hasNext()) {
                printerName = (String) printersIterator.next();
                resume = dbManager.getTotalsBetweenDates(printerName, START_DATE, END_DATE, resumeType.MFP_RESUME);//dbManager.getTotalsBetweenDates(userName, printerName, START_DATE, END_DATE);

                resumeList.add(resume);
            }

            //Close connection with the database
            if (!dbManager.disconnect()) {
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            System.out.println(ex.getStackTrace());

            //Close connection with the database
            if (!dbManager.disconnect()) {
            }
                //logger.error("generateAllReports() : Cannot close DataBase connection");

            session.setAttribute("errorCode", "process_error");
            return mapping.findForward(FAIL);
        }

        session.setAttribute("resumeList", resumeList);
        session.setAttribute("iniDate", strDateIni);
        session.setAttribute("endDate", strDateEnd);
        session.removeAttribute("errorCode");

        return mapping.findForward(SUCCESS);
    }
    
    
   
    private static SearchControls getSimpleSearchControls() {
        SearchControls searchControls = new SearchControls();
        searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
        searchControls.setTimeLimit(30000);
        return searchControls;
    }



    public List getFileNames(String spoolPath) {
        File[] spoolFiles = null;
        List filesList = new ArrayList();
        File f = new File(spoolPath);
        spoolFiles = f.listFiles();

        for (int i = 0; i < spoolFiles.length; i++) {
            System.out.println("CSV Files: " + spoolFiles[i].getName());
            filesList.add(spoolPath + "/" + spoolFiles[i].getName());
        }

        return filesList;
    }

    
    private String parseADAttribute(String ADparam) {
        try {
            String tmp = ADparam;

            tmp = tmp.substring(tmp.indexOf(":") + 1);
            tmp = tmp.trim();

            return tmp;
        } catch (Exception e) {
            return ADparam;
        }
    }
    
 
    private Date getExecutingDate() throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = dateFormat.format(new Date());
        return dateFormat.parse(dateString);
    }
}
