package ricoh.accounting.actions;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import javax.servlet.http.*;

/**
 *
 * @author Alexis.Hidalgo
 */
public class AppAdministration extends org.apache.struts.action.Action {

    private static final String SUCCESS = "SUCCESS";
    private static final String FAIL = "FAIL";

    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception
    {
        HttpSession session = request.getSession(true);
        String actionType = request.getParameter("action");
        String configfile = request.getParameter("configComboList");
        
        try
        {
            if (actionType == null)
            {
                readConfiguration(session, configfile);
                return mapping.findForward(SUCCESS);
            }

            else if (actionType.equals("modify"))
            {
                String configfile2 = (String)session.getAttribute("configfile");
                modifyConfiguration(request,configfile2);
                readConfiguration(session, configfile2);
//                response.setHeader("Refresh", "1");
                session.setAttribute("configfile", configfile2);
                return mapping.findForward("SUCCESS");
            }
            
            else if (actionType.equals("change"))
            {
//                modifyConfiguration(request);

                readConfiguration(session, configfile);
//                response.setHeader("Refresh", "1");
                session.setAttribute("configfile", configfile);
                return mapping.findForward("SUCCESS");
            }
        }
        catch (Exception ex)
        {
            //logger.error(ex.getMessage());
            //logger.error(ex.getStackTrace());
            session.setAttribute("errorCode", "process_error");
            return mapping.findForward(FAIL);
        }
        session.removeAttribute("errorCode");
        
        return mapping.findForward(SUCCESS);
    }

    private void readConfiguration(HttpSession session, String configfile) throws IOException
    {
        if (configfile == null)
            configfile = "web";

        InputStream inputStream = servlet.getServletContext().getResourceAsStream("/WEB-INF/config.properties");
        Properties propFile = new Properties();
        propFile.load(inputStream);
        inputStream.close();

        if (configfile.equalsIgnoreCase("web")){
//            String txtOutput = propFile.getProperty("Output.Path");
//            String zipOutput = propFile.getProperty("ZIP.Path");
            String dbHost = propFile.getProperty("DataBase.Host");
            String dbUser = propFile.getProperty("DataBase.User");
            String dbPass = propFile.getProperty("DataBase.Password");
            String dbName = propFile.getProperty("DataBase.Name");
            String dbPort = propFile.getProperty("DataBase.Port");
//            String extDbHost = propFile.getProperty("ExternalDB.Host");
//            String extDbInstance = propFile.getProperty("ExternalDB.Instance");
//            String extDbUser = propFile.getProperty("ExternalDB.User");
//            String extDbPass = propFile.getProperty("ExternalDB.Password");
//            String extDbName = propFile.getProperty("ExternalDB.Name");
//            String extDbPort = propFile.getProperty("ExternalDB.Port");


//            session.setAttribute("txtOutput", txtOutput);
//            session.setAttribute("zipOutput", zipOutput);
            session.setAttribute("dbHost", dbHost);
            session.setAttribute("dbUser", dbUser);
            session.setAttribute("dbPass", dbPass);
            session.setAttribute("dbName", dbName);
            session.setAttribute("dbPort", dbPort);
//            session.setAttribute("extDbHost", extDbHost);
//            session.setAttribute("extDbInstance", extDbInstance);
//            session.setAttribute("extDbUser", extDbUser);
//            session.setAttribute("extDbPass", extDbPass);
//            session.setAttribute("extDbName", extDbName);
//            session.setAttribute("extDbPort", extDbPort);
        }else if (configfile.equalsIgnoreCase("parser")){
            String parserConfig = propFile.getProperty("Config.Parser");
            
            InputStream inputStream2 = new FileInputStream(parserConfig);
            Properties propParserFile = new Properties();
            propParserFile.load(inputStream2);
            inputStream2.close();
            
            String txtOutput = propParserFile.getProperty("Output.Path");
            String zipOutput = propParserFile.getProperty("ZIP.Path");
            String dbHost = propParserFile.getProperty("DataBase.Host");
            String dbUser = propParserFile.getProperty("DataBase.User");
            String dbPass = propParserFile.getProperty("DataBase.Password");
            String dbName = propParserFile.getProperty("DataBase.Name");
            String dbPort = propParserFile.getProperty("DataBase.Port");
            String extDbHost = propParserFile.getProperty("ExternalStreamlineDB.Host");
            String extDbInstance = propParserFile.getProperty("ExternalStreamlineDB.Instance");
            String extDbUser = propParserFile.getProperty("ExternalStreamlineDB.User");
            String extDbPass = propParserFile.getProperty("ExternalStreamlineDB.Password");
            String extDbName = propParserFile.getProperty("ExternalStreamlineDB.Name");
            String extDbPort = propParserFile.getProperty("ExternalStreamlineDB.Port");
            String smtpserver = propParserFile.getProperty("MAIL.IPSMTPSERVER");
            String smtpport = propParserFile.getProperty("MAIL.PORT");
            String toaddress = propParserFile.getProperty("MAIL.FROMADDRESS");
            String fromaddress = propParserFile.getProperty("MAIL.TOADDRESS");
            String emailsubject = propParserFile.getProperty("MAIL.SUBJECT");
            String parseremailbody = propParserFile.getProperty("MAIL.MESSAGE_PARSERPROCESS");
            String totalsemailbody = propParserFile.getProperty("MAIL.MESSAGE_CHECKTOTALS");


            session.setAttribute("txtOutput", txtOutput);
            session.setAttribute("zipOutput", zipOutput);
            session.setAttribute("dbHost", dbHost);
            session.setAttribute("dbUser", dbUser);
            session.setAttribute("dbPass", dbPass);
            session.setAttribute("dbName", dbName);
            session.setAttribute("dbPort", dbPort);
            session.setAttribute("extDbHost", extDbHost);
            session.setAttribute("extDbInstance", extDbInstance);
            session.setAttribute("extDbUser", extDbUser);
            session.setAttribute("extDbPass", extDbPass);
            session.setAttribute("extDbName", extDbName);
            session.setAttribute("extDbPort", extDbPort);
            session.setAttribute("smtpserver", smtpserver);
            session.setAttribute("smtpport", smtpport);
            session.setAttribute("fromaddress", fromaddress);
            session.setAttribute("toaddress", toaddress);
            session.setAttribute("emailsubject", emailsubject);
            session.setAttribute("parseremailbody", parseremailbody);
            session.setAttribute("totalsemailbody", totalsemailbody);
        } else{
            String extractorConfig = propFile.getProperty("Config.Extractor");
            
            InputStream inputStream3 = new FileInputStream(extractorConfig);
            Properties propExtractorFile = new Properties();
            propExtractorFile.load(inputStream3);
            inputStream3.close();
            
            String txtOutput = propExtractorFile.getProperty("Output.Path");
            String zipOutput = propExtractorFile.getProperty("ZIP.Path");
            String dbHost = propExtractorFile.getProperty("DataBase.Host");
            String dbUser = propExtractorFile.getProperty("DataBase.User");
            String dbPass = propExtractorFile.getProperty("DataBase.Password");
            String dbName = propExtractorFile.getProperty("DataBase.Name");
            String dbPort = propExtractorFile.getProperty("DataBase.Port");
            String extDbHost = propExtractorFile.getProperty("ExternalDB.Host");
            String extDbInstance = propExtractorFile.getProperty("ExternalDB.Instance");
            String extDbUser = propExtractorFile.getProperty("ExternalDB.User");
            String extDbPass = propExtractorFile.getProperty("ExternalDB.Password");
            String extDbName = propExtractorFile.getProperty("ExternalDB.Name");
            String extDbPort = propExtractorFile.getProperty("ExternalDB.Port");


            session.setAttribute("txtOutput", txtOutput);
            session.setAttribute("zipOutput", zipOutput);
            session.setAttribute("dbHost", dbHost);
            session.setAttribute("dbUser", dbUser);
            session.setAttribute("dbPass", dbPass);
            session.setAttribute("dbName", dbName);
            session.setAttribute("dbPort", dbPort);
            session.setAttribute("extDbHost", extDbHost);
            session.setAttribute("extDbInstance", extDbInstance);
            session.setAttribute("extDbUser", extDbUser);
            session.setAttribute("extDbPass", extDbPass);
            session.setAttribute("extDbName", extDbName);
            session.setAttribute("extDbPort", extDbPort);
        }
    }

    private void modifyConfiguration(HttpServletRequest request, String configfile) throws IOException
    {
        if (configfile == null)
            configfile = "web";
        
        //String txtOutput = request.getParameter("txtpath");
        //String zipOutput = request.getParameter("zippath");
        String dbHost = request.getParameter("databasehost");
        String dbUser = request.getParameter("databaseuser");
        String dbPass = request.getParameter("databasepass");
        String dbName = request.getParameter("databasename");
        String dbPort = request.getParameter("databaseport");
       

        InputStream inputStream = servlet.getServletContext().getResourceAsStream("/WEB-INF/config.properties");      
        CommentedProperties propFile = new CommentedProperties();
        propFile.load(inputStream);
        inputStream.close();
        
        if (configfile.equalsIgnoreCase("web")){

//            if (!propFile.getProperty("Output.Path").equals(txtOutput))
//               propFile.setProperty("Output.Path", txtOutput);

//            if (!propFile.getProperty("ZIP.Path").equals(zipOutput))
//               propFile.setProperty("ZIP.Path", zipOutput);

            if (!propFile.getProperty("DataBase.Host").equals(dbHost))
               propFile.setProperty("DataBase.Host", dbHost);

            if (!propFile.getProperty("DataBase.User").equals(dbUser))
               propFile.setProperty("DataBase.User", dbUser);

            if (!propFile.getProperty("DataBase.Password").equals(dbPass))
               propFile.setProperty("DataBase.Password", dbPass);

            if (!propFile.getProperty("DataBase.Name").equals(dbName))
               propFile.setProperty("DataBase.Name", dbName);

            if (!propFile.getProperty("DataBase.Port").equals(dbPort))
               propFile.setProperty("DataBase.Port", dbPort);

//            if (!propFile.getProperty("ExternalDB.Host").equals(extDbHost))
//               propFile.setProperty("ExternalDB.Host", extDbHost);
//
//            if (!propFile.getProperty("ExternalDB.Instance").equals(extDbInstance))
//               propFile.setProperty("ExternalDB.Instance", extDbInstance);
//
//            if (!propFile.getProperty("ExternalDB.User").equals(extDbUser))
//               propFile.setProperty("ExternalDB.User", extDbUser);
//
//            if (!propFile.getProperty("ExternalDB.Password").equals(extDbPass))
//               propFile.setProperty("ExternalDB.Password", extDbPass);
//
//            if (!propFile.getProperty("ExternalDB.Name").equals(extDbName))
//               propFile.setProperty("ExternalDB.Name", extDbName);
//
//            if (!propFile.getProperty("ExternalDB.Port").equals(extDbPort))
//               propFile.setProperty("ExternalDB.Port", extDbPort);

            OutputStream out = new FileOutputStream(servlet.getServletContext().getRealPath("/WEB-INF/config.properties"));
            propFile.store(out, null);
            out.close();
        }else if (configfile.equalsIgnoreCase("parser")){  
            
            String extDbHost = request.getParameter("extdatabasehost");
            String extDbInstance = request.getParameter("extdatabaseinstance");
            String extDbUser = request.getParameter("extdatabaseuser");
            String extDbPass = request.getParameter("extdatabasepass");
            String extDbName = request.getParameter("extdatabasename");
            String extDbPort = request.getParameter("extdatabaseport"); 
            
            String smtpserver = request.getParameter("smtpserver");
            String smtpport = request.getParameter("smtpport");
            String fromaddress = request.getParameter("fromaddress");
            String toaddress = request.getParameter("toaddress");
            String emailsubject = request.getParameter("emailsubject");
            String parseremailbody = request.getParameter("parseremailbody");
            String totalsemailbody = request.getParameter("totalsemailbody");
            
            String parserConfig = propFile.getProperty("Config.Parser");
            
            InputStream inputStream2 = new FileInputStream(parserConfig);
            
            CommentedProperties propParserFile = new CommentedProperties();
            propParserFile.load(inputStream2);
            inputStream2.close();
            
//            Properties propParserFile = new Properties();
//            propParserFile.load(inputStream2);
//            inputStream2.close();
            
//            if (!propParserFile.getProperty("Output.Path").equals(txtOutput))
//               propParserFile.setProperty("Output.Path", txtOutput);
//
//            if (!propParserFile.getProperty("ZIP.Path").equals(zipOutput))
//               propParserFile.setProperty("ZIP.Path", zipOutput);

            if (!propParserFile.getProperty("DataBase.Host").equals(dbHost))
               propParserFile.setProperty("DataBase.Host", dbHost);

            if (!propParserFile.getProperty("DataBase.User").equals(dbUser))
               propParserFile.setProperty("DataBase.User", dbUser);

            if (!propParserFile.getProperty("DataBase.Password").equals(dbPass))
               propParserFile.setProperty("DataBase.Password", dbPass);

            if (!propParserFile.getProperty("DataBase.Name").equals(dbName))
               propParserFile.setProperty("DataBase.Name", dbName);

            if (!propParserFile.getProperty("DataBase.Port").equals(dbPort))
               propParserFile.setProperty("DataBase.Port", dbPort);

            if (!propParserFile.getProperty("ExternalStreamlineDB.Host").equals(extDbHost))
               propParserFile.setProperty("ExternalStreamlineDB.Host", extDbHost);

            if (!propParserFile.getProperty("ExternalStreamlineDB.Instance").equals(extDbInstance))
               propParserFile.setProperty("ExternalStreamlineDB.Instance", extDbInstance);

            if (!propParserFile.getProperty("ExternalStreamlineDB.User").equals(extDbUser))
               propParserFile.setProperty("ExternalStreamlineDB.User", extDbUser);

            if (!propParserFile.getProperty("ExternalStreamlineDB.Password").equals(extDbPass))
               propParserFile.setProperty("ExternalStreamlineDB.Password", extDbPass);

            if (!propParserFile.getProperty("ExternalStreamlineDB.Name").equals(extDbName))
               propParserFile.setProperty("ExternalStreamlineDB.Name", extDbName);

            if (!propParserFile.getProperty("ExternalStreamlineDB.Port").equals(extDbPort))
               propParserFile.setProperty("ExternalStreamlineDB.Port", extDbPort);
            
            if (!propParserFile.getProperty("MAIL.IPSMTPSERVER").equals(smtpserver))
               propParserFile.setProperty("MAIL.IPSMTPSERVER", smtpserver);

            if (!propParserFile.getProperty("MAIL.PORT").equals(smtpport))
               propParserFile.setProperty("MAIL.PORT", smtpport);

            if (!propParserFile.getProperty("MAIL.FROMADDRESS").equals(fromaddress))
               propParserFile.setProperty("MAIL.FROMADDRESS", fromaddress);

            if (!propParserFile.getProperty("MAIL.TOADDRESS").equals(toaddress))
               propParserFile.setProperty("MAIL.TOADDRESS", toaddress);

            if (!propParserFile.getProperty("MAIL.SUBJECT").equals(emailsubject))
               propParserFile.setProperty("MAIL.SUBJECT", emailsubject);

            if (!propParserFile.getProperty("MAIL.MESSAGE_PARSERPROCESS").equals(parseremailbody))
               propParserFile.setProperty("MAIL.MESSAGE_PARSERPROCESS", parseremailbody);

            if (!propParserFile.getProperty("MAIL.MESSAGE_CHECKTOTALS").equals(totalsemailbody))
               propParserFile.setProperty("MAIL.MESSAGE_CHECKTOTALS", totalsemailbody);

            OutputStream out = new FileOutputStream(parserConfig);
            
            propParserFile.store(out, null);
            out.close();
        }else {
            String extractorConfig = propFile.getProperty("Config.Extractor");
            
            InputStream inputStream2 = new FileInputStream(extractorConfig);
            CommentedProperties propExtractorFile = new CommentedProperties();
            propExtractorFile.load(inputStream2);
            inputStream2.close();
            
//            if (!propExtractorFile.getProperty("Output.Path").equals(txtOutput))
//               propExtractorFile.setProperty("Output.Path", txtOutput);
//
//            if (!propExtractorFile.getProperty("ZIP.Path").equals(zipOutput))
//               propExtractorFile.setProperty("ZIP.Path", zipOutput);

            if (!propExtractorFile.getProperty("DataBase.Host").equals(dbHost))
               propExtractorFile.setProperty("DataBase.Host", dbHost);

            if (!propExtractorFile.getProperty("DataBase.User").equals(dbUser))
               propExtractorFile.setProperty("DataBase.User", dbUser);

            if (!propExtractorFile.getProperty("DataBase.Password").equals(dbPass))
               propExtractorFile.setProperty("DataBase.Password", dbPass);

            if (!propExtractorFile.getProperty("DataBase.Name").equals(dbName))
               propExtractorFile.setProperty("DataBase.Name", dbName);

            if (!propExtractorFile.getProperty("DataBase.Port").equals(dbPort))
               propExtractorFile.setProperty("DataBase.Port", dbPort);

//            if (!propExtractorFile.getProperty("ExternalDB.Host").equals(extDbHost))
//               propExtractorFile.setProperty("ExternalDB.Host", extDbHost);
//
//            if (!propExtractorFile.getProperty("ExternalDB.Instance").equals(extDbInstance))
//               propExtractorFile.setProperty("ExternalDB.Instance", extDbInstance);
//
//            if (!propExtractorFile.getProperty("ExternalDB.User").equals(extDbUser))
//               propExtractorFile.setProperty("ExternalDB.User", extDbUser);
//
//            if (!propExtractorFile.getProperty("ExternalDB.Password").equals(extDbPass))
//               propExtractorFile.setProperty("ExternalDB.Password", extDbPass);
//
//            if (!propExtractorFile.getProperty("ExternalDB.Name").equals(extDbName))
//               propExtractorFile.setProperty("ExternalDB.Name", extDbName);
//
//            if (!propExtractorFile.getProperty("ExternalDB.Port").equals(extDbPort))
//               propExtractorFile.setProperty("ExternalDB.Port", extDbPort);

            OutputStream out = new FileOutputStream(extractorConfig);
            propExtractorFile.store(out, null);
            out.close();
        }
    }
}
