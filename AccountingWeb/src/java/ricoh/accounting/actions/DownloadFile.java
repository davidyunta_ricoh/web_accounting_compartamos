package ricoh.accounting.actions;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Properties;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import javax.servlet.http.*;
import javax.servlet.*;
import javax.swing.JFileChooser;
import ricoh.accounting.database.DataBaseManager;
import ricoh.accounting.extractor.Extractor;
import ricoh.accounting.interfaces.IExtractorManager;
import ricoh.accounting.interfaces.IFileManager;
import ricoh.accounting.objects.Action.actionType;
import ricoh.accounting.objects.Resume;
import ricoh.accounting.objects.Action;
import ricoh.accounting.objects.ObjectManager;
import ricoh.accounting.objects.Printer;
import ricoh.accounting.objects.Resume.resumeType;
import ricoh.accounting.ifactory.InterfaceManager;

/**
 *
 * @author Alexis.Hidalgo
 */
public class DownloadFile extends org.apache.struts.action.Action
{
    private static final String SUCCESS = "SUCCESS";
    private static final String FAIL = "FAIL";
    private static final String SUCCESS_MFP = "SUCCESS_MFP";
        private static final String FAIL_MFP = "FAIL_MFP";
    
    private IExtractorManager extractorManager;
    private DataBaseManager dbManager;
    private IFileManager fileManager;
    private ObjectManager objectManager;
    private String MFP_INVENTORY_FILE = "";
    private String output_path = "";
    
    private String tipoVistaInformacion = "";
    private String tipoInforme = "";
    private String type = "";

    private Date START_DATE, END_DATE;

     @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception 
    {
        Date EXEC_DATE;
        String strDateIni, strDateEnd;
        
        
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        Runtime.getRuntime().gc();

        if (extractorManager == null)
            extractorManager = new Extractor(servlet.getServletContext().getRealPath("/WEB-INF/config.properties"));

        extractorManager.setSQL_FILE(servlet.getServletContext().getRealPath("/WEB-INF/sql.properties"));

        objectManager = new ObjectManager();
        fileManager = InterfaceManager.getFileManagerInstance();
        dbManager = InterfaceManager.getDataBaseManagerInstance(servlet.getServletContext().getRealPath("/WEB-INF/config.properties"), servlet.getServletContext().getRealPath("/WEB-INF/sql.properties"));

//        //Leer fichero properties
        Properties prop = new Properties();
        prop.load(new FileInputStream(servlet.getServletContext().getRealPath("/WEB-INF/config.properties")));
        output_path = prop.getProperty("Output.Path");
        
        
        HttpSession session = request.getSession(true);

        String company = (String)session.getAttribute("company");
        String mfpserial = (String)session.getAttribute("mfp");
        String username = (String)session.getAttribute("user");
        Boolean isAdmin = (Boolean)session.getAttribute("isAdmin");
        
        String fileType = request.getParameter("file");

        if (fileType == null)
            return mapping.findForward(FAIL);

        if(fileType.equals("createzip"))
        {
            strDateIni = request.getParameter("iniDate");
            strDateEnd = request.getParameter("endDate");
        }
        else
        {
            strDateIni = (String)session.getAttribute("iniDate");
            strDateEnd = (String)session.getAttribute("endDate");
        }

        if (!fileType.equals("zipusers") && !fileType.equals("zipmfps") && !fileType.equals("zipcecos") && (!fileType.equals("export_mfps")&& (!fileType.equals("import_mfps"))))
        {
            if (strDateIni.isEmpty() || strDateEnd.isEmpty())
            {
                session.setAttribute("errorCode", "empty_date");
                return mapping.findForward(FAIL);
            }

            try
            {
                START_DATE = dateFormat.parse(strDateIni);
                END_DATE = dateFormat.parse(strDateEnd);

                EXEC_DATE = getExecutingDate();
            }
            catch (ParseException ex)
            {
                System.out.println(ex.getMessage());
                System.out.println(ex.getStackTrace());

                session.setAttribute("errorCode", "parse_fail");
                return mapping.findForward(FAIL);
            }

            if (START_DATE.after(END_DATE))
            {
                session.setAttribute("errorCode", "start_after_end");
                return mapping.findForward(FAIL);
            }
            else if (END_DATE.after(EXEC_DATE))
            {
                session.setAttribute("errorCode", "end_after_today");
                return mapping.findForward(FAIL);
            }
        }

        if(fileType.equals("createzip"))
        {
            if (createZipFiles())
                return mapping.findForward("zipmenu");
            else
            {
                session.setAttribute("errorCode", "process_error");
                return mapping.findForward(FAIL);
            }
        }
        else if (fileType.equals("zipusers"))
        {
            if (!downloadUsersZipFile(response))
            {
                session.setAttribute("errorCode", "process_error");
                return mapping.findForward(FAIL);
            }
        }
        else if (fileType.equals("zipmfps"))
        {
            if (!downloadPrintersZipFile(response))
            {
                session.setAttribute("errorCode", "process_error");
                return mapping.findForward(FAIL);
            }
        }
        else if (fileType.equals("zipcecos"))
        {
            if (!downloadCECOsZipFile(response))
            {
                session.setAttribute("errorCode", "process_error");
                return mapping.findForward(FAIL);
            }
        }
        else if (fileType.equals("users"))
        {
            if (!createAndDownloadTxtFile(response, Resume.resumeType.USER_RESUME, isAdmin, company, mfpserial))
            {
                session.setAttribute("errorCode", "process_error");
                return mapping.findForward(FAIL);
            }
        }
        else if (fileType.equals("mfps"))
        {
            if (!createAndDownloadTxtFile(response, Resume.resumeType.MFP_RESUME, isAdmin, company, mfpserial))
            {
                session.setAttribute("errorCode", "process_error");
                return mapping.findForward(FAIL);
            }
        }
        else if (fileType.equals("usermfps"))
        {
            if (!createAndDownloadTxtFile(response, Resume.resumeType.USER_MFP_RESUME, isAdmin, company, mfpserial))
            {
                session.setAttribute("errorCode", "process_error");
                return mapping.findForward(FAIL);
            }
        }
        else if (fileType.equals("useraccumulated"))
        {
            tipoVistaInformacion = (String)session.getAttribute("filtrado");
            tipoInforme = (String)session.getAttribute("tipoInforme");
            type = (String)session.getAttribute("type");  
            if (!createAndDownloadTxtFile(response, Resume.resumeType.USER_ACCUMULATED_RESUME, isAdmin, company, mfpserial))
            {
                session.setAttribute("errorCode", "process_error");
                return mapping.findForward(FAIL);
            }
        }
        else if (fileType.equals("cecos"))
        {
            if (!createAndDownloadTxtFile(response, Resume.resumeType.CC_RESUME, isAdmin, company, mfpserial))
            {
                session.setAttribute("errorCode", "process_error");
                return mapping.findForward(FAIL);
            }
        }
        else if (fileType.equals("useractions"))
        {
            if (!createAndDownloadTxtFile(response, Action.actionType.USER_ACTION, isAdmin, company, username))
            {
                session.setAttribute("errorCode", "process_error");
                return mapping.findForward(FAIL);
            }
        }
        else if (fileType.equals("export_mfps"))
        {
            if (!createAndDownloadMFPFile(response))
            {
                session.setAttribute("errorCode", "process_error");
                return mapping.findForward(FAIL_MFP);
            }
        }
        else if (fileType.equals("import_mfps"))
        {
             //Create FileChooser object
            JFileChooser fc = new JFileChooser(output_path);
            int resp = fc.showOpenDialog(null);
            //check Accept uttonnull
            if (resp == JFileChooser.APPROVE_OPTION)
            {
                File fileSelected = fc.getSelectedFile();
                MFP_INVENTORY_FILE = fileSelected.getAbsolutePath();
                if (!checkMasterFile()){
                   System.out.println("ERROR: Procesing " + MFP_INVENTORY_FILE + " MFPs master file");
                   
                   session.setAttribute("errorCode", "process_error");
                   return mapping.findForward(FAIL_MFP);
                }              
            }
            session.removeAttribute("errorCode");
            return mapping.findForward(SUCCESS_MFP);    
        }

//        String dirrec = "";
//        JFileChooser Abrir =new JFileChooser();
//        Abrir.showOpenDialog(null);
//        dirrec=Abrir.getSelectedFile().getAbsolutePath();
//        FileInputStream x =new FileInputStream(dirrec);
//        DataInputStream entrada =new DataInputStream(x); 
        
        session.removeAttribute("errorCode");
        return mapping.findForward("zipmenu");
    }

    private boolean createAndDownloadTxtFile(HttpServletResponse response, resumeType resumeType, Boolean isAdmin, String company, String mfpserial)
    {
        String filePath = "";

        switch (resumeType)
        {
            case USER_RESUME:
                if (isAdmin)
                    extractorManager.generateResumeUsers(START_DATE, END_DATE);
                else
                    extractorManager.generateResumeUsers(company, START_DATE, END_DATE);
                filePath = extractorManager.getResumeUsersFilePath();
                break;

            case MFP_RESUME:
                if (isAdmin)
                    extractorManager.generateResumePrinters("", mfpserial, START_DATE, END_DATE);
                else
                    extractorManager.generateResumePrinters(company, mfpserial, START_DATE, END_DATE);
                filePath = extractorManager.getResumePrintersFilePath();
                break;

            case USER_MFP_RESUME:
                if (isAdmin)
                    extractorManager.generateResumeUserPrinters(START_DATE, END_DATE);
                else
                    extractorManager.generateResumeUserPrinters(company, START_DATE, END_DATE);

                filePath = extractorManager.getResumeUserPrintersFilePath();
                break;
            case USER_ACCUMULATED_RESUME:
                if (isAdmin)
                    extractorManager.generateResumeUserAccumulated(START_DATE, END_DATE, tipoVistaInformacion, type, tipoInforme);
                else
                    extractorManager.generateResumeUserAccumulated(company, START_DATE, END_DATE, tipoVistaInformacion, type, tipoInforme);

                filePath = extractorManager.getResumeUserAccumulatedFilePath();
                break;
            case CC_RESUME:
                if (isAdmin)
                    extractorManager.generateResumeCostCenters(START_DATE, END_DATE);
                else
                    extractorManager.generateResumeCostCenters(company, START_DATE, END_DATE);
                filePath = extractorManager.getResumeCostCentersFilePath();
                break;

        }

        try
        {
            FileInputStream fis = new FileInputStream(filePath);
            int lengh = fis.available();
            byte[] buffer = new byte[lengh];
            fis.read(buffer);
            fis.close();

            response.setContentType("application/octet-stream");
            response.setHeader("Content-Disposition","attachment;filename=" + filePath.substring(filePath.lastIndexOf("/") + 1, filePath.length()));

            ServletOutputStream ouputStream = response.getOutputStream();
            ouputStream.write(buffer);
            ouputStream.flush();
            ouputStream.close();

            return true;
        }
        catch(Exception ex)
        {
            System.out.println(ex.getMessage());
            System.out.println(ex.getStackTrace());

            return false;
        }
    }
    
     private boolean createAndDownloadMFPFile(HttpServletResponse response)
    {
        String filePath = "";

        extractorManager.generatePrinters();
        filePath = extractorManager.getPrintersFilePath();
        
        try
        {
            FileInputStream fis = new FileInputStream(filePath);
            int lengh = fis.available();
            byte[] buffer = new byte[lengh];
            fis.read(buffer);
            fis.close();

            response.setContentType("application/octet-stream");
            response.setHeader("Content-Disposition","attachment;filename=" + filePath.substring(filePath.lastIndexOf("/") + 1, filePath.length()));

            ServletOutputStream ouputStream = response.getOutputStream();
            ouputStream.write(buffer);
            ouputStream.flush();
            ouputStream.close();

            return true;
        }
        catch(Exception ex)
        {
            System.out.println(ex.getMessage());
            System.out.println(ex.getStackTrace());

            return false;
        }
    }

    private boolean createZipFiles()
    {
        return extractorManager.generateAllReports(START_DATE, END_DATE);
    }

    private boolean downloadUsersZipFile(HttpServletResponse response)
    {
        String usersZipPath = extractorManager.getUsersZipPath();

        try
        {
            FileInputStream fis = new FileInputStream(usersZipPath);
            int lengh = fis.available();
            byte[] buffer = new byte[lengh];
            fis.read(buffer);
            fis.close();

            response.setContentType("application/octet-stream");
            response.setHeader("Content-Disposition","attachment;filename=" + usersZipPath.substring(usersZipPath.lastIndexOf("/") + 1, usersZipPath.length()));

            ServletOutputStream ouputStream = response.getOutputStream();
            ouputStream.write(buffer);
            ouputStream.flush();
            ouputStream.close();

            return true;
        }
        catch(Exception ex)
        {
            System.out.println(ex.getMessage());
            System.out.println(ex.getStackTrace());
            System.err.println(ex.getMessage());
            System.err.println(ex.getStackTrace());

            return false;
        }
    }

    private boolean downloadPrintersZipFile(HttpServletResponse response)
    {
        String printersZipPath = extractorManager.getPrintersZipPath();

        try
        {
            FileInputStream fis = new FileInputStream(printersZipPath);
            int lengh = fis.available();
            byte[] buffer = new byte[lengh];
            fis.read(buffer);
            fis.close();

            response.setContentType("application/octet-stream");
            response.setHeader("Content-Disposition","attachment;filename=" + printersZipPath.substring(printersZipPath.lastIndexOf("/") + 1, printersZipPath.length()));

            ServletOutputStream ouputStream = response.getOutputStream();
            ouputStream.write(buffer);
            ouputStream.flush();
            ouputStream.close();

            return true;
        }
        catch(Exception ex)
        {
            System.out.println(ex.getMessage());
            System.out.println(ex.getStackTrace());
            System.err.println(ex.getMessage());
            System.err.println(ex.getStackTrace());

            return false;
        }
    }

    private boolean downloadCECOsZipFile(HttpServletResponse response)
    {
        String cecosZipPath = extractorManager.getCECOsZipPath();

        try
        {
            FileInputStream fis = new FileInputStream(cecosZipPath);
            int lengh = fis.available();
            byte[] buffer = new byte[lengh];
            fis.read(buffer);
            fis.close();

            response.setContentType("application/octet-stream");
            response.setHeader("Content-Disposition","attachment;filename=" + cecosZipPath.substring(cecosZipPath.lastIndexOf("/") + 1, cecosZipPath.length()));

            ServletOutputStream ouputStream = response.getOutputStream();
            ouputStream.write(buffer);
            ouputStream.flush();
            ouputStream.close();

            return true;
        }
        catch(Exception ex)
        {
            System.out.println(ex.getMessage());
            System.out.println(ex.getStackTrace());
            System.err.println(ex.getMessage());
            System.err.println(ex.getStackTrace());

            return false;
        }
    }

    private boolean createAndDownloadTxtFile(HttpServletResponse response, actionType actionType, Boolean isAdmin, String company, String username)
    {
        String filePath = "";

        switch (actionType)
        {
            case USER_ACTION:
                if (isAdmin)
                    extractorManager.generateDetailsByUser("", username, START_DATE, END_DATE);
                else
                    extractorManager.generateDetailsByUser(company, username, START_DATE, END_DATE);

                filePath = extractorManager.getDetailsUsersFilePath();
                break;

            case MFP_ACTION:
                break;
        }

        try
        {
            FileInputStream fis = new FileInputStream(filePath);
            int lengh = fis.available();
            byte[] buffer = new byte[lengh];
            fis.read(buffer);
            fis.close();

            response.setContentType("application/octet-stream");
            response.setHeader("Content-Disposition","attachment;filename=" + filePath.substring(filePath.lastIndexOf("/") + 1, filePath.length()));

            ServletOutputStream ouputStream = response.getOutputStream();
            ouputStream.write(buffer);
            ouputStream.flush();
            ouputStream.close();

            return true;
        }
        catch(Exception ex)
        {
            System.out.println(ex.getMessage());
            System.out.println(ex.getStackTrace());

            return false;
        }

    }

    private Date getExecutingDate() throws ParseException
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = dateFormat.format(new Date());
        return dateFormat.parse(dateString);
    }
    
    private boolean checkMasterFile()
    {
//        String value;
        Printer p;

        System.out.println("-> Checking " + MFP_INVENTORY_FILE + " master file");

        //Open connection with the database
        if (!dbManager.connect())
        {
            System.out.println("ERROR: Cannot open DataBase connection");
            return false;
        }

        objectManager.setMfpRegisterHash(dbManager.getRegisteredMFPs());

        //Close connection with the database
        if (!dbManager.disconnect())
            System.err.println("checkMasterFile() : Cannot close DataBase connection");

        if (!fileManager.openFile("", MFP_INVENTORY_FILE, false))
        {
            System.out.println("ERROR: Cannot open " + MFP_INVENTORY_FILE + "file");
            return false;
        }

        while (fileManager.hasMoreLines(MFP_INVENTORY_FILE))
        {
            p = new Printer();
            p.setMfpSerial(fileManager.readAttribute(MFP_INVENTORY_FILE, "SERIAL_NUMBER").toLowerCase());
            p.setIpAddress(fileManager.readAttribute(MFP_INVENTORY_FILE, "IP_ADDRESS"));
            p.setModelType(fileManager.readAttribute(MFP_INVENTORY_FILE, "MODEL_NAME").toLowerCase());
            p.setStatus(fileManager.readAttribute(MFP_INVENTORY_FILE, "STATUS").toLowerCase());
            p.setLocation(fileManager.readAttribute(MFP_INVENTORY_FILE, "LOCATION"));
            p.setZone(fileManager.readAttribute(MFP_INVENTORY_FILE, "ZONE"));
            p.setHostname(fileManager.readAttribute(MFP_INVENTORY_FILE, "HOSTNAME"));
            p.setCompany(fileManager.readAttribute(MFP_INVENTORY_FILE, "COMPANY"));
            
//            value = fileManager.readAttribute(MFP_INVENTORY_FILE, "CONTADOR_COLOR");
//            if (value.isEmpty())
//                p.setLastColorCounter(0);
//            else
//                p.setLastColorCounter(Integer.parseInt(value));
//
//            value = fileManager.readAttribute(MFP_INVENTORY_FILE, "CONTADOR_BW");
//            if (value.isEmpty())
//                p.setLastBWCounter(0);
//            else
//                p.setLastBWCounter(Integer.parseInt(value));

            if (!objectManager.getMfpRegisterHash().containsKey(p.getMfpSerial()))
            {
                System.out.println("-> " + MFP_INVENTORY_FILE + " master file has new MFP " + p.getIpAddress() + " (" + p.getMfpSerial() + ")");
                objectManager.add2MfpRegisterHash(p);
            }
            else if (!p.equals((Printer)objectManager.getMfpRegisterHash().get(p.getMfpSerial())))
            {
                System.out.println("-> " + MFP_INVENTORY_FILE + " master file has updated info for MFP " + p.getIpAddress() + " (" + p.getMfpSerial() + ")");
                objectManager.update2MfpRegisterHash(p);
            }
        }
        //Close the input file
        if (!fileManager.closeFile(MFP_INVENTORY_FILE))
            System.err.println("checkMasterFile() : Cannot close " + MFP_INVENTORY_FILE + " file");

        return updateMfpRegisterOnDB();
    }

        
     private boolean updateMfpRegisterOnDB()
    {
        boolean allOK = true;

        final boolean[] THREAD_STATUS = new boolean[2];

        if (objectManager.getINSERT_mfpRegisterHash().size() > 0)
        {
            System.out.println("-> Inserting new MFP(s) from master file to the MFP_REGISTER on DataBase");
        }
        if (objectManager.getUPDATE_mfpRegisterHash().size() > 0)
        {
            System.out.println("-> Updating MFP(s) from master file to the MFP_REGISTER on DataBase");
        }

        //Open connection with the database if it is not openned before
        if (!dbManager.isConnected())
            if (!dbManager.connect())
            {
                System.out.println("ERROR: Cannot open DataBase connection");
                return false;
            }

        try
        {
            Thread InsertMfpRegister_Thread = new Thread(new Runnable()
            {
                public void run()
                {
                    String key;
                    boolean allOK = true;
                    Printer printer;
                    //Insert all the elements from Printers Hash table to the dataBase
                    Iterator it = objectManager.getINSERT_mfpRegisterHash().keySet().iterator();
                    while (it.hasNext() && allOK)
                    {
                        //Obtain the key of the entry
                        key = (String) it.next();
                        printer = (Printer)objectManager.getINSERT_mfpRegisterHash().get(key);
                        //Insert the list of Resumes from the key to the dataBase
                        if (!(allOK = dbManager.registerMFP(printer)))
                            System.err.println("updateMfpRegisterOnDB() : Error inserting mfp " + key + " to the MFP register on DataBase");
                    }
                    THREAD_STATUS[0] = allOK;
                }
            });
            InsertMfpRegister_Thread.start();

            Thread UpdateMfpRegister_Thread = new Thread(new Runnable()
            {
                public void run()
                {
                    String key;
                    boolean allOK = true;
                    Printer printer;
                    //Insert all the elements from Printers Hash table to the dataBase
                    Iterator it = objectManager.getUPDATE_mfpRegisterHash().keySet().iterator();
                    while (it.hasNext() && allOK)
                    {
                        //Obtain the key of the entry
                        key = (String) it.next();
                        printer = (Printer)objectManager.getUPDATE_mfpRegisterHash().get(key);
                        //Insert the list of Resumes from the key to the dataBase
                        if (!(allOK = dbManager.updateRegisteredMFP(printer)))
                            System.err.println("updateMfpRegisterOnDB() : Error updating mfp " + key + " to the MFP register on DataBase");
                    }
                    THREAD_STATUS[1] = allOK;
                }
            });
            UpdateMfpRegister_Thread.start();

            UpdateMfpRegister_Thread.join();
            InsertMfpRegister_Thread.join();

            for (int i = 0; i < THREAD_STATUS.length; i++)
                allOK = allOK & THREAD_STATUS[i];

        }
        catch (InterruptedException ex)
        {
            //Logger.getLogger(Parser.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println(ex.getMessage());
            System.err.println(ex.getStackTrace());
            allOK = false;
        }
        finally
        {
            //Close connection with the database
            if (!dbManager.disconnect())
                System.err.println("updateMfpRegisterOnDB() : Cannot close DataBase connection");

            return allOK;
        }
    }
}