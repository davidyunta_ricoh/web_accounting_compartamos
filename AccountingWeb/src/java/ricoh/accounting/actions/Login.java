package ricoh.accounting.actions;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import javax.servlet.http.*;
import ricoh.accounting.database.DataBaseManager;
import ricoh.accounting.ifactory.InterfaceManager;

/**
 *
 * @author Alexis.Hidalgo
 */
public class Login extends org.apache.struts.action.Action {
    
    private static final String SUCCESS = "SUCCESS";
    private static final String FAIL = "FAIL";
    
    private DataBaseManager dbManager;
    
    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception
    {
        HttpSession session = request.getSession(true);
        
        String userName = request.getParameter("user").toLowerCase();
        String password = request.getParameter("pass");
        String status, company;
        int value = 0;
        String[] result = new String[2];

        System.out.println("Usuario -> "+ userName + " Contraseña -> ******" );// + password);

        try
        {
            dbManager = InterfaceManager.getDataBaseManagerInstance(
                    servlet.getServletContext().getRealPath("/WEB-INF/config.properties"),
                    servlet.getServletContext().getRealPath("/WEB-INF/sql.properties"));

            if (!dbManager.isConnected())
            {
                //Open connection with the database
                if (!dbManager.connect())
                {
                    //logger.error("generateAllReports() : Cannot open DataBase connection");
                    session.setAttribute("errorCode", "db_error");
                    System.out.println("ERROR: Cannot open DataBase connection");

                    return mapping.findForward(FAIL);
                }
            }
//            value = dbManager.authenticateUser(userName, password);
            result = dbManager.authenticateUser(userName, password);
            value = Integer.parseInt(result[0]);
            company = result[1];
            //Close connection with the database
            if (!dbManager.disconnect()){}
                //logger.error("generateAllReports() : Cannot close DataBase connection");

            switch (value)
            {
                case 1: //User is authenticated and administrator
                    session.setAttribute("userName", userName);
                    session.setAttribute("isAdmin", true);
                    session.setAttribute("company", company);
                    status = SUCCESS;
                    break;

                case 2: //User is authenticated but not administrator
                    session.setAttribute("userName", userName);
                    session.setAttribute("isAdmin", false);
                    session.setAttribute("company", company);
                    status = SUCCESS;
                    break;

                default: //User is not authenticated
                    status = FAIL;
                    break;
            }
            
            session.removeAttribute("errorCode");
                    
            return mapping.findForward(status);
        }
        catch (Exception ex)
        {
            //logger.error(ex.getMessage());
            //logger.error(ex.getStackTrace());
            //Close connection with the database
            if (!dbManager.disconnect()){}
                //logger.error("generateAllReports() : Cannot close DataBase connection");
            session.setAttribute("errorCode", "process_error");
            
            return mapping.findForward(FAIL);
        }
    }
}
