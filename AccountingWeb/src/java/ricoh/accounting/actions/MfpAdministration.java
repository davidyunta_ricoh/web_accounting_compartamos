package ricoh.accounting.actions;

import java.util.List;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import javax.servlet.http.*;
import ricoh.accounting.database.DataBaseManager;
import ricoh.accounting.ifactory.InterfaceManager;

/**
 *
 * @author Alexis.Hidalgo
 */
public class MfpAdministration extends org.apache.struts.action.Action {

    private static final String SUCCESS = "SUCCESS";
    private static final String FAIL = "FAIL";

    private DataBaseManager dbManager;

    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception
    {
        HttpSession session = request.getSession(true);
        String actionType = request.getParameter("action");

        try
        {
            dbManager = InterfaceManager.getDataBaseManagerInstance(
                    servlet.getServletContext().getRealPath("/WEB-INF/config.properties"),
                    servlet.getServletContext().getRealPath("/WEB-INF/sql.properties"));

            if (!dbManager.isConnected())
            {
                //Open connection with the database
                if (!dbManager.connect())
                {
                    //logger.error("generateAllReports() : Cannot open DataBase connection");
                    System.out.println("ERROR: Cannot open DataBase connection");
                    session.setAttribute("errorCode", "db_error");
                    return mapping.findForward(FAIL);
                }
            }

            if (actionType == null){
                obtainMFPs(session);
                obtainCompanies(session);
            }
                
            else if (actionType.equals("delete"))
            {
                deleteMFP(request);
                obtainMFPs(session);
            }
            
            else if (actionType.equals("insert"))
            {
                insertMFP(request);
                obtainMFPs(session);
            }
            
            else if (actionType.equals("update"))
            {
                updateMFP(request);
                obtainMFPs(session);
            }
            
            else if (actionType.equals("edit"))
            {
                editMFP(request);
                obtainMFPs(session);
            }
            //Close connection with the database
            if (!dbManager.disconnect()){}
                //logger.error("generateAllReports() : Cannot close DataBase connection");
        }
        catch (Exception ex)
        {
            //logger.error(ex.getMessage());
            //logger.error(ex.getStackTrace());
            //Close connection with the database
            if (!dbManager.disconnect()){}
                //logger.error("generateAllReports() : Cannot close DataBase connection");
            session.setAttribute("errorCode", "process_error");
            return mapping.findForward(FAIL);
        }
        session.removeAttribute("errorCode");
        
        return mapping.findForward(SUCCESS);
    }

    private void obtainMFPs(HttpSession session)
    {
        List mfpsList = dbManager.getTotalMFPs();
        session.setAttribute("mfpsList", mfpsList);
    }
    
    private void obtainCompanies(HttpSession session)
    {
        List companiesList = dbManager.getCompaniesWebUsers();
        session.setAttribute("companiesList", companiesList);
    }
    
    private void deleteMFP(HttpServletRequest request)
    {
        String mfpserial = request.getParameter("mfpserial");
        if (!mfpserial.isEmpty())
        {
            if (!dbManager.deleteMFP_List(mfpserial)){}
            //logger.error("generateAllReports() : Cannot close DataBase connection");
        }
    }

    private void insertMFP(HttpServletRequest request)
    {

        String serialnumber = request.getParameter("serialnumber");
        String hostname = request.getParameter("hostname");
        String ipaddress = request.getParameter("ipaddress");
        String modelname = request.getParameter("modelname");
        String status = request.getParameter("statusCombo");
        String location = request.getParameter("location");
        String zone = request.getParameter("zone");
        String company = request.getParameter("CompanyCombo");


        if (!dbManager.registerMFP_List(serialnumber, modelname, ipaddress, status, location, zone, hostname, company)){}
        //logger.error("generateAllReports() : Cannot close DataBase connection");

    }
    
    private void updateMFP(HttpServletRequest request)
    {

        String mfpserial = request.getParameter("mfpserial");
        String status = request.getParameter("statusComboList");


        if (!mfpserial.isEmpty())
        {
            if (!dbManager.setStatusMFP(mfpserial, status)){}
    //            logger.error("generateAllReports() : Cannot close DataBase connection");
        }

    }
    
    private void editMFP(HttpServletRequest request)
    {

        String serialnumber = request.getParameter("serialnumber");
        String hostname = request.getParameter("hostname");
        String ipaddress = request.getParameter("ipaddress");
        String modelname = request.getParameter("modelname");
        String status = request.getParameter("statusCombo");
        String location = request.getParameter("location");
        String zone = request.getParameter("zone");
        String company = request.getParameter("CompanyCombo");


        if (!dbManager.updateMFP_List(serialnumber, modelname, ipaddress, status, location, zone, hostname, company)){}
        //logger.error("generateAllReports() : Cannot close DataBase connection");
    }
}
