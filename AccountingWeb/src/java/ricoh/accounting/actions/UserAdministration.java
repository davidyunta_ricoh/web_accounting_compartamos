package ricoh.accounting.actions;

import java.util.List;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import javax.servlet.http.*;
import ricoh.accounting.database.DataBaseManager;
import ricoh.accounting.ifactory.InterfaceManager;

/**
 *
 * @author Alexis.Hidalgo
 */
public class UserAdministration extends org.apache.struts.action.Action {

    private static final String SUCCESS = "SUCCESS";
    private static final String FAIL = "FAIL";

    private DataBaseManager dbManager;

    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception
    {
        HttpSession session = request.getSession(true);
        String actionType = request.getParameter("action");

        try
        {
            dbManager = InterfaceManager.getDataBaseManagerInstance(
                    servlet.getServletContext().getRealPath("/WEB-INF/config.properties"),
                    servlet.getServletContext().getRealPath("/WEB-INF/sql.properties"));

            if (!dbManager.isConnected())
            {
                //Open connection with the database
                if (!dbManager.connect())
                {
                    //logger.error("generateAllReports() : Cannot open DataBase connection");
                    System.out.println("ERROR: Cannot open DataBase connection");
                    session.setAttribute("errorCode", "db_error");
                    return mapping.findForward(FAIL);
                }
            }

            if (actionType == null){
                obtainUsers(session);
                obtainCompanies(session);
            }
                
            else if (actionType.equals("delete"))
            {
                deleteUser(request);
                obtainUsers(session);
            }
            
            else if (actionType.equals("insert"))
            {
                insertUser(request);
                obtainUsers(session);
            }
            //Close connection with the database
            if (!dbManager.disconnect()){}
                //logger.error("generateAllReports() : Cannot close DataBase connection");
        }
        catch (Exception ex)
        {
            //logger.error(ex.getMessage());
            //logger.error(ex.getStackTrace());
            //Close connection with the database
            if (!dbManager.disconnect()){}
                //logger.error("generateAllReports() : Cannot close DataBase connection");
            session.setAttribute("errorCode", "process_error");
            return mapping.findForward(FAIL);
        }
        session.removeAttribute("errorCode");
        
        return mapping.findForward(SUCCESS);
    }

    private void obtainUsers(HttpSession session)
    {
        List usersList = dbManager.getRegisteredWebUsers();
        session.setAttribute("usersList", usersList);
    }

    private void obtainCompanies(HttpSession session)
    {
        List companiesList = dbManager.getCompaniesWebUsers();
        session.setAttribute("companiesList", companiesList);
    }
    
    private void deleteUser(HttpServletRequest request)
    {
        String userName = request.getParameter("user");
        if (!userName.isEmpty())
        {
                if (!dbManager.deleteRegisteredWebUser(userName)){}
                //logger.error("generateAllReports() : Cannot close DataBase connection");
        }
    }

    private void insertUser(HttpServletRequest request)
    {
        boolean isAdmin = false;

        String userName = request.getParameter("user").toLowerCase();
        String fulluserName = request.getParameter("fullusername");
        String password = request.getParameter("pass");
        String company = request.getParameter("CompanyCombo");

        if (request.getParameter("isAdmin") != null)
            isAdmin = true;

        if (!userName.isEmpty())
        {
                if (!dbManager.registerWebUser(userName, password, fulluserName, isAdmin, company)){}
                //logger.error("generateAllReports() : Cannot close DataBase connection");
        }
    }
}
