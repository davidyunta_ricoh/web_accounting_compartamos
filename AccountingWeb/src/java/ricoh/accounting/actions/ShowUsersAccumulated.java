/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ricoh.accounting.actions;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import ricoh.accounting.database.DataBaseManager;
import ricoh.accounting.ifactory.InterfaceManager;

/**
 * 
 * @author Francisco.Madera
 */
public class ShowUsersAccumulated extends org.apache.struts.action.Action {
    
    private static final String SUCCESS = "SUCCESS";
    private static final String FAIL = "FAIL";
    
    private DataBaseManager dbManager;
    
    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception
    {
        String filter;
        List filters, accumulatedList = new ArrayList();
        Date START_DATE, END_DATE, EXEC_DATE;
        String sourceType;
     
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        
        HttpSession session = request.getSession(true);
            
        String company = (String)session.getAttribute("company");
        Boolean isAdmin = (Boolean)session.getAttribute("isAdmin");
        
        String strDateIni = request.getParameter("iniDate");
        String strDateEnd = request.getParameter("endDate");
        String tipoVistaInformacion = request.getParameter("tipoVista");
        String tipoInforme = request.getParameter("tipoInforme");
        String type = request.getParameter("type");        
        
        if (strDateIni.isEmpty() || strDateEnd.isEmpty())
        {
            session.setAttribute("errorCode", "empty_date");
            return mapping.findForward(FAIL);
        }
        
        if (tipoVistaInformacion.isEmpty())
        {
            session.setAttribute("errorCode", "empty_tipoVista");
            return mapping.findForward(FAIL);
        }
        
        if (tipoInforme.isEmpty())
        {
            session.setAttribute("errorCode", "empty_tipoInforme");
            return mapping.findForward(FAIL);
        }
        
        if (type.isEmpty())
        {
            session.setAttribute("errorCode", "empty_type");
            return mapping.findForward(FAIL);
        }

        try
        {
            START_DATE = dateFormat.parse(strDateIni);
            END_DATE = dateFormat.parse(strDateEnd);

            EXEC_DATE = getExecutingDate();
        }
        catch (ParseException ex)
        {
            System.out.println(ex.getMessage());
            System.out.println(ex.getStackTrace());

            session.setAttribute("errorCode", "parse_fail");
            return mapping.findForward(FAIL);
        }

        if (START_DATE.after(END_DATE))
        {
            session.setAttribute("errorCode", "start_after_end");
            return mapping.findForward(FAIL);
        }
        else if (END_DATE.after(EXEC_DATE))
        {
            session.setAttribute("errorCode", "end_after_today");
            return mapping.findForward(FAIL);
        }

        try
        {
            dbManager = InterfaceManager.getDataBaseManagerInstance(
                    servlet.getServletContext().getRealPath("/WEB-INF/config.properties"),
                    servlet.getServletContext().getRealPath("/WEB-INF/sql.properties"));

            if (!dbManager.isConnected())
            {
                //Open connection with the database
                if (!dbManager.connect())
                {
                    //logger.error("generateAllReports() : Cannot open DataBase connection");
                    System.out.println("ERROR: Cannot open DataBase connection");
                    session.setAttribute("errorCode", "db_error");
                    return mapping.findForward(FAIL);
                }
            }
            
            if(tipoVistaInformacion.equalsIgnoreCase("Usuario"))
            {
                if (isAdmin)
                    filters = dbManager.getReportingUsers();
                else
                    filters = dbManager.getReportingUsers_PerCompany(company);
            }else if(tipoVistaInformacion.equalsIgnoreCase("Oficina"))
            {
                if (isAdmin)
                    filters = dbManager.getReportingZones();
                else
                    filters = dbManager.getReportingZones_PerCompany(company);
            }
            else if(tipoVistaInformacion.equalsIgnoreCase("Region"))
            {
                if (isAdmin)
                    filters = dbManager.getReportingLocations();
                else
                    filters = dbManager.getReportingLocations_PerCompany(company);
            }
            else
                filters = new ArrayList();
            
            //TODO: Ver Todos los tipos
            if (type.equalsIgnoreCase("imp"))
                sourceType = "printer.print";
            else if (type.equalsIgnoreCase("copy"))
                sourceType = "copy";    
            else if (type.equalsIgnoreCase("hojas"))
                sourceType = "%";    
            else
                sourceType = null;
            
            ListIterator filtersIterator = filters.listIterator();
            while (filtersIterator.hasNext())
            {
                filter = (String)filtersIterator.next();
                List usersAcuumulatedList = dbManager.getUsersAccumulated(filter, START_DATE, END_DATE, type, sourceType, tipoInforme, tipoVistaInformacion);
                accumulatedList.addAll(usersAcuumulatedList);
            }

            //Close connection with the database
            if (!dbManager.disconnect()){}
                //logger.error("generateAllReports() : Cannot close DataBase connection");
        }
        catch (Exception ex)
        {
            System.out.println(ex.getMessage());
            System.out.println(ex.getStackTrace());

            //Close connection with the database
            if (!dbManager.disconnect()){}
                //logger.error("generateAllReports() : Cannot close DataBase connection");

            session.setAttribute("errorCode", "process_error");
            return mapping.findForward(FAIL);
        }

        session.setAttribute("accumulatedList", accumulatedList);
        session.setAttribute("iniDate", strDateIni);
        session.setAttribute("endDate", strDateEnd);
        session.setAttribute("endDate", strDateEnd);
        session.setAttribute("filtrado", tipoVistaInformacion);
        session.setAttribute("tipoInforme", tipoInforme);
        session.setAttribute("type", type);
        
        if(type.equalsIgnoreCase("imp"))
            session.setAttribute("tipo", "Documentos impresos");
        else if(type.equalsIgnoreCase("copy"))
            session.setAttribute("tipo", "Copias");
        else if(type.equalsIgnoreCase("hojas"))
            session.setAttribute("tipo", "Hojas");
        
        if(tipoInforme.equalsIgnoreCase("hour"))
            session.setAttribute("intervaloTiempo", "hora");
        else if(tipoInforme.equalsIgnoreCase("week"))
            session.setAttribute("intervaloTiempo", "semana");
        else if(tipoInforme.equalsIgnoreCase("day"))
            session.setAttribute("intervaloTiempo", "dia");
        else if(tipoInforme.equalsIgnoreCase("month"))
            session.setAttribute("intervaloTiempo", "mes");

        session.removeAttribute("errorCode");

        return mapping.findForward(SUCCESS);
    }
    
    private Date getExecutingDate() throws ParseException
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = dateFormat.format(new Date());
        return dateFormat.parse(dateString);
    }
}
