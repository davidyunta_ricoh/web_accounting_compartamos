/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ricoh.accounting.actions;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import ricoh.accounting.database.DataBaseManager;
import ricoh.accounting.ifactory.InterfaceManager;
import ricoh.accounting.objects.Resume;
import ricoh.accounting.objects.Resume.resumeType;

/**
 *
 * @author Alexis.Hidalgo
 */
public class ShowCECOs extends org.apache.struts.action.Action {

    private static final String SUCCESS = "SUCCESS";
    private static final String FAIL = "FAIL";

    private DataBaseManager dbManager;

    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception
    {
        String cecoName;
        Resume resume;
        List cecosList, resumeList = new ArrayList();
        Date START_DATE, END_DATE, EXEC_DATE;
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        Runtime.getRuntime().gc();

        HttpSession session = request.getSession(true);
        
        String company = (String)session.getAttribute("company");
        Boolean isAdmin = (Boolean)session.getAttribute("isAdmin");

        String strDateIni = request.getParameter("iniDate");
        String strDateEnd = request.getParameter("endDate");

        if (strDateIni.isEmpty() || strDateEnd.isEmpty())
        {
            session.setAttribute("errorCode", "empty_date");
            return mapping.findForward(FAIL);
        }

        try
        {
            START_DATE = dateFormat.parse(strDateIni);
            END_DATE = dateFormat.parse(strDateEnd);

            EXEC_DATE = getExecutingDate();
        }
        catch (ParseException ex)
        {
            System.out.println(ex.getMessage());
            System.out.println(ex.getStackTrace());

            session.setAttribute("errorCode", "parse_fail");
            return mapping.findForward(FAIL);
        }

        if (START_DATE.after(END_DATE))
        {
            session.setAttribute("errorCode", "start_after_end");
            return mapping.findForward(FAIL);
        }
        else if (END_DATE.after(EXEC_DATE))
        {
            session.setAttribute("errorCode", "end_after_today");
            return mapping.findForward(FAIL);
        }

        try
        {
            dbManager = InterfaceManager.getDataBaseManagerInstance(
                    servlet.getServletContext().getRealPath("/WEB-INF/config.properties"),
                    servlet.getServletContext().getRealPath("/WEB-INF/sql.properties"));

            if (!dbManager.isConnected())
            {
                //Open connection with the database
                if (!dbManager.connect())
                {
                    //logger.error("generateAllReports() : Cannot open DataBase connection");
                    System.out.println("ERROR: Cannot open DataBase connection");
                    session.setAttribute("errorCode", "db_error");
                    return mapping.findForward(FAIL);
                }
            }
           
           if (isAdmin)
                cecosList = dbManager.getCCs();
           else
               cecosList = dbManager.getCCs(company);
               
            ListIterator cecosIterator = cecosList.listIterator();
            while (cecosIterator.hasNext())
            {
                cecoName = (String)cecosIterator.next();
                //System.out.println("Escribiendo entrada de usuario: " + cecoName);

                resume = dbManager.getTotalsBetweenDates(cecoName, START_DATE, END_DATE, resumeType.CC_RESUME);

                resumeList.add(resume);
            }

            //Close connection with the database
            if (!dbManager.disconnect()){}
                //logger.error("generateAllReports() : Cannot close DataBase connection");
        }
        catch (Exception ex)
        {
            System.out.println(ex.getMessage());
            System.out.println(ex.getStackTrace());

            //Close connection with the database
            if (!dbManager.disconnect()){}
                //logger.error("generateAllReports() : Cannot close DataBase connection");

            session.setAttribute("errorCode", "process_error");
            return mapping.findForward(FAIL);
        }

        session.setAttribute("resumeList", resumeList);
        session.setAttribute("iniDate", strDateIni);
        session.setAttribute("endDate", strDateEnd);
        session.removeAttribute("errorCode");

        return mapping.findForward(SUCCESS);
    }

    private Date getExecutingDate() throws ParseException
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = dateFormat.format(new Date());
        return dateFormat.parse(dateString);
    }

}
