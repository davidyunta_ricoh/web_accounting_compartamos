package ricoh.accounting.actions;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import javax.servlet.http.*;
import ricoh.accounting.database.DataBaseManager;
import ricoh.accounting.ifactory.InterfaceManager;
import ricoh.accounting.objects.ObjectManager;

/**
 *
 * @author Alexis.Hidalgo
 */
public class CompaniesAdministration extends org.apache.struts.action.Action {

    private static final String SUCCESS = "SUCCESS";
    private static final String FAIL = "FAIL";

    private DataBaseManager dbManager;

    private ObjectManager objectManager = new ObjectManager();
    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception
    {
        HttpSession session = request.getSession(true);
        String actionType = request.getParameter("action");

        try
        {
            dbManager = InterfaceManager.getDataBaseManagerInstance(
                    servlet.getServletContext().getRealPath("/WEB-INF/config.properties"),
                    servlet.getServletContext().getRealPath("/WEB-INF/sql.properties"));

            if (!dbManager.isConnected())
            {
                //Open connection with the database
                if (!dbManager.connect())
                {
                    //logger.error("generateAllReports() : Cannot open DataBase connection");
                    System.out.println("ERROR: Cannot open DataBase connection");
                    session.setAttribute("errorCode", "db_error");
                    return mapping.findForward(FAIL);
                }
            }

            if (actionType == null){
                obtainCompanies(session);
            }
                
            else if (actionType.equals("delete"))
            {
                deleteMFP(request);
                obtainCompanies(session);
            }
            
            else if (actionType.equals("insert"))
            {
                insertMFP(request);
                obtainCompanies(session);
            }
            
//            else if (actionType.equals("update"))
//            {
//                obtainCompanies(session);
//            }
            //Close connection with the database
            if (!dbManager.disconnect()){}
                //logger.error("generateAllReports() : Cannot close DataBase connection");
        }
        catch (Exception ex)
        {
            //logger.error(ex.getMessage());
            //logger.error(ex.getStackTrace());
            //Close connection with the database
            if (!dbManager.disconnect()){}
                //logger.error("generateAllReports() : Cannot close DataBase connection");
            session.setAttribute("errorCode", "process_error");
            return mapping.findForward(FAIL);
        }
        session.removeAttribute("errorCode");
        
        return mapping.findForward(SUCCESS);
    }

    private void obtainMFPs(HttpSession session)
    {
        List mfpsList = dbManager.getTotalMFPs();
        session.setAttribute("mfpsList", mfpsList);
    }
    
    private void obtainCompanies(HttpSession session) throws FileNotFoundException, IOException
    {
              
        List companiesList = dbManager.getCompaniesWebUsers();
        session.setAttribute("companiesList", companiesList);
    }
    
    private void deleteMFP(HttpServletRequest request)
    {
        String company = request.getParameter("companyname");
        if (!company.isEmpty())
        {
            if (!dbManager.deleteCompany(company)){}
            //logger.error("generateAllReports() : Cannot close DataBase connection");
        }
    }

    private void insertMFP(HttpServletRequest request)
    {

        String company = request.getParameter("companyname");

        if (!dbManager.insertCompany(company)){}
        //logger.error("generateAllReports() : Cannot close DataBase connection");

    }
    
}
