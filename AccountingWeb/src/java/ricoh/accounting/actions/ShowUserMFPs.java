/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ricoh.accounting.actions;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import ricoh.accounting.database.DataBaseManager;
import ricoh.accounting.ifactory.InterfaceManager;
import ricoh.accounting.interfaces.IFileManager;
import ricoh.accounting.objects.ObjectManager;
import ricoh.accounting.objects.Resume;
import ricoh.accounting.objects.User;
import ricoh.accounting.remotecc.RemoteccPlugin;

/**
 *
 * @author Alexis.Hidalgo
 */
public class ShowUserMFPs extends org.apache.struts.action.Action {

    private static final String SUCCESS = "SUCCESS";
    private static final String FAIL = "FAIL";

    private DataBaseManager dbManager;
    private ObjectManager objectManager;
    private IFileManager fileManager;
    private String CSV_CCUSER_FOLDER = "";
    private String CC_OBTAINING_ACTIVATION_MODE = "";
    RemoteccPlugin remoteccplugin;
    ;
    
    private String AD_AUTHSIMPLE = "";
    private String AD_HOST = "";
    private String AD_PORT = "";
    private String AD_USER = "";
    private String AD_PASS = "";
    private String AD_BASE = "";
    private String AD_FILTER = "";
    private String AD_USER_USERNAME = "";
    private String AD_USER_FULLNAME = "";
    private String AD_USER_CC = "";
    private String AD_USER_CCNAME = "";

    /**
     * This is the action called from the Struts framework.
     *
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response) {
        String printerName, userName;
        Resume resume;
        List users, userPrinters, resumeList = new ArrayList();
        Date START_DATE, END_DATE, EXEC_DATE;
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        User user;
        Iterator usersIterator, printersIterator;
//        BBVAPlugin bbvaPlug;
        objectManager = new ObjectManager();
        fileManager = InterfaceManager.getFileManagerInstance();

        // Obtain cost center ids from csv'S /WEB-INF/CC
        getCCs();

        Runtime.getRuntime().gc();

        HttpSession session = request.getSession(true);

        if (CC_OBTAINING_ACTIVATION_MODE.isEmpty()) {
            session.setAttribute("CC_ACTIVATION", "false");
        } else {
            session.setAttribute("CC_ACTIVATION", "true");
        }

        String company = (String) session.getAttribute("company");
        Boolean isAdmin = (Boolean) session.getAttribute("isAdmin");

        String strDateIni = request.getParameter("iniDate");
        String strDateEnd = request.getParameter("endDate");

        if (strDateIni.isEmpty() || strDateEnd.isEmpty()) {
            session.setAttribute("errorCode", "empty_date");
            return mapping.findForward(FAIL);
        }

        try {
            START_DATE = dateFormat.parse(strDateIni);
            END_DATE = dateFormat.parse(strDateEnd);

            EXEC_DATE = getExecutingDate();
        } catch (ParseException ex) {
            System.out.println(ex.getMessage());
            System.out.println(ex.getStackTrace());

            session.setAttribute("errorCode", "parse_fail");
            return mapping.findForward(FAIL);
        }

        if (START_DATE.after(END_DATE)) {
            session.setAttribute("errorCode", "start_after_end");
            return mapping.findForward(FAIL);
        } else if (END_DATE.after(EXEC_DATE)) {
            session.setAttribute("errorCode", "end_after_today");
            return mapping.findForward(FAIL);
        }

        try {
            dbManager = InterfaceManager.getDataBaseManagerInstance(
                    servlet.getServletContext().getRealPath("/WEB-INF/config.properties"),
                    servlet.getServletContext().getRealPath("/WEB-INF/sql.properties"));

            if (!dbManager.isConnected()) {
                //Open connection with the database
                if (!dbManager.connect()) {
                    //logger.error("generateAllReports() : Cannot open DataBase connection");
                    System.out.println("ERROR: Cannot open DataBase connection");
                    session.setAttribute("errorCode", "db_error");
                    return mapping.findForward(FAIL);
                }
            }
//            users = dbManager.getReportingUsers();

            if (isAdmin) //****** Suposición: ningún usuario imprimirá en máquinas no pertanecientes a su empresa *************/ 
            {
                users = dbManager.getReportingUsers();
            } else {
                users = dbManager.getReportingUsers_PerCompany(company);
            }

//            bbvaPlug = getPluginInstance();
            usersIterator = users.listIterator();
            while (usersIterator.hasNext()) {
                userName = (String) usersIterator.next();

                // ***************************** ADD FOR BBVA ************
//                user = getUserObject(bbvaPlug, userName);
                // ***************************** ADD FOR BBVA ************
                if (isAdmin) {
                    userPrinters = dbManager.getUserMFPs(userName);
                } else {
                    userPrinters = dbManager.getUserMFPs(userName, company);
                }

                printersIterator = userPrinters.listIterator();
                while (printersIterator.hasNext()) {
                    printerName = (String) printersIterator.next();
                    resume = dbManager.getTotalsBetweenDates(userName, printerName, START_DATE, END_DATE);

                    // ***************************** ADD FOR BBVA *******
//                    resume.setUser(user);
                    // ***************************** ADD FOR BBVA *******
                    // ***************************** ADD FOR MASSIMO *********
                    if (CC_OBTAINING_ACTIVATION_MODE.equalsIgnoreCase("CSV")) {
                        user = getUserByID(userName);
                    } else if (CC_OBTAINING_ACTIVATION_MODE.equalsIgnoreCase("extDB")) {
                        user = getUserObject(remoteccplugin, userName);
                        if (user == null) {
                            user = new User(userName, "", "", "");
                        }
                    } else if (CC_OBTAINING_ACTIVATION_MODE.equalsIgnoreCase("AD")) {
                        if (!userName.isEmpty()) {
//                            user = getADUserByID(userName);
                            user = getUserByID(userName);
                        } else {
                            user = new User(userName, "", "", "");
                        }
                    } else {
                        user = new User(userName, "", "", "");
                    }
////                    user = null;
////                    user = getUserByID(userName);
//                    if (user == null)
//                        user = new User(userName, "", "");
                    resume.setUser(user);
                    // ***************************** ADD FOR MASSIMO *********

                    resumeList.add(resume);
                }
            }
            //Close connection with the database
            if (!dbManager.disconnect()) {
            }
            //logger.error("generateAllReports() : Cannot close DataBase connection");

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            System.out.println(ex.getStackTrace());

            //Close connection with the database
            if (!dbManager.disconnect()) {
            }
                //logger.error("generateAllReports() : Cannot close DataBase connection");

            session.setAttribute("errorCode", "process_error");
            return mapping.findForward(FAIL);
        }

        session.setAttribute("resumeList", resumeList);
        session.setAttribute("iniDate", strDateIni);
        session.setAttribute("endDate", strDateEnd);
        session.removeAttribute("errorCode");

        return mapping.findForward(SUCCESS);
    }

    private RemoteccPlugin getPluginInstance() {
        RemoteccPlugin plugin = null;

        try {
            plugin = new RemoteccPlugin(servlet.getServletContext().getRealPath("/WEB-INF/config.properties"),
                    servlet.getServletContext().getRealPath("/WEB-INF/sql.properties"));
        } catch (FileNotFoundException ex) {
            System.out.println("ERROR: Cannot open External DataBase connection");
            System.out.println(ex.getMessage());
            System.out.println(ex.getStackTrace());
        } catch (IOException ex) {
            System.out.println("ERROR: Cannot open External DataBase connection");
            System.out.println(ex.getMessage());
            System.out.println(ex.getStackTrace());
        }
        return plugin;
    }

    private User getUserObject(RemoteccPlugin remoteplugin, String userName) {
        User user;

        if (this.objectManager.getUser_CCHash().containsKey(userName)) {
            return this.objectManager.getUser_CCHash().get(userName);
        } else {
            if (remoteplugin != null && !userName.isEmpty()) {
                user = remoteplugin.getUserByID(userName);
                if (user == null) {
                    user = new User(userName, "", "", "");
                } else {
                    this.objectManager.fillUser_CCHash(user);
                }
            } else {
                user = new User(userName, "", "", "");
            }

            return user;
        }

    }

    private User getUserByID(String username) {
        User u = new User(username, "", "", "");

        if (objectManager.getCCUserRegisterHash().containsKey(username)) {
            u = (User) objectManager.getCCUserRegisterHash().get(username);
        }

        return u;
    }

    private static SearchControls getSimpleSearchControls() {
        SearchControls searchControls = new SearchControls();
        searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
        searchControls.setTimeLimit(30000);
        return searchControls;
    }

    private void getCCs() {
        //Leer fichero properties
        Properties prop = new Properties();
        try {
            prop.load(new FileInputStream(servlet.getServletContext().getRealPath("/WEB-INF/config.properties")));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ShowUserActions.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception e) {
            Logger.getLogger(ShowUserActions.class.getName()).log(Level.SEVERE, null, e);
        }
        CSV_CCUSER_FOLDER = prop.getProperty("CSV.CCUserPath");
        CC_OBTAINING_ACTIVATION_MODE = prop.getProperty("Mode.CCObtaining");

        AD_AUTHSIMPLE = prop.getProperty("AD.AuthSimple");
        AD_HOST = prop.getProperty("AD.Host");
        AD_PORT = prop.getProperty("AD.Port");
        AD_USER = prop.getProperty("AD.User");
        AD_PASS = prop.getProperty("AD.Pass");
        AD_BASE = prop.getProperty("AD.Base");
        AD_FILTER = prop.getProperty("AD.Filter");
        AD_USER_USERNAME = prop.getProperty("AD.UserUsername");
        AD_USER_FULLNAME = prop.getProperty("AD.UserFullName");
        AD_USER_CC = prop.getProperty("AD.UserCC");
        AD_USER_CCNAME = prop.getProperty("AD.UserCCName");

        System.out.println("CC_OBTAINING_ACTIVATION_MODE: " + CC_OBTAINING_ACTIVATION_MODE);
        if (CC_OBTAINING_ACTIVATION_MODE.equalsIgnoreCase("CSV")) {
            if (!checkCCFiles()) {
                System.out.println("ERROR: Procesing CC " + CSV_CCUSER_FOLDER + " User csv file");
            }
        } else if (CC_OBTAINING_ACTIVATION_MODE.equalsIgnoreCase("extDB")) {
            remoteccplugin = getPluginInstance();
            System.out.println("INFO: [getCCs] We will get CC by means of External DB");
        } else if (CC_OBTAINING_ACTIVATION_MODE.equalsIgnoreCase("AD")) {
            if (!checkCCActiveDirectory()) {
                System.out.println("ERROR: Obtaining CC info from Active Directory");
            }
        } else {
            System.out.println("INFO: [getCCs] Dont' use CC");
        }
    }

    private boolean checkCCFiles() {
//        String value;
        List files = new LinkedList();
        System.out.println("-> Checking " + CSV_CCUSER_FOLDER + " cc files");

        files = getFileNames(CSV_CCUSER_FOLDER);//servlet.getServletContext().getRealPath(CSV_CCUSER_FOLDER));
        Iterator it = files.iterator();
        while (it.hasNext()) {

            String strFile = it.next().toString();
            if (!fileManager.openFile("", strFile, false)) {
                System.out.println("ERROR: Cannot open " + strFile + " file");
                return false;
            }

            while (fileManager.hasMoreLines(strFile)) {
                User u = new User("", "", "", "");
                u.setUsername(fileManager.readAttribute(strFile, "USERNAME").toLowerCase());
                u.setCostCenter(fileManager.readAttribute(strFile, "CC"));
                u.setDepartment(fileManager.readAttribute(strFile, "CCNAME"));
                u.setFullname(fileManager.readAttribute(strFile, "FULLNAME"));

                if (!objectManager.getCCUserRegisterHash().containsKey(u.getUsername())) {
                    System.out.println("-> " + strFile + " master file has new MFP " + u.getUsername() + " ( CC: " + u.getCostCenter() + ")");
                    objectManager.add2CCUserHash(u);
                }
//                else if (!p.equals((Printer)objectManager.getMfpRegisterHash().get(p.getMfpSerial())))
//                {
//                    logger.info(MFP_INVENTORY_FILE + " master file has updated info for MFP " + p.getIpAddress() + " (" + p.getMfpSerial() + ")");
//                    System.out.println("-> " + MFP_INVENTORY_FILE + " master file has updated info for MFP " + p.getIpAddress() + " (" + p.getMfpSerial() + ")");
//                    objectManager.update2MfpRegisterHash(p);
//                }
            }
            //Close the input file
            if (!fileManager.closeFile(strFile)) {
                System.err.println("checkCCFiles() : Cannot close " + strFile + " file");
                return false;
            }
        }

        return true;
    }

    private boolean checkCCActiveDirectory() {
        String provider = "ldap://" + AD_HOST + ":" + AD_PORT;
        Hashtable env = new Hashtable(11);

        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.PROVIDER_URL, provider);
        env.put("com.sun.jndi.ldap.connect.timeout", "10000");

        if (AD_AUTHSIMPLE.equalsIgnoreCase("true")) {
            System.out.println("Authentication: SIMPLE");
            env.put(Context.SECURITY_AUTHENTICATION, "simple");
            env.put(Context.SECURITY_PRINCIPAL, AD_USER);
            env.put(Context.SECURITY_CREDENTIALS, AD_PASS);
        } else {
            System.out.println("Authentication: NONE");
            env.put(Context.SECURITY_AUTHENTICATION, "none");
        }

        try {
            System.out.println("Creating initial context...");
            DirContext ctx = new InitialDirContext(env);
            System.out.println("Initial context created");

            // TODO: ver que filtro aplicamos
            NamingEnumeration namingEnum = ctx.search(AD_BASE, AD_FILTER, getSimpleSearchControls());

            while (namingEnum.hasMore()) {
                SearchResult sRes = (SearchResult) namingEnum.next();
                Attributes attrs = sRes.getAttributes();

                User u = new User("", "", "", "");
                try {
                    if (attrs.get(AD_USER_USERNAME) != null) {
                        String strtmp = parseADAttribute(attrs.get(AD_USER_USERNAME).toString());
                        u.setUsername(strtmp);
                        System.out.println("AD_USER_USERNAME -> " + strtmp);
                    } else {
                        System.out.println("AD_USER_USERNAME -> NULL");
                    }
                    if (attrs.get(AD_USER_CC) != null) {
                        String strtmp = parseADAttribute(attrs.get(AD_USER_CC).toString());
                        u.setCostCenter(strtmp);
                        System.out.println("AD_USER_CC -> " + strtmp);
                    } else {
                        System.out.println("AD_USER_CC -> NULL");
                    }
                    if (attrs.get(AD_USER_CCNAME) != null) {
                        String strtmp = parseADAttribute(attrs.get(AD_USER_CCNAME).toString());
                        u.setDepartment(strtmp);
                        System.out.println("AD_USER_CCNAME -> " + strtmp);
                    } else {
                        System.out.println("AD_USER_CCNAME -> NULL");
                    }
                    if (attrs.get(AD_USER_FULLNAME) != null) {
                        String strtmp = parseADAttribute(attrs.get(AD_USER_FULLNAME).toString());
                        u.setFullname(strtmp);
                        System.out.println("AD_USER_FULLNAME -> " + strtmp);
                    } else {
                        System.out.println("AD_USER_FULLNAME -> NULL");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (!objectManager.getCCUserRegisterHash().containsKey(u.getUsername())) {
                    System.out.println("-> " + AD_HOST + " ActiveDirectory has new user " + u.getUsername() + " ( CC: " + u.getCostCenter() + ")");
                    objectManager.add2CCUserHash(u);
                }
            }

            return true;

        } catch (Exception e) {
            System.out.println("checkCCActiveDirectory() : An error has ocurred: " + e);
            return false;
        }
    }

    public List getFileNames(String spoolPath) {
        File[] spoolFiles = null;
        List filesList = new ArrayList();
        File f = new File(spoolPath);
        spoolFiles = f.listFiles();

        for (int i = 0; i < spoolFiles.length; i++) {
            System.out.println("CSV Files: " + spoolFiles[i].getName());
            filesList.add(spoolPath + "/" + spoolFiles[i].getName());
        }

        return filesList;
    }

    
    private String parseADAttribute(String ADparam) {
        try {
            String tmp = ADparam;

            tmp = tmp.substring(tmp.indexOf(":") + 1);
            tmp = tmp.trim();

            return tmp;
        } catch (Exception e) {
            return ADparam;
        }
    }
    
    
//    private User getUserObject(BBVAPlugin bbvaPlug, String userName)
//    {
//        User user;
//
//        if (this.objectManager.getUser_CCHash().containsKey(userName))
//            return this.objectManager.getUser_CCHash().get(userName);
//        else
//        {
//            if (bbvaPlug != null && !userName.isEmpty())
//            {
//                user = bbvaPlug.getUserByID(userName);
//                if (user == null)
//                    user = new User(userName, "", "");
//                else
//                    this.objectManager.fillUser_CCHash(user);
//            }
//            else
//                user = new User(userName, "", "");
//
//            return user;
//        }
//
//    }
    private Date getExecutingDate() throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = dateFormat.format(new Date());
        return dateFormat.parse(dateString);
    }

}
