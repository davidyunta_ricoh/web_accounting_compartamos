/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ricoh.accounting.network;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import org.apache.log4j.Logger;
import org.snmp4j.CommunityTarget;
import org.snmp4j.PDU;
import org.snmp4j.Snmp;
import org.snmp4j.TransportMapping;
import org.snmp4j.event.ResponseEvent;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.smi.Address;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.UdpAddress;
import org.snmp4j.smi.VariableBinding;
import org.snmp4j.transport.DefaultUdpTransportMapping;
import ricoh.accounting.objects.Printer;
import ricoh.accounting.objects.Resume;

/**
 *
 * @author alexis.hidalgo
 */
public class SnmpManager
{
    //MP C7500 Model type OIDs
    private static final String OID_FULL_COLOR_COPIES_MPC7500 = ".1.3.6.1.4.1.367.3.2.1.2.19.5.1.9.53";
    private static final String OID_SINGLE_COLOR_COPIES_MPC7500 = ".1.3.6.1.4.1.367.3.2.1.2.19.5.1.9.55";
    private static final String OID_TWO_COLOR_COPIES_MPC7500 = ".1.3.6.1.4.1.367.3.2.1.2.19.5.1.9.56";
    private static final String OID_BW_COPIES_MPC7500 = ".1.3.6.1.4.1.367.3.2.1.2.19.5.1.9.54";
    private static final String OID_FULL_COLOR_PRINTS_MPC7500 = ".1.3.6.1.4.1.367.3.2.1.2.19.5.1.9.57";
    private static final String OID_SINGLE_COLOR_PRINTS_MPC7500 = ".1.3.6.1.4.1.367.3.2.1.2.19.5.1.9.59";
    private static final String OID_TWO_COLOR_PRINTS_MPC7500 = ".1.3.6.1.4.1.367.3.2.1.2.19.5.1.9.60";
    private static final String OID_BW_PRINTS_MPC7500 = ".1.3.6.1.4.1.367.3.2.1.2.19.5.1.9.58";
    private static final String OID_COLOR_SCANS_SENT_MPC7500 = ".1.3.6.1.4.1.367.3.2.1.2.19.5.1.9.71";
    private static final String OID_BW_SCANS_SENT_MPC7500 = ".1.3.6.1.4.1.367.3.2.1.2.19.5.1.9.72";
    private static final String OID_FAX_SENT_MPC7500 = ".1.3.6.1.4.1.367.3.2.1.2.19.5.1.9.70";
    private static final String OID_FAX_RECEIVED_MPC7500 = ".1.3.6.1.4.1.367.3.2.1.2.19.5.1.9.61";

    //SP C232SF Model type OIDs
    private static final String OID_FULL_COLOR_COPIES_SPC232SF = ".1.3.6.1.4.1.367.3.2.1.2.19.5.1.9.5";
    private static final String OID_BW_COPIES_SPC232SF = ".1.3.6.1.4.1.367.3.2.1.2.19.5.1.9.4";
    private static final String OID_FULL_COLOR_PRINTS_SPC232SF = ".1.3.6.1.4.1.367.3.2.1.2.19.5.1.9.3";
    private static final String OID_BW_PRINTS_SPC232SF = ".1.3.6.1.4.1.367.3.2.1.2.19.5.1.9.2";
    private static final String OID_FAX_RECEIVED_SPC232SF = ".1.3.6.1.4.1.367.3.2.1.2.19.5.1.9.12";

    private String READ_COMMUNITY = "public";
    private String SNMP_PORT = "161";
    private int SNMP_RETRIES = 2;
    private int SNMP_TIMEOUT = 3000;
    private int SNMP_VERSION = SnmpConstants.version2c;

    private static enum mfpModel { SPC232SF, MPC7500 };

    public static Logger logger = Logger.getLogger(SnmpManager.class.getName());

    public SnmpManager(String propFile) throws IOException
    {
            //Leer fichero properties
            Properties prop = new Properties();
            prop.load(new FileInputStream(propFile));
            READ_COMMUNITY = prop.getProperty("SNMP.ReadCommunity");

            if (!prop.getProperty("SNMP.Port").isEmpty())
                SNMP_PORT = prop.getProperty("SNMP.Port");

            if (!prop.getProperty("SNMP.Retries").isEmpty())
                SNMP_RETRIES = Integer.parseInt(prop.getProperty("SNMP.Retries"));

            if (!prop.getProperty("SNMP.Timeout").isEmpty())
                SNMP_TIMEOUT = Integer.parseInt(prop.getProperty("SNMP.Timeout"));

            if (prop.getProperty("SNMP.Version").equals("v1"))
                SNMP_VERSION = SnmpConstants.version1;
            else if (prop.getProperty("SNMP.Version").equals("v2"))
                SNMP_VERSION = SnmpConstants.version2c;
    }

    public boolean getCounters(Printer printer)
    {
        Resume mfpResume;

        if (printer.getModelType().toLowerCase().equals(Printer.MFP232))
        {
            logger.info("MFP " + printer.getIpAddress() + " (" + printer.getMfpSerial() + ") is an SP C232SF model type");
            mfpResume = snmpGet(printer.getIpAddress(),SNMP_PORT, READ_COMMUNITY, mfpModel.SPC232SF);
            if (mfpResume != null)
            {
                printer.setCountersResume(mfpResume);
                printer.getCountersResume().setId(printer.getMfpSerial());
            }
            return true;
        }
        else if (printer.getModelType().toLowerCase().equals(Printer.MFP7500))
        {
            logger.info("MFP " + printer.getIpAddress() + " (" + printer.getMfpSerial() + ") is an MP C7500 model type");
            mfpResume = snmpGet(printer.getIpAddress(),SNMP_PORT, READ_COMMUNITY, mfpModel.MPC7500);
            if (mfpResume != null)
            {
                printer.setCountersResume(mfpResume);
                printer.getCountersResume().setId(printer.getMfpSerial());
            }
            return true;
        }
        else
        {
            logger.error("getCounters() : MFP " + printer.getIpAddress() + " (" + printer.getMfpSerial() + ") is not an SP C232SF neither MP C7500 model type.");
            return false;
        }
    }

    private Resume snmpGet(String strAddress, String strPort, String communityPassword, mfpModel model)
    {
        Resume resume = null;
        PDU pdu;

        try
        {
            OctetString community = new OctetString(communityPassword);
            strAddress = strAddress + "/" + strPort;
            Address targetaddress = new UdpAddress(strAddress);
            TransportMapping transport = new DefaultUdpTransportMapping();
            transport.listen();
            CommunityTarget comtarget = new CommunityTarget();
            comtarget.setCommunity(community);
            comtarget.setVersion(SNMP_VERSION);
            comtarget.setAddress(targetaddress);
            comtarget.setRetries(SNMP_RETRIES);
            comtarget.setTimeout(SNMP_TIMEOUT);
            pdu = createPDU(model);
            pdu.setType(PDU.GET);
            Snmp snmp = new Snmp(transport);
            ResponseEvent response = snmp.get(pdu,comtarget);
            if(response != null)
            {
                logger.info("Snmp response for " + strAddress + ": " + response.getResponse());
                if (response.getResponse() != null)
                {
                    if(response.getResponse().getErrorStatusText().equalsIgnoreCase("Success"))
                    {
                        PDU pduresponse = response.getResponse();
                        resume = convert2CountersResume(pduresponse, model);
                    }
                    else
                    {
                        logger.error("snmpGet() : Fail response ");
                        System.out.println("-> Fail response ");
                    }
                }
                else
                {
                    logger.error("snmpGet() : The device is offline ");
                    System.out.println("-> The device is offline ");
                }
            }
            else
            {
                logger.error("snmpGet() : Feeling like a TimeOut occurred ");
                System.out.println("-> Feeling like a TimeOut occurred ");
            }
            snmp.close();
        }
        catch(Exception e)
        {
            logger.error(e.getMessage());
            logger.error(e.getStackTrace());
        }
        return resume;
    }

    private PDU createPDU(mfpModel model)
    {
        PDU pdu = new PDU();

        if (model.equals(mfpModel.MPC7500))
        {
            pdu.add(new VariableBinding(new OID(OID_FULL_COLOR_COPIES_MPC7500)));
            pdu.add(new VariableBinding(new OID(OID_SINGLE_COLOR_COPIES_MPC7500)));
            pdu.add(new VariableBinding(new OID(OID_TWO_COLOR_COPIES_MPC7500)));
            pdu.add(new VariableBinding(new OID(OID_BW_COPIES_MPC7500)));
            pdu.add(new VariableBinding(new OID(OID_FULL_COLOR_PRINTS_MPC7500)));
            pdu.add(new VariableBinding(new OID(OID_SINGLE_COLOR_PRINTS_MPC7500)));
            pdu.add(new VariableBinding(new OID(OID_TWO_COLOR_PRINTS_MPC7500)));
            pdu.add(new VariableBinding(new OID(OID_BW_PRINTS_MPC7500)));
            pdu.add(new VariableBinding(new OID(OID_COLOR_SCANS_SENT_MPC7500)));
            pdu.add(new VariableBinding(new OID(OID_BW_SCANS_SENT_MPC7500)));
            pdu.add(new VariableBinding(new OID(OID_FAX_SENT_MPC7500)));
            pdu.add(new VariableBinding(new OID(OID_FAX_RECEIVED_MPC7500)));
        }
        else if (model.equals(mfpModel.SPC232SF))
        {
            pdu.add(new VariableBinding(new OID(OID_FULL_COLOR_COPIES_SPC232SF)));
            pdu.add(new VariableBinding(new OID(OID_BW_COPIES_SPC232SF)));
            pdu.add(new VariableBinding(new OID(OID_FULL_COLOR_PRINTS_SPC232SF)));
            pdu.add(new VariableBinding(new OID(OID_BW_PRINTS_SPC232SF)));
            pdu.add(new VariableBinding(new OID(OID_FAX_RECEIVED_SPC232SF)));
        }
        return pdu;
    }

    private Resume convert2CountersResume(PDU pduresponse, mfpModel model)
    {
        Resume resume = new Resume("", null);

        if (model.equals(mfpModel.MPC7500))
        {
            resume.setCopyColor(getCounterValue(pduresponse.getVariableBindings().elementAt(0).toString()) +
                                getCounterValue(pduresponse.getVariableBindings().elementAt(1).toString()) +
                                getCounterValue(pduresponse.getVariableBindings().elementAt(2).toString()));

            resume.setCopyBW(getCounterValue(pduresponse.getVariableBindings().elementAt(3).toString()));

            resume.setPrintColor(getCounterValue(pduresponse.getVariableBindings().elementAt(4).toString()) +
                                 getCounterValue(pduresponse.getVariableBindings().elementAt(5).toString()) +
                                 getCounterValue(pduresponse.getVariableBindings().elementAt(6).toString()));

            resume.setPrintBW(getCounterValue(pduresponse.getVariableBindings().elementAt(7).toString()));

            resume.setScanSend(getCounterValue(pduresponse.getVariableBindings().elementAt(8).toString()) +
                               getCounterValue(pduresponse.getVariableBindings().elementAt(9).toString()));

            resume.setFaxSend(getCounterValue(pduresponse.getVariableBindings().elementAt(10).toString()));

            resume.setFaxReceived(getCounterValue(pduresponse.getVariableBindings().elementAt(10).toString()));
        }
        else if (model.equals(mfpModel.SPC232SF))
        {
            resume.setCopyColor(getCounterValue(pduresponse.getVariableBindings().elementAt(0).toString()));
            resume.setCopyBW(getCounterValue(pduresponse.getVariableBindings().elementAt(1).toString()));

            resume.setPrintColor(getCounterValue(pduresponse.getVariableBindings().elementAt(2).toString()));
            resume.setPrintBW(getCounterValue(pduresponse.getVariableBindings().elementAt(3).toString()));

            resume.setFaxReceived(getCounterValue(pduresponse.getVariableBindings().elementAt(4).toString()));
        }
        resume.setTotal(resume.getCopyBW() +
                        resume.getCopyColor() +
                        resume.getPrintBW() +
                        resume.getPrintColor() +
                        resume.getFaxReceived());
        return resume;
    }

    private int getCounterValue(String strCounter)
    {
        if(strCounter.contains("="))
        {
            int len = strCounter.indexOf("=");
            strCounter = strCounter.substring(len+1, strCounter.length()).trim();
        }
        if (strCounter.isEmpty())
            return 0;
        else
            return Integer.parseInt(strCounter);
    }

//    public static void main (String[] args)
//    {
//        SnmpManager manager = new SnmpManager();
//        Printer p = new Printer("M8090700277", "172.20.181.46", "aficio mp c7500");
//        manager.getCounters(p);
//
//        Printer p2 = new Printer("S5499602979", "172.23.150.170", "aficio sp c232sf");
//        manager.getCounters(p2);
//    }

}
