/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ricoh.accounting.network;

import java.io.File;
import java.io.FilenameFilter;

/**
 *
 * @author Marc.altayo
 */
public class FileExtensionFilter implements FilenameFilter{
    
    String[] extensions;

    public FileExtensionFilter(String[] extensions) {
        this.extensions = extensions;
    }

    public boolean accept(File dir, String name) {
        for (int i = 0; i < extensions.length; i++) {
            if(name.toLowerCase().endsWith(extensions[i].toLowerCase()))
                return true;
        }
        return false;
    }

}
