/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ricoh.accounting.network;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import org.apache.http.HttpEntity;
import org.apache.http.HttpException;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.auth.AuthScheme;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.AuthState;
import org.apache.http.auth.Credentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CookieStore;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.ExecutionContext;
import org.apache.http.protocol.HTTP;
import org.apache.http.protocol.HttpContext;

/**
 *
 * @author alexis.hidalgo
 */
public class HttpManager
{
    private DefaultHttpClient httpClient;

    public HttpManager()
    {
        httpClient = new DefaultHttpClient();
    }

    public void login()
    {
        try
        {
            HttpGet httpGet = new HttpGet("http://fprintlar02:8080/wsdm/index.html");
            //HttpGet httpGet = new HttpGet("http://10.144.94.10:8080/cap-es/");
            Credentials defaultcreds = new UsernamePasswordCredentials("admin", "password");

            httpClient.getCredentialsProvider().setCredentials(AuthScope.ANY, defaultcreds);

            // Create a local instance of cookie store
            CookieStore cookieStore = new BasicCookieStore();

            // Create local HTTP context
            HttpContext localContext = new BasicHttpContext();
            // Bind custom cookie store to the local context
            localContext.setAttribute(ClientContext.COOKIE_STORE, cookieStore);


            HttpResponse response = httpClient.execute(httpGet, localContext);
            HttpEntity entity = response.getEntity();

            System.out.println("----------------------------------------");
            System.out.println(response.getStatusLine());
            if (entity != null) {
                System.out.println("Response content length: " + entity.getContentLength());
                entity.consumeContent();
            }

            System.out.println("Initial set of cookies:");
            List<Cookie> cookies = cookieStore.getCookies();
            if (cookies.isEmpty()) {
                System.out.println("None");
            } else {
                for (int i = 0; i < cookies.size(); i++) {
                    System.out.println("- " + cookies.get(i).toString());
                }
            }

            HttpPost httpPost = new HttpPost("http://fprintlar02:8080/wsdm/pc/basic.Login");
            //HttpPost httpPost = new HttpPost("http://10.144.94.10:8080/cap-es/login.do");

            response = httpClient.execute(httpPost, localContext);
            entity = response.getEntity();
            System.out.println("----------------------------------------");
            System.out.println(response.getStatusLine());

            if (entity != null) {
                System.out.println("Response content length: " + entity.getContentLength());
                entity.consumeContent();
            }

            System.out.println("Post logon cookies:");
            cookies = cookieStore.getCookies();
            if (cookies.isEmpty()) {
                System.out.println("None");
            } else {
                for (int i = 0; i < cookies.size(); i++) {
                    System.out.println("- " + cookies.get(i).toString());
                }
            }

            List<NameValuePair> formParams = new ArrayList<NameValuePair>();
//            //formParams.add(new BasicNameValuePair("token", ""));
//            formParams.add(new BasicNameValuePair("isSubWnd", "false"));
//            formParams.add(new BasicNameValuePair("wndName", ""));
//            formParams.add(new BasicNameValuePair("ACTION_login", "Login"));
//            formParams.add(new BasicNameValuePair("returnTo", ""));
//            formParams.add(new BasicNameValuePair("user", "admin"));
//            formParams.add(new BasicNameValuePair("password", "ricoh"));
//            //formParams.add(new BasicNameValuePair("return", ""));

            formParams.add(new BasicNameValuePair("process", "loginEvent"));
            formParams.add(new BasicNameValuePair("loginUserName", "admin"));
            formParams.add(new BasicNameValuePair("loginPassword", "password"));

            UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(formParams, HTTP.UTF_8);
            httpPost.setEntity(formEntity);

            System.out.println("POSTing to " + httpPost.getURI().toString());

            response = httpClient.execute(httpPost, localContext);
            entity = response.getEntity();
            System.out.println("----------------------------------------");
            System.out.println(response.getStatusLine());

            if (entity != null) {
                System.out.println("Response content length: " + entity.getContentLength());
                entity.consumeContent();
            }

            System.out.println("Post logon cookies:");
            cookies = cookieStore.getCookies();
            if (cookies.isEmpty()) {
                System.out.println("None");
            } else {
                for (int i = 0; i < cookies.size(); i++) {
                    System.out.println("- " + cookies.get(i).toString());
                }
            }

            httpGet = new HttpGet("http://fprintlar02:8080/wsdm/pc/device.DeviceList");
            response = httpClient.execute(httpGet, localContext);
            entity = response.getEntity();
            System.out.println("----------------------------------------");
            System.out.println(response.getStatusLine());

//            httpPost = new HttpPost("http://10.144.94.10:8080/cap-es/groupMaintenance.do");
//            formParams = new ArrayList<NameValuePair>();
//
//            //formParams.add(new BasicNameValuePair("groupCsvFile", ""));
//            formParams.add(new BasicNameValuePair("filename", "pepe.csv"));
//            formParams.add(new BasicNameValuePair("process", "downloadEvent"));
//
//            formEntity = new UrlEncodedFormEntity(formParams, HTTP.UTF_8);
//            httpPost.setEntity(formEntity);
//
//            System.out.println("POSTing to " + httpPost.getURI().toString());
//
//            response = httpClient.execute(httpPost, localContext);
//            entity = response.getEntity();
//            System.out.println("----------------------------------------");
//            System.out.println(response.getStatusLine());
//
//            if (entity != null) {
//                System.out.println("Response content length: " + entity.getContentLength());
//                entity.consumeContent();
//            }
//
//            System.out.println("Post logon cookies:");
//            cookies = cookieStore.getCookies();
//            if (cookies.isEmpty()) {
//                System.out.println("None");
//            } else {
//                for (int i = 0; i < cookies.size(); i++) {
//                    System.out.println("- " + cookies.get(i).toString());
//                }
//            }

        }
        catch (IOException ex)
        {
            //Logger.getLogger(HttpManager.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(ex.getMessage());
            ex.printStackTrace();

        }
    }

    /*public static void main(String[] args) throws IOException
    {
        //HttpManager client = new HttpManager();
        //client.login();
    }*/

public static void main(String[] args) throws Exception {

        DefaultHttpClient httpclient = new DefaultHttpClient();

        httpclient.getCredentialsProvider().setCredentials(
                new AuthScope("fprintlar02", 8080),
                new UsernamePasswordCredentials("admin", "ricoh"));

        // Create a local instance of cookie store
        CookieStore cookieStore = new BasicCookieStore();

        // Create local HTTP context
        HttpContext localcontext = new BasicHttpContext();
        // Bind custom cookie store to the local context
        localcontext.setAttribute(ClientContext.COOKIE_STORE, cookieStore);


        // Generate BASIC scheme object and stick it to the local
        // execution context
        BasicScheme basicAuth = new BasicScheme();
        localcontext.setAttribute("preemptive-auth", basicAuth);

        // Add as the first request interceptor
        httpclient.addRequestInterceptor(new PreemptiveAuth(), 0);

        HttpHost targetHost = new HttpHost("fprintlar02", 8080, "http");

        
        HttpPost httpPost = new HttpPost("/wsdm/pc/basic.Login");
        List<NameValuePair> formParams = new ArrayList<NameValuePair>();
//            //formParams.add(new BasicNameValuePair("token", ""));
        formParams.add(new BasicNameValuePair("isSubWnd", "false"));
        formParams.add(new BasicNameValuePair("wndName", ""));
        formParams.add(new BasicNameValuePair("ACTION_login", "Login"));
        formParams.add(new BasicNameValuePair("returnTo", ""));
        formParams.add(new BasicNameValuePair("user", "admin"));
        formParams.add(new BasicNameValuePair("password", "ricoh"));
//            //formParams.add(new BasicNameValuePair("return", ""));
        UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(formParams, HTTP.UTF_8);
        httpPost.setEntity(formEntity);


        System.out.println("executing request: " + httpPost.getRequestLine());
        System.out.println("to target: " + targetHost);

        HttpResponse response;
        HttpEntity entity;
        List<Cookie> cookies;
        for (int i = 0; i < 1; i++) {
            response = httpclient.execute(targetHost, httpPost, localcontext);
            entity = response.getEntity();

            System.out.println("----------------------------------------");
            System.out.println(response.getStatusLine());
            if (entity != null) {
                System.out.println("Response content length: " + entity.getContentLength());
                entity.consumeContent();
            }

            System.out.println("Post logon cookies:");
            cookies = cookieStore.getCookies();
            if (cookies.isEmpty()) {
                System.out.println("None");
            } else {
                for (int j = 0; j < cookies.size(); j++) {
                    System.out.println("- " + cookies.get(j).toString());
                }
            }

        }

        HttpGet httpget = new HttpGet("/wsdm/pc/device.DeviceList");
        System.out.println("executing request: " + httpget.getRequestLine());
        System.out.println("to target: " + targetHost);

        response = httpclient.execute(targetHost, httpget, localcontext);
        entity = response.getEntity();

        System.out.println("----------------------------------------");
        System.out.println(response.getStatusLine());
        if (entity != null) {
            System.out.println("Response content length: " + entity.getContentLength());
            entity.consumeContent();
        }

                    System.out.println("Post logon cookies:");
            cookies = cookieStore.getCookies();
            if (cookies.isEmpty()) {
                System.out.println("None");
            } else {
                for (int j = 0; j < cookies.size(); j++) {
                    System.out.println("- " + cookies.get(j).toString());
                }
            }


        httpget = new HttpGet("/wsdm/pc/device.DeviceListExport?pageName=device");
        response = httpclient.execute(targetHost, httpget, localcontext);
        entity = response.getEntity();
        System.out.println("----------------------------------------");
        System.out.println(response.getStatusLine());

//        if (entity != null) {
//            System.out.println("Response content length: " + entity.getContentLength());
//            entity.consumeContent();
//        }

        ZipInputStream zis = new ZipInputStream(httpPost.getEntity().getContent());
            ZipEntry ze;
            while ((ze = zis.getNextEntry()) != null){
                int size;
                byte[] buffer = new byte[2048];
                FileOutputStream fos = new FileOutputStream(ze.getName());
                BufferedOutputStream bos = new BufferedOutputStream(fos, buffer.length);
                while ((size = zis.read(buffer, 0, buffer.length)) != -1) {
                    bos.write(buffer, 0, size);
                }
                bos.flush();
                bos.close();
            }

                        System.out.println("Post logon cookies:");
            cookies = cookieStore.getCookies();
            if (cookies.isEmpty()) {
                System.out.println("None");
            } else {
                for (int j = 0; j < cookies.size(); j++) {
                    System.out.println("- " + cookies.get(j).toString());
                }
            }

        // When HttpClient instance is no longer needed,
        // shut down the connection manager to ensure
        // immediate deallocation of all system resources
        httpclient.getConnectionManager().shutdown();
    }

    static class PreemptiveAuth implements HttpRequestInterceptor {

        public void process(
                final HttpRequest request,
                final HttpContext context) throws HttpException, IOException {

            AuthState authState = (AuthState) context.getAttribute(
                    ClientContext.TARGET_AUTH_STATE);

            // If no auth scheme avaialble yet, try to initialize it preemptively
            if (authState.getAuthScheme() == null) {
                AuthScheme authScheme = (AuthScheme) context.getAttribute(
                        "preemptive-auth");
                CredentialsProvider credsProvider = (CredentialsProvider) context.getAttribute(
                        ClientContext.CREDS_PROVIDER);
                HttpHost targetHost = (HttpHost) context.getAttribute(
                        ExecutionContext.HTTP_TARGET_HOST);
                if (authScheme != null) {
                    Credentials creds = credsProvider.getCredentials(
                            new AuthScope(
                                    targetHost.getHostName(),
                                    targetHost.getPort()));
                    if (creds == null) {
                        throw new HttpException("No credentials for preemptive authentication");
                    }
                    authState.setAuthScheme(authScheme);
                    authState.setCredentials(creds);
                }
            }

        }

    }
}
