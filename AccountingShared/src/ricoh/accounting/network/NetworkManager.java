/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ricoh.accounting.network;

import java.io.IOException;
import java.util.List;
import ricoh.accounting.interfaces.INetworkManager;
import ricoh.accounting.objects.Printer;

/**
 *
 * @author alexis.hidalgo
 */
public class NetworkManager implements INetworkManager
{
    private SmbManager smbManager;
    private SnmpManager snmpManager;
    private HttpManager httpManager;

    public NetworkManager(String propFile) throws IOException
    {
        smbManager = new SmbManager(propFile);
        snmpManager = new SnmpManager(propFile);
    }

    public boolean getCounters(Printer printer)
    {
        return snmpManager.getCounters(printer);
    }

    public boolean isJobLogOnLocalMachine()
    {
        return smbManager.isJobLogOnLocalMachine();
    }

    public boolean downloadLogFile(String fileName, String destPath)
    {
        return smbManager.downloadLogFile(fileName, destPath);
    }

    public boolean downloadLogFile(String fileName)
    {
        return smbManager.downloadLogFile(fileName);
    }

    public void backupLogFile(String fileName, String backupPath)
    {
        smbManager.backupLogFile(fileName, backupPath);
    }

    public void backupLogFile(String fileName)
    {
        smbManager.backupLogFile(fileName);
    }

    public void cleanLogFolder()
    {
        smbManager.cleanLogFolder();
    }

    public void deleteLogFile(String fileName)
    {
        smbManager.deleteLogFile(fileName);
    }

    public List getLogFileNames(String sourcePath, boolean onRemote)
    {
        return smbManager.getLogFileNames(sourcePath, onRemote);
    }

    public List getLogFileNames(boolean onRemote)
    {
        return smbManager.getLogFileNames(onRemote);
    }

    public String getDefaultDestinationPath()
    {
        return smbManager.getDefaultDestinationPath();
    }

    public String getJobLogPath()
    {
        return smbManager.getJobLogPath();
    }
}
