/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ricoh.accounting.network;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileInputStream;
import org.apache.log4j.Logger;

/**
 *
 * @author Alexis.Hidalgo
 */
public class SmbManager
{
    private final String FILTER = "*.csv";
    
    private String URIprefix = "smb://";
    private String credentials = "";

    private String SERVER_USER = "";
    private String SERVER_PASSWORD = "";
    private String SERVER_HOST = "";
    private String SERVER_DOMAIN = "";
    private String SERVER_LOGFOLDER = "";
    private String SERVER_BACKUPFOLDER = "";
    private String SERVER_LOGPATH = "";
    private String LOCAL_DESTINATION = "";

    private SmbFile[] csvFiles = null;
    private File[] csvLocalFiles = null;
    private boolean[] backupState = null;

    private final int BUFFER = 102400;

    public static Logger logger = Logger.getLogger(SmbManager.class.getName());

    public SmbManager(String propFile) throws IOException
    {
            //Leer fichero properties
            Properties prop = new Properties();
            prop.load(new FileInputStream(propFile));
            SERVER_USER = prop.getProperty("JoblogServer.User");
            SERVER_PASSWORD = prop.getProperty("JoblogServer.Password");
            SERVER_HOST = prop.getProperty("JoblogServer.Host");
            SERVER_DOMAIN = prop.getProperty("JoblogServer.Domain");
            SERVER_LOGFOLDER = prop.getProperty("JoblogServer.LogFolder");
            SERVER_BACKUPFOLDER = prop.getProperty("JoblogServer.BackupFolder");
            SERVER_LOGPATH = prop.getProperty("JoblogServer.LogPath");
            LOCAL_DESTINATION = prop.getProperty("CSV.Destination");

            credentials = SERVER_USER + ":" + SERVER_PASSWORD + "@";
            if (credentials.equals(":@"))
                credentials = "";

            if (SERVER_DOMAIN.isEmpty())
                URIprefix =  URIprefix + credentials + SERVER_HOST;
            else
                URIprefix =  URIprefix + SERVER_DOMAIN + ";" + credentials + SERVER_HOST;
    }

    public boolean isJobLogOnLocalMachine()
    {
        try
        {
            String localIP = InetAddress.getLocalHost().getHostAddress();
            String serverIP = InetAddress.getByName(SERVER_HOST).getHostAddress();
            logger.info("Local IP Address = " + localIP);
            logger.info("Joblog Host IP Address = " + serverIP);

            if (localIP.equals(serverIP))
                return true;
            else
                return false;
        }
        catch (UnknownHostException ex)
        {
            logger.error("isJobLogOnLocalMachine():" + ex.getMessage());
            logger.error(ex.getStackTrace());
            return false;
        }
    }
    public boolean downloadLogFile(String fileName, String destPath)
    {
        byte[] buf;
        int index = 0, length;
        boolean found = false, allOK = true;
        SmbFileInputStream fInput = null;
        FileOutputStream fOutput = null;

        if (csvFiles == null)
        {
            logger.error("downloadLogFile() : Cannot download Joblog " + fileName + " to " + destPath + " -> csvFiles list is null");
            return false;
        }

        while (index < csvFiles.length && !found)
        {
            if (csvFiles[index].getName().equals(fileName))
                found = true;
            else
                index++;
        }

        if (!found)
        {
            logger.error("downloadLogFile() : Cannot download Joblog file " + fileName + " to " + destPath + " -> not found on smb folder");
            return false;
        }

        try 
        {
            File destDir = new File(destPath);
            if (!destDir.exists())
                destDir.mkdirs();

            logger.info("Connecting by smb to get Joblog.... " + csvFiles[index]);
            System.out.println("-> Connecting by smb to get Joblog.... " + fileName);

            fInput = new SmbFileInputStream(csvFiles[index]);
            fOutput = new FileOutputStream(destPath + csvFiles[index].getName());

            logger.info("Downloading Joblog to " + destPath);
            System.out.println("-> Downloading Joblog to " + destPath);

            buf = new byte[BUFFER];
            while ((length = fInput.read(buf)) > 0)
                fOutput.write(buf, 0, length);
            
            fOutput.flush();
            allOK = true;
        }
        catch (IOException ex)
        {
            logger.error("downloadLogFile():" + ex.getMessage());
            logger.error(ex.getStackTrace());
            allOK = false;
        }
        finally
        {
            try
            {
                fOutput.close();
                fInput.close();
            }
            catch (IOException ex)
            {
                logger.error("downloadLogFile():" + ex.getMessage());
                logger.error(ex.getStackTrace());
                allOK = false;
            }
            return allOK;
        }
    }

    public boolean downloadLogFile(String fileName)
    {
        return downloadLogFile(fileName, this.LOCAL_DESTINATION);
    }

    /***** Método backupLogFile antiguo (SMB) *****/
//    public void backupLogFile(String fileName, final String backupPath)
//    {
//        int index = 0;
//        boolean found = false;
//
//        logger.info("Backuping Joblog " + fileName + " to " + backupPath);
//        System.out.println("-> Backuping Joblog " + fileName + " to " + backupPath);
//
//        if (csvFiles == null)
//        {
//            logger.error("backupLogFile() : Cannot backup Joblog " + fileName + " to " + backupPath + " -> csvFiles list is null");
//            return;
//        }
//
//        while (index < csvFiles.length && !found)
//        {
//            if (csvFiles[index].getName().equals(fileName))
//                found = true;
//            else
//                index++;
//        }
//
//        if (!found)
//        {
//            logger.error("backupLogFile() : Cannot backup Joblog " + fileName + " to " + backupPath + " -> not found on smb folder");
//            return;
//        }
//
//        copyFile(backupPath, index);
//
//    }
    
    public void backupLogFile(String fileName, final String backupPath)
    {
        int index = 0;
        boolean found = false;

        logger.info("Backuping Joblog " + fileName + " to " + backupPath);
        System.out.println("-> Backuping Joblog " + fileName + " to " + backupPath);

        if (csvLocalFiles == null)
        {
            logger.error("backupLogFile() : Cannot backup Joblog " + fileName + " to " + backupPath + " -> csvFiles list is null");
            return;
        }

        while (index < csvLocalFiles.length && !found)
        {
            if (csvLocalFiles[index].getName().equals(fileName))
                found = true;
            else
                index++;
        }

        if (!found)
        {
            logger.error("backupLogFile() : Cannot backup Joblog " + fileName + " to " + backupPath + " -> not found on smb folder");
            return;
        }

        copyFile(backupPath, index);

    }

    public void backupLogFile(String fileName)
    {
        backupLogFile(fileName, this.SERVER_BACKUPFOLDER);
    }

    public void deleteLogFile(String fileName)
    {
        int index = 0;
        boolean found = false;

        logger.info("Deleting Joblog " + fileName);

        if (csvFiles == null)
        {
            logger.error("deleteLogFile() : Cannot delete Joblog " + fileName +  " -> csvFiles list is null");
            return;
        }

        while (index < csvFiles.length && !found)
        {
            if (csvFiles[index].getName().equals(fileName))
                found = true;
            else
                index++;
        }

        if (!found)
        {
            logger.error("deleteLogFile() : Cannot delete Joblog " + fileName + " -> not found on smb folder");
            return;
        }

        try
        {
            csvFiles[index].delete();
        }
        catch (SmbException ex)
        {
            logger.error("deleteLogFile():" + ex.getMessage());
            logger.error(ex.getStackTrace());
            return;
        }
    }

    public void cleanLogFolder()
    {
        List fails = new ArrayList();
        int index;

        logger.info("Cleaning Joblog source folder");

        if (csvLocalFiles == null)
        {
            logger.error("cleanLogFolder(): Cannot clean Joblog folder -> csvFiles list is null");
            return;
        }

        for (int i = 0; i < csvLocalFiles.length; i++)
        {
            if (backupState[i])
            {
                try
                {
                    csvLocalFiles[i].delete();
                }
                catch (Exception ex)
                {
                    logger.error("cleanLogFolder():" + ex.getMessage());
                    logger.error(ex.getStackTrace());
                    fails.add(i);
                }
            }
            else
                fails.add(i);
        }

        if (!fails.isEmpty())
        {
            logger.error("cleanLogFolder() : Backuping log files. Following files have fail:");
            System.out.println("ERROR: Backuping log files. Following files have fail:");
            for(int i = 0; i < fails.size(); i++)
            {
                index = (Integer)fails.get(i);
                logger.error(csvLocalFiles[index].getName());
                System.out.println(" *** " + csvLocalFiles[index].getName());
            }
        }
        else
        {
            logger.info("All log files backuped correctly");
            System.out.println("-> All log files backuped correctly");
        }
    }
    
   /***** Método cleanLogFolder antiguo (SMB) *****/
//    public void cleanLogFolder()
//    {
//        List fails = new ArrayList();
//        int index;
//
//        logger.info("Cleaning Joblog source folder");
//
//        if (csvFiles == null)
//        {
//            logger.error("cleanLogFolder(): Cannot clean Joblog folder -> csvFiles list is null");
//            return;
//        }
//
//        for (int i = 0; i < csvFiles.length; i++)
//        {
//            if (backupState[i])
//            {
//                try
//                {
//                    csvFiles[i].delete();
//                }
//                catch (SmbException ex)
//                {
//                    logger.error("cleanLogFolder():" + ex.getMessage());
//                    logger.error(ex.getStackTrace());
//                    fails.add(i);
//                }
//            }
//            else
//                fails.add(i);
//        }
//
//        if (!fails.isEmpty())
//        {
//            logger.error("cleanLogFolder() : Backuping log files. Following files have fail:");
//            System.out.println("ERROR: Backuping log files. Following files have fail:");
//            for(int i = 0; i < fails.size(); i++)
//            {
//                index = (Integer)fails.get(i);
//                logger.error(csvFiles[index].getName());
//                System.out.println(" *** " + csvFiles[index].getName());
//            }
//        }
//        else
//        {
//            logger.info("All log files backuped correctly");
//            System.out.println("-> All log files backuped correctly");
//        }
//    }

    public List getLogFileNames(String sourcePath, boolean onRemote)
    {
        try
        {
            String logURI = URIprefix + sourcePath;   //  Original
//            String logURI = URIprefix + ";" + sourcePath;
            logger.info("Getting all Joblog file names from " + logURI);

            List filesList = new ArrayList();

            if (onRemote){
                SmbFile sourceDir = new SmbFile(logURI);
                csvFiles = sourceDir.listFiles(FILTER);
                backupState = new boolean[csvFiles.length];
                
                for (int i = 0; i < csvFiles.length; i++)
                {
                    filesList.add(csvFiles[i].getName());
                    backupState[i] = false;
                }
            }else{
                File sourceDir = new File(sourcePath);
                csvLocalFiles = sourceDir.listFiles(new FileExtensionFilter(new String[]{".csv", ".CSV"}));
                backupState = new boolean[csvLocalFiles.length];
                for (int i = 0; i < csvLocalFiles.length; i++)
                {
                    filesList.add(csvLocalFiles[i].getName());
                    backupState[i] = false;
                }
            }


            return filesList;
        }
        catch (SmbException ex)
        {
            logger.error("getLogFileNames(): " + "SmbException " + ex.getMessage());
            logger.error(ex.getStackTrace());
            return new ArrayList();
        }
        catch (MalformedURLException ex)
        {
            logger.error("getLogFileNames(): " + "MalformedURLException " + ex.getMessage());
            logger.error(ex.getStackTrace());
            return new ArrayList();
        }
    }

    public List getLogFileNames(boolean onRemote)
    {
        return getLogFileNames(this.SERVER_LOGFOLDER, onRemote);
    }

    public String getDefaultDestinationPath()
    {
        return this.LOCAL_DESTINATION;
    }

    public String getJobLogPath()
    {
        return this.SERVER_LOGPATH;
    }

    private void copyFile(String copyPath, final int fileIndex)
    {
        InputStream input = null;  
        OutputStream output = null;  
        
        try
        {
            String backupURI = copyPath;
            File backupDir = new File(backupURI);

            if (!backupDir.exists())
                backupDir.mkdirs();

            input = new FileInputStream(csvLocalFiles[fileIndex]);  
            output = new FileOutputStream(new File(backupURI + csvLocalFiles[fileIndex].getName()));  
            byte[] buf = new byte[1024];  
            int bytesRead;  

            while ((bytesRead = input.read(buf)) > 0) {  
                output.write(buf, 0, bytesRead);  
            }  

            backupState[fileIndex] = true;
        }
        catch (MalformedURLException ex)
        {
            logger.error("copyFile(): " + "MalformedURLException " + ex.getMessage());
            logger.error(ex.getStackTrace());
        }
        catch (Exception ex)
        {
            logger.error("copyFile(): " + "Exception " + ex.getMessage());
            logger.error(ex.getStackTrace());
        } finally {  
            try {
                if (input != null && output != null){
                    input.close();  
                    output.close();
                }
            } catch (IOException ex) {
                java.util.logging.Logger.getLogger(SmbManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }  

    }
    
        /***** Método copyFile antiguo (SMB) *****/
//    private void copyFile(String copyPath, final int fileIndex)
//    {
//        try
//        {
//            String backupURI = URIprefix + copyPath;
//            SmbFile backupDir = new SmbFile(backupURI);
//
//            if (!backupDir.exists())
//                backupDir.mkdirs();
//            
//            csvFiles[fileIndex].copyTo(new SmbFile(backupURI + csvFiles[fileIndex].getName()));
//            backupState[fileIndex] = true;
//        }
//        catch (MalformedURLException ex)
//        {
//            logger.error("copyFile(): " + "MalformedURLException " + ex.getMessage());
//            logger.error(ex.getStackTrace());
//        }
//        catch (SmbException ex)
//        {
//            logger.error("copyFile(): " + "SmbException " + ex.getMessage());
//            logger.error(ex.getStackTrace());
//        }
//
//    }
}
