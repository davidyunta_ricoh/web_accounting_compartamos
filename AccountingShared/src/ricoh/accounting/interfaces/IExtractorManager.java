/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ricoh.accounting.interfaces;

import java.util.Date;

/**
 *
 * @author alexis.hidalgo
 */
public interface IExtractorManager
{
    public final String PRINTERS_FILENAME = "ListadoImpresoras";
    public final String RESUME_PRINTERS_FILENAME = "ContadoresImpresoras";
    public final String RESUME_USERS_FILENAME = "ContadoresUsuarios";
    public final String RESUME_USER_PRINTERS_FILENAME = "ContadoresUsuariosImpresoras";
    public final String RESUME_CECO_FILENAME = "ContadoresCECOs";
    public final String DETAIL_USERS_FILENAME = "ActividadUsuarios";
    public final String DETAIL_PRINTERS_FILENAME = "ActividadImpresoras";
    public final String USERS_ZIP_FILENAME = "InformesUsuarios";
    public final String PRINTERS_ZIP_FILENAME = "InformesImpresoras";
    public final String CECOS_ZIP_FILENAME = "InformesCECOs";
    
    public final String RESUME_USER_ACCUMULATED_FILENAME = "ContadoresImpresorasAc";
    
    public boolean generateAllReports();
    public boolean generateResumeUserPrinters();
    public boolean generateResumePrinters();
    public boolean generateResumeUsers();
    public boolean generateResumeCostCenters();
    public boolean generateDetailsUsers();
    public boolean generateDetailsPrinters();

//    public boolean generateResumeUserPrinters(String userName, Date dateStart, Date dateEnd);
    public boolean generatePrinters();
    public boolean generateAllReports(Date dateStart, Date dateEnd);
    public boolean generateResumePrinters(Date dateStart, Date dateEnd);
    public boolean generateResumePrinters(String company, String mfpserial, Date dateStart, Date dateEnd);
    public boolean generateResumeUsers(Date dateStart, Date dateEnd);
    public boolean generateResumeUsers(String company, Date dateStart, Date dateEnd);
    public boolean generateResumeUserPrinters(Date dateStart, Date dateEnd);
    public boolean generateResumeUserPrinters(String company, Date dateStart, Date dateEnd);
    public boolean generateResumeUserAccumulated(Date dateStart, Date dateEnd, String tipoVistaInformacion, String type, String tipoInforme);
    public boolean generateResumeUserAccumulated(String company, Date dateStart, Date dateEnd, String tipoVistaInformacion, String type, String tipoInforme);
    public boolean generateResumeCostCenters(Date dateStart, Date dateEnd);
    public boolean generateResumeCostCenters(String company, Date dateStart, Date dateEnd);
    public boolean generateDetailsUsers(Date dateStart, Date dateEnd);
    public boolean generateDetailsPrinters(Date dateStart, Date dateEnd);
    public boolean generateDetailsByUser(Date dateStart, Date dateEnd);
    public boolean generateDetailsByUser(String company, String username, Date dateStart, Date dateEnd);

    public String getUsersZipPath();
    public String getPrintersZipPath();
    public String getCECOsZipPath();
    public String getResumeUserPrintersFilePath();
    public String getResumeUserAccumulatedFilePath();
    public String getResumePrintersFilePath();
    public String getResumeUsersFilePath();
    public String getPrintersFilePath();
    public String getResumeCostCentersFilePath();
    public String getDetailsUsersFilePath();
    public String getDetailsPrintersFilePath();

    public void setSQL_FILE(String SQL_FILE);
}