/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ricoh.accounting.interfaces;

import java.util.List;
import ricoh.accounting.objects.Printer;

/**
 *
 * @author Alexis.Hidalgo
 */
public interface INetworkManager
{
    public boolean downloadLogFile(String fileName, String destPath);
    public boolean downloadLogFile(String fileName);
    public void backupLogFile(String fileName, String backupPath);
    public void backupLogFile(String fileName);
    public void cleanLogFolder();
    public void deleteLogFile(String fileName);
    public List getLogFileNames(String sourcePath, boolean onRemote);
    public List getLogFileNames(boolean onRemote);
    public String getDefaultDestinationPath();
    public String getJobLogPath();
    public boolean isJobLogOnLocalMachine();

    public boolean getCounters(Printer printer);
}
