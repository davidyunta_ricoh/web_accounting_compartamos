/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ricoh.accounting.interfaces;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import ricoh.accounting.objects.Action;
import ricoh.accounting.objects.Printer;
import ricoh.accounting.objects.Resume;

/**
 *
 * @author alexis.hidalgo
 */
public interface IDataBaseManager
{
    public final boolean SYNCHRONICE = true;
    public final boolean NOT_SYNCHRONICE = false;

    public boolean connect();
    public boolean disconnect();
    public boolean isConnected();
    
    public boolean checkKnownUserCounters(Date today);
    public boolean checkUnknownUserCounters(Date today);
    
    public boolean insertMFP(String mfpserial);
    public List getReportingUsers();
    public List getReportingUsersForParser();
    public List getReportingUsers(String username);
    public List getReportingUsers_PerCompany(String company);
    public List getReportingUsers_PerCompany(String company, String username);
    public List getMFPs();
    public List getMFPs(String mfpserial);
    public List getMFPs_PerCompany(String company);
    public List getMFPs_PerCompany(String company, String mfpserial);
    public List getMFPsAndCompany();
    public List getRegisterMFPs();
    public List getRemoteMFPs();
    public List getCCs();
    public List getCCs(String company);
    public List getRegisteredWebUsers();
    public List getCompaniesWebUsers();
    public List getTotalMFPs();
    public Map getRegisteredMFPs();
    public Map getRegisteredMFPs(String company);
    public List getReportingMFPs();
    public List getUnknownUserMFPs();
    public List getUnknownUserMFPs(String company);
    public List getUserMFPs(String user, Date dateStart, Date dateEnd);
    public List getUserMFPs(String user);
    public List getUserMFPs(String user, String company);
    public Resume getTotalsBetweenDates(String id, final Date dateStart, final Date dateEnd, Resume.resumeType rType);
    public Resume getTotalsBetweenDates(String username, String mfp, final Date dateStart, final Date dateEnd);
    public List getIdActions(String company, String id, Date dateStart, Date dateEnd, Action.actionType aType);
    public Action getActionFromRS(Action.actionType aType);
    public List getMFPsOnState(String state);
    public List getMFPsOnState(String state, String company);
    public Resume getLastNotReportingResume(String mfpSerial);
    public String getCECOname(String id);
    public String[] getPausedStorageJob(String jobid, String mfpserial);
    public String[] getLastCountersValues(String mfpSerial);
    public List<Printer> getSlnxMfpsFromAccountingDB();
    
    public boolean hasMoreActions(Action.actionType aType);
    public boolean fillActionsRS(Date dateStart, Date dateEnd, Action.actionType aType);
    public boolean fillActionsRS(Date dateStart, Date dateEnd, Action.actionType aType, String username);
    public boolean fillActionsRS_PerCompany(Date dateStart, Date dateEnd, Action.actionType aType, String company);
    public boolean fillActionsRS_PerCompany(Date dateStart, Date dateEnd, Action.actionType aType, String company, String username);
    public boolean fillActionsRSbyID(String id, Date dateStart, Date dateEnd, Action.actionType aType);
    
    public void setSQLqueries(String propFile) throws FileNotFoundException, IOException;
    public boolean registerMFP(Printer printer);
    public boolean registerMFP_List(String mfpserial, String model_type, String ip_address, String status, String location, String zone, String hostname, String company);
    public boolean registerWebUser(String userName, String password, String fulluserName, boolean isAdmin, String company);
    public boolean setStatusMFP(String mfpserial, String status);
//    public int authenticateUser(String userName, String password);
    public String[] authenticateUser(String userName, String password);
    public boolean setReportingMFP(String mfpSerial, boolean isReporting);

    public boolean insertActions(List actionList);
    public boolean insertNewResumeList(List resumeList, Resume.resumeType rType);
    public boolean insertNotReportingResume(Resume resume, Resume.resumeType rType);
    public boolean insertNewUserMfpResumeList(String username, List resumeList);
    public boolean insertCompany(String companyname);
    public boolean insertPausedStorageJob(String jobid, String username, String mfpserial, Date datetime, String iphost);
    //public boolean insertSpecialRegister(Resume insertRegister);
    //public boolean insertNotReportingMFP(Printer printer);

    public boolean updateUserMfpResumeList(String username, String mfp, List resumeList);
    public boolean updateResumeList(String id, List resumeList, Resume.resumeType rType);
    public boolean updateRegisteredMFP(Printer printer);

    public boolean deleteRegisteredWebUser(String userName);
    public boolean deleteMFP_List(String mfpserial);
    public boolean deleteCompany(String companyname);
    public boolean deletePausedStorageJob(String jobid, String mfpserial);
    public boolean deletePausedStorageJobs_PerDate(Date date);
    
    public Printer getMFPbyID(String printerName);

}
