/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ricoh.accounting.interfaces;

import java.util.List;
import ricoh.accounting.objects.Action;
import ricoh.accounting.objects.Resume;

/**
 *
 * @author alexis.hidalgo
 */
public interface IFileManager
{
    public boolean createFile(String path, String fileName);
    public void appendText2File(String fileName, String text);
    public void appendResume2File(String fileName, Resume resume);
    public void appendAction2File(String fileName, Action action, Action.actionType aType);
    public boolean closeFile(String fileName);
    public boolean openFile(String path, String fileName, boolean isJobLog);
    public boolean hasMoreLines(String fileName);
    public String readAttribute(String fileName, String attribute);
    public String getIPHost_StoredJob(String jobid, String mfpserial);
    public boolean createZIPFile(String inputPath, String outputPath, String zipName, List files);
    public boolean createFailedMFPsLog(List mfpList);
    public boolean concatFiles(List fileList, String outputDir, String outputName);
}
