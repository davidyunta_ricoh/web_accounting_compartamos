/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ricoh.accounting.remotecc;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import org.apache.log4j.Logger;
import ricoh.accounting.database.DataBaseConnection;
import ricoh.accounting.objects.User;
/**
 *
 * @author Alexis.Hidalgo
 */
public class RemoteccPlugin
{
    private DataBaseConnection dbLayer;

    private String DB_HOSTNAME = "";
    private String DB_INSTANCE = "";
    private String DB_USER = "";
    private String DB_PASSWORD = "";
    private String DB_NAME = "";
    private String DB_PORT = "";
    private String DB_DRIVER = "";
    private String DB_CONNECTION = "";

    private String QUERY_GET_USER_dbCC = "";

    public static Logger logger = Logger.getLogger(DataBaseConnection.class.getName());

    public RemoteccPlugin(String confFile, String sqlFile) throws FileNotFoundException, IOException
    {
        Properties prop = new Properties();
        prop.load(new FileInputStream(confFile));

        DB_HOSTNAME = prop.getProperty("CC.ExternalDB.Host");
        DB_INSTANCE = prop.getProperty("CC.ExternalDB.Instance");
        DB_USER = prop.getProperty("CC.ExternalDB.User");
        DB_PASSWORD = prop.getProperty("CC.ExternalDB.Password");
        DB_NAME = prop.getProperty("CC.ExternalDB.Name");
        DB_PORT = prop.getProperty("CC.ExternalDB.Port");
        DB_DRIVER = prop.getProperty("CC.ExternalDB.Driver");
        DB_CONNECTION = prop.getProperty("CC.ExternalDB.Connection");

        prop.load(new FileInputStream(sqlFile));
        QUERY_GET_USER_dbCC = prop.getProperty("SQL.GET_CC_USER");

        dbLayer = new DataBaseConnection(DB_HOSTNAME, DB_USER, DB_PASSWORD, DB_NAME, DB_PORT, DB_DRIVER, DB_CONNECTION, DB_INSTANCE);
        dbLayer.createSQLServerConnectionURL();
    }

    public User getUserByID(String username)
    {
        if (!dbLayer.isConnected())
        {
            //Open connection with the database
            if (!dbLayer.connect())
            {
                logger.error("getUserByID() : Cannot open DataBase connection");
                System.out.println("ERROR: Cannot open DataBase connection");
                return null;
            }
        }

        User user = getUserFromDB(username);

        if (user == null)
            user = new User(username, "", "","");

        return user;
    }

    private User getUserFromDB(String username)
    {
        User user = null;
        String query = "";
        PreparedStatement pst = null;

        try
        {
            query = QUERY_GET_USER_dbCC;
            int num_fields = query.split(",").length;

            pst = dbLayer.getConnection().prepareStatement(QUERY_GET_USER_dbCC);
            pst.setString(1, username.toUpperCase());

            ResultSet rs = pst.executeQuery();

            if(rs.next())
            {
                switch (num_fields){
                    case 1:
                        
                        user = new User(username,
                                        rs.getString(1),
                                        "",
                                        "");
                        break;
                    case 2:
                        user = new User(username,
                                rs.getString(1),
                                rs.getString(2),
                                "");
                        break;
                    case 3:
                        user = new User(username,
                                rs.getString(1),
                                rs.getString(2),
                                rs.getString(3));
                        break;
                    default:
                        logger.error("getUserFromDB() : User " + username + ". Fields number not treated correctly");
                }
            }
            else
            {
                logger.info("getUserFromDB() : User " + username + " cannot be found on external DataBase");
            }

            rs.close();
        }
        catch (SQLException ex)
        {
            logger.error("getUserFromDB() : " + QUERY_GET_USER_dbCC);
            logger.error(ex.getMessage());
            logger.error(ex.getStackTrace());
        }
        finally
        {
            return user;
        }
    }

}
