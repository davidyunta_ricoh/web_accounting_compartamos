/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ricoh.accounting.ifactory;

import java.io.FileNotFoundException;
import java.io.IOException;
import org.apache.log4j.Logger;
import ricoh.accounting.database.DataBaseManager;
import ricoh.accounting.files.CSVManager;
import ricoh.accounting.interfaces.IFileManager;
import ricoh.accounting.interfaces.INetworkManager;
import ricoh.accounting.network.NetworkManager;

/**
 *
 * @author alexis.hidalgo
 */
public final class InterfaceManager
{
    public static Logger logger = Logger.getLogger(InterfaceManager.class.getName());

    public static INetworkManager getNetworkManagerInstance(String propFile)
    {
        try
        {
            return new NetworkManager(propFile);
        }
        catch (FileNotFoundException ex)
        {
            logger.error(ex.getMessage());
            logger.error(ex.getStackTrace());
            return null;
        }
        catch (IOException ex)
        {
            logger.error(ex.getMessage());
            logger.error(ex.getStackTrace());
            return null;
        }
    }
    
    public static IFileManager getFileManagerInstance()
    {
        return new CSVManager();
    }

    
    public static DataBaseManager getDataBaseManagerInstance(String configFile, String sqlFile)
    {
        DataBaseManager dbm;

        try
        {
            dbm = new DataBaseManager(configFile);
            if (sqlFile != null)
                dbm.setSQLqueries(sqlFile);

            return dbm;
        }
        catch (FileNotFoundException ex)
        {
            logger.error(ex.getMessage());
            logger.error(ex.getStackTrace());
            return null;
        }
        catch (IOException ex)
        {
            logger.error(ex.getMessage());
            logger.error(ex.getStackTrace());
            return null;
        }
    }

    public static DataBaseManager getDataBaseManagerInstance(String hostName, String userName, String password, String dbName, String dbPort, String dbDriver, String dbConnection)
    {
       return new DataBaseManager(hostName, userName, password, dbName, dbPort, dbDriver, dbConnection);
    }

}
