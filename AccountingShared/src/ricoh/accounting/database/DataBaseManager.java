/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ricoh.accounting.database;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import org.apache.log4j.Logger;
import ricoh.accounting.objects.Action;
import ricoh.accounting.objects.Accumulated;
import ricoh.accounting.objects.Printer;
import ricoh.accounting.objects.Resume;
import ricoh.accounting.objects.User;

/**
 *
 * @author alexis.hidalgo
 */
public class DataBaseManager extends DataBaseConnection
{




    private enum RESUME_COLUMNS { TOTAL,
                                  COLOR_PRINT,
                                  BW_PRINT,
                                  COLOR_COPY,
                                  BW_COPY,
                                  SCAN_SENT,
                                  SCAN_STORED,
                                  FAX_SENT,
                                  FAX_RECEIVED,
                                  DATE,
                                  ID,
                                  ID2,
                                  COMPANYNAME};

    private enum ACTION_COLUMNS { USERNAME,
                                  MFPSERIAL,
                                  DATE,
                                  DATE_TIME,
                                  SOURCETYPE,
                                  NUM_PAGES,
                                  COLORMODE,
                                  PAGE_SIZE,
                                  DESTINATION,
                                  SENDER,
                                  DOCUMENT_NAME,
                                  NUM_COPIES ,
                                  COMPANYNAME,
                                  PAGEMODE};

    private enum MFP_REGISTER_COLUMNS { IP_ADDRESS,
                                        MODEL_TYPE,
                                        STATUS,
                                        LAST_COLOR_COUNTER,
                                        LAST_BW_COUNTER,
                                        HOSTNAME,
                                        LOCATION,
                                        ZONE,
                                        MFPSERIAL,                                        
                                        IS_REPORTING,
                                        COMPANYNAME,
                                        IS_SLNX_MFP};

    private ResultSet usersActionsResultSet = null;
    private ResultSet printersActionsResultSet = null;
    private PreparedStatement usersActionsPST = null;
    private PreparedStatement printersActionsPST = null;
    
    //SELECT QUERIES
    private String QUERY_GET_USERS = "";
    private String QUERY_GET_USERS_FORPARSER = "";
    private String QUERY_GET_USERS_PERUSER = "";
    private String QUERY_GET_USERS_PERCOMPANY = "";
    private String QUERY_GET_USERS_PERCOMPANY_PERUSER = "";
    private String QUERY_GET_MFPS = "";
    private String QUERY_GET_MFPS_PERMFP = "";
    private String QUERY_GET_MFPS_PERCOMPANY = "";
    private String QUERY_GET_MFPS_PERCOMPANY_PERMFP = "";
    private String QUERY_GET_MFPS_REGISTER = "";
    private String QUERY_GET_USERS_dbWEB_USERS = "";
    private String QUERY_GET_COMPANIES_dbWEB_USERS = "";
    private String QUERY_GET_REGISTERED_MFPS_dbMFP_REGISTER = "";
    private String QUERY_GET_REGISTERED_MFPS_PERCOMPANY_dbMFP_REGISTER = "";
    private String QUERY_GET_REPORTING_MFPS_dbMFP_REGISTER = "";
    private String QUERY_GET_STATE_MFPS_dbMFP_REGISTER = "";
    private String QUERY_GET_STATE_MFPS_PERCOMPANY_dbMFP_REGISTER = "";
    private String QUERY_GET_DESCONOCIDO_MFPS = "";
    private String QUERY_GET_DESCONOCIDO_MFPS_PERCOMPANY = "";
    private String QUERY_USER_ACTIONS_dbGENERAL = "";
    private String QUERY_USER_ACTIONS_PerCompany_dbGENERAL = "";
    private String QUERY_MFP_ACTIONS_dbGENERAL = "";
    private String QUERY_ALLUSERS_ACTIONS_dbGENERAL = "";
    private String QUERY_ALLUSERS_ACTIONS_PERUSER_dbGENERAL = "";
    private String QUERY_ALLUSERS_ACTIONS_PERCOMPANY_dbGENERAL = "";
    private String QUERY_ALLUSERS_ACTIONS_PERCOMPANY_PERUSER_dbGENERAL = "";
    private String QUERY_ALLPRINTERS_ACTIONS_dbGENERAL = "";
    private String QUERY_USER_PRINTERS_dbGENERAL = "";
    private String QUERY_LAST_RESUME_USER_dbUSER_COUNTERS = "";
    private String QUERY_LAST_RESUME_MFP_dbMFP_COUNTERS = "";
    private String QUERY_LAST_RESUME_MFP_dbDESCONOCIDO_MFP_COUNTERS = "";
    private String QUERY_GET_RESUME_USER_dbUSER_COUNTERS = "";
    private String QUERY_GET_RESUME_MFP_dbMFP_COUNTERS = "";
    private String QUERY_GET_RESUME_MFP_dbDESCONOCIDO_MFP_COUNTERS = "";
    private String QUERY_AUTHENTICATE_USER_dbWEB_USERS = "";
    private String QUERY_USER_PRINTERS_dbUSER_MFP_COUNTERS = "";
    private String QUERY_USER_PRINTERS_PerCompany_dbUSER_MFP_COUNTERS = "";
    private String QUERY_LAST_RESUME_USER_MFP_dbUSER_MFP_COUNTERS = "";
    private String QUERY_GET_RESUME_USER_MFP_dbUSER_MFP_COUNTERS = "";
    private String QUERY_GET_MFP_dbMFP_REGISTER = "";
    private String QUERY_LAST_RESUME_CC_dbCC_COUNTERS = "";
    private String QUERY_GET_RESUME_CC_dbCC_COUNTERS = "";
    private String QUERY_GET_CCS = "";
    private String QUERY_GET_CCS_PerCompany = "";
    private String QUERY_GET_CC_NAME_dbCC_COUNTERS = "";
    private String QUERY_GET_SUM_USER_COUNTERS_dbUSER_COUNTERS = "";   
    private String QUERY_GET_SUM_MFP_COUNTERS_dbMFP_COUNTERS = "";   
    private String QUERY_GET_SUM_GENERAL_COUNTERS_dbGENERAL = "";   
    private String QUERY_GET_SUM_UNKNOWN_USER_COUNTERS_dbUSER_COUNTERS = "";   
    private String QUERY_GET_SUM_UNKNOWN_MFP_COUNTERS_dbDESCONOCIDO_MFP_COUNTERS = "";   
    private String QUERY_GET_SUM_UNKNOWN_GENERAL_COUNTERS_dbGENERAL = "";   
    private String QUERY_GET_JOB_dbSTOREDJOBS = "";
    private String QUERY_GET_LAST_COUNTERS_PER_MFP_SERIAL = "";
    private String QUERY_GET_SLNX_MFPS_FROM_ACCOUNTING_DB = "";
    private String QUERY_GET_LAST_COUNTERS_PER_USER = "";
    private String QUERY_GET_LAST_COUNTERS_PER_USER_MFP = "";
    private String QUERY_GET_LAST_COUNTERS_PER_COST_CENTER = "";
    private String QUERY_GET_LAST_GENERAL_DATE = "";
    // BANCO COMPARTAMOS
    private String QUERY_GET_USER_DOCS = "";
    private String QUERY_GET_LOCATION_DOCS = "";
    private String QUERY_GET_ZONE_DOCS = "";
    
    private String QUERY_GET_USER_SHEET = "";
    private String QUERY_GET_LOCATION_SHEET = "";
    private String QUERY_GET_ZONE_SHEET = "";
    
    private String QUERY_GET_LOCATIONS = "";
    private String QUERY_GET_LOCATIONS_PERCOMPANY = "";
    private String QUERY_GET_ZONES = "";
    private String QUERY_GET_ZONES_PERCOMPANY = "";
    
    //UPDATE QUERIES
    private String QUERY_UPDATE_RESUME_dbUSER_COUNTERS = "";
    private String QUERY_UPDATE_RESUME_dbMFP_COUNTERS = "";
    private String QUERY_UPDATE_RESUME_dbDESCONOCIDO_MFP_COUNTERS = "";
    private String QUERY_UPDATE_MFP_dbMFP_REGISTER = "";
    private String QUERY_SET_REPORTING_MFP_dbMFP_REGISTER = "";
    private String QUERY_UPDATE_RESUME_dbUSER_MFP_COUNTERS = "";
    private String QUERY_UPDATE_RESUME_dbCC_COUNTERS = "";
    private String QUERY_UPDATE_STATUS_MFP_dbMFP_REGISTER = "";
    private String QUERY_UPDATE_MFP_REGISTER = "";

    //INSERT QUERIES
    private String QUERY_INSERT_RESUME_dbUSER_COUNTERS = "";
    private String QUERY_INSERT_RESUME_dbMFP_COUNTERS = "";
    private String QUERY_INSERT_RESUME_dbDESCONOCIDO_MFP_COUNTERS = "";
    private String QUERY_INSERT_ACTION_dbGENERAL = "";
    private String QUERY_INSERT_MFP_dbMFP_REGISTER = "";
    private String QUERY_REGISTER_USER_dbWEB_USERS = "";
    private String QUERY_INSERT_RESUME_dbUSER_MFP_COUNTERS = "";
    private String QUERY_INSERT_RESUME_dbCC_COUNTERS = "";
    private String QUERY_INSERT_COMPANY_dbCOMPANIES = "";
    private String QUERY_INSERT_JOB_dbSTOREDJOBS = "";

    //DELETE QUERIES
    private String QUERY_DELETE_USER_dbWEB_USERS = "";
    private String QUERY_DELETE_MFP_dbMFP_REGISTER = "";
    private String QUERY_DELETE_COMPANY_dbCOMPANIES = "";
    private String QUERY_DELETE_JOB_dbSTOREDJOBS = "";
    private String QUERY_DELETE_JOBS_PERDATE_dbSTOREDJOBS = "";

    //private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private static final Calendar cal = Calendar.getInstance();

    public static Logger loggerDBM = Logger.getLogger(DataBaseManager.class.getName());

    public DataBaseManager(String propFile) throws FileNotFoundException, IOException
    {
        super(propFile);
        setSQLqueries(propFile);
    }

    public DataBaseManager(String hostName, String userName, String password, String dbName, String dbPort, String dbDriver, String dbConnection)
    {
        super(hostName, userName, password, dbName, dbPort, dbDriver, dbConnection);
    }

    public void setSQLqueries(String propFile) throws FileNotFoundException, IOException
    {
        Properties prop = new Properties();
        prop.load(new FileInputStream(propFile));

        //SELECT QUERIES
        QUERY_GET_USERS = prop.getProperty("SQL.GET_USERS");
        QUERY_GET_USERS_FORPARSER = prop.getProperty("SQL.GET_USERS_FORPARSER");
        QUERY_GET_USERS_PERUSER = prop.getProperty("SQL.GET_USERS_PERUSER");
        QUERY_GET_USERS_PERCOMPANY = prop.getProperty("SQL.GET_USERS_PERCOMPANY");
        QUERY_GET_USERS_PERCOMPANY_PERUSER = prop.getProperty("SQL.GET_USERS_PERCOMPANY_PERUSER");
        QUERY_GET_MFPS = prop.getProperty("SQL.GET_MFPS");
        QUERY_GET_MFPS_PERMFP = prop.getProperty("SQL.GET_MFPS_PERMFP");
        QUERY_GET_MFPS_PERCOMPANY = prop.getProperty("SQL.GET_MFPS_PERCOMPANY");
        QUERY_GET_MFPS_PERCOMPANY_PERMFP = prop.getProperty("SQL.GET_MFPS_PERCOMPANY_PERMFP");
        QUERY_GET_MFPS_REGISTER = prop.getProperty("SQL.GET_MFPS_REGISTER");
        QUERY_GET_USERS_dbWEB_USERS = prop.getProperty("SQL.GET_REGISTERED_USERS");
        QUERY_GET_COMPANIES_dbWEB_USERS = prop.getProperty("SQL.GET_REGISTERED_COMPANIES");
        QUERY_GET_REGISTERED_MFPS_dbMFP_REGISTER = prop.getProperty("SQL.GET_REGISTERED_MFPS");
        QUERY_GET_REGISTERED_MFPS_PERCOMPANY_dbMFP_REGISTER = prop.getProperty("SQL.GET_REGISTERED_MFPS_PERCOMPANY");
        QUERY_GET_REPORTING_MFPS_dbMFP_REGISTER = prop.getProperty("SQL.GET_REPORTING_MFPS");
        QUERY_GET_DESCONOCIDO_MFPS = prop.getProperty("SQL.GET_DESCONOCIDO_MFPS");
        QUERY_GET_DESCONOCIDO_MFPS_PERCOMPANY = prop.getProperty("SQL.GET_DESCONOCIDO_MFPS_PERCOMPANY");
        QUERY_USER_ACTIONS_dbGENERAL = prop.getProperty("SQL.USER_ACTIONS");
        QUERY_USER_ACTIONS_PerCompany_dbGENERAL = prop.getProperty("SQL.USER_ACTIONS_PERCOMPANY");
        QUERY_MFP_ACTIONS_dbGENERAL = prop.getProperty("SQL.MFP_ACTIONS");
        QUERY_ALLUSERS_ACTIONS_dbGENERAL = prop.getProperty("SQL.ALLUSERS_ACTIONS");
        QUERY_ALLUSERS_ACTIONS_PERUSER_dbGENERAL = prop.getProperty("SQL.ALLUSERS_ACTIONS_PERUSER");
        QUERY_ALLUSERS_ACTIONS_PERCOMPANY_dbGENERAL = prop.getProperty("SQL.ALLUSERS_ACTIONS_PERCOMPANY");
        QUERY_ALLUSERS_ACTIONS_PERCOMPANY_PERUSER_dbGENERAL = prop.getProperty("SQL.ALLUSERS_ACTIONS_PERCOMPANY_PERUSER");
        QUERY_ALLPRINTERS_ACTIONS_dbGENERAL = prop.getProperty("SQL.ALLPRINTERS_ACTIONS");
        QUERY_USER_PRINTERS_dbGENERAL = prop.getProperty("SQL.USER_PRINTERS");
        QUERY_LAST_RESUME_USER_dbUSER_COUNTERS = prop.getProperty("SQL.LAST_RESUME_USER");
        QUERY_LAST_RESUME_MFP_dbMFP_COUNTERS = prop.getProperty("SQL.LAST_RESUME_MFP");
        QUERY_LAST_RESUME_MFP_dbDESCONOCIDO_MFP_COUNTERS = prop.getProperty("SQL.LAST_RESUME_DESCONOCIDO_MFP");
        QUERY_GET_RESUME_USER_dbUSER_COUNTERS = prop.getProperty("SQL.DATE_RESUME_USER");
        QUERY_GET_RESUME_MFP_dbMFP_COUNTERS = prop.getProperty("SQL.DATE_RESUME_MFP");
        QUERY_GET_RESUME_MFP_dbDESCONOCIDO_MFP_COUNTERS = prop.getProperty("SQL.DATE_RESUME_DESCONOCIDO_MFP");
        QUERY_GET_STATE_MFPS_dbMFP_REGISTER = prop.getProperty("SQL.GET_STATE_MFPS");
        QUERY_GET_STATE_MFPS_PERCOMPANY_dbMFP_REGISTER = prop.getProperty("SQL.GET_STATE_MFPS_PERCOMPANY");
        QUERY_AUTHENTICATE_USER_dbWEB_USERS = prop.getProperty("SQL.AUTHENTICATE_USER");
        //** NUEVO **//
        QUERY_USER_PRINTERS_dbUSER_MFP_COUNTERS = prop.getProperty("SQL.USER_MFPS");
        QUERY_USER_PRINTERS_PerCompany_dbUSER_MFP_COUNTERS = prop.getProperty("SQL.USER_MFPS_PERCOMPANY");
        QUERY_LAST_RESUME_USER_MFP_dbUSER_MFP_COUNTERS = prop.getProperty("SQL.LAST_RESUME_USER_MFP");
        QUERY_GET_RESUME_USER_MFP_dbUSER_MFP_COUNTERS = prop.getProperty("SQL.DATE_RESUME_USER_MFP");
        QUERY_GET_MFP_dbMFP_REGISTER = prop.getProperty("SQL.GET_MFP");
        QUERY_LAST_RESUME_CC_dbCC_COUNTERS = prop.getProperty("SQL.LAST_RESUME_CC");
        QUERY_GET_RESUME_CC_dbCC_COUNTERS = prop.getProperty("SQL.DATE_RESUME_CC");
        QUERY_GET_CCS = prop.getProperty("SQL.GET_CCS");
        QUERY_GET_CCS_PerCompany = prop.getProperty("SQL.GET_CCS_PerCompany");
        QUERY_GET_CC_NAME_dbCC_COUNTERS = prop.getProperty("SQL.GET_CC_NAME");
        QUERY_GET_SUM_USER_COUNTERS_dbUSER_COUNTERS = prop.getProperty("SQL.GET_TOTAL_USER_COUNTER");
        QUERY_GET_SUM_MFP_COUNTERS_dbMFP_COUNTERS = prop.getProperty("SQL.GET_TOTAL_MFP_COUNTER");
        QUERY_GET_SUM_GENERAL_COUNTERS_dbGENERAL = prop.getProperty("SQL.GET_TOTAL_GENERAL_COUNTER");  
        QUERY_GET_SUM_UNKNOWN_USER_COUNTERS_dbUSER_COUNTERS = prop.getProperty("SQL.GET_TOTAL_UNKNOWN_USER_COUNTER");
        QUERY_GET_SUM_UNKNOWN_MFP_COUNTERS_dbDESCONOCIDO_MFP_COUNTERS = prop.getProperty("SQL.GET_TOTAL_UNKNOWN_MFP_COUNTER");
        QUERY_GET_SUM_UNKNOWN_GENERAL_COUNTERS_dbGENERAL = prop.getProperty("SQL.GET_TOTAL_UNKNOWN_GENERAL_COUNTER");  
        QUERY_GET_JOB_dbSTOREDJOBS = prop.getProperty("SQL.GET_JOB_STOREDJOBS");    
        QUERY_GET_LAST_COUNTERS_PER_MFP_SERIAL = prop.getProperty("SQL.GET_LAST_COUNTERS_PER_MFP_SERIAL");
        QUERY_GET_SLNX_MFPS_FROM_ACCOUNTING_DB = prop.getProperty("SQL.GET_SLNX_MFPS_FROM_ACCOUNTING_DB");
        QUERY_GET_LAST_COUNTERS_PER_USER = prop.getProperty("SQL.GET_LAST_COUNTERS_PER_USER");
        QUERY_GET_LAST_COUNTERS_PER_USER_MFP = prop.getProperty("SQL.GET_LAST_COUNTERS_PER_USER_MFP");
        QUERY_GET_LAST_COUNTERS_PER_COST_CENTER = prop.getProperty("SQL.GET_LAST_COUNTERS_PER_COST_CENTER");
        QUERY_GET_LAST_GENERAL_DATE = prop.getProperty("SQL.GET_LAST_GENERAL_DATE");
        
        // Banco Compartamos
        QUERY_GET_USER_DOCS = prop.getProperty("SQL.GET_USER_DOCS");
        QUERY_GET_LOCATION_DOCS = prop.getProperty("SQL.GET_LOCATION_DOCS");
        QUERY_GET_ZONE_DOCS = prop.getProperty("SQL.GET_ZONE_DOCS");
        
        QUERY_GET_USER_SHEET = prop.getProperty("SQL.GET_USER_SHEET");
        QUERY_GET_LOCATION_SHEET = prop.getProperty("SQL.GET_LOCATION_SHEET");
        QUERY_GET_ZONE_SHEET = prop.getProperty("SQL.GET_ZONE_SHEET");
        //
        
        QUERY_GET_LOCATIONS = prop.getProperty("SQL.GET_LOCATIONS");
        QUERY_GET_ZONES = prop.getProperty("SQL.GET_ZONES");
        QUERY_GET_LOCATIONS_PERCOMPANY = prop.getProperty("SQL.GET_LOCATIONS_PERCOMPANY");;
        QUERY_GET_ZONES_PERCOMPANY = prop.getProperty("SQL.GET_ZONES_PERCOMPANY");;

        
        //UPDATE QUERIES
        QUERY_UPDATE_RESUME_dbUSER_COUNTERS = prop.getProperty("SQL.UPDATE_USER_COUNTERS");
        QUERY_UPDATE_RESUME_dbMFP_COUNTERS = prop.getProperty("SQL.UPDATE_MFP_COUNTERS");
        QUERY_UPDATE_RESUME_dbDESCONOCIDO_MFP_COUNTERS = prop.getProperty("SQL.UPDATE_DESCONOCIDO_MFP_COUNTERS");
        QUERY_UPDATE_MFP_dbMFP_REGISTER = prop.getProperty("SQL.UPDATE_MFP_REGISTER");
        QUERY_SET_REPORTING_MFP_dbMFP_REGISTER = prop.getProperty("SQL.SET_REPORTING_MFP_REGISTER");
        //** NUEVO **//
        QUERY_UPDATE_RESUME_dbUSER_MFP_COUNTERS = prop.getProperty("SQL.UPDATE_USER_MFP_COUNTERS");
        QUERY_UPDATE_RESUME_dbCC_COUNTERS = prop.getProperty("SQL.UPDATE_CC_COUNTERS");
        QUERY_UPDATE_STATUS_MFP_dbMFP_REGISTER = prop.getProperty("SQL.SET_STATUS_MFP_REGISTER");
        QUERY_UPDATE_MFP_REGISTER = prop.getProperty("SQL.UPDATE_MFP_REGISTER");
        
        //INSERT QUERIES
        QUERY_INSERT_RESUME_dbUSER_COUNTERS = prop.getProperty("SQL.INSERT_USER_COUNTERS");
        QUERY_INSERT_RESUME_dbMFP_COUNTERS = prop.getProperty("SQL.INSERT_MFP_COUNTERS");
        QUERY_INSERT_RESUME_dbDESCONOCIDO_MFP_COUNTERS = prop.getProperty("SQL.INSERT_DESCONOCIDO_MFP_COUNTERS");
        QUERY_INSERT_ACTION_dbGENERAL = prop.getProperty("SQL.INSERT_GENERAL");
        QUERY_INSERT_MFP_dbMFP_REGISTER = prop.getProperty("SQL.INSERT_MFP_REGISTER");
        QUERY_REGISTER_USER_dbWEB_USERS = prop.getProperty("SQL.REGISTER_USER");
        //** NUEVO **//
        QUERY_INSERT_RESUME_dbUSER_MFP_COUNTERS = prop.getProperty("SQL.INSERT_USER_MFP_COUNTERS");
        QUERY_INSERT_RESUME_dbCC_COUNTERS = prop.getProperty("SQL.INSERT_CC_COUNTERS");
        QUERY_INSERT_COMPANY_dbCOMPANIES = prop.getProperty("SQL.INSERT_COMPANY_COMPANIES");
        QUERY_INSERT_JOB_dbSTOREDJOBS = prop.getProperty("SQL.INSERT_JOB_STOREDJOBS");
        
        //DELETE QUERIES
        QUERY_DELETE_USER_dbWEB_USERS = prop.getProperty("SQL.DELETE_REGISTERED_USER");
        QUERY_DELETE_MFP_dbMFP_REGISTER = prop.getProperty("SQL.DELETE_REGISTERED_MFP");
        QUERY_DELETE_COMPANY_dbCOMPANIES = prop.getProperty("SQL.DELETE_COMPANY_COMPANIES");
        QUERY_DELETE_JOB_dbSTOREDJOBS = prop.getProperty("SQL.DELETE_JOB_STOREDJOBS");
        QUERY_DELETE_JOBS_PERDATE_dbSTOREDJOBS = prop.getProperty("SQL.DELETE_JOBS_PERDATE_STOREDJOBS");
    }

    public Printer getMFPbyID(String printerName)
    {
        Printer printer = null;

        PreparedStatement pst = null;

        try
        {
            pst = getConnection().prepareStatement(QUERY_GET_MFP_dbMFP_REGISTER);
            pst.setString(1, printerName);

            ResultSet rs = pst.executeQuery();

            if(rs.next())
            {
                printer = new Printer(rs.getString(MFP_REGISTER_COLUMNS.MFPSERIAL.ordinal()+1),
                                      rs.getString(MFP_REGISTER_COLUMNS.IP_ADDRESS.ordinal()+1),
                                      rs.getString(MFP_REGISTER_COLUMNS.MODEL_TYPE.ordinal()+1),
                                      rs.getString(MFP_REGISTER_COLUMNS.STATUS.ordinal()+1),
                                      rs.getInt(MFP_REGISTER_COLUMNS.LAST_COLOR_COUNTER.ordinal()+1),
                                      rs.getInt(MFP_REGISTER_COLUMNS.LAST_BW_COUNTER.ordinal()+1),
                                      rs.getString(MFP_REGISTER_COLUMNS.LOCATION.ordinal()+1),
                                      rs.getString(MFP_REGISTER_COLUMNS.ZONE.ordinal()+1),
                                      rs.getString(MFP_REGISTER_COLUMNS.HOSTNAME.ordinal()+1),
                                      rs.getString(MFP_REGISTER_COLUMNS.COMPANYNAME.ordinal()+1));
            }

            rs.close();
        }
        catch (SQLException ex)
        {
            loggerDBM.error("getMFPbyID() : " + QUERY_GET_MFP_dbMFP_REGISTER);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
        }
        finally
        {
            return printer;
        }

    }

    public List getRegisteredWebUsers()
    {
        List usersList = new ArrayList();
        try
        {
            ResultSet rs = st.executeQuery(QUERY_GET_USERS_dbWEB_USERS);

            while(rs.next()){
                usersList.add(rs.getString(1));
                usersList.add(rs.getString(2));
                usersList.add(rs.getString(3));
            }
                
            rs.close();
        }
        catch (SQLException ex)
        {
            loggerDBM.error("getRegisteredWebUsers() : " + QUERY_GET_USERS_dbWEB_USERS);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
        }
        finally
        {
            return usersList;
        }
    }

    public List getCompaniesWebUsers() {
        List companiesList = new ArrayList();
        try
        {
            ResultSet rs = st.executeQuery(QUERY_GET_COMPANIES_dbWEB_USERS);

            while(rs.next()){
                companiesList.add(rs.getString(1));
            }
                
            rs.close();
        }
        catch (SQLException ex)
        {
            loggerDBM.error("getCompaniesWebUsers() : " + QUERY_GET_COMPANIES_dbWEB_USERS);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
        }
        finally
        {
            return companiesList;
        }
    }
    
    public List getTotalMFPs() {
        List mpfsList = new ArrayList();
        try
        {
            ResultSet rs = st.executeQuery(QUERY_GET_REGISTERED_MFPS_dbMFP_REGISTER);

            while(rs.next()){
                mpfsList.add(rs.getString(1));
                mpfsList.add(rs.getString(2));
                mpfsList.add(rs.getString(3));
                mpfsList.add(rs.getString(6));
                mpfsList.add(rs.getString(7));
                mpfsList.add(rs.getString(8));
                mpfsList.add(rs.getString(9));
                mpfsList.add(rs.getString(10));
            }
                
            rs.close();
        }
        catch (SQLException ex)
        {
            loggerDBM.error("getTotalMFPs() : " + QUERY_GET_REGISTERED_MFPS_dbMFP_REGISTER);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
        }
        finally
        {
            return mpfsList;
        }
    }
    
    public List getMFPsAndCompany() {
        List mpfsList = new ArrayList();
        try
        {
            ResultSet rs = st.executeQuery(QUERY_GET_REGISTERED_MFPS_dbMFP_REGISTER);

            while(rs.next()){
                mpfsList.add(rs.getString(9));
                mpfsList.add(rs.getString(10));
            }
                
            rs.close();
        }
        catch (SQLException ex)
        {
            loggerDBM.error("getMFPsAndCompany() : " + QUERY_GET_REGISTERED_MFPS_dbMFP_REGISTER);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
        }
        finally
        {
            return mpfsList;
        }
    }
    
    public boolean setStatusMFP(String mfpserial, String status) {
        PreparedStatement pst = null;
        try
        {
            //Insert detail into the database
            pst = getConnection().prepareStatement(QUERY_UPDATE_STATUS_MFP_dbMFP_REGISTER);
            pst.setString(1, status);
            pst.setString(2, mfpserial);

            int rows = pst.executeUpdate();
            pst.close();
            return (rows==1);
        }
        catch (SQLException ex)
        {
            loggerDBM.error("setStatusMFP() : " + pst);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
            return false;
        }
    }

    
    public boolean deleteRegisteredWebUser(String userName)
    {
        PreparedStatement pst;
        try
        {
            pst = getConnection().prepareStatement(QUERY_DELETE_USER_dbWEB_USERS);
            pst.setString(1, userName);
            
            int rows = pst.executeUpdate();
            pst.close();
            return (rows==1);
        }
        catch (SQLException ex)
        {
            loggerDBM.error("deleteRegisteredWebUser() : " + QUERY_DELETE_USER_dbWEB_USERS);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
            return false;
        }
    }
    
    public boolean deleteMFP_List(String mfpserial)
    {
        PreparedStatement pst;
        try
        {
            pst = getConnection().prepareStatement(QUERY_DELETE_MFP_dbMFP_REGISTER);
            pst.setString(1, mfpserial);
            
            int rows = pst.executeUpdate();
            pst.close();
            return (rows==1);
        }
        catch (SQLException ex)
        {
            loggerDBM.error("deleteMFP_List() : " + QUERY_DELETE_MFP_dbMFP_REGISTER);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
            return false;
        }
    }
    
    public boolean deleteCompany(String company)
    {
        PreparedStatement pst;
        try
        {
            pst = getConnection().prepareStatement(QUERY_DELETE_COMPANY_dbCOMPANIES);
            pst.setString(1, company);
            
            int rows = pst.executeUpdate();
            pst.close();
            return (rows==1);
        }
        catch (SQLException ex)
        {
            loggerDBM.error("deleteCompany() : " + QUERY_DELETE_COMPANY_dbCOMPANIES);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
            return false;
        }
    }
    
    public List getRemoteMFPs()
    {
        List mfpList = new ArrayList();
        PreparedStatement pst;
        
        try
        {
            pst = getConnection().prepareStatement(QUERY_GET_USERS_PERUSER);
            ResultSet rs = pst.executeQuery();
            
            while(rs.next())
                mfpList.add(rs.getString(1));

            rs.close();
        }
        catch (SQLException ex)
        {
            loggerDBM.error("getRemoteMFPs() : " + QUERY_GET_USERS_PERUSER);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
        }
        finally
        {
            return mfpList;
        }
    }
    
    public boolean insertCompany(String company)
    {
        PreparedStatement pst;
        try
        {
            pst = getConnection().prepareStatement(QUERY_INSERT_COMPANY_dbCOMPANIES);
            System.out.println("insert company" + company +" to DB " );
            pst.setString(1, company);
            
            int rows = pst.executeUpdate();
            pst.close();
            return (rows==1);
        }
        catch (SQLException ex)
        {
            loggerDBM.error("insertCompany() : " + QUERY_INSERT_COMPANY_dbCOMPANIES);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
            return false;
        }
    }

    public boolean registerWebUser(String userName, String password, String fullusername, boolean isAdmin, String company)
    {
        PreparedStatement pst;
        try
        {
            pst = getConnection().prepareStatement(QUERY_REGISTER_USER_dbWEB_USERS);
            pst.setString(1, userName);
            pst.setString(2, password);
            pst.setString(3, fullusername);
            pst.setBoolean(4, isAdmin);
            pst.setString(5, company);

            int rows = pst.executeUpdate();
            pst.close();
            return (rows==1);
        }
        catch (SQLException ex)
        {
            loggerDBM.error("registerWebUser() : " + QUERY_REGISTER_USER_dbWEB_USERS);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
            return false;
        }
    }
    
    public List getReportingZones()
    {
        List zonesList = new ArrayList();
        try
        {
            ResultSet rs = st.executeQuery(QUERY_GET_ZONES);

            while(rs.next()){                   
                zonesList.add(rs.getString(1));
            }
            rs.close();
        }
        catch (SQLException ex)
        {
            loggerDBM.error("getReportingZones() : " + QUERY_GET_ZONES);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
        }

        return zonesList;

    }
    
    public List getReportingZones_PerCompany(String company)
    {
        List zonesList = new ArrayList();
        PreparedStatement pst;
                            
        try
        {             
            pst = getConnection().prepareStatement(QUERY_GET_ZONES_PERCOMPANY);
            pst.setString(1, company);
            ResultSet rs = pst.executeQuery();

            while(rs.next())
                zonesList.add(rs.getString(1));

            rs.close();
        }
        catch (SQLException ex)
        {
            loggerDBM.error("getReportingZones_PerCompany() : " + QUERY_GET_ZONES_PERCOMPANY);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
        }

        return zonesList;

    }

    public List getReportingLocations()
    {
        List locationsList = new ArrayList();
        try
        {
            ResultSet rs = st.executeQuery(QUERY_GET_LOCATIONS);

            while(rs.next()){                   
                locationsList.add(rs.getString(1));
            }
            rs.close();
        }
        catch (SQLException ex)
        {
            loggerDBM.error("getReportingLocations() : " + QUERY_GET_LOCATIONS);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
        }

        return locationsList;

    }
    
    
    public List getReportingLocations_PerCompany(String company)
    {
        List locationsList = new ArrayList();
        PreparedStatement pst;
                            
        try
        {             
            pst = getConnection().prepareStatement(QUERY_GET_LOCATIONS_PERCOMPANY);
            pst.setString(1, company);
            ResultSet rs = pst.executeQuery();

            while(rs.next())
                locationsList.add(rs.getString(1));

            rs.close();
        }
        catch (SQLException ex)
        {
            loggerDBM.error("getReportingLocations_PerCompany() : " + QUERY_GET_LOCATIONS_PERCOMPANY);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
        }

        return locationsList;

    }
    
    public List getReportingUsers()
    {
        List usersList = new ArrayList();
        try
        {
            ResultSet rs = st.executeQuery(QUERY_GET_USERS);

            while(rs.next()){                   
                usersList.add(rs.getString(1));
            }
            rs.close();
        }
        catch (SQLException ex)
        {
            loggerDBM.error("getReportingUsers() : " + QUERY_GET_USERS);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
        }
        finally
        {
            return usersList;
        }
    }
    
    public List getReportingUsersForParser()
    {
        List usersList = new ArrayList();
        String id = "";
        try
        {
            ResultSet rs = st.executeQuery(QUERY_GET_USERS_FORPARSER);

            while(rs.next()){
                id = rs.getString(1);
                if (id.isEmpty()){
                    id= "noname_" + rs.getString(2);
                }                    
                usersList.add(id);
            }
            rs.close();
        }
        catch (SQLException ex)
        {
            loggerDBM.error("getReportingUsersForParser() : " + QUERY_GET_USERS_FORPARSER);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
        }
        finally
        {
            return usersList;
        }
    }
    
    public List getReportingUsers(String username)
    {
        List usersList = new ArrayList();
        PreparedStatement pst;
        
        try
        {
            pst = getConnection().prepareStatement(QUERY_GET_USERS_PERUSER);
            pst.setString(1, username);
            ResultSet rs = pst.executeQuery();
            
            while(rs.next())
                usersList.add(rs.getString(1));

            rs.close();
        }
        catch (SQLException ex)
        {
            loggerDBM.error("getReportingUsers(String username) : " + QUERY_GET_USERS_PERUSER);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
        }
        finally
        {
            return usersList;
        }
    }
    
    public List getReportingUsers_PerCompany(String company)
    {
        List usersList = new ArrayList();
        PreparedStatement pst;
                            
        try
        {             
            pst = getConnection().prepareStatement(QUERY_GET_USERS_PERCOMPANY);
            pst.setString(1, company);
            ResultSet rs = pst.executeQuery();

            while(rs.next())
                usersList.add(rs.getString(1));

            rs.close();
        }
        catch (SQLException ex)
        {
            loggerDBM.error("getReportingUsers_PerCompany() : " + QUERY_GET_USERS_PERCOMPANY);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
        }
        finally
        {
            return usersList;
        }
    }
    
    public List getReportingUsers_PerCompany(String company, String username)
    {
        List usersList = new ArrayList();
        PreparedStatement pst;
                            
        try
        {             
            pst = getConnection().prepareStatement(QUERY_GET_USERS_PERCOMPANY_PERUSER);
            pst.setString(1, company);
            pst.setString(2, username);
            ResultSet rs = pst.executeQuery();

            while(rs.next())
                usersList.add(rs.getString(1));

            rs.close();
        }
        catch (SQLException ex)
        {
            loggerDBM.error("getReportingUsers_PerCompany(String username) : " + QUERY_GET_USERS_PERCOMPANY_PERUSER);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
        }
        finally
        {
            return usersList;
        }
    }

    public List getRegisterMFPs()
    {
        List printersList = new ArrayList();
        try
        {
            ResultSet rs = st.executeQuery(QUERY_GET_MFPS_REGISTER);

            while(rs.next()){
                printersList.add(rs.getString(1));
                printersList.add(rs.getString(2));
                printersList.add(rs.getString(3));
                printersList.add(rs.getString(6));
                printersList.add(rs.getString(7));
                printersList.add(rs.getString(8));
                printersList.add(rs.getString(9));
                printersList.add(rs.getString(11));
            }
            
            rs.close();
        }
        catch (SQLException ex)
        {
            loggerDBM.error("getRegisterMFPs() : " + QUERY_GET_MFPS_REGISTER);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
        }
        finally
        {
            return printersList;
        }
    }
    
    public List getMFPs()
    {
        List printersList = new ArrayList();
        try
        {
            ResultSet rs = st.executeQuery(QUERY_GET_MFPS);

            while(rs.next())
                printersList.add(rs.getString(1));

            rs.close();
        }
        catch (SQLException ex)
        {
            loggerDBM.error("getMFPs() : " + QUERY_GET_MFPS);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
        }
        finally
        {
            return printersList;
        }
    }
    
    public List getMFPs(String mfpserial)
    {
        List printersList = new ArrayList();
        try
        {
            PreparedStatement pst = getConnection().prepareStatement(QUERY_GET_MFPS_PERMFP);
            pst.setString(1, mfpserial);
            ResultSet rs = pst.executeQuery();
//            ResultSet rs = st.executeQuery(QUERY_GET_MFPS_PERMFP);

            while(rs.next())
                printersList.add(rs.getString(1));

            rs.close();
        }
        catch (SQLException ex)
        {
            loggerDBM.error("getMFPs(String mfpserial) : " + QUERY_GET_MFPS);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
        }
        finally
        {
            return printersList;
        }
    }
    
    public List getMFPs_PerCompany(String company)
    {
        List printersList = new ArrayList();
        try
        {
            PreparedStatement pst = getConnection().prepareStatement(QUERY_GET_MFPS_PERCOMPANY);
            pst.setString(1, company);
            ResultSet rs = pst.executeQuery();
//            ResultSet rs = st.executeQuery(QUERY_GET_MFPS_PERCOMPANY);

            while(rs.next())
                printersList.add(rs.getString(1));

            rs.close();
        }
        catch (SQLException ex)
        {
            loggerDBM.error("getMFPs_PerCompany() : " + QUERY_GET_MFPS_PERCOMPANY);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
        }
        finally
        {
            return printersList;
        }
    }
    
    public List getMFPs_PerCompany(String company, String mfpserial)
    {
        List printersList = new ArrayList();
        try
        {
            PreparedStatement pst = getConnection().prepareStatement(QUERY_GET_MFPS_PERCOMPANY_PERMFP);
            pst.setString(1, company);
            pst.setString(2, mfpserial);
            ResultSet rs = pst.executeQuery();
//            ResultSet rs = st.executeQuery(QUERY_GET_MFPS_PERCOMPANY);

            while(rs.next())
                printersList.add(rs.getString(1));

            rs.close();
        }
        catch (SQLException ex)
        {
            loggerDBM.error("getMFPs_PerCompany() : " + QUERY_GET_MFPS_PERCOMPANY);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
        }
        finally
        {
            return printersList;
        }
    }

    public List getCCs()
    {
        List ccList = new ArrayList();
        try
        {
            ResultSet rs = st.executeQuery(QUERY_GET_CCS);

            while(rs.next())
                ccList.add(rs.getString(1));

            rs.close();
        }
        catch (SQLException ex)
        {
            loggerDBM.error("getCCs() : " + QUERY_GET_CCS);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
        }
        finally
        {
            return ccList;
        }
    }

   
    public List getCCs(String company)
    {
        List ccList = new ArrayList();
        PreparedStatement pst = null;
                
        try
        {
            
            pst = getConnection().prepareStatement(QUERY_GET_CCS_PerCompany);
            pst.setString(1, company);
            ResultSet rs = pst.executeQuery();

            while(rs.next())
                ccList.add(rs.getString(1));

            rs.close();
        }
        catch (SQLException ex)
        {
            loggerDBM.error("getCCs() : " + QUERY_GET_CCS);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
        }
        finally
        {
            return ccList;
        }
    }
    
    public Map getRegisteredMFPs() {
       Map printersHash = new HashMap();
        Printer printer;
        
        try
        {
            ResultSet rs = st.executeQuery(QUERY_GET_REGISTERED_MFPS_dbMFP_REGISTER);

            while(rs.next())
            {
                printer = new Printer(rs.getString(MFP_REGISTER_COLUMNS.MFPSERIAL.ordinal()+1),
                                      rs.getString(MFP_REGISTER_COLUMNS.IP_ADDRESS.ordinal()+1),
                                      rs.getString(MFP_REGISTER_COLUMNS.MODEL_TYPE.ordinal()+1),
                                      rs.getString(MFP_REGISTER_COLUMNS.STATUS.ordinal()+1),
                                      rs.getInt(MFP_REGISTER_COLUMNS.LAST_COLOR_COUNTER.ordinal()+1),
                                      rs.getInt(MFP_REGISTER_COLUMNS.LAST_BW_COUNTER.ordinal()+1),
                                      rs.getString(MFP_REGISTER_COLUMNS.LOCATION.ordinal()+1),
                                      rs.getString(MFP_REGISTER_COLUMNS.ZONE.ordinal()+1),
                                      rs.getString(MFP_REGISTER_COLUMNS.HOSTNAME.ordinal()+1),
                                      rs.getString(MFP_REGISTER_COLUMNS.COMPANYNAME.ordinal()));

                printersHash.put(printer.getMfpSerial(), printer);
            }

            rs.close();
        }
        catch (SQLException ex)
        {
            loggerDBM.error("getRegisteredMFPs() : " + QUERY_GET_REGISTERED_MFPS_dbMFP_REGISTER);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
        }
        finally
        {
            return printersHash;
        }
    }
    
    public Map getRegisteredMFPs(String company)
    {
        Map printersHash = new HashMap();
        Printer printer;
        PreparedStatement pst;
        
        try
        {
            pst = getConnection().prepareStatement(QUERY_GET_REGISTERED_MFPS_PERCOMPANY_dbMFP_REGISTER);
            pst.setString(1, company);
            ResultSet rs = pst.executeQuery();

            while(rs.next())
            {
                printer = new Printer(rs.getString(MFP_REGISTER_COLUMNS.MFPSERIAL.ordinal()+1),
                                      rs.getString(MFP_REGISTER_COLUMNS.IP_ADDRESS.ordinal()+1),
                                      rs.getString(MFP_REGISTER_COLUMNS.MODEL_TYPE.ordinal()+1),
                                      rs.getString(MFP_REGISTER_COLUMNS.STATUS.ordinal()+1),
                                      rs.getInt(MFP_REGISTER_COLUMNS.LAST_COLOR_COUNTER.ordinal()+1),
                                      rs.getInt(MFP_REGISTER_COLUMNS.LAST_BW_COUNTER.ordinal()+1),
                                      rs.getString(MFP_REGISTER_COLUMNS.LOCATION.ordinal()+1),
                                      rs.getString(MFP_REGISTER_COLUMNS.ZONE.ordinal()+1),
                                      rs.getString(MFP_REGISTER_COLUMNS.HOSTNAME.ordinal()+1),
                                      rs.getString(MFP_REGISTER_COLUMNS.COMPANYNAME.ordinal()));

                printersHash.put(printer.getMfpSerial(), printer);
            }

            rs.close();
        }
        catch (SQLException ex)
        {
            loggerDBM.error("getRegisteredMFPs() : " + QUERY_GET_REGISTERED_MFPS_dbMFP_REGISTER);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
        }
        finally
        {
            return printersHash;
        }
    }

    public List getReportingMFPs()
    {
        List printersList = new ArrayList();

        try
        {
            ResultSet rs = st.executeQuery(QUERY_GET_REPORTING_MFPS_dbMFP_REGISTER);

            while(rs.next())
            {
                printersList.add(rs.getString(1));
            }

            rs.close();
        }
        catch (SQLException ex)
        {
            loggerDBM.error("getReportingMFPs() : " + QUERY_GET_REPORTING_MFPS_dbMFP_REGISTER);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
        }
        finally
        {
            return printersList;
        }
    }

    public boolean setReportingMFP(String mfpSerial, boolean isReporting)
    {
        PreparedStatement pst = null;
        try
        {
            //Insert detail into the database
            pst = getConnection().prepareStatement(QUERY_SET_REPORTING_MFP_dbMFP_REGISTER);
            pst.setBoolean(1, isReporting);
            pst.setString(2, mfpSerial);

            int rows = pst.executeUpdate();
            pst.close();
            return (rows==1);
        }
        catch (SQLException ex)
        {
            loggerDBM.error("setReportingMFP() : " + pst);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
            return false;
        }
    }

    public boolean registerMFP(Printer printer)
    {
        PreparedStatement pst = null;
        try
        {
            //Insert detail into the database
            pst = getConnection().prepareStatement(QUERY_INSERT_MFP_dbMFP_REGISTER);
            pst.setString(MFP_REGISTER_COLUMNS.IP_ADDRESS.ordinal()+1, printer.getIpAddress());
            pst.setString(MFP_REGISTER_COLUMNS.MODEL_TYPE.ordinal()+1, printer.getModelType());
            pst.setString(MFP_REGISTER_COLUMNS.STATUS.ordinal()+1, printer.getStatus());
            pst.setInt(MFP_REGISTER_COLUMNS.LAST_COLOR_COUNTER.ordinal()+1, printer.getLastColorCounter());
            pst.setInt(MFP_REGISTER_COLUMNS.LAST_BW_COUNTER.ordinal()+1, printer.getLastBWCounter());
            pst.setString(MFP_REGISTER_COLUMNS.MFPSERIAL.ordinal()+1, printer.getMfpSerial());
            pst.setBoolean(MFP_REGISTER_COLUMNS.IS_REPORTING.ordinal()+1, false);
            //** NUEVO **//
            pst.setString(MFP_REGISTER_COLUMNS.LOCATION.ordinal()+1, printer.getLocation());
            pst.setString(MFP_REGISTER_COLUMNS.ZONE.ordinal()+1, printer.getZone());
            pst.setString(MFP_REGISTER_COLUMNS.HOSTNAME.ordinal()+1, printer.getHostname());
            pst.setString(MFP_REGISTER_COLUMNS.COMPANYNAME.ordinal()+1, printer.getCompany());
            pst.setBoolean(MFP_REGISTER_COLUMNS.IS_SLNX_MFP.ordinal()+1, printer.isIsSLNXMFP()); //  TODO: for test

            int rows = pst.executeUpdate();
            pst.close();
            return (rows==1);
        }
        catch (SQLException ex)
        {
            System.out.println("registerMFP() : " + pst);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
            return false;
        }
    }
    
    public boolean insertMFP(String mfpserial)
    {
        PreparedStatement pst = null;
        try
        {
            //Insert detail into the database
            pst = getConnection().prepareStatement(QUERY_INSERT_MFP_dbMFP_REGISTER);
            pst.setString(MFP_REGISTER_COLUMNS.IP_ADDRESS.ordinal()+1, "10.0.0.1");
            pst.setString(MFP_REGISTER_COLUMNS.MODEL_TYPE.ordinal()+1, "model");
            pst.setString(MFP_REGISTER_COLUMNS.STATUS.ordinal()+1, "online");
            pst.setInt(MFP_REGISTER_COLUMNS.LAST_COLOR_COUNTER.ordinal()+1, 0);
            pst.setInt(MFP_REGISTER_COLUMNS.LAST_BW_COUNTER.ordinal()+1, 0);
            pst.setString(MFP_REGISTER_COLUMNS.MFPSERIAL.ordinal()+1, mfpserial);
            pst.setBoolean(MFP_REGISTER_COLUMNS.IS_REPORTING.ordinal()+1, true);
            //** NUEVO **//
            pst.setString(MFP_REGISTER_COLUMNS.LOCATION.ordinal()+1, "location");
            pst.setString(MFP_REGISTER_COLUMNS.ZONE.ordinal()+1, "zona");
            pst.setString(MFP_REGISTER_COLUMNS.HOSTNAME.ordinal()+1, "im000");
            pst.setString(MFP_REGISTER_COLUMNS.COMPANYNAME.ordinal()+1, "massimo");

            int rows = pst.executeUpdate();
            pst.close();
            return (rows==1);
        }
        catch (SQLException ex)
        {
            System.out.println("registerMFP() : " + pst);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
            return false;
        }
    }
    
    public boolean registerMFP_List(String mfpserial, String model_type, String ip_address, String status, String location, String zone, String hostname, String company)
    {
        PreparedStatement pst = null;
        try
        {
            //Insert detail into the database
            pst = getConnection().prepareStatement(QUERY_INSERT_MFP_dbMFP_REGISTER);
            pst.setString(MFP_REGISTER_COLUMNS.IP_ADDRESS.ordinal()+1, ip_address);
            pst.setString(MFP_REGISTER_COLUMNS.MODEL_TYPE.ordinal()+1, model_type);
            pst.setString(MFP_REGISTER_COLUMNS.STATUS.ordinal()+1, status);
            pst.setInt(MFP_REGISTER_COLUMNS.LAST_COLOR_COUNTER.ordinal()+1, 0);
            pst.setInt(MFP_REGISTER_COLUMNS.LAST_BW_COUNTER.ordinal()+1, 0);
            pst.setString(MFP_REGISTER_COLUMNS.MFPSERIAL.ordinal()+1, mfpserial);
            pst.setBoolean(MFP_REGISTER_COLUMNS.IS_REPORTING.ordinal()+1, true);//(Boolean)"online".equalsIgnoreCase(status));
            //** NUEVO **//
            pst.setString(MFP_REGISTER_COLUMNS.LOCATION.ordinal()+1, location);
            pst.setString(MFP_REGISTER_COLUMNS.ZONE.ordinal()+1, zone);
            pst.setString(MFP_REGISTER_COLUMNS.HOSTNAME.ordinal()+1, hostname);
            pst.setString(MFP_REGISTER_COLUMNS.COMPANYNAME.ordinal()+1, company);

            int rows = pst.executeUpdate();
            pst.close();
            return (rows==1);
        }
        catch (SQLException ex)
        {
            System.out.println("registerMFP() : " + pst);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
            return false;
        }
    }
    
    
    public boolean updateMFP_List(String mfpserial, String model_type, String ip_address, String status, String location, String zone, String hostname, String company)
    {
        PreparedStatement pst = null;
        try
        {
            //Insert detail into the database
            pst = getConnection().prepareStatement(QUERY_UPDATE_MFP_REGISTER);
            pst.setString(1, ip_address);
            pst.setString(2, model_type);
            pst.setString(3, status);
            pst.setInt(4, 0);
            pst.setInt(5, 0);
            pst.setString(6, hostname);
            pst.setString(7, location);
            pst.setString(8, zone);
            pst.setString(9, company);
            pst.setString(10, mfpserial);
            //pst.setBoolean(MFP_REGISTER_COLUMNS.IS_REPORTING.ordinal()+1, true);//(Boolean)"online".equalsIgnoreCase(status));

            int rows = pst.executeUpdate();
            pst.close();
            return (rows==1);
        }
        catch (SQLException ex)
        {
            System.out.println("registerMFP() : " + pst);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
            return false;
        }
    }

    public boolean updateRegisteredMFP(Printer printer)
    {
        PreparedStatement pst = null;
        try
        {
            
//            System.out.println(" updateRegisteredMFP ipaddress: " + printer.getIpAddress() + " modeltype: " + printer.getModelType() + " location: " + printer.getLocation() + " hostname: " + printer.getHostname() + " company: " + printer.getCompany() +  " zone: " + printer.getZone());
            //Insert detail into the database

            pst = getConnection().prepareStatement(QUERY_UPDATE_MFP_dbMFP_REGISTER);
            pst.setString(MFP_REGISTER_COLUMNS.IP_ADDRESS.ordinal()+1, printer.getIpAddress());
            pst.setString(MFP_REGISTER_COLUMNS.MODEL_TYPE.ordinal()+1, printer.getModelType());
            pst.setString(MFP_REGISTER_COLUMNS.STATUS.ordinal()+1, printer.getStatus());
            pst.setInt(MFP_REGISTER_COLUMNS.LAST_COLOR_COUNTER.ordinal()+1, printer.getLastColorCounter());
            pst.setInt(MFP_REGISTER_COLUMNS.LAST_BW_COUNTER.ordinal()+1, printer.getLastBWCounter());
            pst.setString(MFP_REGISTER_COLUMNS.MFPSERIAL.ordinal()+2, printer.getMfpSerial());
            
            //** NUEVO **//
            //pst.setString(MFP_REGISTER_COLUMNS.LOCATION.ordinal()+1, printer.getLocation());
            //pst.setString(MFP_REGISTER_COLUMNS.ZONE.ordinal()+1, printer.getZone());
            pst.setString(MFP_REGISTER_COLUMNS.HOSTNAME.ordinal()+1, printer.getHostname());
            pst.setString(MFP_REGISTER_COLUMNS.COMPANYNAME.ordinal()-1, printer.getCompany());

            int rows = pst.executeUpdate();
            pst.close();
            return (rows==1);
        }
        catch (SQLException ex)
        {
            loggerDBM.error("updateRegisteredMFP() : " + pst);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
            return false;
        }
    }

    public List getUnknownUserMFPs()
    {
        List printersList = new ArrayList();
        try
        {
            ResultSet rs = st.executeQuery(QUERY_GET_DESCONOCIDO_MFPS);

            while(rs.next())
                printersList.add(rs.getString(1));

            rs.close();
        }
        catch (SQLException ex)
        {
            loggerDBM.error("getUnknownUserMFPs() : " + QUERY_GET_DESCONOCIDO_MFPS);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
        }
        finally
        {
            return printersList;
        }
    }
    
    public List getUnknownUserMFPs(String company)
    {
        PreparedStatement pst = null;
        List printersList = new ArrayList();
        try
        {
            
            pst = getConnection().prepareStatement(QUERY_GET_DESCONOCIDO_MFPS_PERCOMPANY);
            pst.setString(1, company);
            ResultSet rs = pst.executeQuery();

            while(rs.next())
                printersList.add(rs.getString(1));

            rs.close();
        }
        catch (SQLException ex)
        {
            loggerDBM.error("getUnknownUserMFPs() : " + QUERY_GET_DESCONOCIDO_MFPS_PERCOMPANY);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
        }
        finally
        {
            return printersList;
        }
    }

    public List getUserMFPs(String user, Date dateStart, Date dateEnd)
    {
        List userPrinters = new ArrayList();
        PreparedStatement query = null;

        try
        {
            query = getConnection().prepareStatement(QUERY_USER_PRINTERS_dbGENERAL);
            query.setString(1, user);
            query.setDate(2, new java.sql.Date(dateStart.getTime()));
            query.setDate(3, new java.sql.Date(dateEnd.getTime()));
            ResultSet rs = query.executeQuery();

            while(rs.next())
                userPrinters.add(rs.getString(1));
                    
            rs.close();
            query.close();
        }
        catch (Exception ex)
        {
            loggerDBM.error("getUserMFPs() : " + query);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
        }
        finally
        {
            return userPrinters;
        }
    }

    /** NUEVO **/
    public List getUserMFPs(String user)
    {
        List userPrinters = new ArrayList();
        PreparedStatement query = null;

        try
        {
            query = getConnection().prepareStatement(QUERY_USER_PRINTERS_dbUSER_MFP_COUNTERS);
            query.setString(1, user);
            ResultSet rs = query.executeQuery();

            while(rs.next())
                userPrinters.add(rs.getString(1));

            rs.close();
            query.close();
        }
        catch (Exception ex)
        {
            loggerDBM.error("getUserMFPs() : " + query);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
        }
        finally
        {
            return userPrinters;
        }
    }

    public List getUserMFPs(String user, String company)
    {
        List userPrinters = new ArrayList();
        PreparedStatement query = null;

        try
        {
            query = getConnection().prepareStatement(QUERY_USER_PRINTERS_PerCompany_dbUSER_MFP_COUNTERS);
            query.setString(1, user);
            query.setString(2, company);
            ResultSet rs = query.executeQuery();

            while(rs.next())
                userPrinters.add(rs.getString(1));

            rs.close();
            query.close();
        }
        catch (Exception ex)
        {
            loggerDBM.error("getUserMFPs() : " + query);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
        }
        finally
        {
            return userPrinters;
        }
    }
    
    public final String getCECOname(String id)
    {
        String cecoName = "";

        PreparedStatement pst = null;

        try
        {
            pst = getConnection().prepareStatement(QUERY_GET_CC_NAME_dbCC_COUNTERS);
            pst.setString(1, id);

            ResultSet rs = pst.executeQuery();

            if(rs.next())
                cecoName = rs.getString(1);

            rs.close();
        }
        catch (SQLException ex)
        {
            loggerDBM.error("getCECOname() : " + QUERY_GET_CC_NAME_dbCC_COUNTERS);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
        }
        finally
        {
            return cecoName;
        }
    }

    public final Resume getTotalsBetweenDates(String id, final Date dateStart, final Date dateEnd, Resume.resumeType rType)
    {
        Date dateIni;
        //loggerDBM.debug("getTotalsBetweenDates() : dateEnd=" +  dateEnd);
        try
        {
            dateIni = getPreviousDay(dateStart);
        }
        catch (ParseException ex)
        {
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
            return null;
        }

        Resume startResume = getDateResume(id, dateIni, rType);
        Resume endResume = getDateResume(id, dateEnd, rType);

        if (startResume != null)
            endResume.subtractResumeValues(endResume, startResume);

        if (endResume == null)
        {
            endResume = new Resume(id, dateEnd);
            
            if (rType.equals(Resume.resumeType.CC_RESUME))
                endResume.setUser(new User("", id, this.getCECOname(id),""));
        }

        if (rType.equals(Resume.resumeType.MFP_RESUME) || rType.equals(Resume.resumeType.DESCONOCIDO_MFP_RESUME))
        {
            Printer printer = this.getMFPbyID(id);
            if (printer == null)
            {
                loggerDBM.error("getTotalsBetweenDates() : Cannot obtain mfp " + id + " from MFP Register");
                printer = new Printer();
            }
            endResume.setPrinter(printer);
        }
        //loggerDBM.debug("getTotalsBetweenDates() : ResumedateEnd=" +  endResume.getDate());
        return endResume;

    }

    //** NUEVO **//
    public Resume getTotalsBetweenDates(String username, String mfp, Date dateStart, Date dateEnd)
    {
        Date dateIni;
        //loggerDBM.debug("getTotalsBetweenDates() : dateEnd=" +  dateEnd);
        try
        {
            dateIni = getPreviousDay(dateStart);
        }
        catch (ParseException ex)
        {
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
            return null;
        }

        Resume startResume = getDateResume(username, mfp, dateIni);
        Resume endResume = getDateResume(username, mfp, dateEnd);

        if (startResume != null)
            endResume.subtractResumeValues(endResume, startResume);

        if (endResume == null)
            endResume = new Resume(mfp, dateEnd);

        Printer printer = this.getMFPbyID(mfp);
        if (printer == null)
        {
            loggerDBM.error("getTotalsBetweenDates() : Cannot obtain mfp " + mfp + " from MFP Register");
            printer = new Printer();
        }
        endResume.setPrinter(printer);
        //loggerDBM.debug("getTotalsBetweenDates() : ResumedateEnd=" +  endResume.getDate());
        return endResume;
    }

    public List getIdActions(String company, String id, Date dateStart, Date dateEnd, Action.actionType aType)
    {
        String query = null;
        PreparedStatement pst = null;
        Action action;
        List actionList = new ArrayList();

        switch (aType)
        {
                case USER_ACTION:
                    if (company.equalsIgnoreCase(""))
                        query = QUERY_USER_ACTIONS_dbGENERAL;
                    else
                        query = QUERY_USER_ACTIONS_PerCompany_dbGENERAL;
                    break;

                case MFP_ACTION:
                    query = QUERY_MFP_ACTIONS_dbGENERAL;
                    break;
        }

        try
        {
            pst = getConnection().prepareStatement(query);
            pst.setString(1, id);
            pst.setDate(2, new java.sql.Date(dateStart.getTime()));
            pst.setDate(3, new java.sql.Date(dateEnd.getTime()));
            if (!company.equalsIgnoreCase(""))
               pst.setString(4, company); 
            ResultSet rs = pst.executeQuery();

            while(rs.next())
            {
                action = new Action();
                action.setUserName(rs.getString(ACTION_COLUMNS.USERNAME.ordinal()+1));
                action.setMFPserial(rs.getString(ACTION_COLUMNS.MFPSERIAL.ordinal()+1));
                action.setDate(rs.getDate(ACTION_COLUMNS.DATE.ordinal()+1));
                action.setDateTime(rs.getTimestamp(ACTION_COLUMNS.DATE_TIME.ordinal()+1));
                action.setType(rs.getString(ACTION_COLUMNS.SOURCETYPE.ordinal()+1));
                action.setNumPages(rs.getInt(ACTION_COLUMNS.NUM_PAGES.ordinal()+1));
                action.setColorMode(rs.getString(ACTION_COLUMNS.COLORMODE.ordinal()+1));
                action.setPageSize(rs.getString(ACTION_COLUMNS.PAGE_SIZE.ordinal()+1));
                action.setDestination(rs.getString(ACTION_COLUMNS.DESTINATION.ordinal()+1));
                action.setSender(rs.getString(ACTION_COLUMNS.SENDER.ordinal()+1));
                action.setDocuName(rs.getString(ACTION_COLUMNS.DOCUMENT_NAME.ordinal()+1));

                Printer printer = this.getMFPbyID(action.getMFPserial());
                if (printer == null)
                {
                    loggerDBM.error("getIdActions() : Cannot obtain mfp " + action.getMFPserial() + " from MFP Register");
                    printer = new Printer();
                }
                action.setPrinter(printer);

                actionList.add(action);
            }
            rs.close();
            pst.close();

            return actionList;
        }
        catch (Exception ex)
        {
            loggerDBM.error("getIdActions() : " + pst);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
            return new ArrayList();
        }
    }

    public Action getActionFromRS(Action.actionType aType)
    {
        Action action = null;
        ResultSet rs = null;

        switch (aType)
        {
            case USER_ACTION:
                rs = usersActionsResultSet;
                break;

            case MFP_ACTION:
                rs = printersActionsResultSet;
                break;
        }

        if (rs == null)
        {
            loggerDBM.error("getActionFromRS() : ResultSet is null");
            return null;
        }

        try
        {
            action = new Action();
            action.setUserName(rs.getString(ACTION_COLUMNS.USERNAME.ordinal() + 1));
            action.setMFPserial(rs.getString(ACTION_COLUMNS.MFPSERIAL.ordinal() + 1));
            action.setDate(rs.getDate(ACTION_COLUMNS.DATE.ordinal() + 1));
            action.setDateTime(rs.getTimestamp(ACTION_COLUMNS.DATE_TIME.ordinal() + 1));
            action.setType(rs.getString(ACTION_COLUMNS.SOURCETYPE.ordinal() + 1));
            action.setNumPages(rs.getInt(ACTION_COLUMNS.NUM_PAGES.ordinal() + 1));
            action.setColorMode(rs.getString(ACTION_COLUMNS.COLORMODE.ordinal() + 1));
            action.setPageSize(rs.getString(ACTION_COLUMNS.PAGE_SIZE.ordinal() + 1));
            action.setDestination(rs.getString(ACTION_COLUMNS.DESTINATION.ordinal() + 1));
            action.setSender(rs.getString(ACTION_COLUMNS.SENDER.ordinal() + 1));
            action.setDocuName(rs.getString(ACTION_COLUMNS.DOCUMENT_NAME.ordinal() + 1));

            Printer printer = this.getMFPbyID(action.getMFPserial());
            if (printer == null)
            {
                loggerDBM.error("getActionFromRS() : Cannot obtain mfp " + action.getMFPserial() + " from MFP Register");
                printer = new Printer();
            }
            action.setPrinter(printer);

            return action;
        }
        catch (SQLException ex)
        {
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
            return null;
        }
    }

    public boolean hasMoreActions(Action.actionType aType)
    {
        ResultSet rs = null;
        PreparedStatement pst = null;

        switch (aType)
        {
            case USER_ACTION:
                rs = usersActionsResultSet;
                pst = usersActionsPST;
                break;

            case MFP_ACTION:
                rs = printersActionsResultSet;
                pst = printersActionsPST;
                break;
        }

        if (rs == null)
        {
            loggerDBM.error("hasMoreActions() : ResultSet is null");
            return false;
        }

        try
        {
            if (rs.next())
                return true;

            else
            {
                rs.close();
                pst.close();
                return false;
            }
        }
        catch (SQLException ex)
        {
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
            return false;
        }
    }

    public boolean fillActionsRS(Date dateStart, Date dateEnd, Action.actionType aType)
    {
        String query = null;

        try
        {
            switch (aType)
            {
                case USER_ACTION:
                    query = QUERY_ALLUSERS_ACTIONS_dbGENERAL;
                    usersActionsPST = getConnection().prepareStatement(query);
                    usersActionsPST.setDate(1, new java.sql.Date(dateStart.getTime()));
                    usersActionsPST.setDate(2, new java.sql.Date(dateEnd.getTime()));
                    usersActionsResultSet = usersActionsPST.executeQuery();
                    return true;

                case MFP_ACTION:
                    query = QUERY_ALLPRINTERS_ACTIONS_dbGENERAL;
                    printersActionsPST = getConnection().prepareStatement(query);
                    printersActionsPST.setDate(1, new java.sql.Date(dateStart.getTime()));
                    printersActionsPST.setDate(2, new java.sql.Date(dateEnd.getTime()));
                    printersActionsResultSet = printersActionsPST.executeQuery();
                    return true;

                default:
                    return false;
            }
        }
        catch (Exception ex)
        {
            switch (aType)
            {
                case USER_ACTION:
                    loggerDBM.error("fillActionsRS() : " + usersActionsPST);
                    break;

                case MFP_ACTION:
                    loggerDBM.error("fillActionsRS() : " + printersActionsPST);
                    break;
            }
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
            return false;
        }
    }
    
    public boolean fillActionsRS(Date dateStart, Date dateEnd, Action.actionType aType, String username)
    {
        String query = null;

        try
        {
            switch (aType)
            {
                case USER_ACTION:
                    query = QUERY_ALLUSERS_ACTIONS_PERUSER_dbGENERAL;
                    usersActionsPST = getConnection().prepareStatement(query);
                    usersActionsPST.setDate(1, new java.sql.Date(dateStart.getTime()));
                    usersActionsPST.setDate(2, new java.sql.Date(dateEnd.getTime()));
                    usersActionsPST.setString(3, username);
                    usersActionsResultSet = usersActionsPST.executeQuery();
                    return true;

                case MFP_ACTION:
                    query = QUERY_ALLPRINTERS_ACTIONS_dbGENERAL;
                    printersActionsPST = getConnection().prepareStatement(query);
                    printersActionsPST.setDate(1, new java.sql.Date(dateStart.getTime()));
                    printersActionsPST.setDate(2, new java.sql.Date(dateEnd.getTime()));
                    printersActionsResultSet = printersActionsPST.executeQuery();
                    return true;

                default:
                    return false;
            }
        }
        catch (Exception ex)
        {
            switch (aType)
            {
                case USER_ACTION:
                    loggerDBM.error("fillActionsRS() : " + usersActionsPST);
                    break;

                case MFP_ACTION:
                    loggerDBM.error("fillActionsRS() : " + printersActionsPST);
                    break;
            }
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
            return false;
        }
    }
    
    public boolean fillActionsRS_PerCompany(Date dateStart, Date dateEnd, Action.actionType aType, String company)
    {
        String query = null;

        try
        {
            switch (aType)
            {
                case USER_ACTION:
                    query = QUERY_ALLUSERS_ACTIONS_PERCOMPANY_dbGENERAL;
                    usersActionsPST = getConnection().prepareStatement(query);
                    usersActionsPST.setDate(1, new java.sql.Date(dateStart.getTime()));
                    usersActionsPST.setDate(2, new java.sql.Date(dateEnd.getTime()));
                    usersActionsPST.setString(3, company);
                    usersActionsResultSet = usersActionsPST.executeQuery();
                    return true;

                case MFP_ACTION:
                    query = QUERY_ALLPRINTERS_ACTIONS_dbGENERAL;
                    printersActionsPST = getConnection().prepareStatement(query);
                    printersActionsPST.setDate(1, new java.sql.Date(dateStart.getTime()));
                    printersActionsPST.setDate(2, new java.sql.Date(dateEnd.getTime()));
                    printersActionsResultSet = printersActionsPST.executeQuery();
                    return true;

                default:
                    return false;
            }
        }
        catch (Exception ex)
        {
            switch (aType)
            {
                case USER_ACTION:
                    loggerDBM.error("fillActionsRS() : " + usersActionsPST);
                    break;

                case MFP_ACTION:
                    loggerDBM.error("fillActionsRS() : " + printersActionsPST);
                    break;
            }
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
            return false;
        }
    }
    
    public boolean fillActionsRS_PerCompany(Date dateStart, Date dateEnd, Action.actionType aType, String company, String username)
    {
        String query = null;

        try
        {
            switch (aType)
            {
                case USER_ACTION:
                    query = QUERY_ALLUSERS_ACTIONS_PERCOMPANY_PERUSER_dbGENERAL;
                    usersActionsPST = getConnection().prepareStatement(query);
                    usersActionsPST.setDate(1, new java.sql.Date(dateStart.getTime()));
                    usersActionsPST.setDate(2, new java.sql.Date(dateEnd.getTime()));
                    usersActionsPST.setString(3, company);
                    usersActionsPST.setString(4, username);
                    usersActionsResultSet = usersActionsPST.executeQuery();
                    return true;

                case MFP_ACTION:
                    query = QUERY_ALLPRINTERS_ACTIONS_dbGENERAL;
                    printersActionsPST = getConnection().prepareStatement(query);
                    printersActionsPST.setDate(1, new java.sql.Date(dateStart.getTime()));
                    printersActionsPST.setDate(2, new java.sql.Date(dateEnd.getTime()));
                    printersActionsResultSet = printersActionsPST.executeQuery();
                    return true;

                default:
                    return false;
            }
        }
        catch (Exception ex)
        {
            switch (aType)
            {
                case USER_ACTION:
                    loggerDBM.error("fillActionsRS() : " + usersActionsPST);
                    break;

                case MFP_ACTION:
                    loggerDBM.error("fillActionsRS() : " + printersActionsPST);
                    break;
            }
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
            return false;
        }
    }

    public boolean fillActionsRSbyID(String id, Date dateStart, Date dateEnd, Action.actionType aType)
    {
        String query = null;

        try
        {
            switch (aType)
            {
                case USER_ACTION:
                    query = QUERY_USER_ACTIONS_dbGENERAL;
                    usersActionsPST = getConnection().prepareStatement(query);
                    usersActionsPST.setString(1, id);
                    usersActionsPST.setDate(2, new java.sql.Date(dateStart.getTime()));
                    usersActionsPST.setDate(3, new java.sql.Date(dateEnd.getTime()));
                    usersActionsResultSet = usersActionsPST.executeQuery();
                    return true;

                case MFP_ACTION:
                    query = QUERY_MFP_ACTIONS_dbGENERAL;
                    printersActionsPST = getConnection().prepareStatement(query);
                    printersActionsPST.setString(1, id);
                    printersActionsPST.setDate(2, new java.sql.Date(dateStart.getTime()));
                    printersActionsPST.setDate(3, new java.sql.Date(dateEnd.getTime()));
                    printersActionsResultSet = printersActionsPST.executeQuery();
                    return true;

                default:
                    return false;
            }
        }
        catch (Exception ex)
        {
            switch (aType)
            {
                case USER_ACTION:
                    loggerDBM.error("fillActionsRSbyID() : " + usersActionsPST);
                    break;

                case MFP_ACTION:
                    loggerDBM.error("fillActionsRSbyID() : " + printersActionsPST);
                    break;
            }
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
            return false;
        }
    }

    public List getMFPsOnState(String state)
    {
        List printersList = new ArrayList();
        Printer printer;
        PreparedStatement pst = null;

        try
        {
                        //Get resume from the database
            pst = getConnection().prepareStatement(QUERY_GET_STATE_MFPS_dbMFP_REGISTER);
            pst.setString(1, state);

            ResultSet rs = pst.executeQuery();

            while(rs.next())
            {
                printer = new Printer(rs.getString(MFP_REGISTER_COLUMNS.MFPSERIAL.ordinal()+1),
                                      rs.getString(MFP_REGISTER_COLUMNS.IP_ADDRESS.ordinal()+1),
                                      rs.getString(MFP_REGISTER_COLUMNS.MODEL_TYPE.ordinal()+1),
                                      rs.getString(MFP_REGISTER_COLUMNS.STATUS.ordinal()+1),
                                      rs.getInt(MFP_REGISTER_COLUMNS.LAST_COLOR_COUNTER.ordinal()+1),
                                      rs.getInt(MFP_REGISTER_COLUMNS.LAST_BW_COUNTER.ordinal()+1),
                                      rs.getString(MFP_REGISTER_COLUMNS.LOCATION.ordinal()+1),
                                      rs.getString(MFP_REGISTER_COLUMNS.ZONE.ordinal()+1),
                                      rs.getString(MFP_REGISTER_COLUMNS.HOSTNAME.ordinal()+1),
                                      rs.getString(MFP_REGISTER_COLUMNS.COMPANYNAME.ordinal()));

                printersList.add(printer);
            }

            rs.close();
        }
        catch (SQLException ex)
        {
            loggerDBM.error("getMFPsOnState() : " + QUERY_GET_STATE_MFPS_dbMFP_REGISTER);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
        }
        finally
        {
            return printersList;
        }
    }
    
    public List getMFPsOnState(String state, String company)
    {
        List printersList = new ArrayList();
        Printer printer;
        PreparedStatement pst = null;

        try
        {
                        //Get resume from the database
            pst = getConnection().prepareStatement(QUERY_GET_STATE_MFPS_PERCOMPANY_dbMFP_REGISTER);
            pst.setString(1, state);
            pst.setString(2, company);

            ResultSet rs = pst.executeQuery();

            while(rs.next())
            {
                printer = new Printer(rs.getString(MFP_REGISTER_COLUMNS.MFPSERIAL.ordinal()+1),
                                      rs.getString(MFP_REGISTER_COLUMNS.IP_ADDRESS.ordinal()+1),
                                      rs.getString(MFP_REGISTER_COLUMNS.MODEL_TYPE.ordinal()+1),
                                      rs.getString(MFP_REGISTER_COLUMNS.STATUS.ordinal()+1),
                                      rs.getInt(MFP_REGISTER_COLUMNS.LAST_COLOR_COUNTER.ordinal()+1),
                                      rs.getInt(MFP_REGISTER_COLUMNS.LAST_BW_COUNTER.ordinal()+1),
                                      rs.getString(MFP_REGISTER_COLUMNS.LOCATION.ordinal()+1),
                                      rs.getString(MFP_REGISTER_COLUMNS.ZONE.ordinal()+1),
                                      rs.getString(MFP_REGISTER_COLUMNS.HOSTNAME.ordinal()+1),
                                      rs.getString(MFP_REGISTER_COLUMNS.COMPANYNAME.ordinal()));

                printersList.add(printer);
            }

            rs.close();
        }
        catch (SQLException ex)
        {
            loggerDBM.error("getMFPsOnState() : " + QUERY_GET_STATE_MFPS_dbMFP_REGISTER);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
        }
        finally
        {
            return printersList;
        }
    }

    public String[] authenticateUser(String userName, String password)
    {
        PreparedStatement pst = null;
        boolean isAdmin = false;
        int result = 0;
        String[] response = new String[2];

        try
        {
            //Get resume from the database
            pst = getConnection().prepareStatement(QUERY_AUTHENTICATE_USER_dbWEB_USERS);
            pst.setString(1, userName);
            pst.setString(2, password);

            ResultSet rs = pst.executeQuery();

            if (rs.next())
            {
                if (isAdmin = rs.getBoolean(1)){
                    response[0] = "1";
                    response[1] = rs.getString(2);
                }else{
                    response[0] = "2";
                    response[1] = rs.getString(2);
                }
            }

            rs.close();
        }
        catch (SQLException ex)
        {
            loggerDBM.error("authenticateUser() : " + QUERY_AUTHENTICATE_USER_dbWEB_USERS);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
        }
        finally
        {
            return response;
        }
    }

//    public List getAllPrintersDetails(Date dateStart, Date dateEnd, boolean sync)
//    {
//        PreparedStatement query = null;
//        Action action;
//
//        if (!sync && !allPrintersActionsList.isEmpty() && this.dateStart.equals(dateStart) && this.dateEnd.equals(dateEnd))
//            return allPrintersActionsList;
//
//        else
//        {
//            try
//            {
//                allPrintersActionsList.clear();
//
//                query = con.prepareStatement(QUERY_ALLPRINTERS_ACTIONS_dbGENERAL);
//                query.setDate(1, new java.sql.Date(dateStart.getTime()));
//                query.setDate(2, new java.sql.Date(dateEnd.getTime()));
//                ResultSet rs = query.executeQuery();
//
//                while(rs.next())
//                {
//                    action = new Action();
//                    action.setUserName(rs.getString(ACTION_COLUMNS.ID2.ordinal()));
//                    action.setMFPserial(rs.getString(ACTION_COLUMNS.MFPSERIAL.ordinal()));
//                    action.setDate(rs.getDate(ACTION_COLUMNS.DATE.ordinal()));
//                    action.setDateTime(rs.getTimestamp(ACTION_COLUMNS.DATE_TIME.ordinal()));
//                    action.setType(rs.getString(ACTION_COLUMNS.SOURCETYPE.ordinal()));
//                    action.setNumPages(rs.getInt(ACTION_COLUMNS.NUM_PAGES.ordinal()));
//                    action.setColorMode(rs.getString(ACTION_COLUMNS.COLORMODE.ordinal()));
//                    action.setPageSize(rs.getString(ACTION_COLUMNS.PAGE_SIZE.ordinal()));
//                    action.setDestination(rs.getString(ACTION_COLUMNS.DESTINATION.ordinal()));
//                    action.setSender(rs.getString(ACTION_COLUMNS.SENDER.ordinal()));
//                    action.setDocuName(rs.getString(ACTION_COLUMNS.DOCUMENT_NAME.ordinal()));
//
//                    allPrintersActionsList.add(action);
//                }
//                rs.close();
//                query.close();
//
//                this.dateStart = dateStart;
//                this.dateEnd = dateEnd;
//            }
//            catch (Exception ex)
//            {
//                loggerDBM.error("getAllPrintersDetails() : " + query);
//                loggerDBM.error(ex.getMessage());
//                loggerDBM.error(ex.getStackTrace());
//                allPrintersActionsList.clear();
//            }
//            finally
//            {
//                return allPrintersActionsList;
//            }
//        }
//    }


    public Resume getLastNotReportingResume(String mfpSerial)
    {
        Resume resume = null;

        try
        {
            //Get resume from the database
            Date resumeDate = getPreviousDay(getExecutingDate());
            resume = getDateResume(mfpSerial, resumeDate, Resume.resumeType.DESCONOCIDO_MFP_RESUME);
            if (resume != null)
                return resume;
            else
                return new Resume(mfpSerial, resumeDate);
        }
        catch (ParseException ex)
        {
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
            return new Resume(mfpSerial, null);
        }
    }

    public boolean insertActions(List actionList)
    {
        ListIterator it = actionList.listIterator();
        while (it.hasNext())
        {
            Action action = (Action)it.next();
            //FOR COMPARTAMOS -> ONLY COPIES AND PRINTERS
            if ((!"fax.transfer".equalsIgnoreCase(action.getType())) && (!"scanner.deliver-storage".equalsIgnoreCase(action.getType())) && (!"stapler".equalsIgnoreCase(action.getType())))               
            {
                if (!insertActionInDB(action))
                    return false;
            }
        }
        return true;
    }

    public boolean insertNotReportingResume(Resume resume, Resume.resumeType rType)
    {
        Resume dayResume;
        try
        {
            //** NUEVO **//
            switch (rType)
            {
                case USER_MFP_RESUME:
                    dayResume = getDateResume("", resume.getId(), getExecutingDate());
                    if (dayResume == null)
                        return insertResumeInDB("", resume, getExecutingDate());
                    else
                        return updateResumeInDB("", resume, getExecutingDate());

                default:
                    dayResume = getDateResume(resume.getId(), getExecutingDate(), rType);
                    if (dayResume == null)
                        return insertResumeInDB(resume, getExecutingDate(), rType);
                    else
                        return updateResumeInDB(resume, getExecutingDate(), rType);

            }
        }
        catch (ParseException ex)
        {
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
            return false;
        }
    }

    //** NUEVO **//
    public boolean insertNewUserMfpResumeList(String username, List resumeList)
    {
        try
        {
            //Insert the first element of the resumeList into the USER_MFP_COUNTERS database
            Resume resume = (Resume) resumeList.get(0);

            //System.out.println("insertNewResumeList Insert Date: " + resume.getDate());

            //If the first resume fails to insert, return false
            if (!insertResumeInDB(username, resume, resume.getDate()))
                return false;

            Date nextDate = getNextDay(resume.getDate());

            //Insert recursively the following resumes
            return resumeInsertRecursive(username, resume, nextDate, resumeList.subList(1, resumeList.size()));
        }
        catch (ParseException ex)
        {
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
            return false;
        }
    }

    public boolean insertNewResumeList(List resumeList, Resume.resumeType rType)
    {
        try
        {
            //Insert the first element of the resumeList into the USER_COUNTERS or MFP_COUNTERS database
            Resume resume = (Resume) resumeList.get(0);

            //System.out.println("insertNewResumeList Insert Date: " + resume.getDate());

            //If the first resume fails to insert, return false
            if (!insertResumeInDB(resume, resume.getDate(), rType))
                return false;

            Date nextDate = getNextDay(resume.getDate());

            //Insert recursively the following resumes
            return resumeInsertRecursive(resume, nextDate, resumeList.subList(1, resumeList.size()), rType);
        }
        catch (ParseException ex)
        {
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
            return false;
        }
    }

    //** NUEVO **//
    public boolean updateUserMfpResumeList(String username, String mfp, List resumeList)
    {
        Resume resume, dbResume, insertResume;
        Date insertDate, nextDate, maxDate, previousDate;

        try
        {
            if (resumeList.isEmpty())
            {
                resume = getLastResumeInserted(username, mfp);
                if (resume.getDate().before(getExecutingDate()))
                {
                    nextDate = getNextDay(resume.getDate());

                    return resumeInsertRecursive(username, resume, nextDate, resumeList);
                }
                else
                    return true;
            }
            else
            {
                maxDate = getExecutingDate();
                ListIterator it = resumeList.listIterator();
                while (it.hasNext())
                {
                    resume = (Resume)it.next();

                    insertDate = resume.getDate();
                    insertResume = new Resume(resume.getId(), insertDate);
                    dbResume = getDateResume(username, mfp, insertDate);

                    if (dbResume != null)
                    {
                        insertResume.setResumeValues(resume, dbResume);

                        if (!updateResumeInDB(username, insertResume, insertDate))
                            return false;

                        nextDate = getNextDay(insertDate);

                        if (!resumeUpdateRecursive(username, resume, nextDate, maxDate))
                            return false;
                    }
                    else
                    {
                        previousDate = getPreviousDay(insertDate);
                        dbResume = getDateResume(username, mfp, previousDate);

                        if (dbResume != null)
                        {
                            insertResume.setResumeValues(resume, dbResume);

                            if (!insertResumeInDB(username, insertResume, insertDate))
                                return false;

                            nextDate = getNextDay(insertDate);

                            if (!resumeUpdateRecursive(username, resume, nextDate, maxDate))
                                return false;
                        }
                        else
                        {
                            if (resume.getTotal() != 0)
                            {
                                if (!insertResumeInDB(username, resume, insertDate))
                                    return false;

                                nextDate = getNextDay(insertDate);

                                if (!resumeUpdateRecursive(username, resume, nextDate, maxDate))
                                    return false;
                            }
                        }
                    }
                }
                return true;
            }
        }
        catch (ParseException ex)
        {
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
            return false;
        }
    }

    public boolean updateResumeList(String id, List resumeList, Resume.resumeType rType)
    {
        Resume resume, dbResume, insertResume;
        Date insertDate, nextDate, maxDate, previousDate;        

        try
        {
            if (resumeList.isEmpty())
            {
                resume = getLastResumeInserted(id, rType);
                if (resume.getDate().before(getExecutingDate()))
                {
                    nextDate = getNextDay(resume.getDate());

                    return resumeInsertRecursive(resume, nextDate, resumeList, rType);
                }
                else
                    return true;
            }
            else
            {
                maxDate = getExecutingDate();
                ListIterator it = resumeList.listIterator();
                while (it.hasNext())
                {
                    resume = (Resume)it.next();

                    insertDate = resume.getDate();
                    insertResume = new Resume(resume.getId(), insertDate);
                    insertResume.setUser(resume.getUser());
                    dbResume = getDateResume(id, insertDate, rType);

                    if (dbResume != null)
                    {
                        insertResume.setResumeValues(resume, dbResume);

                        if (!updateResumeInDB(insertResume, insertDate, rType))
                            return false;

                        nextDate = getNextDay(insertDate);

                        if (!resumeUpdateRecursive(resume, nextDate, maxDate, rType))
                            return false;
                    }
                    else
                    {
                        previousDate = getPreviousDay(insertDate);
                        dbResume = getDateResume(id, previousDate, rType);

                        if (dbResume != null)
                        {
                            insertResume.setResumeValues(resume, dbResume);

                            if (!insertResumeInDB(insertResume, insertDate, rType))
                                return false;

                            nextDate = getNextDay(insertDate);

                            if (!resumeUpdateRecursive(resume, nextDate, maxDate, rType))
                                return false;
                        }
                        else
                        {
                            if (resume.getTotal() != 0)
                            {
                                if (!insertResumeInDB(resume, insertDate, rType))
                                    return false;

                                nextDate = getNextDay(insertDate);

                                if (!resumeUpdateRecursive(resume, nextDate, maxDate, rType))
                                    return false;
                            }
                        }
                    }
                }
                return true;
            }
        }
        catch (ParseException ex)
        {
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
            return false;
        }
    }

    /********************** AUXILIAR METHODS ***************************/

    //** NUEVO **/
    private boolean resumeInsertRecursive(String username, Resume resume, Date insertDate, List resumeList) throws ParseException
    {
        Date nextDate;

        if (insertDate.after(getExecutingDate()))
            return true;

        if (resumeList.isEmpty())
        {
            if (!insertResumeInDB(username, resume, insertDate))
                return false;

            nextDate = getNextDay(insertDate);

            return resumeInsertRecursive(username, resume, nextDate, resumeList);
        }
        else
        {
            Resume currentResume = (Resume)resumeList.get(0);
            Date resumeDate = currentResume.getDate();

            if (resumeDate.equals(insertDate))
            {
                if (!insertResumeInDB(username, currentResume, resumeDate))
                    return false;

                nextDate = getNextDay(resumeDate);

                return resumeInsertRecursive(username, currentResume, nextDate, resumeList.subList(1, resumeList.size()));
            }
            else
            {
                if (!insertResumeInDB(username, resume, insertDate))
                    return false;

                nextDate = getNextDay(insertDate);

                return resumeInsertRecursive(username, resume, nextDate, resumeList);
            }
        }
    }

    private boolean resumeInsertRecursive(Resume resume, Date insertDate, List resumeList, Resume.resumeType resumeType) throws ParseException
    {
        Date nextDate;

        if (insertDate.after(getExecutingDate()))
            return true;

        if (resumeList.isEmpty())
        {
            if (!insertResumeInDB(resume, insertDate, resumeType))
                return false;

            nextDate = getNextDay(insertDate);

            return resumeInsertRecursive(resume, nextDate, resumeList, resumeType);
        }
        else
        {
            Resume currentResume = (Resume)resumeList.get(0);
            Date resumeDate = currentResume.getDate();

            if (resumeDate.equals(insertDate))
            {
                if (!insertResumeInDB(currentResume, resumeDate, resumeType))
                    return false;

                nextDate = getNextDay(resumeDate);

                return resumeInsertRecursive(currentResume, nextDate, resumeList.subList(1, resumeList.size()), resumeType);
            }
            else
            {
                if (!insertResumeInDB(resume, insertDate, resumeType))
                    return false;

                nextDate = getNextDay(insertDate);

                return resumeInsertRecursive(resume, nextDate, resumeList, resumeType);
            }
        }
    }

    //** NUEVO **//
    private boolean resumeUpdateRecursive(String username, Resume resume, Date insertDate, Date maxDate) throws ParseException
    {
        Resume insertResume, dbResume;
        Date nextDate, previousDate;

        if (insertDate.after(maxDate))
            return true;

        insertResume = new Resume(resume.getId(), insertDate);
        dbResume = getDateResume(username, resume.getId(), insertDate);

        if (dbResume != null)
        {
            insertResume.setResumeValues(resume, dbResume);

            if (!updateResumeInDB(username, insertResume, insertDate))
                return false;

            nextDate = getNextDay(insertDate);

            return resumeUpdateRecursive(username, resume, nextDate, maxDate);
        }
        else
        {
            previousDate = getPreviousDay(insertDate);
            dbResume = getDateResume(username, resume.getId(), previousDate);

            if (dbResume != null)
            {
                insertResume.setResumeValues(dbResume);

                if (!insertResumeInDB(username, insertResume, insertDate))
                    return false;

                nextDate = getNextDay(insertDate);

                return resumeUpdateRecursive(username, resume, nextDate, maxDate);
            }
            else
                return false;
//            {
//                if (resume.getTotal() != 0)
//                {
//                    if (!insertResumeInDB(resume, insertDate, resumeType))
//                        return false;
//
//                    nextDate = getNextDay(insertDate);
//
//                    if (!resumeUpdateRecursive(resume, nextDate, maxDate, resumeType))
//                        return false;
//                }
//            }
        }
    }

    private boolean resumeUpdateRecursive(Resume resume, Date insertDate, Date maxDate, Resume.resumeType resumeType) throws ParseException
    {
        Resume insertResume, dbResume;
        Date nextDate, previousDate;

        if (insertDate.after(maxDate))
            return true;

        insertResume = new Resume(resume.getId(), insertDate);
        insertResume.setUser(resume.getUser());
        dbResume = getDateResume(resume.getId(), insertDate, resumeType);

        if (dbResume != null)
        {
            insertResume.setResumeValues(resume, dbResume);

            if (!updateResumeInDB(insertResume, insertDate, resumeType))
                return false;

            nextDate = getNextDay(insertDate);

            return resumeUpdateRecursive(resume, nextDate, maxDate, resumeType);
        }
        else
        {
            previousDate = getPreviousDay(insertDate);
            dbResume = getDateResume(resume.getId(), previousDate, resumeType);

            if (dbResume != null)
            {
                insertResume.setResumeValues(dbResume);

                if (!insertResumeInDB(insertResume, insertDate, resumeType))
                    return false;

                nextDate = getNextDay(insertDate);

                return resumeUpdateRecursive(resume, nextDate, maxDate, resumeType);
            }
            else
                return false;
//            {
//                if (resume.getTotal() != 0)
//                {
//                    if (!insertResumeInDB(resume, insertDate, resumeType))
//                        return false;
//
//                    nextDate = getNextDay(insertDate);
//
//                    if (!resumeUpdateRecursive(resume, nextDate, maxDate, resumeType))
//                        return false;
//                }
//            }
        }
    }

    private boolean insertActionInDB(Action action)
    {
        PreparedStatement pst = null;
        try
        {
            //Insert detail into the database
            pst = getConnection().prepareStatement(QUERY_INSERT_ACTION_dbGENERAL);
            pst.setString(ACTION_COLUMNS.USERNAME.ordinal()+1, action.getUserName());
            pst.setString(ACTION_COLUMNS.MFPSERIAL.ordinal()+1, action.getMFPserial());
            pst.setDate(ACTION_COLUMNS.DATE.ordinal()+1, new java.sql.Date(action.getDate().getTime()));
            pst.setTimestamp(ACTION_COLUMNS.DATE_TIME.ordinal()+1, action.getDateTime());
            pst.setString(ACTION_COLUMNS.SOURCETYPE.ordinal()+1, action.getType());
            pst.setInt(ACTION_COLUMNS.NUM_PAGES.ordinal()+1, action.getNumPages());
            pst.setString(ACTION_COLUMNS.COLORMODE.ordinal()+1, action.getColorMode());
            pst.setString(ACTION_COLUMNS.PAGE_SIZE.ordinal()+1, action.getPageSize());
            pst.setString(ACTION_COLUMNS.DESTINATION.ordinal()+1, action.getDestination());
            pst.setString(ACTION_COLUMNS.SENDER.ordinal()+1, action.getSender());
            pst.setString(ACTION_COLUMNS.DOCUMENT_NAME.ordinal()+1, action.getDocuName());
            pst.setInt(ACTION_COLUMNS.NUM_COPIES.ordinal()+1, action.getNumCopies());
            pst.setString(ACTION_COLUMNS.COMPANYNAME.ordinal()+1, action.getCompany());
            pst.setString(ACTION_COLUMNS.PAGEMODE.ordinal()+1, action.getPageMode());
            
            int rows = pst.executeUpdate();
            pst.close();
            return (rows==1);
        }
        catch (SQLException ex)
        {
            loggerDBM.error("insertActionInDB() : " + pst);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
            return false;
        }

    }

    //** NUEVO **//
    private boolean insertResumeInDB(String username, Resume resume, Date insertDate)
    {
        String query = "";
        PreparedStatement pst = null;
        try
        {
            query = QUERY_INSERT_RESUME_dbUSER_MFP_COUNTERS;

            if (query.isEmpty())
                return false;

            //Insert resume into the database
            pst = getConnection().prepareStatement(query);
            pst.setInt(RESUME_COLUMNS.TOTAL.ordinal()+1, resume.getTotal());
            pst.setInt(RESUME_COLUMNS.COLOR_PRINT.ordinal()+1, resume.getPrintColor());
            pst.setInt(RESUME_COLUMNS.BW_PRINT.ordinal()+1, resume.getPrintBW());
            pst.setInt(RESUME_COLUMNS.COLOR_COPY.ordinal()+1, resume.getCopyColor());
            pst.setInt(RESUME_COLUMNS.BW_COPY.ordinal()+1, resume.getCopyBW());
            pst.setInt(RESUME_COLUMNS.SCAN_SENT.ordinal()+1, resume.getScanSend());
            pst.setInt(RESUME_COLUMNS.SCAN_STORED.ordinal()+1, resume.getScanStored());
            pst.setInt(RESUME_COLUMNS.FAX_SENT.ordinal()+1, resume.getFaxSend());
            pst.setInt(RESUME_COLUMNS.FAX_RECEIVED.ordinal()+1, resume.getFaxReceived());
            pst.setDate(RESUME_COLUMNS.DATE.ordinal()+1, new java.sql.Date(insertDate.getTime()));
            pst.setString(RESUME_COLUMNS.ID.ordinal()+1, resume.getId());
            pst.setString(RESUME_COLUMNS.COMPANYNAME.ordinal()+1, resume.getCompany());
            pst.setString(RESUME_COLUMNS.ID2.ordinal()+1, username);

            int rows = pst.executeUpdate();
            pst.close();
            return (rows==1);
        }
        catch (SQLException ex)
        {
            loggerDBM.error("insertResumeInDB() : " + pst);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
            return false;
        }

    }

    private boolean insertResumeInDB(Resume resume, Date insertDate, Resume.resumeType resumeType)
    {
        String query = "";
        PreparedStatement pst = null;
        try 
        {
            switch (resumeType)
            {
                case USER_RESUME:
                    query = QUERY_INSERT_RESUME_dbUSER_COUNTERS;
                    break;

                case MFP_RESUME:
                    query = QUERY_INSERT_RESUME_dbMFP_COUNTERS;
                    break;

                case DESCONOCIDO_MFP_RESUME:
                    query = QUERY_INSERT_RESUME_dbDESCONOCIDO_MFP_COUNTERS;
                    break;

                case CC_RESUME:
                    query = QUERY_INSERT_RESUME_dbCC_COUNTERS;
                    break;

            }

            if (query.isEmpty())
                return false;
            
            //Insert resume into the database
            pst = getConnection().prepareStatement(query);
            pst.setInt(RESUME_COLUMNS.TOTAL.ordinal()+1, resume.getTotal());
            pst.setInt(RESUME_COLUMNS.COLOR_PRINT.ordinal()+1, resume.getPrintColor());
            pst.setInt(RESUME_COLUMNS.BW_PRINT.ordinal()+1, resume.getPrintBW());
            pst.setInt(RESUME_COLUMNS.COLOR_COPY.ordinal()+1, resume.getCopyColor());
            pst.setInt(RESUME_COLUMNS.BW_COPY.ordinal()+1, resume.getCopyBW());
            pst.setInt(RESUME_COLUMNS.SCAN_SENT.ordinal()+1, resume.getScanSend());
            pst.setInt(RESUME_COLUMNS.SCAN_STORED.ordinal()+1, resume.getScanStored());
            pst.setInt(RESUME_COLUMNS.FAX_SENT.ordinal()+1, resume.getFaxSend());
            pst.setInt(RESUME_COLUMNS.FAX_RECEIVED.ordinal()+1, resume.getFaxReceived());
            pst.setDate(RESUME_COLUMNS.DATE.ordinal()+1, new java.sql.Date(insertDate.getTime()));
//            if (resume.getId().equalsIgnoreCase("noname_"+resume.getCompany())||resume.getId().equalsIgnoreCase("nonameCC_" + resume.getCompany())){
//                resume.setId("");
//            }
            pst.setString(RESUME_COLUMNS.ID.ordinal()+1, resume.getId());
            if (resumeType.equals(Resume.resumeType.USER_RESUME)||resumeType.equals(Resume.resumeType.MFP_RESUME) || resumeType.equals(Resume.resumeType.DESCONOCIDO_MFP_RESUME))
                pst.setString(RESUME_COLUMNS.COMPANYNAME.ordinal(), resume.getCompany());
            if (resumeType.equals(Resume.resumeType.CC_RESUME)){
                pst.setString(RESUME_COLUMNS.ID2.ordinal()+1, resume.getUser().getDepartment());
                pst.setString(RESUME_COLUMNS.COMPANYNAME.ordinal()+1, resume.getCompany());
            }
            
            int rows = pst.executeUpdate();
            pst.close();
            return (rows==1);
        } 
        catch (SQLException ex) 
        {
            loggerDBM.error("insertResumeInDB() : " + pst);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
            return false;
        }
    }

    //** NUEVO **//
    private boolean updateResumeInDB(String username, Resume resume, Date insertDate)
    {
        String query = QUERY_UPDATE_RESUME_dbUSER_MFP_COUNTERS;
        PreparedStatement pst = null;
        try
        {
            if (query.isEmpty())
                return false;

            //Insert resume into the database
            pst = getConnection().prepareStatement(query);
            pst.setInt(RESUME_COLUMNS.TOTAL.ordinal()+1, resume.getTotal());
            pst.setInt(RESUME_COLUMNS.COLOR_PRINT.ordinal()+1, resume.getPrintColor());
            pst.setInt(RESUME_COLUMNS.BW_PRINT.ordinal()+1, resume.getPrintBW());
            pst.setInt(RESUME_COLUMNS.COLOR_COPY.ordinal()+1, resume.getCopyColor());
            pst.setInt(RESUME_COLUMNS.BW_COPY.ordinal()+1, resume.getCopyBW());
            pst.setInt(RESUME_COLUMNS.SCAN_SENT.ordinal()+1, resume.getScanSend());
            pst.setInt(RESUME_COLUMNS.SCAN_STORED.ordinal()+1, resume.getScanStored());
            pst.setInt(RESUME_COLUMNS.FAX_SENT.ordinal()+1, resume.getFaxSend());
            pst.setInt(RESUME_COLUMNS.FAX_RECEIVED.ordinal()+1, resume.getFaxReceived());
            pst.setDate(RESUME_COLUMNS.DATE.ordinal()+1, new java.sql.Date(insertDate.getTime()));
            pst.setString(RESUME_COLUMNS.ID.ordinal()+1, resume.getId());
            pst.setString(RESUME_COLUMNS.ID2.ordinal()+1, username);

            int rows = pst.executeUpdate();
            pst.close();
            return (rows==1);
        }
        catch (SQLException ex)
        {
            loggerDBM.error("updateResumeInDB() : " + pst);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
            return false;
        }
    }


    private boolean updateResumeInDB(Resume resume, Date insertDate, Resume.resumeType resumeType)
    {
        String query = "";
        PreparedStatement pst = null;
        try
        {
            switch (resumeType)
            {
                case USER_RESUME:
                    query = QUERY_UPDATE_RESUME_dbUSER_COUNTERS;
                    break;

                case MFP_RESUME:
                    query = QUERY_UPDATE_RESUME_dbMFP_COUNTERS;
                    break;

                case DESCONOCIDO_MFP_RESUME:
                    query = QUERY_UPDATE_RESUME_dbDESCONOCIDO_MFP_COUNTERS;
                    break;

                case CC_RESUME:
                    query = QUERY_UPDATE_RESUME_dbCC_COUNTERS;
                    break;

            }

            if (query.isEmpty())
                return false;

            //Insert resume into the database
            pst = getConnection().prepareStatement(query);
            pst.setInt(RESUME_COLUMNS.TOTAL.ordinal()+1, resume.getTotal());
            pst.setInt(RESUME_COLUMNS.COLOR_PRINT.ordinal()+1, resume.getPrintColor());
            pst.setInt(RESUME_COLUMNS.BW_PRINT.ordinal()+1, resume.getPrintBW());
            pst.setInt(RESUME_COLUMNS.COLOR_COPY.ordinal()+1, resume.getCopyColor());
            pst.setInt(RESUME_COLUMNS.BW_COPY.ordinal()+1, resume.getCopyBW());
            pst.setInt(RESUME_COLUMNS.SCAN_SENT.ordinal()+1, resume.getScanSend());
            pst.setInt(RESUME_COLUMNS.SCAN_STORED.ordinal()+1, resume.getScanStored());
            pst.setInt(RESUME_COLUMNS.FAX_SENT.ordinal()+1, resume.getFaxSend());
            pst.setInt(RESUME_COLUMNS.FAX_RECEIVED.ordinal()+1, resume.getFaxReceived());
            pst.setDate(RESUME_COLUMNS.DATE.ordinal()+1, new java.sql.Date(insertDate.getTime()));
            pst.setString(RESUME_COLUMNS.ID.ordinal()+1, resume.getId());
            
            int rows = pst.executeUpdate();
            pst.close();
            return (rows==1);
        }
        catch (SQLException ex)
        {
            loggerDBM.error("updateResumeInDB() : " + pst);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
            return false;
        }
    }

    //** NUEVO **//
    private Resume getLastResumeInserted(String username, String mfp)
    {
        String query = QUERY_LAST_RESUME_USER_MFP_dbUSER_MFP_COUNTERS;
        Resume resume = null;
        PreparedStatement pst = null;
        try
        {
            if (query.isEmpty())
                return null;

            //Get resume from the database
            pst = getConnection().prepareStatement(query);
            pst.setString(1, username);
            pst.setString(2, mfp);
            pst.setString(3, username);
            pst.setString(4, mfp);

            ResultSet rs = pst.executeQuery();

            if (rs.next())
            {
                resume = new Resume(mfp, rs.getDate(RESUME_COLUMNS.DATE.ordinal()+1));
                resume.setTotal(rs.getInt(RESUME_COLUMNS.TOTAL.ordinal()+1));
                resume.setPrintColor(rs.getInt(RESUME_COLUMNS.COLOR_PRINT.ordinal()+1));
                resume.setPrintBW(rs.getInt(RESUME_COLUMNS.BW_PRINT.ordinal()+1));
                resume.setCopyColor(rs.getInt(RESUME_COLUMNS.COLOR_COPY.ordinal()+1));
                resume.setCopyBW(rs.getInt(RESUME_COLUMNS.BW_COPY.ordinal()+1));
                resume.setScanSend(rs.getInt(RESUME_COLUMNS.SCAN_SENT.ordinal()+1));
                resume.setScanStored(rs.getInt(RESUME_COLUMNS.SCAN_STORED.ordinal()+1));
                resume.setFaxSend(rs.getInt(RESUME_COLUMNS.FAX_SENT.ordinal()+1));
                resume.setFaxReceived(rs.getInt(RESUME_COLUMNS.FAX_RECEIVED.ordinal()+1));
                resume.setCompany(rs.getString(11));
            }

            rs.close();
            pst.close();

            return resume;
        }
        catch (SQLException ex)
        {
            loggerDBM.error("getLastResumeInserted() : " + pst);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
            return null;
        }
    }

    private Resume getLastResumeInserted(String id, Resume.resumeType resumeType)
    {
        String query = "";
        Resume resume = null;
        PreparedStatement pst = null;
        try
        {
            switch (resumeType)
            {
                case USER_RESUME:
                    query = QUERY_LAST_RESUME_USER_dbUSER_COUNTERS;
                    break;

                case MFP_RESUME:
                    query = QUERY_LAST_RESUME_MFP_dbMFP_COUNTERS;
                    break;

                case DESCONOCIDO_MFP_RESUME:
                    query = QUERY_LAST_RESUME_MFP_dbDESCONOCIDO_MFP_COUNTERS;
                    break;

                case CC_RESUME:
                    query = QUERY_LAST_RESUME_CC_dbCC_COUNTERS;
                    break;

            }

            if (query.isEmpty())
                return null;

            //Get resume from the database
            pst = getConnection().prepareStatement(query);
            pst.setString(1, id);
            pst.setString(2, id);

            ResultSet rs = pst.executeQuery();

            if (rs.next())
            {
                resume = new Resume(id, rs.getDate(RESUME_COLUMNS.DATE.ordinal()+1));
                resume.setTotal(rs.getInt(RESUME_COLUMNS.TOTAL.ordinal()+1));
                resume.setPrintColor(rs.getInt(RESUME_COLUMNS.COLOR_PRINT.ordinal()+1));
                resume.setPrintBW(rs.getInt(RESUME_COLUMNS.BW_PRINT.ordinal()+1));
                resume.setCopyColor(rs.getInt(RESUME_COLUMNS.COLOR_COPY.ordinal()+1));
                resume.setCopyBW(rs.getInt(RESUME_COLUMNS.BW_COPY.ordinal()+1));
                resume.setScanSend(rs.getInt(RESUME_COLUMNS.SCAN_SENT.ordinal()+1));
                resume.setScanStored(rs.getInt(RESUME_COLUMNS.SCAN_STORED.ordinal()+1));
                resume.setFaxSend(rs.getInt(RESUME_COLUMNS.FAX_SENT.ordinal()+1));
                resume.setFaxReceived(rs.getInt(RESUME_COLUMNS.FAX_RECEIVED.ordinal()+1));
                
                if (resumeType.equals(Resume.resumeType.CC_RESUME)){
                    resume.setUser(new User("", id, rs.getString(11),""));
                    resume.setCompany(rs.getString(12));
                }else{
                    resume.setCompany(rs.getString(11));
                }
            }

            rs.close();
            pst.close();

            return resume;
        }
        catch (SQLException ex)
        {
            loggerDBM.error("getLastResumeInserted() : " + pst);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
            return null;
        }
    }

    private Resume getDateResume(String id, Date resumeDate, Resume.resumeType resumeType)
    {
        String query = "";
        Resume resume = null;
        PreparedStatement pst = null;
        try
        {
            switch (resumeType)
            {
                case USER_RESUME:
                    query = QUERY_GET_RESUME_USER_dbUSER_COUNTERS;
                    break;

                case MFP_RESUME:
                    query = QUERY_GET_RESUME_MFP_dbMFP_COUNTERS;
                    break;

                case DESCONOCIDO_MFP_RESUME:
                    query = QUERY_GET_RESUME_MFP_dbDESCONOCIDO_MFP_COUNTERS;
                    break;

                case CC_RESUME:
                    query = QUERY_GET_RESUME_CC_dbCC_COUNTERS;
                    break;
            }

            if (query.isEmpty())
                return null;

            //Insert resume into the database
            pst = getConnection().prepareStatement(query);
            pst.setString(1, id);
            pst.setDate(2, new java.sql.Date(resumeDate.getTime()));

            ResultSet rs = pst.executeQuery();

            if (rs.next())
            {
                resume = new Resume(id, resumeDate);
                resume.setTotal(rs.getInt(RESUME_COLUMNS.TOTAL.ordinal()+1));
                resume.setPrintColor(rs.getInt(RESUME_COLUMNS.COLOR_PRINT.ordinal()+1));
                resume.setPrintBW(rs.getInt(RESUME_COLUMNS.BW_PRINT.ordinal()+1));
                resume.setCopyColor(rs.getInt(RESUME_COLUMNS.COLOR_COPY.ordinal()+1));
                resume.setCopyBW(rs.getInt(RESUME_COLUMNS.BW_COPY.ordinal()+1));
                resume.setScanSend(rs.getInt(RESUME_COLUMNS.SCAN_SENT.ordinal()+1));
                resume.setScanStored(rs.getInt(RESUME_COLUMNS.SCAN_STORED.ordinal()+1));
                resume.setFaxSend(rs.getInt(RESUME_COLUMNS.FAX_SENT.ordinal()+1));
                resume.setFaxReceived(rs.getInt(RESUME_COLUMNS.FAX_RECEIVED.ordinal()+1));

                if (resumeType.equals(Resume.resumeType.CC_RESUME)){
                    resume.setUser(new User("", id, rs.getString(10),""));
                    resume.setCompany(rs.getString(11));
                }else{
                    resume.setCompany(rs.getString(10));
                }
                
            }
            rs.close();
            pst.close();

            return resume;
        }
        catch (SQLException ex)
        {
            System.out.println("getDateResume() : " + pst);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
            return null;
        }
    }

    //** NUEVO **//
    private Resume getDateResume(String username, String mfp, Date resumeDate)
    {
        String query = QUERY_GET_RESUME_USER_MFP_dbUSER_MFP_COUNTERS;
        Resume resume = null;
        PreparedStatement pst = null;
        try
        {
            if (query.isEmpty())
                return null;

            //Insert resume into the database
            pst = getConnection().prepareStatement(query);
            pst.setString(1, username);
            pst.setString(2, mfp);
            pst.setDate(3, new java.sql.Date(resumeDate.getTime()));

            ResultSet rs = pst.executeQuery();

            if (rs.next())
            {
                resume = new Resume(mfp, resumeDate);
                resume.setTotal(rs.getInt(RESUME_COLUMNS.TOTAL.ordinal()+1));
                resume.setPrintColor(rs.getInt(RESUME_COLUMNS.COLOR_PRINT.ordinal()+1));
                resume.setPrintBW(rs.getInt(RESUME_COLUMNS.BW_PRINT.ordinal()+1));
                resume.setCopyColor(rs.getInt(RESUME_COLUMNS.COLOR_COPY.ordinal()+1));
                resume.setCopyBW(rs.getInt(RESUME_COLUMNS.BW_COPY.ordinal()+1));
                resume.setScanSend(rs.getInt(RESUME_COLUMNS.SCAN_SENT.ordinal()+1));
                resume.setScanStored(rs.getInt(RESUME_COLUMNS.SCAN_STORED.ordinal()+1));
                resume.setFaxSend(rs.getInt(RESUME_COLUMNS.FAX_SENT.ordinal()+1));
                resume.setFaxReceived(rs.getInt(RESUME_COLUMNS.FAX_RECEIVED.ordinal()+1));
                resume.setCompany(rs.getString(10));
            }
            rs.close();
            pst.close();
            
            return resume;
        }
        catch (SQLException ex)
        {
            System.out.println("getDateResume() : " + pst);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
            return null;
        }
    }

    private Date getExecutingDate() throws ParseException
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = dateFormat.format(new Date());
        return dateFormat.parse(dateString);
    }
    
    public Date getNextDay(Date date) throws ParseException
    {
        cal.setTime(date);
        cal.add(Calendar.DATE, 1);
        return (new Date(cal.getTimeInMillis()));
    }

    public Date getPreviousDay(Date date) throws ParseException
    {
        cal.setTime(date);
        cal.add(Calendar.DATE, -1);
        return (new Date(cal.getTimeInMillis()));
    }
    
    public boolean checkKnownUserCounters(Date today) {
        
        String query = QUERY_GET_SUM_USER_COUNTERS_dbUSER_COUNTERS;
        PreparedStatement pst = null;
        int iUser_Counter = 0, iMFP_Counter = 0, iGeneral_Counter = 0;
        
        try {
            //*** query for Total User_counters
            pst = getConnection().prepareStatement(query);
            pst.setDate(1, new java.sql.Date(today.getTime()));
                
            ResultSet rs = pst.executeQuery();

            if (rs.next())
            {
                iUser_Counter = rs.getInt(1);
            }
            
            rs.close();
            pst.close();
            
            // query for Total MFP_counters
            query = QUERY_GET_SUM_MFP_COUNTERS_dbMFP_COUNTERS;
            pst = getConnection().prepareStatement(query);
            pst.setDate(1, new java.sql.Date(today.getTime()));
                
            ResultSet rs2 = pst.executeQuery();

            if (rs2.next())
            {
                iMFP_Counter = rs2.getInt(1);
            }
            
            rs2.close();
            pst.close();
            
            // query for Total General_counters
            query = QUERY_GET_SUM_GENERAL_COUNTERS_dbGENERAL;
            pst = getConnection().prepareStatement(query);
            pst.setDate(1, new java.sql.Date(today.getTime()));
                
            ResultSet rs3 = pst.executeQuery();

            if (rs3.next())
            {
                iGeneral_Counter = rs3.getInt(1);
            }
            
            rs3.close();
            pst.close();
            
            //CHECKING
            if (iUser_Counter == iMFP_Counter){
                if (iUser_Counter == iGeneral_Counter){
                    return true;
                }else{
                    System.out.println("checkKnownUserCounters() iUser_Counter: " + iUser_Counter +" != iGeneral_Counter: " + iGeneral_Counter + "  iMFP_Counter: " + iMFP_Counter);
                    logger.info("checkKnownUserCounters() iUser_Counter: " + iUser_Counter +" != iGeneral_Counter: " + iGeneral_Counter + "  iMFP_Counter: " + iMFP_Counter);
                    return false;
                }
            }else{
                System.out.println("checkKnownUserCounters() iUser_Counter: " + iUser_Counter +" != iMFP_Counter: " + iMFP_Counter + "  iGeneral_Counter: " + iGeneral_Counter);
                logger.info("checkKnownUserCounters() iUser_Counter: " + iUser_Counter +" != iMFP_Counter: " + iMFP_Counter + "  iGeneral_Counter: " + iGeneral_Counter);
                return false;
            }
            
        } catch (SQLException ex) {
            System.out.println("checkKnownUserCounters() : " + pst);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
            return false;
        }
    }

    public boolean checkUnknownUserCounters(Date today) {
        
        String query = QUERY_GET_SUM_UNKNOWN_USER_COUNTERS_dbUSER_COUNTERS;
        PreparedStatement pst = null;
        int iUser_Counter = 0, iMFP_Counter = 0, iGeneral_Counter = 0;
        
        try {
            //*** query for Total User_counters
            pst = getConnection().prepareStatement(query);
            pst.setDate(1, new java.sql.Date(today.getTime()));
                
            ResultSet rs = pst.executeQuery();

            if (rs.next())
            {
                iUser_Counter = rs.getInt(1);
            }
            
            rs.close();
            pst.close();
            
            // query for Total MFP_counters
            query = QUERY_GET_SUM_UNKNOWN_MFP_COUNTERS_dbDESCONOCIDO_MFP_COUNTERS;
            pst = getConnection().prepareStatement(query);
            pst.setDate(1, new java.sql.Date(today.getTime()));
                
            ResultSet rs2 = pst.executeQuery();

            if (rs2.next())
            {
                iMFP_Counter = rs2.getInt(1);
            }
            
            rs2.close();
            pst.close();
            
            // query for Total General_counters
            query = QUERY_GET_SUM_UNKNOWN_GENERAL_COUNTERS_dbGENERAL;
            pst = getConnection().prepareStatement(query);
            pst.setDate(1, new java.sql.Date(today.getTime()));
                
            ResultSet rs3 = pst.executeQuery();

            if (rs3.next())
            {
                iGeneral_Counter = rs3.getInt(1);
            }
            
            rs3.close();
            pst.close();
            
            //CHECKING
            if (iUser_Counter == iMFP_Counter){
                if (iUser_Counter == iGeneral_Counter){
                    return true;
                }else{
                    System.out.println("checkUnknownUserCounters() iUser_Counter: " + iUser_Counter +" != iGeneral_Counter: " + iGeneral_Counter + " iMFP_Counter: " + iMFP_Counter);
                    logger.info("checkUnknownUserCounters() iUser_Counter: " + iUser_Counter +" != iGeneral_Counter: " + iGeneral_Counter + " iMFP_Counter: " + iMFP_Counter);
                    return false;
                }
            }else{
                System.out.println("checkUnknownUserCounters() iUser_Counter: " + iUser_Counter +" != iMFP_Counter: " + iMFP_Counter + "  iGeneral_Counter: " + iGeneral_Counter);
                logger.info("checkUnknownUserCounters() iUser_Counter: " + iUser_Counter +" != iMFP_Counter: " + iMFP_Counter + "  iGeneral_Counter: " + iGeneral_Counter);
                return false;
            }
            
        } catch (SQLException ex) {
            System.out.println("checkUnknownUserCounters() : " + pst);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
            return false;
        }
    }
    
    public boolean insertPausedStorageJob(String jobid, String username, String mfpserial, Date datetime, String iphost) {
        String query = "";
        PreparedStatement pst = null;
        try 
        {
            query = QUERY_INSERT_JOB_dbSTOREDJOBS;

            if (query.isEmpty())
                return false;
            
            //Insert resume into the database
            pst = getConnection().prepareStatement(query);
            pst.setString(1, jobid);
            pst.setString(2, username);
            pst.setString(3, mfpserial);
            pst.setDate(4, new java.sql.Date(datetime.getTime()));
            pst.setString(5, iphost);
            
            int rows = pst.executeUpdate();
            pst.close();
            return (rows==1);
        } 
        catch (SQLException ex) 
        {
            loggerDBM.error("insertPausedStorageJob() : " + pst);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
            return false;
        }
    }
    
    public String[] getPausedStorageJob(String jobid, String mfpserial){
        String[] strReturn = new String[2];
        String username = "";
        String iphost = "";

        try
        {
            PreparedStatement pst = getConnection().prepareStatement(QUERY_GET_JOB_dbSTOREDJOBS);
            pst.setString(1, jobid);
            pst.setString(2, mfpserial);
            ResultSet rs = pst.executeQuery();

            if(rs.next()){
                strReturn[0] = rs.getString(1);
                strReturn[1] = rs.getString(2);
//                if (username.isEmpty())
//                    username = iphost;
            }else{
                strReturn[0] = null;
            }
            
            rs.close();
        }
        catch (SQLException ex)
        {
            loggerDBM.error("getPausedStorageJob() : " + QUERY_GET_JOB_dbSTOREDJOBS);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
        }
        finally
        {
            return strReturn;
        }
    }
    
    public boolean deletePausedStorageJob(String jobid, String mfpserial){
        PreparedStatement pst;
        try
        {
            pst = getConnection().prepareStatement(QUERY_DELETE_JOB_dbSTOREDJOBS);
            pst.setString(1, jobid);
            pst.setString(2, mfpserial);
            
            int rows = pst.executeUpdate();
            pst.close();
            return (rows==1);
        }
        catch (SQLException ex)
        {
            loggerDBM.error("deletePausedStorageJob() : " + QUERY_DELETE_JOB_dbSTOREDJOBS);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
            return false;
        }
    }
    
    public boolean deletePausedStorageJobs_PerDate(Date date){
        PreparedStatement pst;
        try
        {
            pst = getConnection().prepareStatement(QUERY_DELETE_JOBS_PERDATE_dbSTOREDJOBS);
            pst.setDate(1, new java.sql.Date(date.getTime()));
            
            int rows = pst.executeUpdate();
            pst.close();
            return (rows==1);
        }
        catch (SQLException ex)
        {
            loggerDBM.error("deletePausedStorageJobs_PerDate() : " + QUERY_DELETE_JOBS_PERDATE_dbSTOREDJOBS);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
            return false;
        }
    }
    
    public List<Printer> getSlnxMfpsFromAccountingDB()
    {
        List<Printer> printers = null;
        PreparedStatement pst;
        try
        {
            printers = new ArrayList<Printer>();
            
            pst = getConnection().prepareStatement(QUERY_GET_SLNX_MFPS_FROM_ACCOUNTING_DB);
            
            ResultSet rs = pst.executeQuery();

            while(rs.next()){
                
                Printer printer = new Printer();
                
                try
                {
                    printer.setMfpSerial(rs.getString(1));
                    printer.setModelType(rs.getString(2));
                    printer.setIpAddress(rs.getString(3));
                    printer.setStatus(rs.getString(4));
                    printer.setLastColorCounter(Integer.parseInt(rs.getString(5)));
                    printer.setLastBWCounter(Integer.parseInt(rs.getString(6)));
                    printer.setLocation(rs.getString(7));
                    printer.setZone(rs.getString(8));
                    printer.setHostname(rs.getString(9));
                    printer.setCompany(rs.getString(10));
                    printer.setIsSLNXMFP(true);
                    
                    printers.add(printer);
                }
                catch (IllegalArgumentException iae)
                {
                    logger.error("getSlnxMfpsFromAccountingDB() : getting SLNX MFPS from Accounting DB: ", iae);
                    System.out.println("ERROR: getSlnxMfpsFromAccountingDB() : getting SLNX MFPS from Accounting DB: " + iae);
                    continue;   //  Pasamos a la siguiente MFP
                }
            }  
            rs.close();
            
            if (printers.isEmpty()) return null;
            
            return printers;
        }
        catch (SQLException ex)
        {
            loggerDBM.error("getSlnxMfpsFromAccountingDB() : " + QUERY_GET_SLNX_MFPS_FROM_ACCOUNTING_DB);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
            return null;
        }
        
        
    }
    
//    public String[] getLastCountersValues(String mfpSerial) {
//        
//        String[] strReturn = new String[12];    //  table MFP_COUNTERS: MFPSERIAL,TOTAL,PRINTCOLOR,PRINTBW,COPYCOLOR,COPYBW,SCANSEND,SCANSTORED,FAXSEND,FAXRECEIVED,DATE,COMPANYNAME
//        PreparedStatement pst;
//        try
//        {
//            pst = getConnection().prepareStatement(QUERY_GET_LAST_COUNTERS_PER_MFP_SERIAL);
//            pst.setString(1, mfpSerial);
//            
//            ResultSet rs = pst.executeQuery();
//
//            if(rs.next()){
//                strReturn[0] = rs.getString(1);
//                strReturn[1] = rs.getString(2);
//                strReturn[2] = rs.getString(3);
//                strReturn[3] = rs.getString(4);
//                strReturn[4] = rs.getString(5);
//                strReturn[5] = rs.getString(6);
//                strReturn[6] = rs.getString(7);
//                strReturn[7] = rs.getString(8);
//                strReturn[8] = rs.getString(9);
//                strReturn[9] = rs.getString(10);
//                strReturn[10] = rs.getString(11);
//                strReturn[11] = rs.getString(12);
//                
//            }else{
//                strReturn[0] = null;
//            }
//            rs.close();
//            
//            return strReturn;
//        }
//        catch (SQLException ex)
//        {
//            loggerDBM.error("getLastCountersValues() : " + QUERY_GET_LAST_COUNTERS_PER_MFP_SERIAL);
//            loggerDBM.error(ex.getMessage());
//            loggerDBM.error(ex.getStackTrace());
//            strReturn[0] = null;
//            return strReturn;
//        }
//    }
    public Resume getLastMFPResume(String mfpSerial) {
        
        PreparedStatement pst;
        Resume resume = null;
        try
        {
            pst = getConnection().prepareStatement(QUERY_GET_LAST_COUNTERS_PER_MFP_SERIAL);
            pst.setString(1, mfpSerial);
            
            ResultSet rs = pst.executeQuery();

            if(rs.next()){
                resume = new Resume(mfpSerial, java.sql.Date.valueOf(rs.getString(11)));
                resume.setTotal(Integer.parseInt(rs.getString(2)));
                resume.setPrintColor(Integer.parseInt(rs.getString(3)));
                resume.setPrintBW(Integer.parseInt(rs.getString(4)));
                resume.setCopyColor(Integer.parseInt(rs.getString(5)));
                resume.setCopyBW(Integer.parseInt(rs.getString(6)));
                resume.setScanSend(Integer.parseInt(rs.getString(7)));
                resume.setScanStored(Integer.parseInt(rs.getString(8)));
                resume.setFaxSend(Integer.parseInt(rs.getString(9)));
                resume.setFaxReceived(Integer.parseInt(rs.getString(10)));
                resume.setCompany(rs.getString(12));
                
            }else{
                return null;
            }
            rs.close();
            
            return resume;
        }
        catch (SQLException ex)
        {
            loggerDBM.error("getLastCountersValues() : " + QUERY_GET_LAST_COUNTERS_PER_MFP_SERIAL);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
            return null;
        }
    }
    
    public Resume getLastUserResume(String userLogin)
    {
        PreparedStatement pst;
        Resume resume = null;
        try
        {
            pst = getConnection().prepareStatement(QUERY_GET_LAST_COUNTERS_PER_USER);
            pst.setString(1, userLogin);
            
            ResultSet rs = pst.executeQuery();

            if(rs.next()){
                resume = new Resume(userLogin, java.sql.Date.valueOf(rs.getString(11)));
                resume.setTotal(Integer.parseInt(rs.getString(2)));
                resume.setPrintColor(Integer.parseInt(rs.getString(3)));
                resume.setPrintBW(Integer.parseInt(rs.getString(4)));
                resume.setCopyColor(Integer.parseInt(rs.getString(5)));
                resume.setCopyBW(Integer.parseInt(rs.getString(6)));
                resume.setScanSend(Integer.parseInt(rs.getString(7)));
                resume.setScanStored(Integer.parseInt(rs.getString(8)));
                resume.setFaxSend(Integer.parseInt(rs.getString(9)));
                resume.setFaxReceived(Integer.parseInt(rs.getString(10)));
                resume.setCompany(rs.getString(12));
                
            }else{
                return null;
            }
            rs.close();
            
            return resume;
        }
        catch (SQLException ex)
        {
            loggerDBM.error("getLastUserResume() : " + QUERY_GET_LAST_COUNTERS_PER_USER);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
            return null;
        }
    }
    
    public Resume getLastUserMfpResume(String userLogin, String mfpSerial)
    {
        PreparedStatement pst;
        Resume resume = null;
        try
        {
            pst = getConnection().prepareStatement(QUERY_GET_LAST_COUNTERS_PER_USER_MFP);
            pst.setString(1, userLogin);
            pst.setString(2, mfpSerial);
            
            ResultSet rs = pst.executeQuery();

            if(rs.next()){
                resume = new Resume(userLogin, java.sql.Date.valueOf(rs.getString(12)));
                resume.setTotal(Integer.parseInt(rs.getString(3)));
                resume.setPrintColor(Integer.parseInt(rs.getString(4)));
                resume.setPrintBW(Integer.parseInt(rs.getString(5)));
                resume.setCopyColor(Integer.parseInt(rs.getString(6)));
                resume.setCopyBW(Integer.parseInt(rs.getString(7)));
                resume.setScanSend(Integer.parseInt(rs.getString(8)));
                resume.setScanStored(Integer.parseInt(rs.getString(9)));
                resume.setFaxSend(Integer.parseInt(rs.getString(10)));
                resume.setFaxReceived(Integer.parseInt(rs.getString(11)));
                resume.setCompany(rs.getString(13));
                
            }else{
                return null;
            }
            rs.close();
            
            return resume;
        }
        catch (SQLException ex)
        {
            loggerDBM.error("getLastUserMfpResume() : " + QUERY_GET_LAST_COUNTERS_PER_USER_MFP);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
            return null;
        }
    }
    
    public Resume getLastCostCenterResume(String costCenter)
    {
        PreparedStatement pst;
        Resume resume = null;
        User user = null;
        try
        {
            pst = getConnection().prepareStatement(QUERY_GET_LAST_COUNTERS_PER_COST_CENTER);
            pst.setString(1, costCenter);
            
            ResultSet rs = pst.executeQuery();

            if(rs.next()){
                resume = new Resume(rs.getString(1), java.sql.Date.valueOf(rs.getString(12)));
                resume.setTotal(Integer.parseInt(rs.getString(3)));
                resume.setPrintColor(Integer.parseInt(rs.getString(4)));
                resume.setPrintBW(Integer.parseInt(rs.getString(5)));
                resume.setCopyColor(Integer.parseInt(rs.getString(6)));
                resume.setCopyBW(Integer.parseInt(rs.getString(7)));
                resume.setScanSend(Integer.parseInt(rs.getString(8)));
                resume.setScanStored(Integer.parseInt(rs.getString(9)));
                resume.setFaxSend(Integer.parseInt(rs.getString(10)));
                resume.setFaxReceived(Integer.parseInt(rs.getString(11)));
                resume.setCompany(rs.getString(13));
//                user = new User("", "", rs.getString(2), "");   //  Recuperamos en un User el nombre del CC
                user = new User("", "", costCenter, "");   //  Recuperamos en un User el nombre del CC
                resume.setUser(user);
                
            }else{
                return null;
            }
            rs.close();
            
            return resume;
        }
        catch (SQLException ex)
        {
            loggerDBM.error("getLastCostCenterResume() : " + QUERY_GET_LAST_COUNTERS_PER_COST_CENTER);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
            return null;
        }
    }
    
    public String getLastGeneralDate()
    {
        PreparedStatement pst;
        String lastDate = "";
        try
        {
            pst = getConnection().prepareStatement(QUERY_GET_LAST_GENERAL_DATE);
            ResultSet rs = pst.executeQuery();

            if(rs.next()){
                lastDate = rs.getString(1);
            }
            else
            {
                return "";
            }
            rs.close();
            
            return lastDate;
        }
        catch (SQLException ex)
        {
            loggerDBM.error("getLastGeneralCounterID() : " + QUERY_GET_LAST_GENERAL_DATE);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
            return null;
        }
    }
    
    // Queries para Banco Compartamos
    
    public List getUsersAccumulated(String filter, Date dateStart, Date dateEnd, String type, String sourceType, String reportType, String viewType) {
        List usersAccumulated = new ArrayList();
        
        PreparedStatement query = null;

        try
        {
            query = getQueryStringInformes(type, viewType);
                        
            if (query != null)
            {

                query.setString(1, reportType);

                Calendar calendar = Calendar.getInstance();
                calendar.setTime(dateEnd);
                calendar.add(Calendar.DATE, 1);
                
                if(type.equalsIgnoreCase("hojas"))
                {
                    query.setString(2, filter);
                    query.setDate(3, new java.sql.Date(dateStart.getTime()));
                    query.setDate(4, new java.sql.Date(calendar.getTime().getTime()));
                }
                else
                {
                    query.setString(2, sourceType);
                    query.setString(3, filter);
                    query.setDate(4, new java.sql.Date(dateStart.getTime()));
                    query.setDate(5, new java.sql.Date(calendar.getTime().getTime()));
                
                }
                
                ResultSet rs = query.executeQuery();

                if(rs.next())
                {
                    Accumulated accumulated = new Accumulated();
                    accumulated.setFilterRow(filter);

                    do{
                        Accumulated.ContadorDocumentos registro = new Accumulated.ContadorDocumentos();
                        registro.setDate(rs.getTimestamp(1));
                        registro.setPages(rs.getInt(3));

                        if(accumulated.getListaRegistros().contains(registro))
                        {
                            int position = accumulated.getListaRegistros().indexOf(registro);
                            Accumulated.ContadorDocumentos reg = accumulated.getListaRegistros().get(position);
                            reg.setPages(reg.getPages() + registro.getPages());
                        }
                        else
                            accumulated.getListaRegistros().add(registro);

                    }while(rs.next());
                        
                    usersAccumulated.add(accumulated);
                }
                
                rs.close();
                query.close();
            }
        }
        catch (Exception ex)
        {
            loggerDBM.error("getUserMFPs() : " + query);
            loggerDBM.error(ex.getMessage());
            loggerDBM.error(ex.getStackTrace());
        }
        return usersAccumulated;
    }
    
    private PreparedStatement getQueryStringInformes(String type, String viewType) throws SQLException
    {
        PreparedStatement query = null;
        
        if(viewType.equalsIgnoreCase("Usuario"))  
            if(type.equalsIgnoreCase("hojas"))
                query = getConnection().prepareStatement(QUERY_GET_USER_SHEET);
            else
                query = getConnection().prepareStatement(QUERY_GET_USER_DOCS);
        else if (viewType.equalsIgnoreCase("Oficina"))
            if(type.equalsIgnoreCase("hojas"))
                query = getConnection().prepareStatement(QUERY_GET_ZONE_SHEET);
            else
                query = getConnection().prepareStatement(QUERY_GET_ZONE_DOCS);
        else if(viewType.equalsIgnoreCase("Region"))
            if(type.equalsIgnoreCase("hojas"))
                query = getConnection().prepareStatement(QUERY_GET_LOCATION_SHEET);
            else
                query = getConnection().prepareStatement(QUERY_GET_LOCATION_DOCS);
        
        return query;
    }
    
}
