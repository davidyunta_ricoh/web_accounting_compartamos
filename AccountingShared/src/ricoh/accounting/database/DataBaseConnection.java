/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ricoh.accounting.database;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import org.apache.log4j.Logger;

/**
 *
 * @author alexis.hidalgo
 */
public class DataBaseConnection
{
    private Connection con = null;
    protected Statement st = null;

    private String DB_HOSTNAME = "";
    private String DB_INSTANCE = "";
    private String DB_USER = "";
    private String DB_PASSWORD = "";
    private String DB_NAME = "";
    private String DB_PORT = "";
    private String DB_DRIVER = "";
    private String DB_CONNECTION = "";

    private String connectionURL = "";

    public static Logger logger = Logger.getLogger(DataBaseConnection.class.getName());

    public DataBaseConnection(String propFile) throws FileNotFoundException, IOException
    {
        Properties prop = new Properties();
        prop.load(new FileInputStream(propFile));

        DB_HOSTNAME = prop.getProperty("DataBase.Host");
        DB_USER = prop.getProperty("DataBase.User");
        DB_PASSWORD = prop.getProperty("DataBase.Password");
        DB_NAME = prop.getProperty("DataBase.Name");
        DB_PORT = prop.getProperty("DataBase.Port");
        DB_DRIVER = prop.getProperty("DataBase.Driver");
        DB_CONNECTION = prop.getProperty("DataBase.Connection");

        this.createPostgreSQLConnectionURL();
    }

    public DataBaseConnection(String hostName, String userName, String password, String dbName, String dbPort, String dbDriver, String dbConnection)
    {
        DB_HOSTNAME = hostName;
        DB_USER = userName;
        DB_PASSWORD = password;
        DB_NAME = dbName;
        DB_PORT = dbPort;
        DB_DRIVER = dbDriver;
        DB_CONNECTION = dbConnection;
    }

    public DataBaseConnection(String hostName, String userName, String password, String dbName, String dbPort, String dbDriver, String dbConnection, String dbInstance)
    {
        DB_HOSTNAME = hostName;
        DB_INSTANCE = dbInstance;
        DB_USER = userName;
        DB_PASSWORD = password;
        DB_NAME = dbName;
        DB_PORT = dbPort;
        DB_DRIVER = dbDriver;
        DB_CONNECTION = dbConnection;
    }

    public void createPostgreSQLConnectionURL()
    {
        this.connectionURL = DB_CONNECTION + "//" + DB_HOSTNAME+ "/" + DB_NAME;
        if (!DB_PORT.isEmpty())
            this.connectionURL = this.connectionURL + ":" + DB_PORT;
    }

    public void createSQLServerConnectionURL()
    {
        this.connectionURL = DB_CONNECTION + "//" + DB_HOSTNAME;
        if (!DB_PORT.isEmpty())
            this.connectionURL = this.connectionURL + ":" + DB_PORT;
        if (DB_INSTANCE.isEmpty())
            this.connectionURL = this.connectionURL + ";database=" + DB_NAME + ";";
        else
            this.connectionURL = this.connectionURL + ";instanceName=" + DB_INSTANCE + ";database=" + DB_NAME + ";";
    }

    public boolean connect()
    {
        logger.info("Coneccting to DataBase " + this.connectionURL);

        try
        {
            Class.forName(DB_DRIVER);
            con = DriverManager.getConnection(this.connectionURL, DB_USER, DB_PASSWORD); //usuario+contraseña

            if (getConnection() != null)
            {
                st = getConnection().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
                logger.info("CONNECTION to DataBase OK!");
                return true;
            }
            else
            {
                logger.error("CONNECTION to DataBase FAIL!");
                return false;
            }
        }
        catch (SQLException ex)
        {
            logger.error(ex.getMessage());
            logger.error(ex.getStackTrace());
            return false;
        }
        catch (ClassNotFoundException ex)
        {
            logger.error(ex.getMessage());
            logger.error(ex.getStackTrace());
            return false;
        }
    }

    public boolean disconnect()
    {
        logger.info("Disconnecting from DataBase ");
        try
        {
            /**** Cerramos la conexión ****/
            if (getConnection() != null)
                getConnection().close();

            if (st != null)
                st.close();

            return true;
        }
        catch (SQLException ex)
        {
            logger.error(ex.getMessage());
            logger.error(ex.getStackTrace());
            return false;
        }
    }

    public boolean isConnected()
    {
        try
        {
            if (getConnection() != null)
                return !con.isClosed();

            else
                return false;
        }
        catch (SQLException ex)
        {
            logger.error(ex.getMessage());
            logger.error(ex.getStackTrace());
            return false;
        }

    }

    /**
     * @return the con
     */
    public Connection getConnection() {
        return con;
    }
}
