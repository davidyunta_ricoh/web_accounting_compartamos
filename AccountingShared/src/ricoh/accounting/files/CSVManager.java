/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ricoh.accounting.files;

import com.csvreader.CsvReader;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import org.apache.log4j.Logger;
import ricoh.accounting.interfaces.IExtractorManager;
import ricoh.accounting.interfaces.IFileManager;
import ricoh.accounting.objects.Action;
import ricoh.accounting.objects.Printer;
import ricoh.accounting.objects.Resume;

/**
 *
 * @author alexis.hidalgo
 */
public class CSVManager implements IFileManager
{
    private static Map filesHash = new HashMap();
    private Map filesHash_tmp = new HashMap();
    private List<String[]> arraylist = new ArrayList<String[]>();
    
    public static Logger logger = Logger.getLogger(CSVManager.class.getName());

    public synchronized boolean createFile(final String path, final String fileName)
    {
        logger.info("Creating " + fileName + ".csv output file");
        System.out.println("-> Creating " + fileName + ".csv output file");

        try
        {
            if (filesHash.containsKey(fileName))
            {
                logger.error("createFile() : " + fileName + ".csv is already being created. Cannot write the file.");
                return false;
            }
            else
            {
                String filePath;

                if (path.isEmpty())
                    filePath = fileName;
                else
                {
                    File dir = new File(path);
                    if (!dir.exists())
                        if(!dir.mkdirs())
                            return false;
                    
                    if (path.lastIndexOf("/") == (path.length()-1))
                        filePath = path + fileName;
                    else
                        filePath = path + "\\" + fileName;
                }

                FileWriter writer = new FileWriter(filePath + ".csv");
                System.out.println("createFile(): " + filePath + ".csv" + " Added to fileshash" );
                filesHash.put(fileName, writer);
                return true;
            }
        }
        catch (IOException ex)
        {
            System.out.println("createFile(): " + ex.getMessage());
            logger.error(ex.getMessage());
            logger.error(ex.getStackTrace());
            return false;
        }
    }

    public synchronized void appendText2File(final String fileName, final String text)
    {
        if (filesHash.containsKey(fileName))
        {
            try
            {
                FileWriter writer = (FileWriter) filesHash.get(fileName);
                writer.append(text);
                filesHash.put(fileName, writer);
            }
            catch (IOException ex)
            {
                logger.error(ex.getMessage());
                logger.error(ex.getStackTrace());
            }
        }
        else
            logger.error("appendText2File() : " + fileName + ".csv does not exist. Cannot write to file.");
    }

    public synchronized void appendResume2File(final String fileName, final Resume resume)
    {
        String entry;
        SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy");
        SimpleDateFormat monthFormat = new SimpleDateFormat("MM");


        //logger.debug("appendResume2File() : ResumeDate=" + resume.getDate());
        if (filesHash.containsKey(fileName))
        {
            try
            {
                FileWriter writer = (FileWriter) filesHash.get(fileName);

                if (fileName.equals(IExtractorManager.RESUME_USER_PRINTERS_FILENAME))
                {
                    if (resume.getUser().getUsername().isEmpty())
                        entry = resume.getPrinter().getHostname() + ";";
                    else
                        entry = resume.getUser().getUsername() + ";";
                    
                    //entry = entry + resume.getUser().getFullname() + ";";
                    //entry = entry + resume.getUser().getDepartment() + ";";
                    //entry = entry + resume.getUser().getCostCenter() + ";";
                    entry = entry + resume.getPrinter().getHostname() + ";";

                    if (resume.getPrinter().getMfpSerial().isEmpty())
                        entry = entry + ";";
                    else
                        entry = entry + resume.getPrinter().getMfpSerial() + ";";

                    entry = entry + resume.getPrinter().getLocation() + ";";
                    entry = entry + resume.getPrinter().getZone() + ";";
                }
                else
                {
                    if (resume.getId().isEmpty())
                        entry = ";";
                    else
                        entry = resume.getId() + ";";

                    if (fileName.equals(IExtractorManager.RESUME_USERS_FILENAME))
                    {
                        entry = entry + resume.getUser().getFullname() + ";";
                        entry = entry + resume.getUser().getDepartment() + ";";
                        entry = entry + resume.getUser().getCostCenter() + ";";
                    }

                    if (fileName.equals(IExtractorManager.RESUME_PRINTERS_FILENAME))
                    {
                        entry = entry + resume.getPrinter().getHostname() + ";";
                        entry = entry + resume.getPrinter().getLocation() + ";";
                        entry = entry + resume.getPrinter().getZone() + ";";
                    }

                    if (fileName.equals(IExtractorManager.RESUME_CECO_FILENAME))
                    {
                        //logger.info(resume.getId() + "->" + resume.getDate().toString());
                        entry = entry + resume.getUser().getDepartment() + ";";
                    }

                }

                entry = entry + resume.getTotal() + ";"
                              + resume.getPrintColor() + ";"
                              + resume.getPrintBW() + ";"
                              + resume.getCopyColor() + ";"
                              + resume.getCopyBW() + ";"
//                              + resume.getScanSend() + ";"
//                              + resume.getScanStored() + ";"
//                              + resume.getFaxSend() + ";"
//                              + resume.getFaxReceived() + ";"
                              + monthFormat.format(resume.getDate()) + ";"
                              + yearFormat.format(resume.getDate()) + "\n";

                //logger.debug("appendResume2File() : ResumeDateFormat=" + monthFormat.format(resume.getDate()));
                writer.append(entry);
                filesHash.put(fileName, writer);
            }
            catch (IOException ex)
            {
                logger.error(ex.getMessage());
                logger.error(ex.getStackTrace());
            }
        }
        else
            logger.error("appendResume2File() : " + fileName + ".csv does not exist. Cannot write to file.");
    }

    public synchronized void appendAction2File(final String fileName, final Action action, final Action.actionType aType)
    {
        String entry;

        if (filesHash.containsKey(fileName))
        {
            try
            {
                FileWriter writer = (FileWriter) filesHash.get(fileName);

                entry = convertAction2Text(action, aType);

                writer.append(entry);
                filesHash.put(fileName, writer);
            }
            catch (IOException ex)
            {
                logger.error(ex.getMessage());
                logger.error(ex.getStackTrace());
            }
        }
        else
            logger.error("appendAction2File() : " + fileName + ".csv does not exist. Cannot write to file.");
    }
    
    public synchronized boolean closeFile(final String fileName)
    {
        logger.info("Closing " + fileName + "  file");
        System.out.println("-> Closing " + fileName + " file");

        if (filesHash.containsKey(fileName))
        {
            try
            {
                Object obj = filesHash.get(fileName);

                if (FileWriter.class.isInstance(obj))
                {
                    FileWriter writer = (FileWriter) obj;
                    writer.close();

                }
                else if (CsvReader.class.isInstance(obj))
                {
                    CsvReader reader = (CsvReader) obj;
                    reader.close();
                }
                filesHash.remove(fileName);
                return true;
            }
            catch (IOException ex) 
            {
                System.out.println("closeFile(): " + ex.getMessage());
                logger.error(ex.getMessage());
                logger.error(ex.getStackTrace());
                return false;
            }
        }
        else
        {
            System.out.println("closeFile() : " + fileName + " does not exist. Cannot close file.");
            logger.error("closeFile() : " + fileName + " does not exist. Cannot close file.");
            return false;
        }
    }

    public synchronized boolean openFile(final String path, final String fileName, final boolean isJobLog)
    {
        logger.info("Openning " + fileName + " file");
        System.out.println("-> Openning " + fileName + " file");

        try
        {
            if (filesHash.containsKey(fileName))
            {
                logger.error("openFile() : " + fileName + " is already openned.");
                return false;
            }
            else
            {
                String filePath;
                
                if (path.isEmpty())
                    filePath = fileName;
                else
                {
                    if (path.lastIndexOf("/") == (path.length()-1))
                        filePath = path + fileName;
                    else
                        filePath = path + "\\" + fileName;
                }

                CsvReader reader = new CsvReader(filePath);
                if (isJobLog)
                {
                    if (reader.readRecord())
                    {
                        reader.readHeaders();
                        while (reader.getHeaders().length < 2)
                        {
                            reader.readHeaders();
                            reader.skipLine();
                        }
                    }
                    
                    CsvReader reader_tmp = new CsvReader(filePath);                
                    filesHash_tmp.put(fileName, reader_tmp);
                    arraylist = readLines(fileName);
                }
                else
                {
                    reader.setDelimiter(';');

                    if(reader.readHeaders())
                        reader.getHeaders();

                    reader.skipLine();
                }
                

                filesHash.put(fileName, reader);
                return true;
            }
        }
        catch (IOException ex)
        {
            System.out.println("openFile() : " + ex.getMessage());
            logger.error(ex.getMessage());
            logger.error(ex.getStackTrace());
            return false;
        }
    }

    public boolean hasMoreLines(final String fileName)
    {
        if (filesHash.containsKey(fileName))
        {
            try
            {
                CsvReader reader = (CsvReader) filesHash.get(fileName);
                return reader.readRecord();
            }
            catch (IOException ex)
            {
                logger.error(ex.getMessage());
                logger.error(ex.getStackTrace());
                return false;
            }
        }
        else
        {
            logger.error("hasMoreLines() : " + fileName + ".csv is not openned. Cannot read from file.");
            return false;
        }
    }

    public String readAttribute(final String fileName, final String attribute)
    {
        if (filesHash.containsKey(fileName))
        {
            try
            {
                CsvReader reader = (CsvReader) filesHash.get(fileName);
                return reader.get(attribute);
            }
            catch (IOException ex)
            {
                logger.error(ex.getMessage());
                logger.error(ex.getStackTrace());
                return null;
            }
        }
        else
        {
            logger.error("readAttribute() : " + fileName + ".csv is not openned. Cannot read from file.");
            return null;
        }
    }

    public boolean createZIPFile(String inputPath, String outputPath, String zipName, List files)
    {
        byte[] buf = new byte[1024];
        logger.info("Creating " + zipName + " ZIP file on " + outputPath);
        System.out.println("-> Creating " + zipName + " ZIP file on " + outputPath);

        try
        {
            File zipDir = new File(outputPath);
            if (!zipDir.exists())
                if (!zipDir.mkdirs())
                {
                    logger.error("createZIPFile() : Cannot create output directory " + outputPath);
                    return false;
                }

            if (!outputPath.isEmpty())
            {
                if (outputPath.lastIndexOf("/") != (outputPath.length()-1))
                    outputPath = outputPath + "/";
            }
            File zipFile = new File(outputPath + zipName + ".zip");

            if (!inputPath.isEmpty())
            {
                if (inputPath.lastIndexOf("/") != (inputPath.length()-1))
                    inputPath = inputPath + "/";
            }

            ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipFile));
            ListIterator filesIterator = files.listIterator();
            String fileName, filePath;
            while (filesIterator.hasNext())
            {
                fileName = filesIterator.next() + ".csv";
                filePath = inputPath + fileName;
                FileInputStream in = new FileInputStream(filePath);
                out.putNextEntry(new ZipEntry(fileName));

                int len;
                while ((len = in.read(buf)) > 0)
                    out.write(buf, 0, len);

                out.closeEntry();
                in.close();
            }
            out.close();
            return true;
        }
        catch (IOException ex)
        {
            logger.error(ex.getMessage());
            logger.error(ex.getStackTrace());
            return false;
        }
    }

    private String convertAction2Text(final Action action, final Action.actionType aType)
    {
        String text = "";
        
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy");
        SimpleDateFormat monthFormat = new SimpleDateFormat("MM");
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
        
        switch (aType)
        {
                case USER_ACTION:
                    //Username field
                    if (action.getUserName().isEmpty())
                        text = action.getPrinter().getHostname() + ";";
                    else
                        text = action.getUserName() + ";";

                    //Department and Costcenter field
                    if (action.getUser() != null)
                    {
                        text = text + action.getUser().getFullname() + ";";
                        text = text + action.getUser().getDepartment() + ";";
                        text = text + action.getUser().getCostCenter() + ";";
                    }
                    else
                       text = text + ";";

                    //Hostname field
                    if (action.getPrinter() != null)
                        text = text + action.getPrinter().getHostname() + ";";
                    else
                        text = text + ";";

                    //MFP serial field
                    text = text + action.getMFPserial() + ";";

                    //MFP Location and Zone
                    if (action.getPrinter() != null)
                    {
                        text = text + action.getPrinter().getLocation() + ";";
                        text = text + action.getPrinter().getZone() + ";";
                    }
                    else
                        text = text + ";;";

                    break;

                case MFP_ACTION:
                    //MFP serial field
                    text = action.getMFPserial() + ";";

                    //Hostname field
                    if (action.getPrinter() != null)
                        text = text + action.getPrinter().getHostname() + ";";
                    else
                        text = text + ";";

                    //MFP Location and Zone
                    if (action.getPrinter() != null)
                    {
                        text = text + action.getPrinter().getLocation() + ";";
                        text = text + action.getPrinter().getZone() + ";";
                    }
                    else
                        text = text + ";;";

                    //Username field
                    if (action.getUserName().isEmpty())
                        text = text + action.getMFPserial() + ";";
                    else
                        text = text + action.getUserName() + ";";

                    //Department and Costcenter field
                    if (action.getUser() != null)
                    {
                        text = text + action.getUser().getDepartment() + ";";
                        text = text + action.getUser().getCostCenter() + ";";
                    }
                    else
                       text = text + ";";

                    break;
        }

        //Date and Time fields
        if (action.getDate() != null)
            text = text + dateFormat.format(action.getDate()) + ";";
        else
            text = text + ";";

        if (action.getDateTime() != null)
            text = text + timeFormat.format(action.getDateTime()) + ";";
        else
            text = text + ";";

        //Type field
        if(action.getType().equals("printer.print") || action.getType().equals("printer.secret-print") || action.getType().equals("printer.trial-storage-print")
                || action.getType().equals("printer.trial-print") || action.getType().equals("printer.pausedocument-print")
                || action.getType().equals("printer.keepdocument-storage-print") || action.getType().equals("printer.keepdocument-reprint")
                || action.getType().equals("report") || action.getType().equals("report.state") || action.getType().equals("fax.pctranser")
                || action.getType().equals("documentbox.storedocument-print"))
            text = text + "Impresión;";
        else if(action.getType().equals("copy") || action.getType().equals("copy.storage"))
            text = text + "Copia;";
        else if(action.getType().equals("copy.storage") || action.getType().equals("scanner.storage") || action.getType().equals("documentbox.storage")
                || action.getType().equals("scanner.deliver-storage") || action.getType().equals("documentbox.storage-network")
                || action.getType().equals("scanner.linkdeliver-storage"))
            text = text + "Escaneado almacenado;";
        else if(action.getType().equals("scanner.deliver") || action.getType().equals("scanner.deliver-storage") || action.getType().equals("scanner.storedocument-forward")
                || action.getType().equals("scanner.linkdeliver-storage") || action.getType().equals("scanner.storedocument-deliver")
                || action.getType().equals("scanner.storedocument-linkdeliver") || action.getType().equals("documentbox.storedocument-forward"))
            text = text + "Escaneado enviado;";
        else if(action.getType().equals("fax.transfer") || action.getType().equals("fax.pctranser"))
            text = text + "Fax enviado;";
        else if(action.getType().equals("fax.print") || action.getType().equals("fax.receive")
                || action.getType().equals("fax.receive-deliver") || action.getType().equals("fax.receive-storage"))
            text = text + "Fax recibido;";
        else
            text = text + ";";


        //Number of print pages field
        text = text + action.getNumPages() + ";" ;

        //Color Mode field
        if(action.getColorMode().equals("blackandwhite"))
            text = text + "b/n;";
        else if(action.getColorMode().equals("color"))
            text = text + "color;";
        else
            text = text + ";";

        //Page Size field
        if (action.getPageSize().isEmpty())
            text = text + ";";
        else
            text = text + action.getPageSize() + ";";

        //Detail field
        if(action.getType().equals("fax.transfer") || action.getType().equals("fax.pctransfer") || action.getType().equals("scanner.deliver")
                || action.getType().equals("scanner.deliver-storage") || action.getType().equals("scanner.storedocument-forward")
                || action.getType().equals("scanner.linkdeliver-storage") || action.getType().equals("scanner.storedocument-deliver")
                || action.getType().equals("scanner.storedocument-linkdeliver") || action.getType().equals("documentbox.storedocument-forward"))
        {
            if (action.getType().equals("scanner.deliver-storage") || action.getType().equals("scanner.linkdeliver-storage"))
                text = text + "(escaneado almacenado)";
            if (action.getType().equals("documentbox.storedocument-forward"))
                text = text + "(documentbox enviado)";
            if (action.getDestination().isEmpty())
                text = text + "(desconocido);";
            else
                text = text + action.getDestination() + ";";
        }
        else if(action.getType().equals("fax.print") || action.getType().equals("fax.receive")
                || action.getType().equals("fax.receive-deliver") || action.getType().equals("fax.receive-storage"))
        {
            if (action.getSender().isEmpty())
                text = text + "(desconocido);";
            else
                text = text + action.getSender() + ";";
        }
        else if(action.getType().equals("printer.print") || action.getType().equals("printer.secret-print") || action.getType().equals("printer.trial-storage-print")
                || action.getType().equals("printer.trial-print") || action.getType().equals("printer.pausedocument-print")
                || action.getType().equals("printer.keepdocument-storage-print") || action.getType().equals("printer.keepdocument-reprint"))
                text = text + action.getDocuName() + ";";
        else if (action.getType().equals("fax.print"))
            text = text + action.getDocuName() + ";";
        else if (action.getType().equals("report") || action.getType().equals("report.state"))
            text = text + action.getDocuName() + ";";
        else if (action.getType().equals("documentbox.storedocument-print"))
            text = text + action.getDocuName() + ";";
        else if(action.getType().equals("copy"))
            text = text + "(copia);";
        else if(action.getType().equals("copy.storage"))
            text = text + "(copia almacenada);";
        else if (action.getType().equals("scanner.storage"))
            text = text + "(escaneado almacenado);";
        else if (action.getType().equals("documentbox.storage") || action.getType().equals("documentbox.storage-network"))
            text = text + "(documentbox almacenado);";
        else
            text = text + "(desconocido);";

        //Month field
        text = text + monthFormat.format(action.getDate()) + ";";

        //Year field
        text = text + yearFormat.format(action.getDate()) + "\n";

        //Return resulting text
        return text;
    }

    public boolean createFailedMFPsLog(List mfpList)
    {
        Printer p;
        SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyyMMdd_hhmmss");

        if (!mfpList.isEmpty())
        {
            java.sql.Timestamp timeStamp = new Timestamp(new Date().getTime());
            String filePath = "logs/FAILED_MFPS_" + timeStampFormat.format(timeStamp) + ".log";

            logger.info("Creating FAILED_MFPS_" + timeStampFormat.format(timeStamp) + ".log file");
            System.out.println("-> Creating FAILED_MFPS_" + timeStampFormat.format(timeStamp) + ".log file");

            File dir = new File("logs/");
            if (!dir.exists())
                if(!dir.mkdirs())
                {
                    logger.error("createFailedMFPsLog() : Cannot create directory \"logs/\"");
                    return false;
                }

            try
            {
                FileWriter writer = new FileWriter(filePath);
                writer.append("NUMSERIE;IP;MODELO\n");

                for (int i=0; i<mfpList.size(); i++)
                {
                    p = (Printer) mfpList.get(i);
                    writer.append(p.getMfpSerial() + ";");
                    writer.append(p.getIpAddress() + ";");
                    writer.append(p.getModelType() + "\n");
                }

                writer.close();
                return true;
            }
            catch (IOException ex)
            {
                logger.error(ex.getMessage());
                logger.error(ex.getStackTrace());
                return false;
            }
        }
        else
            return true;
    }

    public boolean concatFiles(List fileList, String outputDir, String outputName)
    {
        String line, file;
        File f;
        FileInputStream inputFile;
        BufferedReader buff;

        if (!outputDir.isEmpty())
        {
            if (outputDir.lastIndexOf("/") != (outputDir.length()-1))
                outputDir = outputDir + "/";
        }

        try
        {
            //BufferedWriter out = new BufferedWriter(new FileWriter(outputDir + outputName + ".csv", true));
            if (!createFile(outputDir, outputName))
                return false;

            ListIterator it = fileList.listIterator();
            while (it.hasNext())
            {
                file = outputDir + it.next() + ".csv";
                inputFile = new FileInputStream (file);
                buff  = new BufferedReader(new InputStreamReader(inputFile));
                while((line = buff.readLine())!= null)
                {
                    //out.write(line + "\n");
                    appendText2File(outputName, line + "\n");
                }
                buff.close();
                inputFile.close();

                f = new File(file);
                f.deleteOnExit();
            }
            if (!closeFile(outputName))
                return false;
            else
                return true;
        }
        catch (IOException ex)
        {
            logger.error(ex.getMessage());
            logger.error(ex.getStackTrace());
            return false;
        }
    }
    

    private List<String[]> readLines(String filename) throws IOException  { 
        
        CsvReader reader = (CsvReader) filesHash_tmp.get(filename);
          
        List<String[]> result = new ArrayList<String[]>(); 
        String nextLine; 
        while (reader.readRecord()) {
            result.add(reader.getValues()); 
        } 
        return result; 
    } 
    
    public String getIPHost_StoredJob(String jobid, String mfpserial) { 
                
        String result = "", serial; 
        String[] value;
        Iterator<String[]> it = arraylist.iterator(); 
        
        while (it.hasNext()) {         
            value = it.next();
            if (value.length > 157){
                serial = value[1];
                if (value[157].equalsIgnoreCase(jobid) && serial.equalsIgnoreCase("serialNo:" + mfpserial)){
                    System.out.println("jobid: " + value[157]);
                    System.out.println("iphost: " + value[25]);
                    System.out.println("serial number: " + value[1]);
                    result = value[25];
                    return result;
                }
            }
        }

        return result; 
    } 

}
