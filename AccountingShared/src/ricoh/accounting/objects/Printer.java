/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ricoh.accounting.objects;

import java.io.Serializable;

/**
 *
 * @author alexis.hidalgo
 */
public class Printer implements Serializable
{
    private String mfpSerial = "";
    private String ipAddress = "";
    private String modelType = "";
    private String status = "";
    private int lastColorCounter = 0;
    private int lastBWCounter = 0;
    private Resume countersResume;
    private String location = "";
    private String zone = "";
    private String hostname = "";
    private String company = "";
    private boolean isSLNXMFP = false;

    public static final String ONLINE = "online";
    public static final String OFFLINE = "offline";
    public static final String MFP232 = "aficio sp c232sf";
    public static final String MFP7500 = "aficio mp c7500";

    
    public Printer(String mfpSerial, String ipAddress, String modelType, String status, 
            int lastColorCounter, int lastBWcounter, String location, String zone, String hostname,String company)
    {
        this.mfpSerial = mfpSerial;
        this.ipAddress = ipAddress;
        this.modelType = modelType;
        this.status = status;
        this.lastColorCounter = lastColorCounter;
        this.lastBWCounter = lastBWcounter;
        this.location = location;
        this.zone = zone;
        this.hostname = hostname;
        this.company = company;
        this.isSLNXMFP = false;     //  TODO: for test
        //countersResume = new Resume(mfpSerial, new Date());
    }
    
    
    public Printer(String mfpSerial, String ipAddress, String modelType, String status, 
            int lastColorCounter, int lastBWcounter, String location, String zone, String hostname,String company, boolean isSlnxMfp)
    {
        this.mfpSerial = mfpSerial;
        this.ipAddress = ipAddress;
        this.modelType = modelType;
        this.status = status;
        this.lastColorCounter = lastColorCounter;
        this.lastBWCounter = lastBWcounter;
        this.location = location;
        this.zone = zone;
        this.hostname = hostname;
        this.company = company;
        this.isSLNXMFP = isSlnxMfp;
    }

    public Printer()
    {}

    /**
     * @return the mfpSerial
     */
    public String getMfpSerial() {
        return mfpSerial;
    }

    /**
     * @param mfpSerial the mfpSerial to set
     */
    public void setMfpSerial(String mfpSerial) {
        this.mfpSerial = mfpSerial;
    }

    /**
     * @return the ipAddress
     */
    public String getIpAddress() {
        return ipAddress;
    }

    /**
     * @param ipAddress the ipAddress to set
     */
    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    /**
     * @return the modelType
     */
    public String getModelType() {
        return modelType;
    }

    /**
     * @param modelType the modelType to set
     */
    public void setModelType(String modelType) {
        this.modelType = modelType;
    }

    /**
     * @return the countersResume
     */
    public Resume getCountersResume() {
        return countersResume;
    }

    /**
     * @param countersResume the countersResume to set
     */
    public void setCountersResume(Resume countersResume) {
        this.countersResume = countersResume;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the lastColorCounter
     */
    public int getLastColorCounter() {
        return lastColorCounter;
    }

    /**
     * @param lastColorCounter the lastColorCounter to set
     */
    public void setLastColorCounter(int lastColorCounter) {
        this.lastColorCounter = lastColorCounter;
    }

    /**
     * @return the lastBWCounter
     */
    public int getLastBWCounter() {
        return lastBWCounter;
    }

    /**
     * @param lastBWCounter the lastBWCounter to set
     */
    public void setLastBWCounter(int lastBWCounter) {
        this.lastBWCounter = lastBWCounter;
    }
    
    public boolean isIsSLNXMFP() {
        return isSLNXMFP;
    }

    public void setIsSLNXMFP(boolean isSLNXMFP) {
        this.isSLNXMFP = isSLNXMFP;
    }

    public boolean equals(Printer p)
    {
        boolean bipaddress  = true, bmodeltype = true, blocation = true, bzone = true, bhostname = true;

        if (this.ipAddress != null)
            bipaddress = this.ipAddress.equalsIgnoreCase(p.getIpAddress());
        if (this.modelType != null)
            bmodeltype = this.modelType.equalsIgnoreCase(p.getModelType());
        if (this.location != null)
            blocation = this.location.equalsIgnoreCase(p.getLocation());
        if (this.zone != null)
            bzone = this.zone.equalsIgnoreCase(p.getZone());
        if (this.hostname != null)
            bhostname = this.hostname.equalsIgnoreCase(p.getHostname());
        
        return (bipaddress &&
                bmodeltype &&
                blocation &&
                bzone &&
                bhostname
                );
    }

    /**
     * @return the location
     */
    public String getLocation() {
        return location;
    }

    /**
     * @param location the location to set
     */
    public void setLocation(String location) {
        this.location = location;
    }

    /**
     * @return the zone
     */
    public String getZone() {
        return zone;
    }

    /**
     * @param zone the zone to set
     */
    public void setZone(String zone) {
        this.zone = zone;
    }

    /**
     * @return the hostname
     */
    public String getHostname() {
        return hostname;
    }

    /**
     * @param hostname the hostname to set
     */
    public void setHostname(String hostname) {
        this.hostname = hostname;
    }
    
        /**
     * @return the company
     */
    public String getCompany() {
        return company;
    }

    /**
     * @param hostname the hostname to set
     */
    public void setCompany(String company) {
        this.company = company;
    }
}
