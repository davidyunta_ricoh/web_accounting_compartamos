/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ricoh.accounting.objects;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Class to represent accumulated accounting
 * @author Francisco.Madera
 */
public class Accumulated {
    
    public static class ContadorDocumentos {
        private Date date;
        private int pages = 0;

        public Date getDate() {
            return date;
        }

        public void setDate(Date date) {
            this.date = date;
        }

        public int getPages() {
            return pages;
        }

        public void setPages(int totalPages) {
            this.pages = totalPages;
        }

        @Override
        public boolean equals(Object obj) {
            return ((ContadorDocumentos)obj).getDate().getTime() == this.date.getTime(); //To change body of generated methods, choose Tools | Templates.
        }

    }
    
    private String filterRow;
    List<ContadorDocumentos> listaRegistros;

    public Accumulated() {
        this.listaRegistros = new ArrayList<ContadorDocumentos>();
    }
    
    
    /**
     * @param filterRow the filterRow to set
     */
    public void setFilterRow(String filterRow) {
        this.filterRow = filterRow;
    }
    
    /**
     * @return the filterRow
     */
    public String getFilterRow() {
        return filterRow;
    }

    public List<ContadorDocumentos> getListaRegistros() {
        return listaRegistros;
    }

    public void setListaRegistros(List<ContadorDocumentos> listaRegistros) {
        this.listaRegistros = listaRegistros;
    }

    
    public int getTotalPages() {
        int cont = 0;
          
        for (ContadorDocumentos registro : this.listaRegistros ) 
        {
            cont = cont + registro.getPages();
        }
    
        return cont;
    }
    
    public int getTotalPagesByBate(Date date) {
        int cont = 0;
        ContadorDocumentos tmp = new ContadorDocumentos();
        tmp.setDate(date);
        
        int index = this.getListaRegistros().indexOf(tmp);
        if(index != -1)
           cont = this.getListaRegistros().get(index).getPages();

        return cont;
    }
    

}
