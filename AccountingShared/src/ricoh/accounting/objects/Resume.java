/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ricoh.accounting.objects;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author alexis.hidalgo
 */
public class Resume implements Serializable
{
    private String id;
    private int total = 0;
    private int printColor = 0;
    private int printBW = 0;
    private int copyColor = 0;
    private int copyBW = 0;
    private int scanSend = 0;
    private int scanStored = 0;
    private int faxSend = 0;
    private int faxReceived = 0;
    private Date date;
    private Printer printer;
    private User user;
    private String company;

    public static enum resumeType { USER_RESUME, MFP_RESUME, DESCONOCIDO_MFP_RESUME, USER_MFP_RESUME, CC_RESUME, USER_ACCUMULATED_RESUME };

    public Resume(String id, Date date)
    {
        this.id = id;
        this.date = date;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the total
     */
    public int getTotal() {
        return total;
    }

    /**
     * @param total the total to set
     */
    public void setTotal(int total) {
        this.total = total;
    }

    /**
     * @return the printColor
     */
    public int getPrintColor() {
        return printColor;
    }

    /**
     * @param printColor the printColor to set
     */
    public void setPrintColor(int printColor) {
        this.printColor = printColor;
    }

    /**
     * @return the printBW
     */
    public int getPrintBW() {
        return printBW;
    }

    /**
     * @param printBW the printBW to set
     */
    public void setPrintBW(int printBW) {
        this.printBW = printBW;
    }

    /**
     * @return the copyColor
     */
    public int getCopyColor() {
        return copyColor;
    }

    /**
     * @param copyColor the copyColor to set
     */
    public void setCopyColor(int copyColor) {
        this.copyColor = copyColor;
    }

    /**
     * @return the copyBW
     */
    public int getCopyBW() {
        return copyBW;
    }

    /**
     * @param copyBW the copyBW to set
     */
    public void setCopyBW(int copyBW) {
        this.copyBW = copyBW;
    }

    /**
     * @return the scanSend
     */
    public int getScanSend() {
        return scanSend;
    }

    /**
     * @param scanSend the scanSend to set
     */
    public void setScanSend(int scanSend) {
        this.scanSend = scanSend;
    }

    /**
     * @return the scanStored
     */
    public int getScanStored() {
        return scanStored;
    }

    /**
     * @param scanStored the scanStored to set
     */
    public void setScanStored(int scanStored) {
        this.scanStored = scanStored;
    }

    /**
     * @return the faxSend
     */
    public int getFaxSend() {
        return faxSend;
    }

    /**
     * @param faxSend the faxSend to set
     */
    public void setFaxSend(int faxSend) {
        this.faxSend = faxSend;
    }

    /**
     * @return the faxReceived
     */
    public int getFaxReceived() {
        return faxReceived;
    }

    /**
     * @param faxReceived the faxReceived to set
     */
    public void setFaxReceived(int faxReceived) {
        this.faxReceived = faxReceived;
    }

    /**
     * @return the date
     */
    public Date getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * @return the printer
     */
    public Printer getPrinter() {
        return printer;
    }

    /**
     * @param printer the printer to set
     */
    public void setPrinter(Printer printer) {
        this.printer = printer;
    }

    /**
     * @return the user
     */
    public User getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(User user) {
        this.user = user;
    }
    
        /**
     * @return the company
     */
    public String getCompany() {
        return company;
    }

    /**
     * @param id the id to set
     */
    public void setCompany(String company) {
        this.company = company;
    }

    public void checkResumeValues(Resume lastResume)
    {
        if (this.copyBW < lastResume.copyBW)
            this.copyBW = this.copyBW + lastResume.copyBW;

        if (this.copyColor < lastResume.copyColor)
            this.copyColor = this.copyColor + lastResume.copyColor;

        if (this.printBW < lastResume.printBW)
            this.printBW = this.printBW + lastResume.printBW;

        if (this.printColor < lastResume.printColor)
            this.printColor = this.printColor + lastResume.printColor;

        if (this.scanSend < lastResume.scanSend)
            this.scanSend = this.scanSend + lastResume.scanSend;

        if (this.scanStored < lastResume.scanStored)
            this.scanStored = this.scanStored + lastResume.scanStored;

        if (this.faxSend < lastResume.faxSend)
            this.faxSend = this.faxSend + lastResume.faxSend;

        if (this.faxReceived < lastResume.faxReceived)
            this.faxReceived = this.faxReceived + lastResume.faxReceived;

        this.total = this.copyBW + this.copyColor + this.printBW + this.printColor + this.faxReceived;
   }

    
    public void addResumeValues(Resume resume1)
    {
        this.setTotal(this.total + resume1.getTotal());
        this.setPrintColor(this.printColor + resume1.getPrintColor());
        this.setPrintBW(this.printBW + resume1.getPrintBW());
        this.setCopyColor(this.copyColor + resume1.getCopyColor());
        this.setCopyBW(this.copyBW + resume1.getCopyBW());
        this.setScanSend(this.scanSend + resume1.getScanSend());
        this.setScanStored(this.scanStored + resume1.getScanStored());
        this.setFaxSend(this.faxSend + resume1.getFaxSend());
        this.setFaxReceived(this.faxReceived + resume1.getFaxReceived());
    }
    
    /**
     *
     * @param resume1
     * @param resume2
     */
    public void setResumeValues(Resume resume1, Resume resume2)
    {
        this.setTotal(resume1.getTotal() + resume2.getTotal());
        this.setPrintColor(resume1.getPrintColor() + resume2.getPrintColor());
        this.setPrintBW(resume1.getPrintBW() + resume2.getPrintBW());
        this.setCopyColor(resume1.getCopyColor() + resume2.getCopyColor());
        this.setCopyBW(resume1.getCopyBW() + resume2.getCopyBW());
        this.setScanSend(resume1.getScanSend() + resume2.getScanSend());
        this.setScanStored(resume1.getScanStored() + resume2.getScanStored());
        this.setFaxSend(resume1.getFaxSend() + resume2.getFaxSend());
        this.setFaxReceived(resume1.getFaxReceived() + resume2.getFaxReceived());
        this.setCompany(resume1.getCompany());
        
    }

    public void subtractResumeValues(Resume resume1, Resume resume2)
    {
        this.setTotal(resume1.getTotal() - resume2.getTotal());
        this.setPrintColor(resume1.getPrintColor() - resume2.getPrintColor());
        this.setPrintBW(resume1.getPrintBW() - resume2.getPrintBW());
        this.setCopyColor(resume1.getCopyColor() - resume2.getCopyColor());
        this.setCopyBW(resume1.getCopyBW() - resume2.getCopyBW());
        this.setScanSend(resume1.getScanSend() - resume2.getScanSend());
        this.setScanStored(resume1.getScanStored() - resume2.getScanStored());
        this.setFaxSend(resume1.getFaxSend() - resume2.getFaxSend());
        this.setFaxReceived(resume1.getFaxReceived() - resume2.getFaxReceived());
    }

    /**
     * 
     * @param resume
     */
    public void setResumeValues(Resume resume)
    {
        this.setTotal(resume.getTotal());
        this.setPrintColor(resume.getPrintColor());
        this.setPrintBW(resume.getPrintBW());
        this.setCopyColor(resume.getCopyColor());
        this.setCopyBW(resume.getCopyBW());
        this.setScanSend(resume.getScanSend());
        this.setScanStored(resume.getScanStored());
        this.setFaxSend(resume.getFaxSend());
        this.setFaxReceived(resume.getFaxReceived());
        this.setCompany(resume.getCompany());
    }

    public void updateResumeValues(int numPages, String colorMode, String type)
    {
        if (type.equals("copy") || type.equals("copy.storage") || type.equals("documentbox.storedocument-print")
                    || type.equals("printer.print") || type.equals("printer.secret-print") || type.equals("printer.trial-storage-print")
                    || type.equals("printer.trial-print") || type.equals("printer.pausedocument-print")
                    || type.equals("printer.keepdocument-storage-print") || type.equals("printer.keepdocument-reprint")
                    || type.equals("report") || type.equals("report.state") || type.equals("fax.print") || type.equals("fax.receive")
                    || type.equals("fax.receive-deliver") || type.equals("fax.receive-storage") || type.equals("fax.pctranser"))
            this.setTotal(this.getTotal() + numPages);

        if (type.equals("printer.print") || type.equals("printer.secret-print") || type.equals("printer.trial-storage-print")
                    || type.equals("printer.trial-print") || type.equals("printer.pausedocument-print")
                    || type.equals("printer.keepdocument-storage-print") || type.equals("printer.keepdocument-reprint")
                    || type.equals("report") || type.equals("report.state") || type.equals("fax.pctranser")
                    || type.equals("documentbox.storedocument-print"))
        {
            if (colorMode.equals("color"))
            {
                this.setPrintColor(this.getPrintColor() + numPages);
            }

            if (colorMode.equals("blackandwhite"))
            {
                this.setPrintBW(this.getPrintBW() + numPages);
            }
        }

        if (type.equals("copy") || type.equals("copy.storage"))
        {
            if (colorMode.equals("color"))
            {
                this.setCopyColor(this.getCopyColor() + numPages);
            }

            if (colorMode.equals("blackandwhite"))
            {
                this.setCopyBW(this.getCopyBW() + numPages);
            }
        }

        if (type.equals("scanner.deliver") || type.equals("scanner.deliver-storage") || type.equals("scanner.storedocument-forward")
                || type.equals("scanner.linkdeliver-storage") || type.equals("scanner.storedocument-deliver")
                || type.equals("scanner.storedocument-linkdeliver") || type.equals("documentbox.storedocument-forward"))
            this.setScanSend(this.getScanSend() + numPages);

        if (type.equals("copy.storage") || type.equals("scanner.storage") || type.equals("documentbox.storage")
                || type.equals("scanner.deliver-storage") || type.equals("documentbox.storage-network")
                || type.equals("scanner.linkdeliver-storage"))
            this.setScanStored(this.getScanStored() + numPages);

        if (type.equals("fax.transfer") || type.equals("fax.pctranser"))
            this.setFaxSend(this.getFaxSend() + numPages);

        if (type.equals("fax.print") || type.equals("fax.receive")
                || type.equals("fax.receive-deliver") || type.equals("fax.receive-storage"))
            this.setFaxReceived(this.getFaxReceived() + numPages);

    }

}
