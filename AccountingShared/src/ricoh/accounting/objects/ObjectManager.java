/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ricoh.accounting.objects;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Vector;

/**
 *
 * @author alexis.hidalgo
 */
public class ObjectManager {

    private Map reportingUsersHash;
    private Map reportingMFPsHash;
    private Map unknownUser_ReportingMFPsHash;
    private Map newReportingUsersHash;
    private Map newReportingMFPsHash;
    private Map unknownUser_NewReportingMFPsHash;
    private Map notReportingMFPsHash;
    private Map mfpRegisterHash;
    private Map INSERT_mfpRegisterHash;
    private Map UPDATE_mfpRegisterHash;
    private Map<String, User> CCUserHash;
    private Map INSERT_CCUserHash;
    /** NUEVO **/
    private Map<String, Vector> User_MFPsHash;
    private Map<String, User> User_CCHash;
    private Map<String, String> CC_CCnameHash;
    private Map<String, String> Mfpserial_companyHash;
    private Map reportingCCHash;
    private Map newReportingCCHash;
    private List actionList;
    private List fail2CommunicateList;
    private static final boolean WITH_UPDATE_CASCADE = true;
    private static final boolean WITH_NO_UPDATE_CASCADE = false;
    public static final int EXISTENT_MFP = 0;
    public static final int NEW_MFP = 1;

    public ObjectManager() {
        reportingUsersHash = new HashMap();
        reportingMFPsHash = new HashMap();
        unknownUser_ReportingMFPsHash = new HashMap();
        newReportingUsersHash = new HashMap();
        newReportingMFPsHash = new HashMap();
        unknownUser_NewReportingMFPsHash = new HashMap();
        notReportingMFPsHash = new HashMap();
        mfpRegisterHash = new HashMap();
        INSERT_mfpRegisterHash = new HashMap();
        UPDATE_mfpRegisterHash = new HashMap();
        INSERT_CCUserHash = new HashMap();

        /** NUEVO **/
        User_MFPsHash = new HashMap<String, Vector>();
        User_CCHash = new HashMap<String, User>();
        CC_CCnameHash = new HashMap<String, String>();
        Mfpserial_companyHash = new HashMap<String, String>();
        reportingCCHash = new HashMap();
        newReportingCCHash = new HashMap();
        CCUserHash = new HashMap<String, User>();

        actionList = new ArrayList();
        fail2CommunicateList = new ArrayList();
    }

    public void clearHashTables() {
        //Clear the users hash tables
        reportingUsersHash.clear();
        newReportingUsersHash.clear();

        //Clear the printers hash tables
        reportingMFPsHash.clear();
        newReportingMFPsHash.clear();
        unknownUser_ReportingMFPsHash.clear();
        unknownUser_NewReportingMFPsHash.clear();
        //notReportingMFPsHash.clear();

        //** NUEVO **/
        User_MFPsHash.clear();
        User_CCHash.clear();
        CC_CCnameHash.clear();
        Mfpserial_companyHash.clear();
        reportingCCHash.clear();
        newReportingCCHash.clear();
//        CCUserHash.clear();
    }

    public void fillReportingMFPsHash(List mfpList) {
        String mfpName;
        List resumeList;

        //Add a new entry inside the reportingMFPsHash for each printer
        ListIterator it = mfpList.listIterator();
        while (it.hasNext()) {
            mfpName = (String) it.next();
            resumeList = new ArrayList();
            reportingMFPsHash.put(mfpName, resumeList);
        }
    }

    public void fillUnknownUser_ReportingMFPsHash(List mfpList) {
        String mfpName;
        List resumeList;

        //Add a new entry inside the reportingMFPsHash for each printer
        ListIterator it = mfpList.listIterator();
        while (it.hasNext()) {
            mfpName = (String) it.next();
            resumeList = new ArrayList();
            unknownUser_ReportingMFPsHash.put(mfpName, resumeList);
        }
    }

    public void add2NotReportingMFPsHash(Printer printer) {
        notReportingMFPsHash.put(printer.getMfpSerial(), printer);
    }

    public void fillReportingUsersHash(List usersList) {
        String userName;
        List resumeList;

        //Add a new entry inside the reportingUsersHash for each user
        ListIterator it = usersList.listIterator();
        while (it.hasNext()) {
            userName = (String) it.next();
            resumeList = new ArrayList();
            reportingUsersHash.put(userName, resumeList);
        }
    }

    public void fillMfpserial_companyHash(List mfpList) {
        String mfpserial, company;

        //Add a new entry inside the reportingUsersHash for each user
        ListIterator it = mfpList.listIterator();
        while (it.hasNext()) {
            mfpserial = (String) it.next();
            company = (String) it.next();
            Mfpserial_companyHash.put(mfpserial, company);
        }
    }

    public void fillReportingCCsHash(List ccList) {
        String CostCenter;
        List resumeList;

        //Add a new entry inside the reportingCCsHash for each Cost Center
        ListIterator it = ccList.listIterator();
        while (it.hasNext()) {
            CostCenter = (String) it.next();
            resumeList = new ArrayList();
            reportingCCHash.put(CostCenter, resumeList);
        }
    }

    public void fillUser_MFPsHash(String username, List mfpList) {
        String mfpName;
        List resumeList;
        Map<String, List> mfpMap = new HashMap<String, List>();
        Map<String, List> newMfpMap = new HashMap<String, List>();

        //Add a new entry inside the mfpMap for each printer
        ListIterator it = mfpList.listIterator();
        while (it.hasNext()) {
            mfpName = (String) it.next();
            resumeList = new ArrayList();
            mfpMap.put(mfpName, resumeList);
        }

        Vector<Map> mfpMapsVector = new Vector<Map>();
        mfpMapsVector.add(EXISTENT_MFP, mfpMap);
        mfpMapsVector.add(NEW_MFP, newMfpMap);

//        if (username.contains("noname"))
//            username = "";
        User_MFPsHash.put(username, mfpMapsVector);
    }

    public Map getReportingUsersHash() {
        return this.reportingUsersHash;
    }

    public Map getNewReportingUsersHash() {
        return this.newReportingUsersHash;
    }

    public Map getReportingMFPsHash() {
        return this.reportingMFPsHash;
    }

    public Map getNewReportingMFPsHash() {
        return this.newReportingMFPsHash;
    }

    public Map getUnknownUser_ReportingMFPsHash() {
        return this.unknownUser_ReportingMFPsHash;
    }

    public Map getUnknownUser_NewReportingMFPsHash() {
        return this.unknownUser_NewReportingMFPsHash;
    }

    public Map getNotReportingMFPsHash() {
        return this.notReportingMFPsHash;
    }

    public Map getMfpserial_companyHash() {
        return this.Mfpserial_companyHash;
    }

    public Map<String, Vector> getUserMFPsHash() {
        return this.User_MFPsHash;
    }

    public void clearActionList() {
        //Clean the actions List
        actionList.clear();
    }

    public void addAction(Action action) {
        //Add the Action object to the action List
        actionList.add(action);
    }

    public List getActionList() {
        return this.actionList;
    }

    /**
     * @return the mfpRegisterHash
     */
    public Map getMfpRegisterHash() {
        return mfpRegisterHash;
    }

    /**
     * @param mfpRegisterHash the mfpRegisterHash to set
     */
    public void setMfpRegisterHash(Map mfpRegisterHash) {
        this.mfpRegisterHash = mfpRegisterHash;
    }

    public void add2MfpRegisterHash(Printer printer) {
        mfpRegisterHash.put(printer.getMfpSerial(), printer);
        INSERT_mfpRegisterHash.put(printer.getMfpSerial(), printer);
    }

    public Map getCCUserRegisterHash() {
        return CCUserHash;
    }

    public void add2CCUserHash(User user) {
        CCUserHash.put(user.getUsername(), user);
        INSERT_CCUserHash.put(user.getUsername(), user);
    }

    public void update2MfpRegisterHash(Printer printer) {
        mfpRegisterHash.put(printer.getMfpSerial(), printer);
        UPDATE_mfpRegisterHash.put(printer.getMfpSerial(), printer);
    }

    public void add2Fail2CommunicateList(Printer printer) {
        fail2CommunicateList.add(printer);
    }

    /**
     * @return the INSERT_mfpRegisterHash
     */
    public Map getINSERT_mfpRegisterHash() {
        return INSERT_mfpRegisterHash;
    }

    /**
     * @return the UPDATE_mfpRegisterHash
     */
    public Map getUPDATE_mfpRegisterHash() {
        return UPDATE_mfpRegisterHash;
    }

    /**
     * @return the fail2CommunicateList
     */
    public List getFail2CommunicateList() {
        return fail2CommunicateList;
    }

    public void updateResumeInfo(Action action, Resume.resumeType resumeType) {
        Resume resume = null;
        List resumeList;
        Map hash = null, auxHash = null;
        String id = "";
        int pos;
        boolean existCompany = false;

        switch (resumeType) {
            case MFP_RESUME:
                hash = reportingMFPsHash;
                auxHash = newReportingMFPsHash;
                id = action.getMFPserial();
                break;

            case DESCONOCIDO_MFP_RESUME:
                hash = unknownUser_ReportingMFPsHash;
                auxHash = unknownUser_NewReportingMFPsHash;
                id = action.getMFPserial();
                break;

            case USER_RESUME:
                hash = reportingUsersHash;
                auxHash = newReportingUsersHash;
                id = action.getUserName();
//                if (id.isEmpty()){
//                    id = "noname_" + action.getCompany(); 
//                }
                break;

            /** NUEVO **/
            case USER_MFP_RESUME:
                if (User_MFPsHash.containsKey(action.getUserName())) {
                    hash = (Map) User_MFPsHash.get(action.getUserName()).elementAt(EXISTENT_MFP);
                    auxHash = (Map) User_MFPsHash.get(action.getUserName()).elementAt(NEW_MFP);
                } else {
                    hash = new HashMap();
                    auxHash = new HashMap();
                }
                id = action.getMFPserial();
//                if (action.getUserName().isEmpty()){
//                    action.setUserName("noname_" +action.getCompany()); 
//                }
                break;

            case CC_RESUME:
                hash = reportingCCHash;
                auxHash = newReportingCCHash;
                if (action.getUserName().split("_").length > 2) {
//                    System.out.println("CC_RESUME username: " + action.getUserName().split("_")[2]);
//                    System.out.println("CC_RESUME User_CCHash: " + User_CCHash.get(action.getUserName().split("_")[2]));
                    if (User_CCHash != null && User_CCHash.get(action.getUserName()) != null) {
                        id = User_CCHash.get(action.getUserName()).getCostCenter(); //(action.getUserName().split("_")[2]).getCostCenter();           
                    }
                } else {
//                    System.out.println("CC_RESUME else username: " + action.getUserName());
//                    System.out.println("CC_RESUME else User_CCHash: " + User_CCHash.get(action.getUserName()));
                    id = User_CCHash.get(action.getUserName()).getCostCenter();
                }
                if (id.isEmpty()) {
                    id = action.getUserName();//"noname_" +action.getCompany(); 
                }
                break;
        }

        //System.out.println("ID : " + id);
        if (hash != null && auxHash != null) {
            //System.out.println("hash != null && auxHash != null");
            if (hash.containsKey(id)) {//The MFP or the User has been reporting before, so we´ve got it inside the corresponding Hash table
                //System.out.println("ID exists in hash");
                //Obtain the list of Resumes for this id
                resumeList = (List) hash.get(id);

                if (!resumeList.isEmpty()) {
                    //Obtain the Resume of this date from the list of Resumes
                    resume = getResumeByDate(resumeList, action.getDate());

                    if (resume != null) {//Exists a Resume for this date, so we need to update it

                        //Update all the values of the Resume
                        resume.setCompany(action.getCompany());
                        resume.updateResumeValues(action.getNumPages(), action.getColorMode(), action.getType());
                        //Update the id Resumes list on the corresponding Hash table
                        hash.put(id, resumeList);
                    } else {//There is no Resume for this date, so we need to create it, and insert into the Resumes list

                        //Create a new Resume for this id on this date
                        resume = new Resume(id, action.getDate());
                        if (action.getUserName().split("_").length > 2) {
                            resume.setUser(User_CCHash.get(action.getUserName().split("_")[2]));
                        } else {
                            resume.setUser(User_CCHash.get(action.getUserName()));
                        }

                        resume.setCompany(action.getCompany());
                        resume.updateResumeValues(action.getNumPages(), action.getColorMode(), action.getType());
                        //Add the new Resume to the Resumes list ordered by date, do not update following dates results
                        addResumeByDate(resumeList, resume, WITH_NO_UPDATE_CASCADE);
                        //Update the id Resumes list on the corresponding Hash table
                        hash.put(id, resumeList);
                    }
                } else {//There is no Resume entered for this MFP or User, so we need to start it

                    //Create a new Resume for this id on this date
                    resume = new Resume(id, action.getDate());
                    if (action.getUserName().split("_").length > 2) {
                        resume.setUser(User_CCHash.get(action.getUserName().split("_")[2]));
                    } else {
                        resume.setUser(User_CCHash.get(action.getUserName()));
                    }

                    resume.setCompany(action.getCompany());
                    resume.updateResumeValues(action.getNumPages(), action.getColorMode(), action.getType());
                    //Add the id and its Resume for this date to the corresponding Hash table
                    resumeList.add(resume);
                    hash.put(id, resumeList);
                }
            } else {//The MFP or the User has not reported before, it is the first time it reports, so we do not have it inside the corresponding Hash table
                //System.out.println("ID NOT exists in hash");
//                if ((id.isEmpty())&&(auxHash.get(id)!=null))
//                    existCompany = EqualCompanyByDate((List) auxHash.get(id),action.getDate(),action.getCompany());

                if (auxHash.containsKey(id)) {//The id has appeared before on this CSV file, so we´ve got it inside the corresponding auxHash table

                    //System.out.println("ID exists in aux hash");
                    //Obtain the list of Resumes for this id
                    resumeList = (List) auxHash.get(id);
                    //Obtain the Resume of this date from the list of Resumes
                    resume = getResumeByDate(resumeList, action.getDate());

                    if (resume != null) {//Exists a Resume for this date, so we need to update it
                        //System.out.println("ID exists in aux hash. Resume list exists");
                        pos = resumeList.indexOf(resume);
                        //Create an auxiliar Resume with the new values
                        Resume auxResume = new Resume(id, action.getDate());
                        auxResume.updateResumeValues(action.getNumPages(), action.getColorMode(), action.getType());
                        //Update the values of the Resume
                        resume.setResumeValues(resume, auxResume);
                        resume.setCompany(action.getCompany());
                        //Update the values for all the posible Resumes of the following days with the new values
                        updateListValues(resumeList, auxResume, pos + 1);
                        //Update the id Resumes list on the corresponding auxHash table
                        auxHash.put(id, resumeList);
                    } else {//There is no Resume for this date, so we need to create it, and insert into the Resumes list
                        //System.out.println("ID exists in aux hash. Resume list NOT exists");
                        //Create a new Resume for this id on this date
                        resume = new Resume(id, action.getDate());
                        if (action.getUserName().split("_").length > 2) {
                            resume.setUser(User_CCHash.get(action.getUserName().split("_")[2]));
                        } else {
                            resume.setUser(User_CCHash.get(action.getUserName()));
                        }

                        resume.setCompany(action.getCompany());
                        resume.updateResumeValues(action.getNumPages(), action.getColorMode(), action.getType());
                        //Add the new Resume to the Resumes list ordered by date
                        addResumeByDate(resumeList, resume, WITH_UPDATE_CASCADE);
                        //Update the id Resumes list on the corresponding auxHash table
                        auxHash.put(id, resumeList);
                    }
                } else {//The id has not appeared before on this CSV file, so we need to initialize its resume, and add it to the corresponding auxHash table
                    //System.out.println("ID NOT exists in aux hash");
                    //Create a new Resume for this id on this date
                    resume = new Resume(id, action.getDate());
                    if (action.getUserName().split("_").length > 2) {
                        resume.setUser(User_CCHash.get(action.getUserName().split("_")[2]));
                    } else {
                        resume.setUser(User_CCHash.get(action.getUserName()));
                    }

                    resume.setCompany(action.getCompany());
                    resume.updateResumeValues(action.getNumPages(), action.getColorMode(), action.getType());
                    //Add the id and its Resume for this date to the corresponding auxHash table
                    resumeList = new ArrayList();
                    resumeList.add(resume);
//                    if ((id.isEmpty() && auxHash.containsKey(id)))
//                        id = "noname"+ auxHash.size()+1;
                    auxHash.put(id, resumeList);
                }
            }

            //** NUEVO **//
            //Actualizar los hash de mfps para el usuario correspondiente
            if (resumeType.equals(Resume.resumeType.USER_MFP_RESUME)) {
                Vector<Map> mfpMapVector = new Vector<Map>();
                mfpMapVector.add(EXISTENT_MFP, hash);
                mfpMapVector.add(NEW_MFP, auxHash);

                User_MFPsHash.put(action.getUserName(), mfpMapVector);
            }
        }
    }

    private Resume getResumeByDate(List resumeList, Date date) {
        Resume resume = null;
        boolean found = false;

        //Search the list of Resumes to find the target resume
        ListIterator it = resumeList.listIterator();
        while (it.hasNext() && !found) {
            resume = (Resume) it.next();
            //If dates are equal, we have found the resume
            if (resume.getDate().equals(date)) {
                found = true;
            }
        }
        //If we have found the Resume, it is contained on resume variable, so return it.
        if (found) {
            return resume;
        } else //If we have not found the Resume, return a null value.
        {
            return null;
        }
    }

    private boolean EqualCompanyByDate(List resumeList, Date date, String company) {
        Resume resume = null;
        boolean found = false;

        //Search the list of Resumes to find the target resume
        ListIterator it = resumeList.listIterator();
        while (it.hasNext() && !found) {
            resume = (Resume) it.next();
            //If dates are equal, we have found the resume
            if (resume.getDate().equals(date) && (resume.getCompany().equalsIgnoreCase(company))) {
                found = true;
            }
        }
        //If we have found the Resume, it is contained on resume variable, so return it.
        if (found) {
            return true;
        } else //If we have not found the Resume, return a null value.
        {
            return false;
        }
    }

    private void addResumeByDate(List resumeList, Resume insertResume, boolean cascade) {
        Resume resume = null;
        int insertIndex = 0;

        //Search for the position to insert into the resumeList
        ListIterator it = resumeList.listIterator();
        while (it.hasNext()) {
            resume = (Resume) it.next();
            if (resume.getDate().before(insertResume.getDate())) {
                insertIndex++; //Increase the position to insert into
            } else {
                break; //We have found the position to insert into
            }
        }

        if (cascade) {
            if (insertIndex > 0) {
                //Update the Resume object with the previous Resume values
                resume = (Resume) resumeList.get(insertIndex - 1);

                Resume newResume = new Resume(insertResume.getId(), insertResume.getDate());
                newResume.setResumeValues(insertResume, resume);
                if (newResume.getUser() == null) {
                    newResume.setUser(new User("", "", "", ""));
                }
                //Insert the new Resume object
                resumeList.add(insertIndex, newResume);
            } else //Insert the new Resume object
            {
                resumeList.add(insertIndex, insertResume);
            }

            //Update the values for the rest of the elements on the resumeList
            updateListValues(resumeList, insertResume, insertIndex + 1);
        } else //Insert the new Resume object
        {
            resumeList.add(insertIndex, insertResume);
        }
    }

    private void updateListValues(List resumeList, Resume resume, int start) {
        Resume current;

        for (int i = start; i < resumeList.size(); i++) {
            current = (Resume) resumeList.get(i);
            current.setResumeValues(current, resume);
        }
    }

    /**
     * @return the User_CCHash
     */
    public Map<String, User> getUser_CCHash() {
        return User_CCHash;
    }

    /**
     * @return the CC_CCnameHash
     */
    public Map<String, String> getCC_CCnameHash() {
        return CC_CCnameHash;
    }

    /**
     * @return the reportingCCHash
     */
    public Map<String, Resume> getReportingCCHash() {
        return reportingCCHash;
    }

    /**
     * @return the newReportingCCHash
     */
    public Map<String, Resume> getNewReportingCCHash() {
        return newReportingCCHash;
    }

    public void fillUser_CCHash(User user) {
        if (!User_CCHash.containsKey(user.getUsername())) {
            User_CCHash.put(user.getUsername(), user);
        }
    }

    public void fillCC_CCnameHash(User user) {
        if (!CC_CCnameHash.containsKey(user.getCostCenter())) {
            CC_CCnameHash.put(user.getCostCenter(), user.getDepartment());
        }
    }
}
