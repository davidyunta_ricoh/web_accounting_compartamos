/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ricoh.accounting.objects;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author alexis.hidalgo
 */
public class Action implements Serializable
{
    private Date date;
    private String type = "";
    private String MFPserial = "";
    private String userName = "";
    private java.sql.Timestamp dateTime;
    private int numPages = 0;
    private int numCopies = 0;
    private String docuName = "";
    private String colorMode = "";
    private String pageSize = "";
    private String destination = "";
    private String sender = "";
    private Printer printer;
    private User user;
    private String company;
    private String pageMode;

    public static enum actionType { USER_ACTION, MFP_ACTION };

    public Action(){}

    public Action(Date date, String type, String MFPserial, String userName, java.sql.Timestamp dateTime, int numPages, int numCopies,
            String docuName, String colorMode, String pageSize, String destination, String sender, String company)
    {
        this.date = date;
        this.type = type;
        this.MFPserial = MFPserial;
        this.userName = userName;
        this.dateTime = dateTime;
        this.numPages = numPages;
        this.numCopies = numCopies;
        this.docuName = docuName;
        this.colorMode = colorMode;
        this.pageSize = pageSize;
        this.destination = destination;
        this.sender = sender;
        this.company = company;
    }

    /**
     * @return the date
     */
    public Date getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the MFPserial
     */
    public String getMFPserial() {
        return MFPserial;
    }

    /**
     * @param MFPserial the MFPserial to set
     */
    public void setMFPserial(String MFPserial) {
        this.MFPserial = MFPserial;
    }

    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * @return the dateTime
     */
    public java.sql.Timestamp getDateTime() {
        return dateTime;
    }

    /**
     * @param dateTime the dateTime to set
     */
    public void setDateTime(java.sql.Timestamp dateTime) {
        this.dateTime = dateTime;
    }

    /**
     * @return the numPages
     */
    public int getNumPages() {
        return numPages;
    }

    /**
     * @param numPages the numPages to set
     */
    public void setNumPages(int numPages) {
        this.numPages = numPages;
    }

    /**
     * @return the numCopies
     */
    public int getNumCopies() {
        return numCopies;
    }

    /**
     * @param numCopies the numCopies to set
     */
    public void setNumCopies(int numCopies) {
        this.numCopies = numCopies;
    }

    /**
     * @return the docuName
     */
    public String getDocuName() {
        return docuName;
    }

    /**
     * @param docuName the docuName to set
     */
    public void setDocuName(String docuName) {
        this.docuName = docuName;
    }

    /**
     * @return the colorMode
     */
    public String getColorMode() {
        return colorMode;
    }

    /**
     * @param colorMode the colorMode to set
     */
    public void setColorMode(String colorMode) {
        this.colorMode = colorMode;
    }

    /**
     * @return the pageSize
     */
    public String getPageSize() {
        return pageSize;
    }

    /**
     * @param pageSize the pageSize to set
     */
    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    /**
     * @return the destination
     */
    public String getDestination() {
        return destination;
    }

    /**
     * @param destination the destination to set
     */
    public void setDestination(String destination) {
        this.destination = destination;
    }

    /**
     * @return the sender
     */
    public String getSender() {
        return sender;
    }

    /**
     * @param sender the sender to set
     */
    public void setSender(String sender) {
        this.sender = sender;
    }
    
    /**
     * @return the sender
     */
    public String getCompany() {
        return this.company;
    }

    /**
     * @param sender the sender to set
     */
    public void setCompany(String company) {
        this.company = company;
    }

    /**
     * @return the printer
     */
    public Printer getPrinter() {
        return printer;
    }

    /**
     * @param printer the printer to set
     */
    public void setPrinter(Printer printer) {
        this.printer = printer;
    }

    /**
     * @return the user
     */
    public User getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * @return the pageMode
     */
    public String getPageMode() {
        return pageMode;
    }

    /**
     * @param pageMode the pageMode to set
     */
    public void setPageMode(String pageMode) {
        this.pageMode = pageMode;
    }
    
    

}
