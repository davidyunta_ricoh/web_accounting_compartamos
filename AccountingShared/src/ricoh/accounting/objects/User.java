/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ricoh.accounting.objects;

import java.io.Serializable;

/**
 *
 * @author Alexis.Hidalgo
 */
public class User implements Serializable
{
    private String username = "";
    private String costCenter = "";
    private String department = "";
    private String fullname = "";

    public User(String username, String costCenter, String department, String fullname)
    {
        this.username = username;
        this.department = department;
        this.costCenter = costCenter;
        this.fullname = fullname;
    }
    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the costCenter
     */
    public String getCostCenter() {
        return costCenter;
    }

    /**
     * @param costCenter the costCenter to set
     */
    public void setCostCenter(String costCenter) {
        this.costCenter = costCenter;
    }

    /**
     * @return the department
     */
    public String getDepartment() {
        return department;
    }

    /**
     * @param department the department to set
     */
    public void setDepartment(String department) {
        this.department = department;
    }

        /**
     * @return the department
     */
    public String getFullname() {
        return fullname;
    }

    /**
     * @param department the department to set
     */
    public void setFullname(String fullname) {
        this.fullname = fullname;
    }
    
}
