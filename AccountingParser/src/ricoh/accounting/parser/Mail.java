package ricoh.accounting.parser;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */



import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author Javier.Burgos
 */
public class Mail {

    public Mail(boolean parserMail) throws IOException{

        String mesg = "";
        //Leer fichero properties mail
        Properties prop = new Properties();
        prop.load(new FileInputStream("conf/config.properties"));
        String server= prop.getProperty("MAIL.IPSMTPSERVER");
        if ((server != null) && (!server.equalsIgnoreCase(""))){
            String port = prop.getProperty("MAIL.PORT");
            String fromaddress= prop.getProperty("MAIL.FROMADDRESS");
            String toaddress = prop.getProperty("MAIL.TOADDRESS");
            String subject = prop.getProperty("MAIL.SUBJECT");
            if (parserMail)
                mesg = prop.getProperty("MAIL.MESSAGE_PARSERPROCESS");
            else
                mesg = prop.getProperty("MAIL.MESSAGE_CHECKTOTALS");

            Properties props = new Properties();
            props.setProperty("mail.smtp.host", server);
            props.setProperty("mail.smtp.starttls.enable", "true");
            props.setProperty("mail.smtp.port", port);//587 por defecto
            //props.setProperty("mail.smtp.user", "gescribano@iberdrola.es");
            props.setProperty("mail.smtp.auth", "false");

            try{
                    Session ssession = Session.getDefaultInstance(props);
                    MimeMessage message = new MimeMessage(ssession);
                    message.setFrom(new InternetAddress(fromaddress));
                    message.addRecipient(Message.RecipientType.TO, new InternetAddress(toaddress));
                    //message.addRecipient(Message.RecipientType.CC, new InternetAddress("javier.burgos@ricoh.es"));
                    message.setSubject(subject);//"Ricoh: ***ERROR en proceso Parser Accounting***");
                   // Fill the message
                    message.setText(mesg);
                    Transport.send(message);

            } catch (Exception ex1) {
                ex1.printStackTrace();
            }
        }
    }

}
