/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ricoh.accounting.parser;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.UnknownHostException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import ricoh.accounting.database.DataBaseManager;
import ricoh.accounting.ifactory.InterfaceManager;
import ricoh.accounting.interfaces.IFileManager;
import ricoh.accounting.interfaces.INetworkManager;
import ricoh.accounting.objects.Action;
import ricoh.accounting.objects.ObjectManager;
import ricoh.accounting.objects.Printer;
import ricoh.accounting.objects.Resume;
import ricoh.accounting.objects.User;
import ricoh.accounting.remotecc.RemoteccPlugin;
import ricoh.accounting.remotecommunication.MFPIncluder;

/**
 *
 * @author alexis.hidalgo
 */
public class Parser
{
    private DataBaseManager dbManager;
    private IFileManager fileManager;
    private INetworkManager networkManager;

    private ObjectManager objectManager;
    private RemoteccPlugin remoteccplugin;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private String MFP_INVENTORY_FILE = "";
    private String CSV_CCUSER_FOLDER = "";
    private String CAP_AUTHENTICATION_ACTIVATION = "";
    private String CC_OBTAINING_ACTIVATION_MODE = "";
    private static final String CC_OBTAINING_CSV_MODE = "CSV";
    private static final String CC_OBTAINING_AD_MODE = "AD";
    private static final String CC_OBTAINING_EXTDB_MODE = "extDB";
    private String AD_AUTHSIMPLE = "";
    private String AD_HOST = "";
    private String AD_PORT = "";
    private String AD_USER = "";
    private String AD_PASS = "";
    private String AD_BASE = "";
    private String AD_FILTER = "";
    private String AD_USER_USERNAME = "";
    private String AD_USER_FULLNAME = "";
    private String AD_USER_CC = "";
    private String AD_USER_CCNAME = "";
    
    public static final String COMPANY_NAME = "Compartamos";
    
    public static Logger logger = Logger.getLogger(Parser.class.getName());
    
    

    public Parser()
    {
        logger.debug("Parser()");
        try
        {
            
            objectManager = new ObjectManager();
            logger.debug("creating fileManager...");
            fileManager = InterfaceManager.getFileManagerInstance();
            
            logger.debug("creating dbManager...");
            dbManager = InterfaceManager.getDataBaseManagerInstance("conf/config.properties", "conf/sql.properties");
            
            logger.debug("creating networkManager...");
            networkManager = InterfaceManager.getNetworkManagerInstance("conf/config.properties");

            //Leer fichero properties
            Properties prop = new Properties();
            logger.debug("loading conf/config.properties");
            prop.load(new FileInputStream("conf/config.properties"));
            
            MFP_INVENTORY_FILE = prop.getProperty("MFP.InventoryFile");
            logger.debug("MFP_INVENTORY_FILE -> " + MFP_INVENTORY_FILE );
            
            CSV_CCUSER_FOLDER = prop.getProperty("CSV.CCUserPath");
            logger.debug("CSV_CCUSER_FOLDER -> " + CSV_CCUSER_FOLDER );
            
//            AD_AUTHSIMPLE = prop.getProperty("AD.AuthSimple");
//            AD_HOST = prop.getProperty("AD.Host");
//            AD_PORT = prop.getProperty("AD.Port");
//            AD_USER = prop.getProperty("AD.User");
//            AD_PASS = prop.getProperty("AD.Pass");
//            AD_BASE = prop.getProperty("AD.Base");
//            AD_FILTER = prop.getProperty("AD.Filter");
//            AD_USER_USERNAME = prop.getProperty("AD.UserUsername");
//            AD_USER_FULLNAME = prop.getProperty("AD.UserFullName");
//            AD_USER_CC = prop.getProperty("AD.UserCC");
//            AD_USER_CCNAME = prop.getProperty("AD.UserCCName");
//            
//            logger.info("*** Active Directory Settings ***");
//            logger.info("AD.AuthSimple -> " + AD_AUTHSIMPLE);
//            logger.info("AD.Host   -> " + AD_HOST);
//            logger.info("AD.Port   -> " + AD_PORT);
//            logger.info("AD.User   -> " + AD_USER);
//            logger.info("AD.Pass   -> " + "*******");
//            logger.info("AD.Base   -> " + AD_BASE);
//            logger.info("AD.Filter -> " + AD_FILTER);
//            logger.info("AD.UserUsername -> " + AD_USER_USERNAME);
//            logger.info("AD.UserFullName -> " + AD_USER_FULLNAME);
//            logger.info("AD.UserCC -> " + AD_USER_CC);
//            logger.info("AD.UserCCName -> " + AD_USER_CCNAME);
//            logger.info("");

            CAP_AUTHENTICATION_ACTIVATION = prop.getProperty("Mode.Capauthentication");
            logger.info("CAP_AUTHENTICATION_ACTIVATION -> " + CAP_AUTHENTICATION_ACTIVATION);
            
            CC_OBTAINING_ACTIVATION_MODE = prop.getProperty("Mode.CCObtaining");
            logger.info("CC_OBTAINING_ACTIVATION_MODE -> " + CC_OBTAINING_ACTIVATION_MODE);
            
            if (CC_OBTAINING_ACTIVATION_MODE.equalsIgnoreCase(CC_OBTAINING_EXTDB_MODE))
                remoteccplugin = new RemoteccPlugin("conf/config.properties", "conf/sql.properties");
            
        }
        catch (Exception ex)
        {
            logger.error("Parser() - An exception occurred");
            logger.error(ex.getMessage());
            logger.error(ex.getStackTrace());
        }
    }

    public boolean processOneFile(String path, String fileName, boolean updateDB)
    {
        logger.info("Processing file " + fileName);
        System.out.println("-> Processing file " + fileName);

        //Clear actions list
        objectManager.clearActionList();
        //Clear users and printers hash tables
        objectManager.clearHashTables();

        //Open connection with the database
        if (!dbManager.connect())
        {
            logger.error("processOneFile() : Cannot open DataBase connection");
            System.out.println("ERROR: Cannot open DataBase connection");
            return false;
        }

        //Get all the users that actually are inside the database
        List usersList = dbManager.getReportingUsers();
        objectManager.fillReportingUsersHash(usersList);

        //Get all the printers that actually are inside the database
        List mfpList = dbManager.getMFPs();
        objectManager.fillReportingMFPsHash(mfpList);

        //Get all Desconocido's printers that actually are inside the database
        mfpList = dbManager.getUnknownUserMFPs();
        objectManager.fillUnknownUser_ReportingMFPsHash(mfpList);

        /** NUEVO **/
        //Obtener lista de impresoras de cada usuario
        //Rellenar Hash de usuarios - impresoras
        String username;
        User user;
        ListIterator it = usersList.listIterator();
        while(it.hasNext())
        {
            username = (String)it.next();
            mfpList = dbManager.getUserMFPs(username);
            objectManager.fillUser_MFPsHash(username, mfpList);

//            Fill user - cost center info
            if (CC_OBTAINING_ACTIVATION_MODE.equalsIgnoreCase(CC_OBTAINING_CSV_MODE)){
                if (!username.isEmpty()){
                    if (username.split("_").length > 2)
                        user = getUserByID(username.split("_")[2]);
                    else
                        user = getUserByID(username);
                }else
                    user = new User("", "", "", "");
            }else if (CC_OBTAINING_ACTIVATION_MODE.equalsIgnoreCase(CC_OBTAINING_EXTDB_MODE)){
                if (!username.isEmpty()){
                    user= remoteccplugin.getUserByID(username);
                }else
                    user = new User("", "", "", "");
            }
            else if (CC_OBTAINING_ACTIVATION_MODE.equalsIgnoreCase(CC_OBTAINING_AD_MODE)){
                if (!username.isEmpty()) {
//                    user = getADUserByID(username);
                    user = getUserByID(username);   //  TODO: for test (para optimizar)
                } else {
                    user = new User(username, "", "", "");
                }
            }
            else{
                user = new User(username, "", "", "");
            }
            
            objectManager.fillUser_CCHash(user);
            objectManager.fillCC_CCnameHash(user);
        }

        //Get all Cost Centers that actually are inside the database
        List ccList = dbManager.getCCs();
        objectManager.fillReportingCCsHash(ccList);

        List mfpSerial = dbManager.getMFPsAndCompany();
        objectManager.fillMfpserial_companyHash(mfpSerial);
        
        //Close connection with the database
        if (!dbManager.disconnect())
            logger.error("processOneFile() : Cannot close DataBase connection");

        if (!path.isEmpty() && !fileName.isEmpty())
        {
            if (!parseFile(path, fileName))
            {
                logger.error("processOneFile() : Error parsing file " + fileName);
                System.out.println("ERROR: Processing file " + fileName);
                return false;
            }
        }
        
        
        if (updateDB)
        {
            //Update the database with the new information
            return updateDataBase();
        }
        else
            return true;
    }

    private boolean parseFile(String path, String fileName)
    {
        //Open the file to parse
        if (!fileManager.openFile(path, fileName, true))
        {
            logger.error("parseFile() : Cannot open CSV file " + fileName);
            return false;
        }

        //Open connection with the database
        if (!dbManager.connect())
        {
            logger.error("parseFile() : Cannot open DataBase connection");
            System.out.println("ERROR: Cannot open DataBase connection");
            return false;
        }
        
        while (fileManager.hasMoreLines(fileName))
        {
            if(fileManager.readAttribute(fileName, "general#finishState") != null)
            {
                //Create an Action object with all the details of the action
                if (!createReportedAction(fileName))
                {
                    logger.error("parseFile() : Error creating Action from file " + fileName);
                    return false;
                }
            }
        }
        //Close the input file
        if (!fileManager.closeFile(fileName))
            logger.error("parseFile() : Cannot close CSV file " + fileName);
        
        //postscript version OYSHO
        Date date = null;
        try {
            date = new Date();
            long ms = date.getTime();
            ms = ms - 345600000;
            date = new Date(ms);
            date = dateFormat.parse((date.getYear()+1900) + "-" + (date.getMonth()+1) + "-" + date.getDate());
        } catch (ParseException ex) {
            java.util.logging.Logger.getLogger(Parser.class.getName()).log(Level.SEVERE, null, ex);
        }
        if(!dbManager.deletePausedStorageJobs_PerDate(date))
            logger.info("parseFile() : Cannot delete Stored Jobs date<= " + date);
        //fin postscript version OYSHO
        
        //Close connection with the database
        if (!dbManager.disconnect())
            logger.error("parseFile() : Cannot close DataBase connection");
        
        return true;
    }

    private User getUserByID(String username){
        User u = new User(username,"","","");
        
        if (objectManager.getCCUserRegisterHash().containsKey(username)){
            u = (User)objectManager.getCCUserRegisterHash().get(username);
        }
                
        return u;
    }
    
    private boolean obtainNotReportingCounters()
    {
        boolean allOK = true;

        Iterator it = objectManager.getMfpRegisterHash().keySet().iterator();
        while (it.hasNext())
        {
            String mfpName = (String)it.next();
            Printer printer = (Printer)objectManager.getMfpRegisterHash().get(mfpName);

            if (printer.getStatus().equals(Printer.ONLINE) &&
                    (printer.getModelType().equals(Printer.MFP232) || printer.getModelType().equals(Printer.MFP7500)))
            {
                logger.info("Obtaining counters via SNMP for MFP " + printer.getIpAddress() + " (" + printer.getMfpSerial() + ")");
                System.out.println("-> Obtaining counters via SNMP for MFP " + printer.getIpAddress() + " (" + printer.getMfpSerial() + ")");

                if (!networkManager.getCounters(printer))
                {
                    logger.error("obtainNotReportingCounters() : Obtaining counters via SNMP for MFP " + printer.getIpAddress() + " (" + printer.getMfpSerial() + ")");
                    System.out.println("ERROR: Obtaining counters via SNMP for MFP " + printer.getIpAddress() + " (" + printer.getMfpSerial() + ")");
                    allOK = false;
                }
                else
                {
                    try
                    {
                        if (printer.getCountersResume() == null)
                        {
                            objectManager.add2Fail2CommunicateList(printer);
                            printer.setCountersResume(new Resume(printer.getMfpSerial(), null));
                        }

                        if (!createNotReportedAction(printer.getCountersResume()))
                        {
                            logger.error("obtainNotReportingCounters() : Error creating Actions for mfp " + printer.getMfpSerial());
                            allOK = false;
                        }
                        else
                            objectManager.add2NotReportingMFPsHash(printer);
                    }
                    catch (ParseException ex)
                    {
                        logger.error(ex.getMessage());
                        logger.error(ex.getStackTrace());
                        allOK = false;
                    }
                }
            }
        }
        return allOK;
    }

    private void setAndRegisterActionValues(String username, String id, String type, int numPages, String colorMode) throws ParseException
    {
        Action action = new Action();
        Date date = new Date();
        Date insertDate = dateFormat.parse(dateFormat.format(date));
        java.sql.Timestamp insertDateTime = new Timestamp(date.getTime());

        action.setUserName(username);
        action.setMFPserial(id);
        action.setType(type);
        action.setNumPages(numPages);
        action.setColorMode(colorMode);
        action.setDate(insertDate);
        action.setDateTime(insertDateTime);

        objectManager.addAction(action);
        objectManager.updateResumeInfo(action, Resume.resumeType.USER_RESUME);

        //Update the Resume info for the current CC
        if (!objectManager.getUser_CCHash().containsKey(action.getUserName()))
        {
            User user = new User("", "", "", "");
            objectManager.fillUser_CCHash(user);
            objectManager.fillCC_CCnameHash(user);
        }
        objectManager.updateResumeInfo(action, Resume.resumeType.CC_RESUME);

    }

    private boolean createNotReportedAction(Resume resume) throws ParseException
    {
        //Open connection with the database
        if (!dbManager.connect())
        {
            logger.error("createNotReportedAction() : Cannot open DataBase connection");
            return false;
        }

        //Get the last Resume inserted for this mfp (yesterday resume)
        Resume lastResume = dbManager.getLastNotReportingResume(resume.getId());

        //Close connection with the database
        if (!dbManager.disconnect())
            logger.error("createNotReportedAction() : Cannot close DataBase connection");

        //Check that obtained counters are not smaller than last resume counters
        resume.checkResumeValues(lastResume);

        //If BW prints is not 0, create the action and register it
        int numPages = resume.getPrintBW() - lastResume.getPrintBW();
        if (numPages > 0)
        {
            setAndRegisterActionValues("", resume.getId(), "printer.print", numPages, "blackandwhite");
        }

        //If BW copies is not 0, create the action and register it
        numPages = resume.getCopyBW() - lastResume.getCopyBW();
        if (numPages > 0)
        {
            setAndRegisterActionValues("", resume.getId(), "copy", numPages, "blackandwhite");
        }

        //If Color prints is not 0, create the action and register it
        numPages = resume.getPrintColor() - lastResume.getPrintColor();
        if (numPages > 0)
        {
            setAndRegisterActionValues("", resume.getId(), "printer.print", numPages, "color");
        }

        //If Color copies is not 0, create the action and register it
        numPages = resume.getCopyColor() - lastResume.getCopyColor();
        if (numPages > 0)
        {
            setAndRegisterActionValues("", resume.getId(), "copy", numPages, "color");
        }

        //If Sent scans is not 0, create the action and register it
        numPages = resume.getScanSend() - lastResume.getScanSend();
        if (numPages > 0)
        {
            setAndRegisterActionValues("", resume.getId(), "scanner.deliver", numPages, "");
        }

        //If Sent faxes is not 0, create the action and register it
        numPages = resume.getFaxSend() - lastResume.getFaxSend();
        if (numPages > 0)
        {
            setAndRegisterActionValues("", resume.getId(), "fax.transfer", numPages, "");
        }

        //If received faxes is not 0, create the action and register it
        numPages = resume.getFaxReceived() - lastResume.getFaxReceived();
        if (numPages > 0)
        {
            setAndRegisterActionValues("", resume.getId(), "fax.receive", numPages, "blackandwhite");
        }
        return true;
    }

    private boolean createReportedAction(String fileName) {
        
        String timeStamp, type, MFPserial, userName = "", docuName, colorMode, pageSize, destination, sender, company = "", iphost = "";
        Date date, datetime2;
        int totalColor = 0, totalBW = 0, numCopies = 0, numPages = 0,
                numPagesBKsmall = 0, numPagesBKbig = 0,
                numPages1Csmall = 0, numPages1Cbig = 0,
                numpages2Csmall = 0, numPages2Cbig = 0,
                numPagesFCsmall = 0, numPagesFCbig = 0;

        String value = "", jobid = "", username_tmp = "", hostaddress = "";
        String[] storedJob = new String[2];
        Action mfpAction;

        
        try {
            
            timeStamp = fileManager.readAttribute(fileName, "general#occurrenceDate");
            timeStamp = timeStamp.replace("T", " ");
            datetime2 = timeStampFormat.parse(timeStamp);

            java.sql.Timestamp dateTime = new Timestamp(datetime2.getTime());

            date = dateFormat.parse(timeStamp);

            type = fileManager.readAttribute(fileName, "general#originalType").toLowerCase();
            MFPserial = fileManager.readAttribute(fileName, "general#logSourceId").toLowerCase();
            MFPserial = MFPserial.replace("serialno:", "");

            //prueba integridad datos
            String job_tmp = fileManager.readAttribute(fileName, "source_memory#srcMemDocumentId").toLowerCase();
            if (objectManager.getMfpserial_companyHash().containsKey(MFPserial)) {
                
                company = (String) objectManager.getMfpserial_companyHash().get(MFPserial);
                System.out.println("MFP Serial no: " + MFPserial + " have " + company + " as associated company");
                
            } else {
                
                System.out.println("MFP Serial no: " + MFPserial + " doesn't have associated company");
                
            }


            //***** ADDED FOR MASSIMO DUTTI  *********//
            if (CAP_AUTHENTICATION_ACTIVATION.equalsIgnoreCase("")) {
                
                if (!fileManager.readAttribute(fileName, "source_pdl#pdlName").toLowerCase().equalsIgnoreCase("postscript")) { //pcl jobs   
                    
                    userName = fileManager.readAttribute(fileName, "source_pdl#pdlPcLoginName").toLowerCase();
                    if (userName.isEmpty()) {
                        userName = fileManager.readAttribute(fileName, "source_memory#srcMemPcLoginName").toLowerCase();
                    }
                    
                } else { //postscript job
                    
                    if (type.equalsIgnoreCase("printer.pausedocument-storage")) { // stored hold printing event
                        username_tmp = fileManager.readAttribute(fileName, "source_pdl#pdlClientUserName").toLowerCase();
                        jobid = fileManager.readAttribute(fileName, "destination_memory#desMemDocumentId").toLowerCase();
                        hostaddress = fileManager.readAttribute(fileName, "general#hostAddressBody").toLowerCase();
                        if (!dbManager.insertPausedStorageJob(jobid, username_tmp, MFPserial, datetime2, hostaddress)) {
                            logger.error("createReportedAction() : Cannot insert Paused stored job to ddbb");
                        }

                    } else { //no stored hold printing event
                        userName = fileManager.readAttribute(fileName, "source_pdl#pdlClientUserName").toLowerCase();
                    }
                }
                
                
                if (type.equalsIgnoreCase("printer.pausedocument-print")) { // print stored hold printing event
                    
                    if (userName.isEmpty()) {
                        
                        jobid = fileManager.readAttribute(fileName, "source_memory#srcMemDocumentId").toLowerCase();
                        storedJob = dbManager.getPausedStorageJob(jobid, MFPserial);
                        userName = storedJob[0]; //storedJob[0] --> username
                        if (userName != null) {
                            
                            if (!dbManager.deletePausedStorageJob(jobid, MFPserial)) {
                                logger.error("createReportedAction() : Cannot delete Paused stored job: " + jobid);
                            }
                            if (userName.isEmpty()) {
                                iphost = storedJob[1];
                            }
                            
                        } else {
                            iphost = fileManager.getIPHost_StoredJob(jobid, MFPserial);
                            userName = "";
                        }
                    }
                }

            }

            
            if (CAP_AUTHENTICATION_ACTIVATION.equalsIgnoreCase("true")) {
                userName = fileManager.readAttribute(fileName, "general#displayName").toLowerCase();
                if (userName.isEmpty()) {
                    userName = fileManager.readAttribute(fileName, "general#clientNameBody").toLowerCase();
                }
            }

            //***** ADDED FOR MASSIMO DUTTI  *********//
            if (CAP_AUTHENTICATION_ACTIVATION.equalsIgnoreCase("false") && userName.isEmpty()) {
                userName = fileManager.readAttribute(fileName, "source_memory#srcMemUserId").toLowerCase();
            }

            if (userName.equals("(noname)")) {
                userName = "";
            }

            //***** ADDED FOR OYSHO *********//
            if (userName.isEmpty() && iphost.isEmpty()) {
                iphost = fileManager.readAttribute(fileName, "general#hostAddressBody").toLowerCase();
            }

            if (type.contains("storage")) {
                
                docuName = fileManager.readAttribute(fileName, "destination_memory#desMemDocumentName");
                
            } else if (type.contains(".storedocument") || type.equals("printer.secret-print")) {
                
                docuName = fileManager.readAttribute(fileName, "source_memory#srcMemDocumentName");
                
            } //***** ADDED FOR MASSIMO DUTTI  *********//
            else if (!CAP_AUTHENTICATION_ACTIVATION.equalsIgnoreCase("true") && type.contains(".pausedocument-print")) {
                
                docuName = fileManager.readAttribute(fileName, "source_memory#srcMemJobDocumentName");
                
            } else {
                
                docuName = fileManager.readAttribute(fileName, "source_pdl#pdlJobDocumentName");
                
            }

            colorMode = "";

            if (type.contains("scanner.") || type.contains("documentbox.storage")) {
                pageSize = fileManager.readAttribute(fileName, "source_scan#scanOriginalSizeName").toLowerCase();
            } else {
                pageSize = fileManager.readAttribute(fileName, "destination_plot#plotPaperSize").toLowerCase();
            }

            destination = "";
            sender = "";

            if (type.equals("copy") || type.equals("copy.storage") || type.equals("documentbox.storedocument-print")
                    || type.equals("printer.print") || type.equals("printer.secret-print") || type.equals("printer.trial-storage-print")
                    || type.equals("printer.trial-print") || type.equals("printer.pausedocument-print")
                    || type.equals("printer.keepdocument-storage-print") || type.equals("printer.keepdocument-reprint")
                    || type.equals("report") || type.equals("report.state") || type.equals("fax.pctranser")) {
                
                if (type.equals("fax.pctransfer")) {
                    destination = fileManager.readAttribute(fileName, "destination_network#desNetAddress");
                    if (!destination.equals(fileManager.readAttribute(fileName, "destination_network#desNetAddressName"))) {
                        destination = fileManager.readAttribute(fileName, "destination_network#desNetAddressName") + " " + destination;
                    }

                }
                
                //Black and White big size counter value
                value = fileManager.readAttribute(fileName, "destination_plot#plotPrintCountBKa");
                if (!value.isEmpty()) {
                    numPagesBKbig = Integer.parseInt(value);
                }

                //Black and White small size counter value
                value = fileManager.readAttribute(fileName, "destination_plot#plotPrintCountBKb");
                if (!value.isEmpty()) {
                    numPagesBKsmall = Integer.parseInt(value);
                }

                //One color big size counter value
                value = fileManager.readAttribute(fileName, "destination_plot#plotPrintCount1Ca");
                if (!value.isEmpty()) {
                    numPages1Cbig = Integer.parseInt(value);
                }

                //One color small size counter value
                value = fileManager.readAttribute(fileName, "destination_plot#plotPrintCount1Cb");
                if (!value.isEmpty()) {
                    numPages1Csmall = Integer.parseInt(value);
                }

                //Two color big size counter value
                value = fileManager.readAttribute(fileName, "destination_plot#plotPrintCount2Ca");
                if (!value.isEmpty()) {
                    numPages2Cbig = Integer.parseInt(value);
                }

                //Two color small size counter value
                value = fileManager.readAttribute(fileName, "destination_plot#plotPrintCount2Cb");
                if (!value.isEmpty()) {
                    numpages2Csmall = Integer.parseInt(value);
                }

                //Full color big size counter value
                value = fileManager.readAttribute(fileName, "destination_plot#plotPrintCountFCa");
                if (!value.isEmpty()) {
                    numPagesFCbig = Integer.parseInt(value);
                }

                //Full color small size counter value
                value = fileManager.readAttribute(fileName, "destination_plot#plotPrintCountFCb");
                if (!value.isEmpty()) {
                    numPagesFCsmall = Integer.parseInt(value);
                }

                totalBW = numPagesBKsmall + 2 * numPagesBKbig;
                totalColor = numPages1Csmall + 2 * numPages1Cbig + numpages2Csmall + 2 * numPages2Cbig + numPagesFCsmall + 2 * numPagesFCbig;

                //Copies counter value
                value = fileManager.readAttribute(fileName, "destination_plot#plotCopies");
                if (!value.isEmpty()) {
                    numCopies = Integer.parseInt(value);
                }

                //Exception for old mfp models
                if ((totalBW == 0) && (totalColor == 0)) {
                    
                    colorMode = fileManager.readAttribute(fileName, "destination_plot#plotColorMode");
                    value = fileManager.readAttribute(fileName, "destination_plot#plotPrintPages");
                    
                    if (!value.isEmpty() && colorMode.equals("blackandwhite")) {
                        totalBW = Integer.parseInt(value);
                    } else if (!value.isEmpty() && colorMode.equals("fullcolor")) {
                        totalColor = Integer.parseInt(value);
                    }
                }


                if (totalColor == 0 && totalBW != 0) {  //The job is in black and white
                
                    colorMode = "blackandwhite";
                    if (numPagesBKbig > 0) {
                        pageSize = "a3";
                    }

                    mfpAction = new Action(date, type, MFPserial, userName, dateTime, totalBW,
                            numCopies, docuName, colorMode, pageSize, destination, sender, company);

                    registerAction(mfpAction, iphost, job_tmp);
                    
                } else if (totalColor != 0) { //The job is in color, and may contain black and white pages
                
                    colorMode = "color";
                    if (numPages1Cbig > 0 || numPages2Cbig > 0 || numPagesFCbig > 0) {
                        pageSize = "a3";
                    }

                    mfpAction = new Action(date, type, MFPserial, userName, dateTime, totalColor,
                            numCopies, docuName, colorMode, pageSize, destination, sender, company);

                    registerAction(mfpAction, iphost, job_tmp);

                    if (totalBW != 0) { //The job is in color, and contains black and white pages
                    
                        colorMode = "blackandwhite";
                        if (numPagesBKbig > 0) {
                            pageSize = "a3";
                        } else {
                            pageSize = fileManager.readAttribute(fileName, "destination_plot#plotPaperSize").toLowerCase();
                        }

                        mfpAction = new Action(date, type, MFPserial, userName, dateTime, totalBW,
                                numCopies, docuName, colorMode, pageSize, destination, sender, company);

                        registerAction(mfpAction, iphost, job_tmp);
                    }
                }
                
            } else if (type.contains("scanner.") || type.contains("documentbox.") || type.contains("fax.")) {
                
                destination = fileManager.readAttribute(fileName, "destination_network#desNetAddress");

                if (type.contains("scanner.") || type.contains("documentbox.")) {
                    
                    colorMode = fileManager.readAttribute(fileName, "source_scan#scanColorMode").toLowerCase();

                    if (type.equals("scanner.storedocument-forward") || type.equals("documentbox.storedocument-forward")
                            || type.equals("scanner.storedocument-deliver") || type.equals("scanner.storedocument-linkdeliver")) {
                        value = fileManager.readAttribute(fileName, "destination_network#desNetSendPages");
                    } else {
                        value = fileManager.readAttribute(fileName, "source_scan#scanOriginalSidePages");
                    }
                    
                } else if (type.equals("fax.print") || type.equals("fax.receive")
                        || type.equals("fax.receive-deliver") || type.equals("fax.receive-storage")) {
                    
                    colorMode = "blackandwhite";

                    sender = fileManager.readAttribute(fileName, "source_network#srcNetReceiveName");
                    value = fileManager.readAttribute(fileName, "source_network#srcNetReceivePages");
                    
                } else if (type.equals("fax.transfer")) {
                    
                    if (!destination.equals(fileManager.readAttribute(fileName, "destination_network#desNetAddressName"))) {
                        destination = fileManager.readAttribute(fileName, "destination_network#desNetAddressName") + " " + destination;
                    }

                    value = fileManager.readAttribute(fileName, "destination_network#desNetSendPages");
                    
                }

                if (!value.isEmpty()) {
                    numPages = Integer.parseInt(value);
                }

                mfpAction = new Action(date, type, MFPserial, userName, dateTime, numPages,
                        numCopies, docuName, colorMode, pageSize, destination, sender, company);

                registerAction(mfpAction, iphost, job_tmp);
            }
            
            return true;
            
        } catch (ParseException ex) {
            logger.error("createReportedAction() : Trying to parse ID " + fileManager.readAttribute(fileName, "general#logLinkId")
                    + fileManager.readAttribute(fileName, "general#occurrenceDate"));
            logger.error(ex.getMessage());
            logger.error(ex.getStackTrace());
            return false;
        }
        
    }

    private void registerAction(Action mfpAction, String iphost, String job)
    {
        String iphost_aux = "";
        User user;
        System.out.println("Evento de " + mfpAction.getUserName());
        
        if (!iphost.equalsIgnoreCase("")) {
            iphost_aux = "_" + iphost;
        }
        if (mfpAction.getUserName().isEmpty()) {
            if (iphost_aux.isEmpty()) {
                System.out.println(" iphost_aux type: " + mfpAction.getType() + "jobid: " + job);
            }
            System.out.println(" user --> mfpAction.getUserName(): " + "noname_" + mfpAction.getCompany() + iphost_aux);
            mfpAction.setUserName("noname_" + mfpAction.getCompany() + iphost_aux);
        }
        
        
        if (!objectManager.getUser_CCHash().containsKey(mfpAction.getUserName())) {
            if (CC_OBTAINING_ACTIVATION_MODE.equalsIgnoreCase(CC_OBTAINING_CSV_MODE)) {
                if (mfpAction.getUserName().isEmpty()) {
                    user = new User("", "", "", "");
                } else {
                    if (!iphost.equalsIgnoreCase("") && mfpAction.getUserName().contains(iphost)) {
                        user = getUserByID(iphost);
                    } else {
                        user = getUserByID(mfpAction.getUserName());
                    }
                }
            } else if (CC_OBTAINING_ACTIVATION_MODE.equalsIgnoreCase(CC_OBTAINING_EXTDB_MODE)) {
                if (!mfpAction.getUserName().isEmpty()) {
                    user = remoteccplugin.getUserByID(mfpAction.getUserName());
                } else {
                    user = new User("", "", "", "");
                }
            } else if (CC_OBTAINING_ACTIVATION_MODE.equalsIgnoreCase(CC_OBTAINING_AD_MODE)) {
                if (!mfpAction.getUserName().isEmpty()) {
//                    user = getADUserByID(mfpAction.getUserName());
                    user = getUserByID(mfpAction.getUserName());    //  TODO: for test (para optimizar)
                } else {
                    user = new User(mfpAction.getUserName(), "", "", "");
                }
            } else {
                if (mfpAction.getUserName().isEmpty()) {
                    user = new User("", "", "", "");
                } else {
                    if (!iphost.equalsIgnoreCase("") && mfpAction.getUserName().contains(iphost)) {
                        user = getUserByID(iphost);
                    } else {
                        user = getUserByID(mfpAction.getUserName());
                    }
                }
            }

            objectManager.fillUser_CCHash(user);
            objectManager.fillCC_CCnameHash(user);
        }
        
        if (iphost != null && iphost.isEmpty()) {
            System.out.println("CECO=" + (objectManager.getUser_CCHash().get(mfpAction.getUserName()).getCostCenter()));
        } else if (iphost != null) {
            if (objectManager.getUser_CCHash().get(iphost) != null) {
                System.out.println("CECO iphost =" + (objectManager.getUser_CCHash().get(iphost).getCostCenter()));//get(iphost).getCostCenter()));
            } else {
                System.out.println("CECO iphost =" + (objectManager.getUser_CCHash().get(mfpAction.getUserName()).getCostCenter()));//get(iphost).getCostCenter()));
            }
        }
            
        //Add the Action object to the action List
        objectManager.addAction(mfpAction);
        //Update the Resume info for the current MFP
        objectManager.updateResumeInfo(mfpAction, Resume.resumeType.MFP_RESUME);
        //Update the Resume info for the current User
        objectManager.updateResumeInfo(mfpAction, Resume.resumeType.USER_RESUME);

        //If it is "desconocido" user, update the Resume for the current Desconocido MFP
        if (mfpAction.getUserName().contains("noname")) {
            objectManager.updateResumeInfo(mfpAction, Resume.resumeType.DESCONOCIDO_MFP_RESUME);
        }

        //** NUEVO **//
        //Actualizar el Resumen para la dupla Usuario - MFP actual
        objectManager.updateResumeInfo(mfpAction, Resume.resumeType.USER_MFP_RESUME);
        if (!CC_OBTAINING_ACTIVATION_MODE.equalsIgnoreCase("")) {
            //Update the Resume info for the current CC
            objectManager.updateResumeInfo(mfpAction, Resume.resumeType.CC_RESUME);
        }
        
    }

    private boolean updateDataBase()
    {
        boolean allOK = true;
        
        final boolean[] THREAD_STATUS = new boolean[11];

        logger.info("Updating the Database");
        System.out.println("-> Updating the Database");

        //Open connection with the database if it is not openned before
        if (!dbManager.isConnected())
            if (!dbManager.connect())
            {
                logger.error("updateDataBase() : Cannot open DataBase connection");
                System.out.println("ERROR: Cannot open DataBase connection");
                return false;
            }

        try
        {
            Thread General_Thread = new Thread(new Runnable()
            {
                public void run() {
                    //Insert all the elements from the actionList to the dataBase
                    THREAD_STATUS[0] = dbManager.insertActions(objectManager.getActionList());
                    if (!THREAD_STATUS[0])
                        logger.error("updateDataBase() : Error inserting Actions to DataBase");
                }
            });
            General_Thread.start();

            Thread NewUsers_Thread = new Thread(new Runnable()
            {
                public void run() {
                    String key;
                    boolean allOK = true;
                    //Insert all the elements from newUsers Hash table to the dataBase
                    Iterator it = objectManager.getNewReportingUsersHash().keySet().iterator();
                    while (it.hasNext() && allOK)
                    {
                        //Obtain the key of the entry
                        key = (String) it.next();
                        //Insert the list of Resumes from the key to the dataBase
                        if (!(allOK = dbManager.insertNewResumeList((List) objectManager.getNewReportingUsersHash().get(key), Resume.resumeType.USER_RESUME)))
                            logger.error("updateDataBase() : Error inserting Counters of user " + key + " to DataBase");
                    }
                    THREAD_STATUS[1] = allOK;
                }
            });
            NewUsers_Thread.start();
            
            Thread NewPrinters_Thread = new Thread(new Runnable() 
            {
                public void run() {
                    String key;
                    boolean allOK = true;
                    //Insert all the elements from newPrinters Hash table to the dataBase
                    Iterator it = objectManager.getNewReportingMFPsHash().keySet().iterator();
                    while (it.hasNext() && allOK)
                    {
                        //Obtain the key of the entry
                        key = (String) it.next();
                        //Insert the list of Resumes from the key to the dataBase
                        if (allOK = dbManager.insertNewResumeList((List) objectManager.getNewReportingMFPsHash().get(key), Resume.resumeType.MFP_RESUME))
                            dbManager.setReportingMFP(key, true);
                        else
                            logger.error("updateDataBase() : Error inserting Counters of mfp " + key + " to DataBase");
                    }
                    THREAD_STATUS[2] = allOK;
                }
            });
            NewPrinters_Thread.start();

            Thread DesconocidoNewPrinters_Thread = new Thread(new Runnable()
            {
                public void run() {
                    String key;
                    boolean allOK = true;
                    //Insert all the elements from newPrinters Hash table to the dataBase
                    Iterator it = objectManager.getUnknownUser_NewReportingMFPsHash().keySet().iterator();
                    while (it.hasNext() && allOK)
                    {
                        //Obtain the key of the entry
                        key = (String) it.next();
                        //Insert the list of Resumes from the key to the dataBase
                        if (!(allOK = dbManager.insertNewResumeList((List) objectManager.getUnknownUser_NewReportingMFPsHash().get(key), Resume.resumeType.DESCONOCIDO_MFP_RESUME)))
                            logger.error("updateDataBase() : Error inserting \"desconocido\" Counters for mfp " + key + " to DataBase");
                    }
                    THREAD_STATUS[3] = allOK;
                }
            });
            DesconocidoNewPrinters_Thread.start();

            //** NUEVO **//
            Thread NewUserPrinters_Thread = new Thread(new Runnable()
            {
                public void run() {
                    String key, username;
                    Map mfpMap;
                    Iterator mfpIt;
                    boolean allOK = true;
                    //Insert all the elements from newPrinters Hash table to the dataBase
                    Iterator userIt = objectManager.getUserMFPsHash().keySet().iterator();
                    while (userIt.hasNext() && allOK)
                    {
                        username = (String)userIt.next();
                        mfpMap = (Map) objectManager.getUserMFPsHash().get(username).elementAt(ObjectManager.NEW_MFP);
                        mfpIt = mfpMap.keySet().iterator();
                        while (mfpIt.hasNext() && allOK)
                        {
                            //Obtain the key of the entry
                            key = (String) mfpIt.next();
                            //Insert the list of Resumes from the key to the dataBase
                            if (!(allOK = dbManager.insertNewUserMfpResumeList(username, (List) mfpMap.get(key))))
                                logger.error("updateDataBase() : Error inserting " + username + " Counters for mfp " + key + " to DataBase");
                        }
                    }
                    THREAD_STATUS[4] = allOK;
                }
            });
            NewUserPrinters_Thread.start();

            Thread Users_Thread = new Thread(new Runnable() 
            {
                public void run() {
                    String key;
                    boolean allOK = true;
                    //Insert all the elements from Users Hash table to the dataBase
                    Iterator it = objectManager.getReportingUsersHash().keySet().iterator();
                    while (it.hasNext() && allOK)
                    {
                        //Obtain the key of the entry
                        key = (String) it.next();
                        //Insert the list of Resumes from the key to the dataBase
                        if (!(allOK = dbManager.updateResumeList(key, (List) objectManager.getReportingUsersHash().get(key), Resume.resumeType.USER_RESUME)))
                            logger.error("updateDataBase() : Error inserting Counters of user " + key + " to DataBase");
                    }
                    THREAD_STATUS[5] = allOK;
                }
            });
            Users_Thread.start();
            
            Thread Printers_Thread = new Thread(new Runnable() 
            {
                public void run() {
                    String key;
                    boolean allOK = true;
                    //Insert all the elements from Printers Hash table to the dataBase
                    Iterator it = objectManager.getReportingMFPsHash().keySet().iterator();
                    while (it.hasNext() && allOK)
                    {
                        //Obtain the key of the entry
                        key = (String) it.next();
                        //Insert the list of Resumes from the key to the dataBase
                        if (!(allOK = dbManager.updateResumeList(key, (List) objectManager.getReportingMFPsHash().get(key), Resume.resumeType.MFP_RESUME)))
                            logger.error("updateDataBase() : Error inserting Counters of mfp " + key + " to DataBase");
                    }
                    THREAD_STATUS[6] = allOK;
                }
            });
            Printers_Thread.start();

            Thread DesconocidoPrinters_Thread = new Thread(new Runnable()
            {
                public void run() {
                    String key;
                    boolean allOK = true;
                    //Insert all the elements from Printers Hash table to the dataBase
                    Iterator it = objectManager.getUnknownUser_ReportingMFPsHash().keySet().iterator();
                    while (it.hasNext() && allOK)
                    {
                        //Obtain the key of the entry
                        key = (String) it.next();
                        //Insert the list of Resumes from the key to the dataBase
                        if (!(allOK = dbManager.updateResumeList(key, (List) objectManager.getUnknownUser_ReportingMFPsHash().get(key), Resume.resumeType.DESCONOCIDO_MFP_RESUME)))
                            logger.error("updateDataBase() : Error inserting \"desconocido\" Counters for mfp " + key + " to DataBase");
                    }
                    THREAD_STATUS[7] = allOK;
                }
            });
            DesconocidoPrinters_Thread.start();

            Thread UserPrinters_Thread = new Thread(new Runnable()
            {
                public void run() {
                    String key, username;
                    Map mfpMap;
                    Iterator mfpIt;
                    boolean allOK = true;
                    //Insert all the elements from newPrinters Hash table to the dataBase
                    Iterator userIt = objectManager.getUserMFPsHash().keySet().iterator();
                    while (userIt.hasNext() && allOK)
                    {
                        username = (String)userIt.next();
                        mfpMap = (Map) objectManager.getUserMFPsHash().get(username).elementAt(ObjectManager.EXISTENT_MFP);
                        mfpIt = mfpMap.keySet().iterator();
                        while (mfpIt.hasNext() && allOK)
                        {
                            //Obtain the key of the entry
                            key = (String) mfpIt.next();
                            //Insert the list of Resumes from the key to the dataBase
                            if (!(allOK = dbManager.updateUserMfpResumeList(username, key, (List) mfpMap.get(key))))
                                logger.error("updateDataBase() : Error inserting " + username + " Counters for mfp " + key + " to DataBase");
                        }
                    }
                    THREAD_STATUS[8] = allOK;
                }
            });
            UserPrinters_Thread.start();

            Thread NewCostCenter_Thread = new Thread(new Runnable()
            {
                public void run() {
                    String key;
                    boolean allOK = true;
                    //Insert all the elements from newCost Center Hash table to the dataBase
                    Iterator it = objectManager.getNewReportingCCHash().keySet().iterator();
                    while (it.hasNext() && allOK)
                    {
                        //Obtain the key of the entry
                        key = (String) it.next();
                        //Insert the list of Resumes from the key to the dataBase
                        if (!(allOK = dbManager.insertNewResumeList((List) objectManager.getNewReportingCCHash().get(key), Resume.resumeType.CC_RESUME)))
                            logger.error("updateDataBase() : Error inserting Counters of CC " + key + " to DataBase");
                    }
                    THREAD_STATUS[9] = allOK;
                }
            });
            NewCostCenter_Thread.start();

            Thread CostCenter_Thread = new Thread(new Runnable()
            {
                public void run() {
                    String key;
                    boolean allOK = true;
                    //Insert all the elements from Cost Center Hash table to the dataBase
                    Iterator it = objectManager.getReportingCCHash().keySet().iterator();
                    while (it.hasNext() && allOK)
                    {
                        //Obtain the key of the entry
                        key = (String) it.next();
                        //Insert the list of Resumes from the key to the dataBase
                        if (!(allOK = dbManager.updateResumeList(key, (List) objectManager.getReportingCCHash().get(key), Resume.resumeType.CC_RESUME)))
                            logger.error("updateDataBase() : Error inserting Counters of CC " + key + " to DataBase");
                    }
                    THREAD_STATUS[10] = allOK;
                }
            });
            CostCenter_Thread.start();

            DesconocidoNewPrinters_Thread.join();
            NewCostCenter_Thread.join();
            CostCenter_Thread.join();
            NewPrinters_Thread.join();
            DesconocidoPrinters_Thread.join();
            NewUserPrinters_Thread.join();
            Printers_Thread.join();
            NewUsers_Thread.join();
            Users_Thread.join();
            UserPrinters_Thread.join();
            General_Thread.join();

            for (int i = 0; i < THREAD_STATUS.length; i++)
                allOK = allOK & THREAD_STATUS[i];
        }
        catch (InterruptedException ex)
        {
            logger.error(ex.getMessage());
            logger.error(ex.getStackTrace());
            allOK = false;
        }
        finally
        {
            //Close connection with the database
            if (!dbManager.disconnect())
                logger.error("updateDataBase() : Cannot close DataBase connection");

            if (!allOK)
            {
                logger.error("updateDataBase() : Updating the DataBase");
                System.out.println("ERROR: Updating the DataBase");
            }
            return allOK;
        }
    }

    private boolean insertNotReportingCounters2DB()
    {
        String key;
        boolean allOK = true;
        Printer printer;

        logger.info("Updating the Database for Not Reporting MFP(s)");
        System.out.println("-> Updating the Database for Not Reporting MFP(s)");

        //Open connection with the database if it is not openned before
        if (!dbManager.isConnected())
            if (!dbManager.connect())
            {
                logger.error("insertNotReportingCounters2DB() : Cannot open DataBase connection");
                System.out.println("ERROR: Cannot open DataBase connection");
                return false;
            }

        //Insert all the elements from Printers Hash table to the dataBase
        Iterator it = objectManager.getNotReportingMFPsHash().keySet().iterator();
        while (it.hasNext() && allOK)
        {
            //Obtain the key of the entry
            key = (String) it.next();
            printer = (Printer)objectManager.getNotReportingMFPsHash().get(key);
            //Insert the list of Resumes from the key to the dataBase
            if (allOK = dbManager.insertNotReportingResume(printer.getCountersResume(), Resume.resumeType.MFP_RESUME))
            {
                dbManager.setReportingMFP(printer.getMfpSerial(), true);
                if (allOK = dbManager.insertNotReportingResume(printer.getCountersResume(), Resume.resumeType.DESCONOCIDO_MFP_RESUME))
                {
                    if (!(allOK = dbManager.insertNotReportingResume(printer.getCountersResume(), Resume.resumeType.USER_MFP_RESUME)))
                        logger.error("insertNotReportingCounters2DB() : Error inserting \"desconocido\" - " + key + " Counters to DataBase");
                }
                else
                    logger.error("insertNotReportingCounters2DB() : Error inserting \"desconocido\" Counters for mfp " + key + " to DataBase");
            }
            else
                logger.error("insertNotReportingCounters2DB() : Error inserting Counters for mfp " + key + " to DataBase");
        }
                
        //Close connection with the database
        if (!dbManager.disconnect())
            logger.error("insertNotReportingCounters2DB() : Cannot close DataBase connection");

        if (!allOK)
        {
            logger.error("insertNotReportingCounters2DB() : Updating the Database for Not Reporting MFP(s)");
            System.out.println("ERROR: Updating the Database for Not Reporting MFP(s)");
        }
        return allOK;

    }

    public boolean processFolder()
    {
        logger.info("processFolder()");
        
        Date date = null;
        String path, fileName = "";
        boolean allOK = true;
        List logFiles = null;
        boolean response = true;
       
        //  TODO: for Streamline BD test
        if (!checkMFPs())
        {
            logger.error("processFolder() : Processing MFPs List DB");
            System.out.println("ERROR: Processing MFPs List DB");
        }
        
        getStreamlineCounters();
//        if (!getStreamlineCounters())
//        {
//            logger.error("getStreamlineCounters() : Getting Streamline Counters");
//            System.out.println("ERROR: Getting Streamline Counters");
//        }
        
        //  FIN: for Streamline BD test

        
        
        System.out.println("CSV_CC_OBTAINING_ACTIVATION: " + CC_OBTAINING_ACTIVATION_MODE);
        if (CC_OBTAINING_ACTIVATION_MODE.equalsIgnoreCase(CC_OBTAINING_CSV_MODE) && !checkCCFiles())
        {
            logger.error("processFolder() : Procesing CC " + CSV_CCUSER_FOLDER + " User csv file");
            System.out.println("ERROR: Processing CC " + CSV_CCUSER_FOLDER + " User csv file");
        }
        else if (CC_OBTAINING_ACTIVATION_MODE.equalsIgnoreCase(CC_OBTAINING_EXTDB_MODE))
        {
            logger.info("processFolder() : We will get CC from external DDBB");
            System.out.println("INFO: We will get CC from external DDBB");
        }
        else if (CC_OBTAINING_ACTIVATION_MODE.equalsIgnoreCase(CC_OBTAINING_AD_MODE)) {
            if (!checkCCActiveDirectory()) {
                logger.error("processFolder() : Obtaining CC info from Active Directory");
                System.out.println("ERROR: Obtaining CC info from Active Directory");
            }
        }
        else
        {
            logger.info("processFolder() : Obtention CC disabled");
            System.out.println("INFO: Obtention CC disabled");
        }
        
        
        boolean onRemote = !networkManager.isJobLogOnLocalMachine();

        if (onRemote)
        {
            logger.info("JobLog file is on remote host machine");
            System.out.println("-> JobLog file is on remote host machine");
            path = networkManager.getDefaultDestinationPath();
        }
        else
        {
            logger.info("JobLog file is on local machine");
            System.out.println("-> JobLog file is on local machine");
            path = networkManager.getJobLogPath();
        }
        

        logFiles = networkManager.getLogFileNames(onRemote);
        ListIterator it = logFiles.listIterator();
        if (logFiles.size() != 0)
        {
            while (it.hasNext() && allOK)
            {
                fileName = it.next().toString();
                if (onRemote)
                {
                    if (networkManager.downloadLogFile(fileName))
                    {
                        if (processOneFile(path, fileName, it.hasNext()))
                            networkManager.deleteLogFile(fileName);
                        else
                            allOK = false;
                    }
                    else
                        allOK = false;
                }
                else
                {
                    if (processOneFile(path, fileName, it.hasNext()))
                        networkManager.backupLogFile(fileName);
                    else
                        allOK = false;

                    if (!it.hasNext() && allOK)
                        networkManager.cleanLogFolder();
                }
            }
        }
        else
        {
            allOK = processOneFile("", "", false);
        }
        
        //** proceso de borrado automatico de los PausedStorage events caducados ***/
        //Open connection with the database
        if (!dbManager.connect())
        {
            logger.error("parseFile() : Cannot open DataBase connection");
            System.out.println("ERROR: Cannot open DataBase connection");
            return false;
        }


        try {
            date = new Date();
            long ms = date.getTime();
            ms = ms - 345600000; //4 dias en ms
            date = new Date(ms);
            date = dateFormat.parse((date.getYear()+1900) + "-" + (date.getMonth()+1) + "-" + date.getDate());
        } catch (ParseException ex) {
            java.util.logging.Logger.getLogger(Parser.class.getName()).log(Level.SEVERE, null, ex);
        }
        if(!dbManager.deletePausedStorageJobs_PerDate(date))
            logger.info("parseFile() : Cannot delete Stored Jobs date<= " + date);
        
        //Close connection with the database
        if (!dbManager.disconnect())
            logger.error("parseFile() : Cannot close DataBase connection");
        
       //** Fin proceso de borrado automatico de los PausedStorage events caducados***/
        
        if (allOK)
        {
            if (!(allOK = fileManager.createFailedMFPsLog(objectManager.getFail2CommunicateList())))
            {
                logger.error("processFolder() : PARSER has encountered problems creating Failed MFPs log file. The file is not created.");
                System.out.println("ERROR: PARSER has encountered problems creating Failed MFPs log file. The file is not created.");
                response = false;
            }

            if (allOK = updateDataBase())
            {
                logger.info("processFolder() : PARSER has finished WITHOUT ERRORS. " + (logFiles.size()) + " files parsed CORRECTLY.");
                System.out.println("-> PARSER has finished WITHOUT ERRORS. " + (logFiles.size()) + " files parsed CORRECTLY.");
            }
            else
            {
                logger.error("processFolder() : PARSER has finished WITH ERRORS.");
                System.out.println("ERROR: PARSER has finished WITH ERRORS.");
                response = false;
            }
        }
        else
        {
            logger.error("processFolder() : PARSER has finished WITH ERRORS. Stopped at file " + fileName);
            System.out.println("ERROR: PARSER has finished WITH ERRORS. Stopped at file " + fileName);
            response = false;
        }
        return response;
    }
    
    public boolean checkTotals(Date today)
    {
        boolean bReturn1 = true, bReturn2 = true;
        //Open connection with the database
        if (!dbManager.connect())
        {
            logger.error("processOneFile() : Cannot open DataBase connection");
            System.out.println("ERROR: Cannot open DataBase connection");
            return false;
        }

        //Known User Counters checking
        if(!dbManager.checkKnownUserCounters(today))
        {
            logger.error("checkKnownUserCounters() : Counters validation failed");
            System.out.println("ERROR: Counters validation failed");
            bReturn1 = false;
        }
        
        //Unknown User Counters checking
        if(!dbManager.checkUnknownUserCounters(today))
        {
            logger.error("checkUnknownUserCounters() : Counters validation failed");
            System.out.println("ERROR: Counters validation failed");
            bReturn2 = false;
        }
        
        //Close connection with the database
        if (!dbManager.disconnect())
            logger.error("processOneFile() : Cannot close DataBase connection");
        
        return (bReturn1 & bReturn2);
    }

    private void insertMFP(String mfpserial){
        if (!dbManager.connect())
        {
            logger.error("checkMasterFile() : Cannot open DataBase connection");
            System.out.println("ERROR: Cannot open DataBase connection");
        }
        
        boolean b = dbManager.insertMFP(mfpserial);
        
        if (!dbManager.disconnect())
            logger.error("checkMasterFile() : Cannot close DataBase connection");
    }
    
    private boolean checkMFPs()
    {
        Printer p;
        List mfplist = null;

        logger.info("Checking Remote MFPs List");
        System.out.println("-> Checking Remote MFPs List");

                
        MFPIncluder includer;
        try {
            includer = new MFPIncluder("conf/config.properties","conf/sql.properties");
            
            mfplist = includer.getStreamlineMFPs();
            
        } catch (FileNotFoundException ex) {
            java.util.logging.Logger.getLogger(Parser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(Parser.class.getName()).log(Level.SEVERE, null, ex);
        }

        //Open connection with the database
        if (!dbManager.connect())
        {
            System.out.println("ERROR: Cannot open DataBase connection");
            return false;
        }
        
        objectManager.setMfpRegisterHash(dbManager.getRegisteredMFPs());
        
        List companiesList = dbManager.getCompaniesWebUsers();
        System.out.println("companiesList size: " + companiesList.size());
        
        ListIterator it = mfplist.listIterator();
        while(it.hasNext())
        {
            p = new Printer();
            p = (Printer)it.next();
            if (!objectManager.getMfpRegisterHash().containsKey(p.getMfpSerial()))
            {
                logger.info("Remote comunication has new MFP " + p.getIpAddress() + " (" + p.getMfpSerial() + ")");
                System.out.println("-> Remote comunication has new MFP " + p.getIpAddress() + " (" + p.getMfpSerial() + ")");
                objectManager.add2MfpRegisterHash(p);
            }
            else if (!p.equals((Printer)objectManager.getMfpRegisterHash().get(p.getMfpSerial())))
            {
                logger.info("Remote comunication  has updated info for MFP " + p.getIpAddress() + " (" + p.getMfpSerial() + ")");
                System.out.println("-> Remote comunication has updated info for MFP " + p.getIpAddress() + " (" + p.getMfpSerial() + ")");
                objectManager.update2MfpRegisterHash(p);
            }

            if (!companiesList.contains(p.getCompany())){
                if (!dbManager.insertCompany(p.getCompany()))
                    logger.error("insertCompany() : error inserting company to db");
                else
                    companiesList = dbManager.getCompaniesWebUsers();
            }
            
        }

        //Close connection with the database
        if (!dbManager.disconnect())
            System.err.println("checkMasterFile() : Cannot close DataBase connection");

        return updateMfpRegisterOnDB();
    }

     private boolean checkCCFiles()
    {
        List files = new LinkedList();
        logger.info("Checking " + CSV_CCUSER_FOLDER + " cc files");
        System.out.println("-> Checking " + CSV_CCUSER_FOLDER + " cc files");

        files = getFileNames(CSV_CCUSER_FOLDER);
        Iterator it = files.iterator();
        while(it.hasNext()){
        
            String strFile = it.next().toString();
            if (!fileManager.openFile("", strFile, false))
            {
                logger.error("checkCCFiles() : Cannot open " + strFile + " file");
                System.out.println("ERROR: Cannot open " + strFile + " file");
                return false;
            }

            while (fileManager.hasMoreLines(strFile))
            {
                User u = new User("", "", "", "");
                u.setUsername(fileManager.readAttribute(strFile, "USERNAME").toLowerCase());
                u.setCostCenter(fileManager.readAttribute(strFile, "CC"));
                u.setDepartment(fileManager.readAttribute(strFile, "CCNAME"));
                u.setFullname(fileManager.readAttribute(strFile, "FULLNAME"));
                
                if (!objectManager.getCCUserRegisterHash().containsKey(u.getUsername()))
                {
                    logger.info(strFile + " CC file has new user " + u.getUsername() + " ( CC: " + u.getCostCenter()+ ")");
                    System.out.println("-> " + strFile + " master file has new MFP " + u.getUsername() + " ( CC: " +  u.getCostCenter() + ")");
                    objectManager.add2CCUserHash(u);
                }
//                else if (!p.equals((Printer)objectManager.getMfpRegisterHash().get(p.getMfpSerial())))
//                {
//                    logger.info(MFP_INVENTORY_FILE + " master file has updated info for MFP " + p.getIpAddress() + " (" + p.getMfpSerial() + ")");
//                    System.out.println("-> " + MFP_INVENTORY_FILE + " master file has updated info for MFP " + p.getIpAddress() + " (" + p.getMfpSerial() + ")");
//                    objectManager.update2MfpRegisterHash(p);
//                }
            }
            //Close the input file
            if (!fileManager.closeFile(strFile)){
                logger.error("checkCCFiles() : Cannot close " + strFile + " file");
                return false;
            }
        }

        return true;
    }
     
     private boolean checkCCActiveDirectory()
     {
         logger.debug("checkCCActiveDirectory() - INI");
         
         String provider = "ldap://" + AD_HOST + ":" + AD_PORT;
         Hashtable env = new Hashtable(11);

         env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
         env.put(Context.PROVIDER_URL, provider);
         env.put("com.sun.jndi.ldap.connect.timeout", "10000");

         if (AD_AUTHSIMPLE.equalsIgnoreCase("true")) {
             System.out.println("Authentication: SIMPLE");
             env.put(Context.SECURITY_AUTHENTICATION, "simple");
             env.put(Context.SECURITY_PRINCIPAL, AD_USER);
             env.put(Context.SECURITY_CREDENTIALS, AD_PASS);
         } 
         else {
             System.out.println("Authentication: NONE");
             env.put(Context.SECURITY_AUTHENTICATION, "none");
         }
         
         try {
             System.out.println("Creating initial context...");
             DirContext ctx = new InitialDirContext(env);
             System.out.println("Initial context created");

             // TODO: ver que filtro aplicamos (Search Query)
             NamingEnumeration namingEnum = ctx.search(AD_BASE, AD_FILTER, getSimpleSearchControls());

             while (namingEnum.hasMore()) {
                 SearchResult sRes = (SearchResult) namingEnum.next();
                 Attributes attrs = sRes.getAttributes();
                 
                 User u = new User("", "", "", "");
                 try {
                     if (attrs.get(AD_USER_USERNAME) != null) {
                         String strtmp = parseADAttribute(attrs.get(AD_USER_USERNAME).toString());
                         u.setUsername(strtmp);
                         logger.debug("AD_USER_USERNAME -> " + strtmp);
                     } else {
                         logger.debug("AD_USER_USERNAME -> NULL");
                     }
                     if (attrs.get(AD_USER_CC) != null) {
                         String strtmp = parseADAttribute(attrs.get(AD_USER_CC).toString());
                         u.setCostCenter(strtmp);
                         logger.debug("AD_USER_CC -> " + strtmp);
                     } else {
                         logger.debug("AD_USER_CC -> NULL");
                     }
                     if (attrs.get(AD_USER_CCNAME) != null) {
                         String strtmp = parseADAttribute(attrs.get(AD_USER_CCNAME).toString());
                         u.setDepartment(strtmp);
                         logger.debug("AD_USER_CCNAME -> " + strtmp);
                     } else {
                         logger.debug("AD_USER_CCNAME -> NULL");
                     }
                     if (attrs.get(AD_USER_FULLNAME) != null) {
                         String strtmp = parseADAttribute(attrs.get(AD_USER_FULLNAME).toString());
                         u.setFullname(strtmp);
                         logger.debug("AD_USER_FULLNAME -> " + strtmp);
                     } else {
                         logger.debug("AD_USER_FULLNAME -> NULL");
                     }
                 } catch (Exception e) {
                     e.printStackTrace();
                 }
                 
                 if (!objectManager.getCCUserRegisterHash().containsKey(u.getUsername()))
                 {
                     logger.info(AD_HOST + " ActiveDirectory has new user " + u.getUsername() + " ( CC: " + u.getCostCenter() + ")");
                     System.out.println("-> " + AD_HOST + " ActiveDirectory has new user " + u.getUsername() + " ( CC: " + u.getCostCenter() + ")");
                     objectManager.add2CCUserHash(u);
                 }
             }
             logger.debug("checkCCActiveDirectory() - END");
             
             return true;
             
         } catch (Exception e) {
             logger.error("checkCCActiveDirectory() : An error has ocurred: " + e);
             logger.debug("checkCCActiveDirectory() - END");
             System.out.println("checkCCActiveDirectory() : An error has ocurred: " + e);
             e.printStackTrace();
             return false;
         }
     }
     
/*     private User getADUserByID(String name)
     {
         String provider = "ldap://" + AD_HOST + ":" + AD_PORT;
         Hashtable env = new Hashtable(11);

         env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
         env.put(Context.PROVIDER_URL, provider);
         env.put("com.sun.jndi.ldap.connect.timeout", "10000");

         if (AD_AUTHSIMPLE.equalsIgnoreCase("true")) {
             env.put(Context.SECURITY_AUTHENTICATION, "simple");
             env.put(Context.SECURITY_PRINCIPAL, AD_USER);
             env.put(Context.SECURITY_CREDENTIALS, AD_PASS);
         } 
         else {
             env.put(Context.SECURITY_AUTHENTICATION, "none");
         }
         
         try {
             DirContext ctx = new InitialDirContext(env);

             // TODO: reemplazar el nombre de los campos por los del AD del cliente
             String filter = "(|(name="+name+")(cn="+name+")(sn="+name+")(displayName="+name+")(givenName="+name+")(dmdName="+name+")(sAMAccountName="+name+"))";
             
             NamingEnumeration namingEnum = ctx.search(AD_BASE, filter, getSimpleSearchControls());

             if (namingEnum.hasMore()) {
                 SearchResult sRes = (SearchResult) namingEnum.next();
                 Attributes attrs = sRes.getAttributes();
                 
                 User user = new User("", "", "", "");
                 // TODO: debemos saber a que campos del AD se corresponden el nombre y CC del user
                 user.setUsername(attrs.get(AD_USER_USERNAME).toString());
//                 user.setCostCenter(attrs.get(AD_USER_CC).toString());
//                 user.setDepartment(attrs.get(AD_USER_CCNAME).toString());
//                 user.setFullname(attrs.get(AD_USER_FULLNAME).toString());
                 
                 return user;
             }
             else
             {
                 return new User(name, "", "", "");
             }
             
         } catch (Exception e) {
             logger.error("getADUserByID() : An error has ocurred: " + e);
             System.out.println("getADUserByID() : An error has ocurred: " + e);
             return new User(name, "", "", "");
         }
     }*/
     
     private static SearchControls getSimpleSearchControls() {
        SearchControls searchControls = new SearchControls();
        searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
        searchControls.setTimeLimit(30000);
        return searchControls;
    }
     
    public List getFileNames(String spoolPath)
    {
        File[] spoolFiles = null;
        List filesList = new ArrayList();
        File f = new File(spoolPath);
        spoolFiles = f.listFiles();

        for (int i = 0; i < spoolFiles.length; i++)
        { 
            logger.info("Parser process CSV Files: " + spoolFiles[i].getName());
            filesList.add(spoolPath+"/"+spoolFiles[i].getName());          
        }

        return filesList;
    }
    
    private boolean updateMfpRegisterOnDB()
    {
        boolean allOK = true;

        final boolean[] THREAD_STATUS = new boolean[2];

        if (objectManager.getINSERT_mfpRegisterHash().size() > 0)
        {
            logger.info("Inserting new MFP(s) from Remote communication to the MFP_REGISTER on DataBase");
            System.out.println("-> Inserting new MFP(s) from Remote communication  to the MFP_REGISTER on DataBase");
        }
        if (objectManager.getUPDATE_mfpRegisterHash().size() > 0)
        {
            logger.info("Updating MFP(s) from Remote communication to the MFP_REGISTER on DataBase");
            System.out.println("-> Updating MFP(s) from Remote communication to the MFP_REGISTER on DataBase");
        }

        //Open connection with the database if it is not openned before
        if (!dbManager.isConnected())
            if (!dbManager.connect())
            {
                logger.error("updateMfpRegisterOnDB() : Cannot open DataBase connection");
                System.out.println("ERROR: Cannot open DataBase connection");
                return false;
            }

        try
        {
            Thread InsertMfpRegister_Thread = new Thread(new Runnable()
            {
                public void run()
                {
                    String key;
                    boolean allOK = true;
                    Printer printer;
                    //Insert all the elements from Printers Hash table to the dataBase
                    Iterator it = objectManager.getINSERT_mfpRegisterHash().keySet().iterator();
                    while (it.hasNext() && allOK)
                    {
                        //Obtain the key of the entry
                        key = (String) it.next();
                        printer = (Printer)objectManager.getINSERT_mfpRegisterHash().get(key);
                        //Insert the list of Resumes from the key to the dataBase
                        if (!(allOK = dbManager.registerMFP(printer)))
                            logger.error("updateMfpRegisterOnDB() : Error inserting mfp " + key + " to the MFP register on DataBase");
                    }
                    THREAD_STATUS[0] = allOK;
                }
            });
            InsertMfpRegister_Thread.start();

            Thread UpdateMfpRegister_Thread = new Thread(new Runnable()
            {
                public void run()
                {
                    String key;
                    boolean allOK = true;
                    Printer printer;
                    //Insert all the elements from Printers Hash table to the dataBase
                    Iterator it = objectManager.getUPDATE_mfpRegisterHash().keySet().iterator();
                    while (it.hasNext() && allOK)
                    {
                        //Obtain the key of the entry
                        key = (String) it.next();
                        printer = (Printer)objectManager.getUPDATE_mfpRegisterHash().get(key);
//                        System.out.println(" UpdateMfpRegister_Thread ipaddress: " + printer.getIpAddress() + " modeltype: " + printer.getModelType() + " location: " + printer.getLocation() + " hostname: " + printer.getHostname() + " company: " + printer.getCompany() +  " zone: " + printer.getZone());
                        //Insert the list of Resumes from the key to the dataBase
                        if (!(allOK = dbManager.updateRegisteredMFP(printer)))
                            logger.error("updateMfpRegisterOnDB() : Error updating mfp " + key + " to the MFP register on DataBase");
                    }
                    THREAD_STATUS[1] = allOK;
                }
            });
            UpdateMfpRegister_Thread.start();

            UpdateMfpRegister_Thread.join();
            InsertMfpRegister_Thread.join();

            for (int i = 0; i < THREAD_STATUS.length; i++)
                allOK = allOK & THREAD_STATUS[i];

        }
        catch (InterruptedException ex)
        {
            //Logger.getLogger(Parser.class.getName()).log(Level.SEVERE, null, ex);
            logger.error(ex.getMessage());
            logger.error(ex.getStackTrace());
            allOK = false;
        }
        finally
        {
            //Close connection with the database
            if (!dbManager.disconnect())
                logger.error("updateMfpRegisterOnDB() : Cannot close DataBase connection");

            return allOK;
        }
    }
    
    //  mergeMFPS(): devuelve una lista resultado de hacer un merge de las dos listas de MFPs (remote y StreamlineNX),
    //  con prioridad la de StreamLine e identificadas por la Primary Key "MFPSERIAL".
    private List mergeMFPS(List remoteMfpList, List streamlineMfpList) {
        
        if(remoteMfpList == null || remoteMfpList.isEmpty())
        {
            logger.debug("remoteMFPList is null. Getting streamlineMFPList only");
            System.out.println("remoteMFPList is null. Getting streamlineMFPList only");
            return streamlineMfpList;
        }
        if(streamlineMfpList == null || streamlineMfpList.isEmpty())
        {
            logger.debug("streamlineMfpList is null. Getting remoteMfpList only");
            System.out.println("streamlineMfpList is null. Getting remoteMfpList only");
            return remoteMfpList;
        }
        
        
        List mergedMFPS = streamlineMfpList;

        Iterator it = remoteMfpList.iterator();
        while(it.hasNext())
        {
            Boolean found = false;
            Printer remotePrinter = (Printer) it.next();
            String rmteMfpSerial = remotePrinter.getMfpSerial();
            
            Iterator it2 = streamlineMfpList.iterator();
            while(it2.hasNext())
            {
                Printer slnxPrinter = (Printer) it2.next();
                String slnxMfpSerial = slnxPrinter.getMfpSerial();
                if(rmteMfpSerial.equals(slnxMfpSerial))
                {
                    found = true;
//                    logger.debug("MFP " + slnxMfpSerial + " is already in the list");
//                    System.out.println("MFP " + slnxMfpSerial + " is already in the list");
                    break;
                }
            }
            
            if(!found)
            {
//                logger.debug("MFP " + rmteMfpSerial + " is not in the merged MFP list. Adding to the list...");
//                System.out.println("MFP " + rmteMfpSerial + " is not in the merged MFP list. Adding to the list...");
                mergedMFPS.add(remotePrinter);
            }
        }
        
        return mergedMFPS;
    }

    //  getStreamlineCounters(): obtiene los contadores de la BDD de Streamline
    private boolean getStreamlineCounters() {
        
        logger.debug("Obtaining Streamline Counters");
        System.out.println("-> Obtaining Streamline Counters");
        boolean allOk = true;
        
        if (!getSLNXMFPCounters())
        {
            logger.error("Getting Streamline MFP Counters");
            System.out.println("ERROR: Getting Streamline MFP Counters");
            allOk = false;
        }
        
        if (!getSLNXUserCounters())
        {
//            logger.error("Getting Streamline User Counters");
//            System.out.println("ERROR: Getting Streamline User Counters");
            allOk = false;
        }
        
        if (!getSLNXUserMFPCounters())
        {
//            logger.error("Getting Streamline User-MFP Counters");
//            System.out.println("ERROR: Getting Streamline User-MFP Counters");
            allOk = false;
        }

        if (!getSLNXGeneralCounters())
        {
//            logger.error("Getting Streamline General Counters");
//            System.out.println("ERROR: Getting Streamline General Counters");
            allOk = false;
        }
        
//        if (!getSLNXCostCenterCounters())
//        {
////            logger.error("Getting Streamline CostCenter Counters");
////            System.out.println("ERROR: Getting Streamline CostCenter Counters");
//            allOk = false;
//        }
        
        return allOk;
    }
    
    
    boolean getSLNXMFPCounters()
    {
        logger.debug("getSLNXMFPCounters()");
        System.out.println("getSLNXMFPCounters()");
        
        if (!dbManager.isConnected())
        {
            if (!dbManager.connect())
            {
                logger.error("getSLNXMFPCounters() : Cannot open Accounting DataBase connection");
                System.out.println("ERROR: Cannot open Accounting DataBase connection");
                return false;
            }
        }
        
        try
        {
            //  Obtenemos un listado de los MFPSERIAL de cada MFP gestionada por Streamline que hay en la BDD de Accounting
            List<Printer> slnxMfps = dbManager.getSlnxMfpsFromAccountingDB();
            ListIterator it1 = slnxMfps.listIterator();
            boolean allOK = true;

            while (it1.hasNext())
            {
                Printer prntr = (Printer) it1.next();
                String mfpSerial = prntr.getMfpSerial();
                System.out.println("Processing MFP with Serial: " + mfpSerial);

                MFPIncluder includer;
                try
                {
                    includer = new MFPIncluder("conf/config.properties","conf/sql.properties");
                    List<Resume> resumesToInsert = new ArrayList();
                    
                    //  Para cada MFP obtenemos de la BDD de Accounting el último registro del que tenemos contadores
                    Resume lastResume = dbManager.getLastMFPResume(mfpSerial);
                    
                    //  1- Obtener Resume de la MFP hasta hoy
                    Resume lastSlnxResume = includer.getLastSlnxMFPResume(mfpSerial);
                    if (lastSlnxResume != null && lastSlnxResume.getTotal() != 0)
                    {
                        //  2- Si no existía todavía ningún registro en la BDD de Accounting, insertamos el obtenido y listos
                        //  2.1- En caso de que existieran registros,
                        //  comprobar si hay alguno vacío desde el último hasta hoy. En ese caso, rellenarlos con el del último día
                        //  Estos vacíos añadirlos antes a la lista resumesToInsert y luego añadir el de lastSlnxResume para insertarlos por orden
                        if (lastResume != null)
                        {
                            int emptyResumes = 0;

                            emptyResumes = getNEmptyResumesFromLastDay(lastResume.getDate());
                            Date nextDay = dbManager.getNextDay(lastResume.getDate());
                            while (emptyResumes > 0)
                            {
                                Resume res = new Resume(mfpSerial, nextDay);
                                res.setResumeValues(lastResume);
                                resumesToInsert.add(res);                            
                                
                                nextDay = dbManager.getNextDay(nextDay);
                                emptyResumes--;
                            }
                        }
                        
                        lastSlnxResume.setCompany(COMPANY_NAME);
                        resumesToInsert.add(lastSlnxResume);
                        
                        if (!dbManager.insertNewResumeList(resumesToInsert, Resume.resumeType.MFP_RESUME))
                        {
                            logger.error("insertNewResumeList(): Se ha producido un error al insertar la lista de contadores de la MFP " + mfpSerial);
                            System.out.println("ERROR: insertNewResumeList() - Se ha producido un error al insertar la lista de contadores de la MFP " + mfpSerial);
                            allOK = false;
                            continue;
                        }
                    }
                    else
                    {
                        logger.info("getSLNXMFPCounters() : No Resume data to insert for MFP " + mfpSerial);
                        continue;
                    }
                }
                catch (IllegalArgumentException iae)
                {
                    logger.error("Parsing Last Accounting Counters: ", iae);
                    System.out.println("ERROR: parsing Last Accounting Counters: " + iae);
                    continue;
                }
            }
            
            return allOK;
        }
        catch (Exception e)
        {
            logger.error("getSLNXMFPCounters() : ", e);
            System.out.println("ERROR: getSLNXMFPCounters() " + e);
            return false;
        }
    }
    
    boolean getSLNXUserCounters()
    {
        logger.debug("getSLNXUserCounters()");
        System.out.println("getSLNXUserCounters()");
        
        if (!dbManager.isConnected())
        {
            if (!dbManager.connect())
            {
                logger.error("getSLNXUserCounters() : Cannot open Accounting DataBase connection");
                System.out.println("ERROR: Cannot open Accounting DataBase connection");
                return false;
            }
        }
        
        try
        {
            //  Obtenemos un listado de los Users(LoginUserName) existentes en la tabla USERCODe de StreamlineNX
            MFPIncluder includer;
            includer = new MFPIncluder("conf/config.properties","conf/sql.properties");
            List<String> slnxUsers = includer.getSlnxUserLoginNames();
            if (slnxUsers == null) return false;
             
            ListIterator it1 = slnxUsers.listIterator();
            boolean allOK = true;

            while (it1.hasNext())
            {
                String userLogin = (String) it1.next();
                System.out.println("User: " + userLogin);

                try
                {
                    List<Resume> resumesToInsert = new ArrayList();
                    
                    //  Para cada User obtenemos de la BDD de Accounting el último registro del que tenemos contadores
                    String fullName = includer.getUsersFullName(userLogin);
                    if(fullName.isEmpty())
                        fullName = userLogin;
                    Resume lastUserResume = dbManager.getLastUserResume(fullName);
                    
                    //  1- Obtener Resume del User hasta hoy
                    Resume lastSlnxUserResume = includer.getLastSlnxUserResume(userLogin);
                    if (lastSlnxUserResume != null && lastSlnxUserResume.getTotal() != 0)
                    {
                        //  2- Si no existía todavía ningún registro en la BDD de Accounting, insertamos el obtenido y listos
                        //  2.1- En caso de que existieran registros,
                        //  comprobar si hay alguno vacío desde el último hasta hoy. En ese caso, rellenarlos con el del último día
                        //  Estos vacíos añadirlos antes a la lista resumesToInsert y luego añadir el de lastSlnxResume para insertarlos por orden
                        if (lastUserResume != null)
                        {
                            int emptyResumes = 0;

                            emptyResumes = getNEmptyResumesFromLastDay(lastUserResume.getDate());
                            Date nextDay = dbManager.getNextDay(lastUserResume.getDate());
                            while (emptyResumes > 0)
                            {
                                Resume res = new Resume(fullName, nextDay);
                                res.setResumeValues(lastUserResume);
                                resumesToInsert.add(res);                            
                                
                                nextDay = dbManager.getNextDay(nextDay);
                                emptyResumes--;
                            }
                        }
                        
                        lastSlnxUserResume.setCompany(COMPANY_NAME);
                        resumesToInsert.add(lastSlnxUserResume);
                        
                        if (!dbManager.insertNewResumeList(resumesToInsert, Resume.resumeType.USER_RESUME))
                        {
                            logger.error("insertNewResumeList(): Se ha producido un error al insertar la lista de contadores del User " + userLogin);
                            System.out.println("ERROR: insertNewResumeList() - Se ha producido un error al insertar la lista de contadores del User " + userLogin);
                            allOK = false;
                            continue;
                        }
                    }
                    else
                    {
                        logger.info("getSLNXUserCounters() : No Resume data to insert for user " + userLogin);
                        continue;
                    }
                }
                catch (IllegalArgumentException iae)
                {
                    logger.error("Parsing Last Accounting Counters: ", iae);
                    System.out.println("ERROR: parsing Last Accounting Counters: " + iae);
                    continue;
                }
            }
            
            return allOK;
        }
        catch (Exception e)
        {
            logger.error("getSLNXUserCounters() : ", e);
            System.out.println("ERROR: getSLNXUserCounters() " + e);
            return false;
        }
    }
    
    public boolean getSLNXUserMFPCounters()
    {
        logger.debug("getSLNXUserMFPCounters()");
        System.out.println("getSLNXUserMFPCounters()");
        
        if (!dbManager.isConnected())
        {
            if (!dbManager.connect())
            {
                logger.error("getSLNXUserMFPCounters() : Cannot open Accounting DataBase connection");
                System.out.println("ERROR: Cannot open Accounting DataBase connection");
                return false;
            }
        }
        
        try
        {
            MFPIncluder includer;
            includer = new MFPIncluder("conf/config.properties","conf/sql.properties");
            
            //  Obtenemos un listado de los Users(LoginUserName) existentes en la tabla USERCODE de StreamlineNX
            List<String> slnxUsers = includer.getSlnxUserLoginNames();
            if (slnxUsers == null) return false;
            ListIterator itUsr = slnxUsers.listIterator();
            //  Obtenemos un listado de los MFPSERIAL de cada MFP gestionada por Streamline que hay en la BDD de Accounting
            List<Printer> slnxMfps = dbManager.getSlnxMfpsFromAccountingDB();
            
            boolean allOK = true;

            while (itUsr.hasNext())
            {
                String userLogin = (String) itUsr.next();
                System.out.println("- User: " + userLogin);
                
                ListIterator itMfp = slnxMfps.listIterator();
                while (itMfp.hasNext())
                {
                    Printer prntr = (Printer) itMfp.next();
                    String mfpSerial = prntr.getMfpSerial();
                    System.out.println(" MFP Serial: " + mfpSerial);

                    try
                    {
                        List<Resume> resumesToInsert = new ArrayList();

                        //  Para cada User-MFP obtenemos de la BDD de Accounting el último registro del que tenemos contadores
                        String fullName = includer.getUsersFullName(userLogin);
                        if(fullName.isEmpty())
                            fullName = userLogin;
                        Resume lastUserMfpResume = dbManager.getLastUserMfpResume(fullName, mfpSerial);

                        //  1- Obtener Resume del User-MFP hasta hoy
                        Resume lastSlnxUserMfpResume = includer.getLastSlnxUserMfpResume(userLogin, mfpSerial);
                        if (lastSlnxUserMfpResume != null && lastSlnxUserMfpResume.getTotal() != 0)
                        {
                            //  2- Si no existía todavía ningún registro en la BDD de Accounting, insertamos el obtenido y listos
                            //  2.1- En caso de que existieran registros,
                            //  comprobar si hay alguno vacío desde el último hasta hoy. En ese caso, rellenarlos con el del último día
                            //  Estos vacíos añadirlos antes a la lista resumesToInsert y luego añadir el de lastSlnxResume para insertarlos por orden
                            if (lastUserMfpResume != null)
                            {
                                int emptyResumes = 0;

                                emptyResumes = getNEmptyResumesFromLastDay(lastUserMfpResume.getDate());
                                Date nextDay = dbManager.getNextDay(lastUserMfpResume.getDate());
                                while (emptyResumes > 0)
                                {
                                    Resume res = new Resume(mfpSerial, nextDay);
                                    res.setResumeValues(lastUserMfpResume);
                                    res.setPrinter(prntr);
                                    resumesToInsert.add(res);                            

                                    nextDay = dbManager.getNextDay(nextDay);
                                    emptyResumes--;
                                }
                            }

                            lastSlnxUserMfpResume.setCompany(COMPANY_NAME);
                            lastSlnxUserMfpResume.setPrinter(prntr);
                            resumesToInsert.add(lastSlnxUserMfpResume);
                            
                            if (!dbManager.insertNewUserMfpResumeList(fullName, resumesToInsert))
                            {
                                logger.error("insertNewResumeList(): Se ha producido un error al insertar la lista de contadores del User " + userLogin);
                                System.out.println("ERROR: insertNewResumeList() - Se ha producido un error al insertar la lista de contadores del User " + userLogin);
                                allOK = false;
                                continue;
                            }
                        }
                        else
                        {
                            logger.info("No Resume data to insert for user " + userLogin);
//                            System.out.println("No Resume data to insert for user " + userLogin);
                            continue;
                        }

                    }
                    catch (IllegalArgumentException iae)
                    {
                        logger.error("Parsing Last Accounting Counters: ", iae);
                        System.out.println("ERROR: parsing Last Accounting Counters: " + iae);
                        continue;
                    }
                }
            }
            
            return allOK;
        }
        catch (Exception e)
        {
            logger.error("getSLNXUserMFPCounters() : ", e);
            System.out.println("ERROR: getSLNXUserMFPCounters() " + e);
            return false;
        }
    }
    
    public boolean getSLNXCostCenterCounters()
    {
        logger.debug("getSLNXCostCenterCounters()");
        System.out.println("getSLNXCostCenterCounters()");
        
        if (!dbManager.isConnected())
        {
            if (!dbManager.connect())
            {
                logger.error("getSLNXCostCenterCounters() : Cannot open Accounting DataBase connection");
                System.out.println("ERROR: Cannot open Accounting DataBase connection");
                return false;
            }
        }
        
        try
        {
            //  Obtenemos un listado de los CC existentes en la tabla COSTCENTER de StreamlineNX
            MFPIncluder includer;
            includer = new MFPIncluder("conf/config.properties","conf/sql.properties");
            List<String> slnxCostCenters = includer.getSlnxCostCenterNames();
            if (slnxCostCenters == null) return false;
            
            ListIterator it1 = slnxCostCenters.listIterator();
            boolean allOK = true;

            while (it1.hasNext())
            {
                String costCenter = (String) it1.next();
                System.out.println("CostCenter: " + costCenter);

                try
                {
                    List<Resume> resumesToInsert = new ArrayList();
                    
                    //  Para cada CostCenter obtenemos de la BDD de Accounting el último registro del que tenemos contadores
                    Resume lastCostCenterResume = dbManager.getLastCostCenterResume(costCenter);
                    
                    //  1- Obtener Resume del CostCenter hasta hoy
                    Resume lastSlnxCostCenterResume = includer.getLastSlnxCostCenterResume(costCenter);
                    if (lastSlnxCostCenterResume != null && lastSlnxCostCenterResume.getTotal() != 0)
                    {
                        //  2- Si no existía todavía ningún registro en la BDD de Accounting, insertamos el obtenido y listos
                        //  2.1- En caso de que existieran registros,
                        //  comprobar si hay alguno vacío desde el último hasta hoy. En ese caso, rellenarlos con el del último día
                        //  Estos vacíos añadirlos antes a la lista resumesToInsert y luego añadir el de lastSlnxResume para insertarlos por orden
                        if (lastCostCenterResume != null)
                        {
                            //  Mantenemos el CC_CODE que tenia en los registros anteriores.
                            lastSlnxCostCenterResume.setId(lastCostCenterResume.getId());
                            lastSlnxCostCenterResume.setUser(lastCostCenterResume.getUser());
                            
                            int emptyResumes = 0;

                            emptyResumes = getNEmptyResumesFromLastDay(lastCostCenterResume.getDate());
                            Date nextDay = dbManager.getNextDay(lastCostCenterResume.getDate());
                            while (emptyResumes > 0)
                            {
                                Resume res = new Resume(lastCostCenterResume.getId(), nextDay);
                                res.setResumeValues(lastCostCenterResume);
                                res.setUser(lastCostCenterResume.getUser());
                                resumesToInsert.add(res);                            
                                
                                nextDay = dbManager.getNextDay(nextDay);
                                emptyResumes--;
                            }
                        }
                        
                        lastSlnxCostCenterResume.setCompany(COMPANY_NAME);
                        
                        resumesToInsert.add(lastSlnxCostCenterResume);
                        
                        if (!dbManager.insertNewResumeList(resumesToInsert, Resume.resumeType.CC_RESUME))
                        {
                            logger.error("insertNewResumeList(): Se ha producido un error al insertar la lista de contadores del CostCenter " + costCenter);
                            System.out.println("ERROR: insertNewResumeList() - Se ha producido un error al insertar la lista de contadores del CostCenter " + costCenter);
                            allOK = false;
                            continue;
                        }
                    }
                    else
                    {
                        logger.info("getSLNXCostCenterCounters() : No Resume data to insert for costCenter " + costCenter);
                        continue;
                    }
                    
                }
                catch (IllegalArgumentException iae)
                {
                    logger.error("Parsing Last Accounting Counters: ", iae);
                    System.out.println("ERROR: parsing Last Accounting Counters: " + iae);
                    continue;
                }
            }
            
            return allOK;
        }
        catch (Exception e)
        {
            logger.error("getSLNXCostCenterCounters() : ", e);
            System.out.println("ERROR: getSLNXCostCenterCounters() " + e);
            return false;
        }
    }
    
    public boolean getSLNXGeneralCounters()
    {
        logger.debug("getSLNXGeneralCounters()");
        System.out.println("getSLNXGeneralCounters()");
        
        if (!dbManager.isConnected())
        {
            if (!dbManager.connect())
            {
                logger.error("getSLNXGeneralCounters() : Cannot open Accounting DataBase connection");
                System.out.println("ERROR: Cannot open Accounting DataBase connection");
                return false;
            }
        }
        
        //  1 - Obtenemos la fecha/hora del último registro de GENERAL de las MFP gestionadas por SLNX
        String lastGeneralDate = "";
        
        lastGeneralDate = dbManager.getLastGeneralDate();
        
        if (lastGeneralDate != null)
        {
            //  2 - Con esa fecha/hora obtenemos todo el ActivityData de SLNX superior a la fecha
            List<Action> lastSlnxActionList = null;
            try
            {
                //  Obtenemos un listado de los CC existentes en la tabla COSTCENTER de StreamlineNX
                MFPIncluder includer;
                includer = new MFPIncluder("conf/config.properties","conf/sql.properties");

                //  3 - Procesamos la info y la metemos en List<Action>
                lastSlnxActionList = includer.getLastSlnxActivityDataFromDate(lastGeneralDate);

                if (lastSlnxActionList != null)
                {
                    if (lastSlnxActionList.isEmpty())
                    {
                        logger.info("getSLNXGeneralCounters() : No Activity Data to insert");
                        System.out.println("getSLNXGeneralCounters() No Activity Data to insert");
                        return true;
                    }
                    
                    //  4 - Insertamos con insertActions()
                    if (!dbManager.insertActions(lastSlnxActionList))
                    {
                        logger.error("getSLNXGeneralCounters() : An error occurred while inserting General Counters Action List");
                        System.out.println("getSLNXGeneralCounters() ERROR: An error occurred while inserting General Counters Action List");
                        return false;
                    }
                    return true;
                }
                else
                {
//                    logger.info("getSLNXGeneralCounters() : Could not obtain last SLNX ActivityData");
//                    System.out.println("getSLNXGeneralCounters() Could not obtain last SLNX ActivityData");
                    return false;
                }

            }
            catch (Exception e)
            {
                logger.error("getSLNXGeneralCounters() : ", e);
                System.out.println("ERROR: getSLNXGeneralCounters() " + e);
                return false;
            }
        }
        else
        {
            logger.error("getSLNXGeneralCounters() : Could not obtain last General  Date");
            System.out.println("ERROR: getSLNXGeneralCounters() Could not obtain last General Date");
            return false;
        }
    }

    public static void main(String[] args) throws UnknownHostException
    {
        PropertyConfigurator.configure("conf/log4j.properties");

        Date TODAY_DATE = new Date();
        
        Thread.setDefaultUncaughtExceptionHandler(new MyUncaughtExceptionHandler());

        logger.info("Inicializando Parser()");
        Parser parser = new Parser();

        if (!parser.processFolder()){
            try {
                Mail mail = new Mail(true);
                logger.info("Se ha enviado email de notificación de Error");
            } catch (IOException ex) {
                java.util.logging.Logger.getLogger(Parser.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        
        if (!parser.checkTotals(TODAY_DATE)){
            try {
                Mail mail = new Mail(false);
                 logger.info("Se ha enviado email de notificación de descuadre en los contadores totales");
            } catch (IOException ex) {
                java.util.logging.Logger.getLogger(Parser.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }

    private int getNEmptyResumesFromLastDay(Date date) {
        
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date today = new Date();
            today = sdf.parse(sdf.format(today));
            
            date = dbManager.getNextDay(date);
            
            int emptyDays = 0;
            while (date.before(today) && !date.equals(today))
            {
                emptyDays++;
                date = dbManager.getNextDay(date);
            }
            
            return emptyDays;
            
        } catch (ParseException ex) {
            logger.error("getNEmptyResumesFromLastDay() Error", ex);
            return -1;
        }
    }

    private String parseADAttribute(String ADparam) {
        try {
            String tmp = ADparam;
            
            tmp = tmp.substring(tmp.indexOf(":") + 1);
            tmp = tmp.trim();
            
            return tmp;
        } catch(Exception e) {
            return ADparam;
        }
    }
    
    public static class MyUncaughtExceptionHandler implements Thread.UncaughtExceptionHandler {

        public void uncaughtException(Thread t, Throwable e) {
//            Mail mail = new Mail("Se ha producido un error en el proceso Parser del Accounting. Por favor consulta el fichero de logs.");
             logger.info("Se ha enviado email de notificación de Error");

             Logger.getLogger(getClass()).error("El thread " + t.getName() +
                    " ha lanzado un error inesperado:", e);
        }
    }
}


