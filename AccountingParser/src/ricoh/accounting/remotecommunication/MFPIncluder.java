/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ricoh.accounting.remotecommunication;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import javax.swing.text.html.HTMLEditorKit.Parser;
import org.apache.log4j.Logger;
import ricoh.accounting.database.DataBaseConnection;
import ricoh.accounting.objects.Action;
import ricoh.accounting.objects.Printer;
import ricoh.accounting.objects.Resume;
import ricoh.accounting.objects.User;

/**
 *
 * @author Alexis.Hidalgo
 */
public class MFPIncluder {

//    private DataBaseConnection remoteDBLayer;
//    private String REMOTE_DB_HOSTNAME = "";
//    private String REMOTE_DB_INSTANCE = "";
//    private String REMOTE_DB_USER = "";
//    private String REMOTE_DB_PASSWORD = "";
//    private String REMOTE_DB_NAME = "";
//    private String REMOTE_DB_PORT = "";
//    private String REMOTE_DB_DRIVER = "";
//    private String REMOTE_DB_CONNECTION = "";
//    private String REMOTE_QUERY_GET_MFPS = "";
    
    private DataBaseConnection streamlineDBLayer;
    private String STREAMLINE_DB_HOSTNAME = "";
    private String STREAMLINE_DB_INSTANCE = "";
    private String STREAMLINE_DB_USER = "";
    private String STREAMLINE_DB_PASSWORD = "";
    private String STREAMLINE_DB_NAME = "";
    private String STREAMLINE_DB_PORT = "";
    private String STREAMLINE_DB_DRIVER = "";
    private String STREAMLINE_DB_CONNECTION = "";
    private String STREAMLINE_QUERY_GET_MFPS = "";
    
    private String STREAMLINE_QUERY_GET_MFP_ACTIVITY_DATA = "";
    private String STREAMLINE_QUERY_GET_MFP_COUNTER_PRINTCOLOR = "";
    private String STREAMLINE_QUERY_GET_MFP_COUNTER_PRINTBW = "";
    private String STREAMLINE_QUERY_GET_MFP_COUNTER_COPYCOLOR = "";
    private String STREAMLINE_QUERY_GET_MFP_COUNTER_COPYBW = "";
    private String STREAMLINE_QUERY_GET_MFP_COUNTER_SCANSEND = "";
    private String STREAMLINE_QUERY_GET_MFP_COUNTER_SCANSTORED = "";
    private String STREAMLINE_QUERY_GET_MFP_COUNTER_FAXSEND = "";
    private String STREAMLINE_QUERY_GET_MFP_COUNTER_FAXRECEIVED = "";
    private String STREAMLINE_QUERY_GET_USER_LIST = "";
    private String STREAMLINE_QUERY_GET_USER_LAST_COUNTERS = "";
    private String STREAMLINE_QUERY_GET_USER_MFP_LAST_COUNTERS = "";
    private String STREAMLINE_QUERY_GET_COST_CENTER_LIST = "";
    private String STREAMLINE_QUERY_GET_COST_CENTER_LAST_COUNTERS = "";
    private String STREAMLINE_QUERY_GET_LAST_ACTIVITY_DATA_FROM_DATE = "";
    private String STREAMLINE_QUERY_GET_FULL_NAME_USER = "";
    
    private static final Calendar cal = Calendar.getInstance();
    
    private static final int PRINTCOLOR = 1;
    private static final int PRINTBW    = 2;
    private static final int COPYCOLOR  = 3;
    private static final int COPYBW     = 4;
    private static final int SCANSEND   = 5;
//    private static final int SCANSTORED = 6;
    private static final int FAXSEND    = 6;
//    private static final int FAXRECEIVED = 8;
    
    public static Logger logger = Logger.getLogger(DataBaseConnection.class.getName());

    public MFPIncluder(String confFile, String sqlFile) throws FileNotFoundException, IOException {
        
        logger.debug("MFPIncluder()");
        
        Properties prop = new Properties();
        logger.debug("Loading config File...");
        prop.load(new FileInputStream(confFile));

        
        //  Configuración de la BDD de Remote
//        REMOTE_DB_HOSTNAME = prop.getProperty("ExternalRemoteDB.Host");
//        REMOTE_DB_INSTANCE = prop.getProperty("ExternalRemoteDB.Instance");
//        REMOTE_DB_USER = prop.getProperty("ExternalRemoteDB.User");
//        REMOTE_DB_PASSWORD = prop.getProperty("ExternalRemoteDB.Password");
//        REMOTE_DB_NAME = prop.getProperty("ExternalRemoteDB.Name");
//        REMOTE_DB_PORT = prop.getProperty("ExternalRemoteDB.Port");
//        REMOTE_DB_DRIVER = prop.getProperty("ExternalRemoteDB.Driver");
//        REMOTE_DB_CONNECTION = prop.getProperty("ExternalRemoteDB.Connection");
//        
//        logger.info("*** REMOTE CONNECTION SETTINGS ***");
//        logger.info("REMOTE_DB_HOSTNAME -> " + REMOTE_DB_HOSTNAME);
//        logger.info("REMOTE_DB_INSTANCE -> " + REMOTE_DB_INSTANCE);
//        logger.info("REMOTE_DB_USER -> " + REMOTE_DB_USER);
//        logger.info("REMOTE_DB_PASSWORD -> " + "********");
//        logger.info("REMOTE_DB_NAME -> " + REMOTE_DB_NAME);
//        logger.info("REMOTE_DB_PORT -> " + REMOTE_DB_PORT);
//        logger.info("REMOTE_DB_DRIVER -> " + REMOTE_DB_DRIVER);
//        logger.info("REMOTE_DB_CONNECTION -> " + REMOTE_DB_CONNECTION);
//        logger.info("");

//        prop.load(new FileInputStream(sqlFile));
//        REMOTE_QUERY_GET_MFPS = prop.getProperty("SQL.GET_MFPS_REMOTE");
//
//        remoteDBLayer = new DataBaseConnection(REMOTE_DB_HOSTNAME, REMOTE_DB_USER, REMOTE_DB_PASSWORD, REMOTE_DB_NAME, REMOTE_DB_PORT, REMOTE_DB_DRIVER, REMOTE_DB_CONNECTION, REMOTE_DB_INSTANCE);
//        remoteDBLayer.createSQLServerConnectionURL();


        //  Configuración de la BDD de Streamline
        STREAMLINE_DB_HOSTNAME = prop.getProperty("ExternalStreamlineDB.Host");
        STREAMLINE_DB_INSTANCE = prop.getProperty("ExternalStreamlineDB.Instance");
        STREAMLINE_DB_USER = prop.getProperty("ExternalStreamlineDB.User");
        STREAMLINE_DB_PASSWORD = prop.getProperty("ExternalStreamlineDB.Password");
        STREAMLINE_DB_NAME = prop.getProperty("ExternalStreamlineDB.Name");
        STREAMLINE_DB_PORT = prop.getProperty("ExternalStreamlineDB.Port");
        STREAMLINE_DB_DRIVER = prop.getProperty("ExternalStreamlineDB.Driver");
        STREAMLINE_DB_CONNECTION = prop.getProperty("ExternalStreamlineDB.Connection");
        
        logger.info("*** STREAMLINE CONNECTION SETTINGS ***");
        logger.info("STREAMLINE_DB_HOSTNAME -> " + STREAMLINE_DB_HOSTNAME);
        logger.info("STREAMLINE_DB_INSTANCE -> " + STREAMLINE_DB_INSTANCE);
        logger.info("STREAMLINE_DB_USER -> " + STREAMLINE_DB_USER);
        logger.info("STREAMLINE_DB_PASSWORD -> " + "********");
        logger.info("STREAMLINE_DB_NAME -> " + STREAMLINE_DB_NAME);
        logger.info("STREAMLINE_DB_PORT -> " + STREAMLINE_DB_PORT);
        logger.info("STREAMLINE_DB_DRIVER -> " + STREAMLINE_DB_DRIVER);
        logger.info("STREAMLINE_DB_CONNECTION -> " + STREAMLINE_DB_CONNECTION);
        logger.info("");
        

        prop.load(new FileInputStream(sqlFile));
        STREAMLINE_QUERY_GET_MFPS = prop.getProperty("SQL.GET_MFPS_STREAMLINE");
        
        //  QUERY'S PARA OBTENER LOS CONTADORES TOTALES DE LA BDD DE STREAMLINE
        STREAMLINE_QUERY_GET_MFP_ACTIVITY_DATA = prop.getProperty("SQL.GET_MFP_ACTIVITY_DATA");
        STREAMLINE_QUERY_GET_MFP_COUNTER_PRINTCOLOR = prop.getProperty("SQL.GET_MFP_COUNTER_PRINTCOLOR");
        STREAMLINE_QUERY_GET_MFP_COUNTER_PRINTBW = prop.getProperty("SQL.GET_MFP_COUNTER_PRINTBW");
        STREAMLINE_QUERY_GET_MFP_COUNTER_COPYCOLOR = prop.getProperty("SQL.GET_MFP_COUNTER_COPYCOLOR");
        STREAMLINE_QUERY_GET_MFP_COUNTER_COPYBW = prop.getProperty("SQL.GET_MFP_COUNTER_COPYBW");
        STREAMLINE_QUERY_GET_MFP_COUNTER_SCANSEND = prop.getProperty("SQL.GET_MFP_COUNTER_SCANSEND");
        STREAMLINE_QUERY_GET_MFP_COUNTER_SCANSTORED = prop.getProperty("SQL.GET_MFP_COUNTER_SCANSTORED");
        STREAMLINE_QUERY_GET_MFP_COUNTER_FAXSEND = prop.getProperty("SQL.GET_MFP_COUNTER_FAXSEND");
        STREAMLINE_QUERY_GET_MFP_COUNTER_FAXRECEIVED = prop.getProperty("SQL.GET_MFP_COUNTER_FAXRECEIVED");
        STREAMLINE_QUERY_GET_USER_LIST = prop.getProperty("SQL.GET_SLNX_USER_LIST");
        STREAMLINE_QUERY_GET_USER_LAST_COUNTERS = prop.getProperty("SQL.GET_SLNX_USER_LAST_COUNTERS");
        STREAMLINE_QUERY_GET_USER_MFP_LAST_COUNTERS = prop.getProperty("SQL.GET_SLNX_USER_MFP_LAST_COUNTERS");
        STREAMLINE_QUERY_GET_COST_CENTER_LIST = prop.getProperty("SQL.GET_SLNX_COST_CENTER_LIST");
        STREAMLINE_QUERY_GET_COST_CENTER_LAST_COUNTERS = prop.getProperty("SQL.GET_SLNX_COST_CENTER_LAST_COUNTERS");
        STREAMLINE_QUERY_GET_LAST_ACTIVITY_DATA_FROM_DATE = prop.getProperty("SQL.GET_SLNX_LAST_ACTIVITY_DATA_FROM_DATE");
        STREAMLINE_QUERY_GET_FULL_NAME_USER = prop.getProperty("SQL.GET_SLNX_FULL_NAME_USER");
                
        streamlineDBLayer = new DataBaseConnection(STREAMLINE_DB_HOSTNAME, STREAMLINE_DB_USER, STREAMLINE_DB_PASSWORD, STREAMLINE_DB_NAME, STREAMLINE_DB_PORT, STREAMLINE_DB_DRIVER, STREAMLINE_DB_CONNECTION, STREAMLINE_DB_INSTANCE);
        streamlineDBLayer.createSQLServerConnectionURL();
    }
    
    public List getStreamlineMFPs() {
        
        logger.info("getStreamlineMFPs()");
        System.out.println("getStreamlineMFPs()");
        
        List mfplist = new ArrayList();
        Printer printer;

        if (!streamlineDBLayer.isConnected()) {
            //Open connection with the database
            if (!streamlineDBLayer.connect()) {
                logger.error("getStreamlineMFPs() : Cannot open DataBase connection");
                System.out.println("ERROR: Cannot open DataBase connection");
                return null;
            }
        }

        PreparedStatement pst = null;

        try
        {
            pst = streamlineDBLayer.getConnection().prepareStatement(STREAMLINE_QUERY_GET_MFPS);

            ResultSet rs = pst.executeQuery();

            while (rs.next())
            {
                String company = "";
                String location = "";
                String zone = "";
                
                if ((rs.getString(1) != null) && (!rs.getString(1).isEmpty()))
                {
                    
                    String serial = rs.getString(1);
                    if (serial != null)
                    {
                        serial = serial.toLowerCase();
                    }
                    else if (serial == null)
                    {
                        serial = "";
                    }

                    String modelName = rs.getString(2);
                    if (modelName != null) {
                        modelName = modelName.toLowerCase();
                    }
                    else if (modelName == null)
                    {
                        modelName = "";
                    }
                    
                    String ipAddress = rs.getString(3);
                    if (ipAddress == null)
                    {
                        ipAddress = "";
                    }
                    
                    company = "Compartamos";       //TODO: Ver compañia actual
                    location = rs.getString(4);   //  TODO: pendiente (si quieren añadir el campo location/zone)
                    if (location == null)
                    {
                        location = "";
                    }
                    //zone = rs.getString(5);
                    
                    System.out.println(serial + " | " + ipAddress + " | " + modelName);
                    
                    printer = new Printer(serial,
                            ipAddress,
                            modelName,
                            "online",
                            0,
                            0,
                            location,
                            zone,
                            ipAddress,  //  TODO: provisional hasta saber si realmente podemos obtener el Hostname o no
                            company,
                            true);

                    mfplist.add(printer);
                }
            }
            rs.close();
        }
        catch (SQLException ex)
        {
            logger.error("getStreamlineMFPs() : " + STREAMLINE_QUERY_GET_MFPS);
            logger.error(ex.getMessage());
            logger.error(ex.getStackTrace());
        }
        finally 
        {
            return mfplist;
        }
    }
    
    public List getSlnxUserLoginNames()
    {
        if (!streamlineDBLayer.isConnected()) {
            //Open connection with the database
            if (!streamlineDBLayer.connect()) {
                logger.error("getSlnxMFPResumes() : Cannot open SLNX DataBase connection");
                System.out.println("ERROR: Cannot open SLNX DataBase connection");
                return null;
            }
        }
        
        List<String> userList = null;
        PreparedStatement pst = null;
        try
        {
            userList = new ArrayList();
            ResultSet rs;
            
            pst = streamlineDBLayer.getConnection().prepareStatement(STREAMLINE_QUERY_GET_USER_LIST);
            rs = pst.executeQuery();
            
            while (rs.next())
            {
                String userLogin = rs.getString(1);
                
                if (userLogin != null && userLogin != "")
                {
                    userList.add(userLogin);
                }
            }
            
            if (userList.isEmpty())
            {
                return null;
            }
            return userList;
        }
        catch (SQLException ex)
        {
            logger.error("getSlnxUserLoginNames() : ");
            logger.error(ex.getMessage());
            logger.error(ex.getStackTrace());
            return null;
        }
    }

    public Resume getLastSlnxMFPResume(String mfpSerial) throws ParseException
    {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date today = new Date();
        today = sdf.parse(sdf.format(today));
  
        int printColor = 0;
        int printBw = 0;
        int copyColor = 0;
        int copyBw = 0;
        int scanSend = 0;
        int scanStored = 0;
        int faxSend = 0;
        int faxReceived = 0;
        int total = 0;
        
        if (!streamlineDBLayer.isConnected()) {
            //Open connection with the database
            if (!streamlineDBLayer.connect()) {
                logger.error("getSlnxMFPResumes() : Cannot open SLNX DataBase connection");
                System.out.println("ERROR: Cannot open SLNX DataBase connection");
                return null;
            }
        }
        
        PreparedStatement pst = null;
        Resume resume = new Resume(mfpSerial, today);
        try
        {
            ResultSet rs;
            
            /** PRINTCOLOR **/
            pst = streamlineDBLayer.getConnection().prepareStatement(STREAMLINE_QUERY_GET_MFP_COUNTER_PRINTCOLOR);
            pst.setString(1, mfpSerial);
            rs = pst.executeQuery();
            printColor = rs.next()? rs.getInt(1) : 0;   //  Si no existen datos, ponemos el contador 0.
//            System.out.println("printColor = " + printColor);
            resume.setPrintColor(printColor);
            
            /** PRINTBW **/
            pst = streamlineDBLayer.getConnection().prepareStatement(STREAMLINE_QUERY_GET_MFP_COUNTER_PRINTBW);
            pst.setString(1, mfpSerial);
            rs = pst.executeQuery();
            printBw = rs.next()? rs.getInt(1) : 0;
//            System.out.println("printBw = " + printBw);
            resume.setPrintBW(printBw);
            
            /** COPYCOLOR **/
            pst = streamlineDBLayer.getConnection().prepareStatement(STREAMLINE_QUERY_GET_MFP_COUNTER_COPYCOLOR);
            pst.setString(1, mfpSerial);
            rs = pst.executeQuery();
            copyColor = rs.next()? rs.getInt(1) : 0;
//            System.out.println("copyColor = " + copyColor);
            resume.setCopyColor(copyColor);
            
            /** COPYBW **/
            pst = streamlineDBLayer.getConnection().prepareStatement(STREAMLINE_QUERY_GET_MFP_COUNTER_COPYBW);
            pst.setString(1, mfpSerial);
            rs = pst.executeQuery();
            copyBw = rs.next()? rs.getInt(1) : 0;
//            System.out.println("copyBw = " + copyBw);
            resume.setCopyBW(copyBw);
            
            //FOR COMPARTAMOS - > NOT SCAND AND FAX
//            /** SCANSEND **/
//            //  TODO: por limitaciones de SLNX, se almacena en SCANSEND todos los escaneos, tanto Stored como Send
//            pst = streamlineDBLayer.getConnection().prepareStatement(STREAMLINE_QUERY_GET_MFP_COUNTER_SCANSEND);
//            pst.setString(1, mfpSerial);
//            rs = pst.executeQuery();
//            scanSend = rs.next()? rs.getInt(1) : 0;
////            System.out.println("scanSend = " + scanSend);
//            resume.setScanSend(scanSend);
//            scanSend=0;
            
//            /** FAXSEND **/
//            //  TODO: por limitaciones de SLNX, se almacena en FAXSEND todos los fax, tanto Received como Send
//            pst = streamlineDBLayer.getConnection().prepareStatement(STREAMLINE_QUERY_GET_MFP_COUNTER_FAXSEND);
//            pst.setString(1, mfpSerial);
//            rs = pst.executeQuery();
//            faxSend = rs.next()? rs.getInt(1) : 0;
////            System.out.println("faxSend = " + faxSend);
//            resume.setFaxSend(faxSend);
//            faxSend = 0;
            
            rs.close();
            
            total = printColor + printBw + copyColor + copyBw + scanSend + scanStored + faxSend + faxReceived;
            resume.setTotal(total);
            
            return resume;
        }
        catch (SQLException ex)
        {
            logger.error("getLastSlnxResume() : ");
            logger.error(ex.getMessage());
            logger.error(ex.getStackTrace());
            return null;
        }
    }
    
    public Resume getLastSlnxUserResume(String userLogin) throws ParseException
    {
        System.out.println("getLastSlnxUserResume()");
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date today = new Date();
        today = sdf.parse(sdf.format(today));
  
        int printColor = 0;
        int printBw = 0;
        int copyColor = 0;
        int copyBw = 0;
        int scanSend = 0;
        int scanStored = 0;
        int faxSend = 0;
        int faxReceived = 0;
        int total = 0;
        
        if (!streamlineDBLayer.isConnected()) {
            //Open connection with the database
            if (!streamlineDBLayer.connect()) {
                logger.error("getLastSlnxUserResume() : Cannot open SLNX DataBase connection");
                System.out.println("getLastSlnxUserResume ERROR: Cannot open SLNX DataBase connection");
                return null;
            }
        }
        
        PreparedStatement pst = null;
        String fullName = getUsersFullName(userLogin);
        if(fullName.isEmpty())
            fullName = userLogin;
        Resume resume = new Resume(fullName, today);
        try
        {
            ResultSet rs;
            
            pst = streamlineDBLayer.getConnection().prepareStatement(STREAMLINE_QUERY_GET_USER_LAST_COUNTERS);
            pst.setString(1, userLogin);
            rs = pst.executeQuery();
            
            while (rs.next())
            {
                switch (rs.getInt(2)) {
                    case PRINTCOLOR:
                        printColor = rs.getInt(1);
                        resume.setPrintColor(printColor);
                        break;
                        
                    case PRINTBW:
                        printBw = rs.getInt(1);
                        resume.setPrintBW(printBw);
                        break;
                    
                    case COPYCOLOR:
                        copyColor = rs.getInt(1);
                        resume.setCopyColor(copyColor);
                        break;
                        
                    case COPYBW:
                        copyBw = rs.getInt(1);
                        resume.setCopyBW(copyBw);
                        break;
                        
                    //FOR COMPARTAMOS - > NOT SCAND AND FAX    
                        //  TODO: por limitaciones de SLNX, todos los escaneos se almacenan en SCANSEND
//                    case SCANSEND:
//                        resume.setScanSend(rs.getInt(1));
//                        break;
//                        
//                    case SCANSTORED:
//                        resume.setScanStored(rs.getInt(1));
//                        break;
//                    
                        //  TODO: por limitaciones de SLNX, todos los fax se almacenan en FAXSEND
//                    case FAXSEND:
//                        resume.setFaxSend(rs.getInt(1));
//                        break;
//                        
//                    case FAXRECEIVED:
//                        resume.setFaxReceived(rs.getInt(1));
//                        break;

                }
                        
            }
            rs.close();
            
            total = printColor + printBw + copyColor + copyBw + scanSend + scanStored + faxSend + faxReceived;
            resume.setTotal(total);
            
            return resume;
        }
        catch (SQLException ex)
        {
            logger.error("getLastSlnxResume() : ");
            logger.error(ex.getMessage());
            logger.error(ex.getStackTrace());
            return null;
        }
    }
    
    public Resume getLastSlnxUserMfpResume(String userLogin, String mfpSerial) throws ParseException
    {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date today = new Date();
        today = sdf.parse(sdf.format(today));
  
        int printColor = 0;
        int printBw = 0;
        int copyColor = 0;
        int copyBw = 0;
        int scanSend = 0;
        int scanStored = 0;
        int faxSend = 0;
        int faxReceived = 0;
        int total = 0;
        
        if (!streamlineDBLayer.isConnected()) {
            //Open connection with the database
            if (!streamlineDBLayer.connect()) {
                logger.error("getLastSlnxUserMfpResume() : Cannot open SLNX DataBase connection");
                System.out.println("getLastSlnxUserMfpResume ERROR: Cannot open SLNX DataBase connection");
                return null;
            }
        }
        
        PreparedStatement pst = null;
        Resume resume = new Resume(mfpSerial, today);
        try
        {
            ResultSet rs;
            
            pst = streamlineDBLayer.getConnection().prepareStatement(STREAMLINE_QUERY_GET_USER_MFP_LAST_COUNTERS);
            pst.setString(1, userLogin);
            pst.setString(2, mfpSerial);
            rs = pst.executeQuery();
            
            while (rs.next())
            {
                switch (rs.getInt(2)) {
                    case PRINTCOLOR:
                        printColor = rs.getInt(1);
                        resume.setPrintColor(printColor);
                        break;
                        
                    case PRINTBW:
                        printBw = rs.getInt(1);
                        resume.setPrintBW(printBw);
                        break;
                    
                    case COPYCOLOR:
                        copyColor = rs.getInt(1);
                        resume.setCopyColor(copyColor);
                        break;
                        
                    case COPYBW:
                        copyBw = rs.getInt(1);
                        resume.setCopyBW(copyBw);
                        break;
                    
                    //FOR COMPARTAMOS - > NOT SCAND AND FAX  
                        //  TODO: por limitaciones de SLNX, se almacenan todos los escaneos en SCANSEND
//                    case SCANSEND:
//                        resume.setScanSend(rs.getInt(1));
//                        break;
                        
//                    case SCANSTORED:
//                        resume.setScanStored(rs.getInt(1));
//                        break;
//                    
                        //  TODO: por limitaciones de SLNX, se almacenan todos los fax en FAXSEND
//                    case FAXSEND:
//                        resume.setFaxSend(rs.getInt(1));
//                        break;
                        
//                    case FAXRECEIVED:
//                        resume.setFaxReceived(rs.getInt(1));
//                        break;

                }
                        
            }
            rs.close();
            
            total = printColor + printBw + copyColor + copyBw + scanSend + scanStored + faxSend + faxReceived;
            resume.setTotal(total);
            
            return resume;
        }
        catch (SQLException ex)
        {
            logger.error("getLastSlnxUserMfpResume() : " + STREAMLINE_QUERY_GET_USER_MFP_LAST_COUNTERS);
            logger.error(ex.getMessage());
            logger.error(ex.getStackTrace());
            return null;
        }
    }
    
    public List<String> getSlnxCostCenterNames()
    {
        if (!streamlineDBLayer.isConnected()) {
            //Open connection with the database
            if (!streamlineDBLayer.connect()) {
                logger.error("getSlnxCostCenterNames() : Cannot open SLNX DataBase connection");
                System.out.println("ERROR: Cannot open SLNX DataBase connection");
                return null;
            }
        }
        
        List<String> userList = null;
        PreparedStatement pst = null;
        try
        {
            userList = new ArrayList();
            ResultSet rs;
            
            pst = streamlineDBLayer.getConnection().prepareStatement(STREAMLINE_QUERY_GET_COST_CENTER_LIST);
            rs = pst.executeQuery();
            
            while (rs.next())
            {
                String costCenterName = rs.getString(1);
                
                if (costCenterName != null && costCenterName != "")
                {
                    userList.add(costCenterName);
                }
            }
            
            if (userList.isEmpty())
            {
                return null;
            }
            return userList;
        }
        catch (SQLException ex)
        {
            logger.error("getSlnxCostCenterNames() : ");
            logger.error(ex.getMessage());
            logger.error(ex.getStackTrace());
            return null;
        }
    }
    
    public Resume getLastSlnxCostCenterResume(String costCenter) throws ParseException
    {
//        System.out.println("getLastSlnxCostCenterResume()");
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date today = new Date();
        today = sdf.parse(sdf.format(today));
  
        int printColor = 0;
        int printBw = 0;
        int copyColor = 0;
        int copyBw = 0;
        int scanSend = 0;
        int scanStored = 0;
        int faxSend = 0;
        int faxReceived = 0;
        int total = 0;
        User user = null;
        
        if (!streamlineDBLayer.isConnected()) {
            //Open connection with the database
            if (!streamlineDBLayer.connect()) {
                logger.error("Cannot open SLNX DataBase connection");
                System.out.println("ERROR: Cannot open SLNX DataBase connection");
                return null;
            }
        }
        
        PreparedStatement pst = null;
        Resume resume = new Resume(costCenter, today);
        
        try
        {
            user = new User("", "", costCenter, "");   //  Recuperamos en un User el nombre del CC
            resume.setUser(user);
            
            ResultSet rs;
            
            pst = streamlineDBLayer.getConnection().prepareStatement(STREAMLINE_QUERY_GET_COST_CENTER_LAST_COUNTERS);
            pst.setString(1, costCenter);
            rs = pst.executeQuery();
            
            while (rs.next())
            {
                switch (rs.getInt(2)) {
                    case PRINTCOLOR:
                        printColor = rs.getInt(1);
                        resume.setPrintColor(printColor);
                        break;
                        
                    case PRINTBW:
                        printBw = rs.getInt(1);
                        resume.setPrintBW(printBw);
                        break;
                    
                    case COPYCOLOR:
                        copyColor = rs.getInt(1);
                        resume.setCopyColor(copyColor);
                        break;
                        
                    case COPYBW:
                        copyBw = rs.getInt(1);
                        resume.setCopyBW(copyBw);
                        break;
                        
                        //  TODO: por limitaciones de SLNX, se almacenan en ScanSend todos los escaneos
                    case SCANSEND:
                        resume.setScanSend(rs.getInt(1));
                        break;
                        
//                    case SCANSTORED:
//                        resume.setScanStored(rs.getInt(1));
//                        break;
                    
                        //  TODO: por limitaciones de SLNX, se almacenan en FaxSend todos los fax
                    case FAXSEND:
                        resume.setFaxSend(rs.getInt(1));
                        break;
                        
//                    case FAXRECEIVED:
//                        resume.setFaxReceived(rs.getInt(1));
//                        break;

                }
                        
            }
            rs.close();
            
            total = printColor + printBw + copyColor + copyBw + scanSend + scanStored + faxSend + faxReceived;
            resume.setTotal(total);
            
            System.out.println("getLastSlnxCostCenterResume() - End");
            return resume;
        }
        catch (SQLException ex)
        {
            logger.error("getLastSlnxCostCenterResume() : ");
            logger.error(ex.getMessage());
            logger.error(ex.getStackTrace());
            return null;
        }
        
        
    }
    
    public List getLastSlnxActivityDataFromDate(String lastGeneralDate)
    {
        if (!streamlineDBLayer.isConnected()) {
            //Open connection with the database
            if (!streamlineDBLayer.connect()) {
                logger.error("Cannot open SLNX DataBase connection");
                System.out.println("ERROR: Cannot open SLNX DataBase connection");
                return null;
            }
        }
        
        PreparedStatement pst = null;
        List<Action> actionList = null;
        
        try
        {
            ResultSet rs;
            
            pst = streamlineDBLayer.getConnection().prepareStatement(STREAMLINE_QUERY_GET_LAST_ACTIVITY_DATA_FROM_DATE);
            if (lastGeneralDate.equals(""))
            {
                pst.setString(1, "1900-01-01 00:00:00");
            }
            else
            {
                pst.setString(1, lastGeneralDate);
            }
            
            rs = pst.executeQuery();
            
            actionList = new ArrayList<Action>();
            while (rs.next())
            {
                Action action = new Action();
                action.setDate(rs.getDate("DATETIME"));
                action.setDateTime(rs.getTimestamp("DATETIME"));
                action.setType(getActionType(rs.getInt("ACTIVITY_TYPE")));   
                action.setMFPserial(rs.getString("DEVICE_CODE"));
                action.setUserName(rs.getString("LOGIN_USER_NAME"));
                action.setNumPages(rs.getInt("AMOUNT"));    //  TODO: puede que vengan negativos, ¿descartarlos?
                action.setNumCopies(1);  // SLNX no distingue entre copias. Contabiliza el total en una copia
//                action.setDocuName();   //  TODO: pendiente (Tabla JobNameActivityData.JOB_NAME) -> relacionar con ActivityData mediante el campo DATETIME -> Aunque parece que esto ultimo es bastante complicado debido a la estructura de la BDD
                action.setColorMode(getColorMode(rs.getInt("COLOR_MODE")));
                action.setPageSize(getPageSize(rs.getInt("PAPER_SIZE")));
//                action.setSender(); //  TODO: pendiente (si se puede obtener el Sender)
//                action.setDestination();    //  TODO: pendiente (si se puede obtener el Destination)
                action.setCompany("Compartamos");
                action.setPageMode(getPageMode(rs.getInt("SD_MODE")));
                
                String fullName = getUsersFullName(action.getUserName());
                if(fullName.isEmpty())
                    fullName = action.getUserName();
                action.setUserName(fullName);
                        
                actionList.add(action);
            }
            
            rs.close();
            
            return actionList;
        }
        catch (SQLException ex)
        {
            logger.error("getLastSlnxActivityDataFromDate() : ");
            logger.error(ex.getMessage());
            logger.error(ex.getStackTrace());
            return null;
        }
    }
    
    //  TODO: MUY IMPORTANTE devolver el resultado correcto, sino hacer el checkTotals(Today) en el main fallará
    private String getActionType(int activityType)
    {
        //  Options(9): report, report.state, copy, printer.print, printer.pausedocument-print, scanner.deliver-storage, scanner.deliver, fax.receive, fax.transfer
        switch (activityType)
        {
            case -1:
                return "Unknown";
                
            case 1:
                return "copy";
                
            case 2:
                return "printer.print";
                
            case 3:
                return "fax.transfer";   //TODO: pendiente buscar equivalencia entre receive y transfer
                
            case 4:
                return "scanner.deliver-storage";  //  TODO: pendiente buscar equivalencia entre deliver-storage y deliver
                
            case 5:
                return "staple";
                
            default:
                return "Unknown";
        }
    }
    
    private String getColorMode(int colorMode)
    {
        //  Options(ACC): blackandwhite, color, fullcolor
        //  Options(SLNX): bw, mono, twin, full
        switch (colorMode)
        {
            case 0:
                return "Unknown";
                
            case 1:
                return "blackandwhite";
                
            case 2://   SLNX: MONO_COLOR
                return "color"; //  TODO: pendiente de si es BW o Color
            
            case 3://   SLNX: TWIN_COLOR
                return "color";
                
            case 4:
//                return "fullcolor";//   TODO: en la web si es fullcolor aparece vacío.
                return "color";//   TODO: color: for test only
                
            default:
                return "Unknown";
        }
    }
    
    private String getPageMode(int pageMode)
    {
        //  Options(ACC): Unknown, simplex, duplex
        //  Options(SLNX): Unknown, simplex, duplex
        switch (pageMode)
        {
            case -1:
                return "Unknown";
                
            case 1:
                return "simplex";
                
            case 2:
                return "duplex"; 
            default:
                return "Unknown";
        }
    }
    
    private String getPageSize(int pageSize)
    {
        //  Options(ACC): a3, a4, a5, letter, standard, b5, freem
        switch (pageSize)
        {
            case 0:
                return "Unknown";
                
            case 4:
            case 132:
                return "a3";
                
            case 5:
            case 133:
                return "a4";
                
            case 6:
            case 134:
                return "a5";
                
            case 14:
            case 142:
                return "b5";
                
                //  TODO: tamaño pendiente letter, standard y freem
                
            default:
                return "Unknown";
        }
    }
    
    
    public String getUsersFullName(String login) 
    {
        
        if (!streamlineDBLayer.isConnected()) {
            //Open connection with the database
            if (!streamlineDBLayer.connect()) {
                logger.error("Cannot open SLNX DataBase connection");
                System.out.println("ERROR: Cannot open SLNX DataBase connection");
                return null;
            }
        }
        
        String nombre = "";
        PreparedStatement query = null;

        try
        {
            query = streamlineDBLayer.getConnection().prepareStatement(STREAMLINE_QUERY_GET_FULL_NAME_USER);
            
                        
            if (query != null)
            {

                query.setString(1, login);
                
                ResultSet rs = query.executeQuery();

                if(rs.next())
                {
                    nombre = rs.getString(1);
                }
                
                rs.close();
                query.close();
            }
        }
        catch (SQLException ex)
        {
            logger.error("getUsersFullName() : ");
            logger.error(ex.getMessage());
            logger.error(ex.getStackTrace());
            return null;
        }
        return nombre;
    }
    

}
